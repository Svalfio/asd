({  
   doInit: function (cmp, event, helper) {  
     helper.apexMethod(cmp);  
   },
    
    handleSelect: function (cmp, event, helper) {  
        
        var recordId = event.getParam('name');
        if(recordId != 'KO'){
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": recordId
            });
            navEvt.fire();
        }
    }  
 })