({  
    apexMethod : function(cmp) {  
        console.log("HELPER");
        var action = cmp.get("c.getAccountHierarchy");  
        action.setParams({ 
            accountId : cmp.get("v.recordId") 
        });  
        action.setCallback(this, function(response) {  
            var state = response.getState();  
            if (state === "SUCCESS") {  
                console.log("RECORDID "+cmp.get("v.recordId"));
                console.log("Response: "+JSON.stringify(response.getReturnValue()));
                cmp.set( "v.items", response.getReturnValue());
                console.log("Dati Tabella :"+JSON.stringify(cmp.get("v.items")));
            }  
        });  
        $A.enqueueAction(action);  
    }  
 })