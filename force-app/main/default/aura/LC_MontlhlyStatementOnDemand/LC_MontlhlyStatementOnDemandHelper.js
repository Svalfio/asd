({
    fetchPickListVal: function(component) {
            var action = component.get("c.retrieveInformation");
            action.setParams({ 
                accnId : component.get("v.recordId")
            });
        
        	console.log('accnId '+ component.get("v.recordId"));
        	
            let optsCountry = [];
        	let optsReseller = [];
        
        	optsCountry.push({
                class: "optionClass",
                label: '',
                value: ''
            });
        	
            optsReseller.push({
                class: "optionClass",
                label: '',
                value: ''
            });
            action.setCallback(this, function(response) {
                if (response.getState() == "SUCCESS") {
                    let allValues = response.getReturnValue();
                    console.log('allValues: ' + allValues);
                    for (var i = 0; i < allValues.length; i++){        
                        let tmpArray = allValues[i].split('|');
                        if(tmpArray[0] == 'Country'){
                            optsCountry.push({
                                class: "optionClass",
                                label: tmpArray[2],
                                value: tmpArray[2]
                            });
                        }
                        else{
                            optsReseller.push({
                                class: "optionClass",
                                label: tmpArray[2],
                                value: tmpArray[1]
                            });
                        }                        
                    }

                    component.find("Country").set("v.options", optsCountry);
                    component.find("Reseller").set("v.options", optsReseller);
                }
            });
            $A.enqueueAction(action);
        }
})