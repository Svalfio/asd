({
    doInit: function(component, event, helper) {
        console.log('start do init');
        helper.fetchPickListVal(component);
    },
    
    onPicklistChangeRes: function(component, event, helper) {
        console.log('Start onPicklistChangeRes');
        component.set("v.selectedReseller",event.getSource().get("v.value"));
    },
    
    onPicklistChangeCnt: function(component, event, helper) {
        console.log('Start onPicklistChangeCnt');
        component.set("v.selectedCountry",event.getSource().get("v.value"));
    },
    
    closeModal: function(component, event, helper) {
        console.log('Start Cancel');
        $A.get("e.force:closeQuickAction").fire();
    },
    
    download: function(component, event, helper) {
        console.log('Start download');
        var cnt = component.get("v.selectedCountry");
        var res = component.get("v.selectedReseller");
        var recId = component.get("v.recordId");
        
        console.log('cnt: '+cnt);
        console.log('res: '+res);
        console.log('recId: '+recId);
        
        if(cnt == '' || cnt == null){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type": "Error",
                "title": "Error!",
                "message": "Please select a country."
            });
            toastEvent.fire();
       }
       else{
            var action = component.get("c.callMonthly");
           	action.setParams({ 
                accnId : recId,
                country : cnt,
                subResellerId : res
            });
            action.setCallback(this, function(response) {
                if (response.getState() == "SUCCESS") {
                    var endpoint = response.getReturnValue();
                    console.log('endpoint: ' + endpoint);
					
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                         "url": endpoint
                    });
                    urlEvent.fire();
                }
                else{
                    toastEvent.setParams({
                        "type": "Error",
                        "title": "Error!",
                        "message": "It's not possible download the file. Please contact your System Administrator."
                    });
                    toastEvent.fire();
                }
            });
            $A.enqueueAction(action);
       } 
    }
})