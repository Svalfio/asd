({
	doinit : function(component, event, helper) {
		var action = component.get('c.getWalletId'); 
        var userId = $A.get( '$SObjectType.CurrentUser.Id' );
        action.setParams({
            "UserId" : userId
        });
        action.setCallback(this, function(a){
            var state = a.getState(); // get the response state
            if(state == 'SUCCESS') {
                component.set('v.notifications', a.getReturnValue());
                window.open('/partner/s/wallet/'+a.getReturnValue(), '_self');

            }
        });
        $A.enqueueAction(action);
	}
})