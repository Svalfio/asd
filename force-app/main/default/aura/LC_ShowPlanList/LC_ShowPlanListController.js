({
    getProd: function (component, event, helper) {

        var userId = component.get('v.userId');
        var countryString = component.get('v.countryString');
        component.set('v.loading', true);
        var action2 = component.get('c.checkWallet');
        action2.setParams({
            "userId": userId,
        });
        action2.setCallback(this, function (a1) {
            var state = a1.getState(); // get the response state
            if (state == 'SUCCESS') {
                component.set('v.checkWalletAmount', a1.getReturnValue());
            }
        });
        $A.enqueueAction(action2);
        
        var action = component.get('c.getProductList');
        action.setParams({
            "userId": userId,
            "countryString": countryString
        });
        action.setCallback(this, function (a) {
            var state = a.getState(); // get the response state
            if (state == 'SUCCESS') {
                component.set('v.products', a.getReturnValue());
                component.set('v.loading', false);
            }else{
                component.set('v.loading', false);
            }
        });
        $A.enqueueAction(action);

    }
})