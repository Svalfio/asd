({
    doinit : function(component, event, helper) {
        var action = component.get('c.returnMessage');
        var cmpTarget = component.find('Modalbox1');
        action.setParams({record_Id : component.get("v.recordId")});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                if(response.getReturnValue() != ""){
                var MessageText = response.getReturnValue();
                console.log("MessageText--->>> " + MessageText);
                 component.set("v.PopUpMessage", MessageText);
                console.log(component.get("v.PopUpMessage"));
                 $A.util.addClass(cmpTarget, 'slds-fade-in-open');
                }
            }
            else if(state === "ERROR") {
                var errors = response.getError();
                if(errors){
                    if(errors[0] && errors[0].message){
                        console.log("Error Message: " + errors[0].message);
                    }
                }
                else{
                    console.log("Unknown Error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    closeNewModal : function(component, event, helper){
       
        var cmpBack = component.find('Modalbackdrop');
        var cmpTarget = component.find('Modalbox1');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open');
    },
})