({
	doInit : function(component, event, helper) {
        console.log('start doInit');
        var opts = [];
        opts.push({
            class: "None",
            label: "None",
            value: "None"
        });
        for (var i = 1; i <= 12; i++) {
            opts.push({
                class: "optionClass",
                label: i,
                value: i
            });
        }

        component.find("mese").set("v.options", opts);
        
        var opts2 = [];
        
                opts2.push({
            class: "None",
            label: "None",
            value: "None"
        });
        
        var baseYear = 2018;
        
        for (var i = 1; i <= 30; i++) {
            opts2.push({
                class: "optionClass",
                label: baseYear + i,
                value: baseYear + i
            });
        }

        component.find("year").set("v.options", opts2);
        
    },
    
    showPDF : function(component, event, helper) {
		var selectedcountry = component.get("v.selectedCountry");
         var selectedmonth = component.get("v.selectedMonth");
        var selectedyear = component.get("v.selectedYear");
        
        if(selectedcountry == "None" || selectedcountry == null || selectedmonth == "None" || selectedmonth == null || selectedyear == "None" || selectedyear == null){
            var toastEvent = $A.get("e.force:showToast");
            	toastEvent.setParams({
                	"title": "Error!",
                    "type": "error",
                	"message": "Please select all fields before generate PDF"
            	});
            	toastEvent.fire();
        }
        else{
                let pageReference = {
                type: 'standard__webPage',
                attributes: {
                        url: '/apex/CommissionPrefilledInvoice?prefilledInvoiceId='+component.get('v.recordId')+'-'+selectedcountry+'-'+selectedmonth+'-'+selectedyear
                    }
                };
                var navService = component.find("nav");
                navService.generateUrl(pageReference)
                    .then($A.getCallback(function (url) {
                        console.log(url);
                        navService.navigate(pageReference);
                    }), $A.getCallback(function (error) {
                        console.log(error);
                }));
    
            }
        },
    
    showWord : function(component, event, helper) {
		var selectedcountry = component.get("v.selectedCountry");
        var selectedmonth = component.get("v.selectedMonth");
        var selectedyear = component.get("v.selectedYear");
        
        if(selectedcountry == "None" || selectedcountry == null || selectedmonth == "None" || selectedmonth == null || selectedyear == "None" || selectedyear == null){
            var toastEvent = $A.get("e.force:showToast");
            	toastEvent.setParams({
                	"title": "Error!",
                    "type": "error",
                	"message": "Please select all fields before generate Word"
            	});
            	toastEvent.fire();
        }
        else{
                let pageReference = {
                type: 'standard__webPage',
                attributes: {
                        url: '/apex/CommissionPrefilledInvoiceWord?prefilledInvoiceId='+component.get('v.recordId')+'-'+selectedcountry+'-'+selectedmonth+'-'+selectedyear
                    }
                };
                var navService = component.find("nav");
                navService.generateUrl(pageReference)
                    .then($A.getCallback(function (url) {
                        console.log(url);
                        navService.navigate(pageReference);
                    }), $A.getCallback(function (error) {
                        console.log(error);
                }));
    
            }
        },
    
    onPicklistChangeMonth: function(component, event, helper) {
        console.log('Start onPicklistChangeMonth');
        component.set("v.selectedMonth",event.getSource().get("v.value"));
    },
    
    onPicklistChangeYear: function(component, event, helper) {
        console.log('Start onPicklistChangeYear');
        component.set("v.selectedYear",event.getSource().get("v.value"));
    },
})