({
	doinit : function(component, event, helper) {
		var action = component.get('c.getCampaigns'); 
        var recordId = component.get('v.recordId');
        console.log(recordId);
        var hostname = window.location.hostname;
		var arr = hostname.split("/");
		var instance = arr[0];
        console.log(instance);
        component.set('v.url', instance);
        action.setParams({
            "AccountId" : recordId
        });
        action.setCallback(this, function(a){
            var state = a.getState(); // get the response state
            if(state == 'SUCCESS') {
                component.set('v.campaigns', a.getReturnValue());
                if(a.getReturnValue().length > 3){
                    component.set('v.recordNumber', '3+')
                }else{
                	component.set('v.recordNumber', a.getReturnValue().length);
                }
            }
        });
        $A.enqueueAction(action);
	}
})