({
    doInit: function (cmp, event, helper) {
        console.log("doInit");
        
        cmp.set('v.mycolumns', [
        {label: 'Model Name', fieldName: 'Commission_Model_Name__c', type: 'text', sortable: false, editable: false},
        {label: 'Type', fieldName: 'Name', type: 'text', sortable: false, editable: false},
        {label: 'Country', fieldName: 'Commission_Model_Country__c', type: 'text', sortable: false, editable: false,cellAttributes: { alignment: 'center' }},
        {label: 'Start Date', fieldName: 'Commission_Model_Start_Date__c', type: 'date', sortable: false, editable: false,cellAttributes: { alignment: 'center' }},
        {label: 'End Date', fieldName: 'Commission_Model_End_Date__c', type: 'date', sortable: false, editable: false,cellAttributes: { alignment: 'center' }},
        {label: 'Amount', fieldName: 'Commission_Amount_Calc__c', type: 'text', sortable: false, editable: false,cellAttributes: { alignment: 'center' }},
        {label: 'Percentage %', fieldName: 'Commission_Percentage__c', type: 'number', sortable: false, editable: true,cellAttributes: { alignment: 'center' }},
        {label: 'Reseller', fieldName: 'Sub_Reseller_Name__c', type: 'text', sortable: false, editable: false, cellAttributes: { alignment: 'center' }}    
    	]);
        
        
        var action = cmp.get("c.retrieveIndirectCommissionItems");  
        action.setParams({ 
            resId : cmp.get("v.recordId"),
            country : cmp.get("v.selectedCountry"),
            commName : cmp.get("v.selectedModel"),
            status : cmp.get("v.selectedStatus"),
            resellerName : cmp.get("v.selectedReseller")
        });  
        action.setCallback(this, function(response) {  
            var state = response.getState();  
            if (state === "SUCCESS") {
                cmp.set( "v.commissionItems", response.getReturnValue());
                console.log(response.getReturnValue());
            }  
        });  
        $A.enqueueAction(action);  
    },
    
    handleSave: function (cmp, event, helper) {
        console.log('*** start handlesave ***');
        var draftValues = event.getParam('draftValues');
        console.log('*** draftvalues ***:' + draftValues);
        
        var action = cmp.get("c.updateCommissionItems");
        action.setParams({"lst" : draftValues});
        action.setCallback(this,function(response){
        var state=response.getState();
        if(state='SUCCESS'){
        	console.log('+++SUCCESS++++');
            var resultCall = response.getReturnValue();
            console.log('resultCall***:' +resultCall);
            if(resultCall == 'OK'){
                cmp.set("v.commissionItems",draftValues);
                $A.get('e.force:refreshView').fire();
            }
            else{
                if(resultCall.includes("You can only Set Amount or Percentage for Commissioning Rules"))
                    resultCall = "Attention! It's not possible to set both amount and percentage!";
                var toastEvent = $A.get("e.force:showToast");
            	toastEvent.setParams({
                	"title": "Error!",
                    "type": "error",
                	"message": resultCall
            	});
            	toastEvent.fire();
            }
            }

        });
        $A.enqueueAction(action); 
    },
    
    isRefreshed: function(component, event, helper) {
    	location.reload();
	}
})