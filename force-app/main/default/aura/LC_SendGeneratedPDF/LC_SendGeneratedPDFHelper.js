({
    email: function (component) {
        var attachmentId = component.get("v.attachmentId");
        var emailAddress = component.get("v.emailAddress");
        var nameCustomer = component.get("v.nameCustomer");
        var action = component.get("c.sendMail");
        action.setParams({
            "attachmentId": attachmentId,
            "emailAddress": emailAddress,
            "name": nameCustomer
        });
        action.setCallback(this, function (a) {
            var state = a.getState(); // get the response state
            if (state == 'SUCCESS') {
                
            }
        });

        $A.enqueueAction(action);
    }
})