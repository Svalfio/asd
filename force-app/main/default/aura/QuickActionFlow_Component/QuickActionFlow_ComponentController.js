({
    init : function (component) {
        var flow = component.find("flowId");
        var action = component.get("c.getAccountId");
        action.setParams({QuoteId : component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var AccountId = response.getReturnValue();
                if(AccountId!=''){
                    var inputVariables = [
                        { name : "varAccountId", type : "String", value: AccountId },
                        { name : "varQuote", type : "String", value: component.get("v.recordId") }
                    ];
                    flow.startFlow("Eutelsat_Customer_Portal",inputVariables);
                } else {
                    console.log('No Account Find');
                }
            } else if (state === "ERROR") {
                console.log('Error in QuickAction Component Controller');
            }
        });
        $A.enqueueAction(action);
    },
})