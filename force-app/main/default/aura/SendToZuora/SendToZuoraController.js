({
    invoke : function(component, event, helper){
        var action = component.get("c.callZQuoteGlobal");
        action.setParams({
            "quoteId" : component.get("v.recordId"),
            "accountId" : component.get("v.accountId"),
            "ZaccountId" : component.get("v.ZaccountId"),
            "generateInvoice" : component.get("v.generateInvoice")
        });
        
        var params = event.getParam('arguments');
        var callback;
        if (params) {
            callback = params.callback;
        }
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("From server: " + response.getReturnValue());
                // return doesn't work for async server action call
                //return response.getReturnValue();
                // call the callback passed into aura:method
                if (callback){
                    callback(response.getReturnValue());
               	 	var x = JSON.parse(response.getReturnValue()) 
                	//component.set("v.outcome", x);
                }
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                          errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
        $A.get('e.force:refreshView').fire();
    },
})