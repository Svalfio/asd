({
    /**
     * doInit
     * Load account from Salesforce
     */
    doInit: function(component, event, helper) {
        console.log("doInit CheckCoverageController");

 var action2 = component.get("c.getProfileInfo");
        action2.setCallback(this, function (response) {
            var state = response.getState();
            if (state == "SUCCESS" && component.isValid()) {
                console.log("success");
                var result = response.getReturnValue();
                console.log(result);
                if (result.Name === 'Distributor' || result.Name === 'Sub-Distributor') {
                    component.set("v.isWholesale", true);
                }

            } else {
                console.error("fail:" + response.getError()[0].message);
            }
        });
        $A.enqueueAction(action2);      


        // Create the action
        /*var action = component.get("c.getAccount");
        action.setParams({"accountId": component.get("v.accountId")});
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var account = component.get("v.account");
                account = response.getReturnValue();
                component.set("v.account", account);
            } else {
                console.log("Failed with state: " + state);
            }
        });
        // Send action off to be executed
        $A.enqueueAction(action);*/
        
       if (window.location.href.indexOf("/partner") !== -1) {
            var basePath = component.get("v.basePath");
                basePath = "/partner/s/sfsites/c";
                component.set("v.basePath", basePath);
        }
        
        if ( window.addEventListener ) {
            window.addEventListener('message', handleMessage, false);
        } else if ( window.attachEvent ) {
            window.attachEvent('onmessage', handleMessage);
        }

        /**
         * handleMessage
         * Manages iframe messages
         * 
         * @param Event event
         */
        function handleMessage(event) {
            var data = event.data;
            if (data.func === 'sendCoordinates') {
                var latitude = component.get("v.latitude");
                var longitude = component.get("v.longitude");
                latitude = parseFloat(data.params[0]);
                longitude = parseFloat(data.params[1]);
                component.set("v.latitude", latitude);
                component.set("v.longitude", longitude);
            } else if (data.func === 'setCountOfServices') {
                var countOfServices = component.get("v.countOfServices");
                countOfServices = parseInt(data.params[0]);
                component.set("v.countOfServices", countOfServices);
            } else if (data.func === 'sendAddress') {
                var street = component.get("v.street");
                var zipcode = component.get("v.zipcode");
                var city = component.get("v.city");
                var country = component.get("v.country");
                street = "" + data.params[0];
                zipcode = "" + data.params[1];
                city = "" + data.params[2];
                country = "" + data.params[3];
                component.set("v.street", street);
                component.set("v.zipcode", zipcode);
                component.set("v.city", city);
                component.set("v.country", country);
            }
        }
    }
})