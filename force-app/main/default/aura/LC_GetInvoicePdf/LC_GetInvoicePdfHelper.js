({

    zuoraCall: function (component) {
        var zuoraId = component.get("v.zuoraId");
        var quoteId = component.get("v.quoteId"); 
        var action = component.get("c.getInvoice");
        action.setParams({
            "zuoraId": zuoraId,
            "quoteId": quoteId
        });
        action.setCallback(this, function (a) {
            var state = a.getState(); // get the response state
            if (state == 'SUCCESS') {
                alert(a.getReturnValue());
                component.set("v.attachmentId", a.getReturnValue());
                alert("v.attachmentId");
                component.set("v.yes", true);
            }
        });

        $A.enqueueAction(action);
    }
})