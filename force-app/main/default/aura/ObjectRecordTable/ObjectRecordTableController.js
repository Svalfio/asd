({
    init: function (cmp, event, helper) {
        var Metadata = '';
        Metadata = helper.getMetadata(cmp, event);

    },

    resetColumns: function (cmp, event, helper) {
        var Metadata = '';
        helper.resetLocalStorage();
        Metadata = helper.getMetadata(cmp, event);
    },

    storeColumnWidths: function (cmp, event, helper) {
        helper.storeColumnWidths(event.getParam('columnWidths'));
    },

    handleSelect: function (cmp, event, helper) {
        var selectedRows = event.getParam('selectedRows');
        console.log('selectedRows' + selectedRows);
        if (selectedRows.length > 0 && typeof selectedRows[0].Subscription_Type__c !== "undefined" && selectedRows[0].Subscription_Type__c === "VNO") {
            cmp.set("v.subsType", true);
        } else {
            cmp.set("v.subsType", false);
        }
        var selectedId = event.getParam('keyField');
        cmp.set("v.selectedRowsCount", selectedRows.length);
        helper.getSelectedRows(cmp, event, selectedRows);
    },

    updateColumnSorting: function (cmp, event, helper) {
        cmp.set('v.isLoading', true);
        // We use the setTimeout method here to simulate the async
        // process of the sorting data, so that user will see the
        // spinner loading when the data is being sorted.
        setTimeout($A.getCallback(function () {
            var fieldName = event.getParam('fieldName');
            var sortDirection = event.getParam('sortDirection');
            cmp.set("v.sortedBy", fieldName);
            cmp.set("v.sortedDirection", sortDirection);
            helper.sortData(cmp, fieldName, sortDirection);
            cmp.set('v.isLoading', false);
        }), 0);
    },
})