({
    
    getMetadata : function( cmp, event){
        var action = cmp.get("c.getMetadata2");
        action.setParams({MetadataName : cmp.get("v.MetadataName") , sObjectType:cmp.get("v.sObjectType"), OrderField:cmp.get("v.OrderField"),
                          UsedField : cmp.get("v.FieldsList")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var Metadata = response.getReturnValue();
                this.getColumnsDefinitions(cmp, event,Metadata);
            }
            else if (state === "INCOMPLETE") {
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });		
        $A.enqueueAction(action);
        
    },
    
    getColumnsDefinitions: function(cmp, event,Metadata) {
        var columnsWidths = this.getColumnWidths();
        
        var action = cmp.get("c.getColumns");
        action.setParams({Metadata2 : Metadata , sObjectType:cmp.get("v.sObjectType"), MetadataName: cmp.get("v.MetadataName"),
                          UsedField : cmp.get("v.FieldsList")});
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var JsonMap = response.getReturnValue();
                var columnsList = JSON.parse(JsonMap);
                cmp.set('v.columns',columnsList); 
                this.getData(cmp, event,Metadata);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });	
        $A.enqueueAction(action);
    },
    
    
    getData: function (cmp, event, Metadata) {
        debugger;
        var action = cmp.get("c.getData");
        action.setParams({sObjectType : cmp.get("v.sObjectType") , MetaData2:Metadata, whereConditions:cmp.get("v.whereConditions"),
            MetadataFilterField: cmp.get("v.MetadataFilterField"), MetadataName: cmp.get("v.MetadataName")
            // , fieldSet: cmp.get("v.SubTypeFieldSet")
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var JsonMap = response.getReturnValue();
                var dataList = JSON.parse(JsonMap);
                console.log(dataList);
                cmp.set('v.data', dataList); 
                
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });	
        $A.enqueueAction(action);
    },
    
    resetLocalStorage: function () {
        localStorage.setItem('datatable-in-action', null);
    },
    
    storeColumnWidths: function (widths) {
        localStorage.setItem('datatable-in-action', JSON.stringify(widths));
    },
    
    getColumnWidths: function () {
        var widths = localStorage.getItem('datatable-in-action');
        
        try {
            widths = JSON.parse(widths);
        } catch(e) {
            return [];
        }
        return Array.isArray(widths) ? widths : [];
    },
    
    sortData: function (cmp, fieldName, sortDirection) {
        var data = cmp.get("v.data");
        var reverse = sortDirection !== 'asc';
        
        data = Object.assign([],
                             data.sort(this.sortBy(fieldName, reverse ? -1 : 1))
                            );
        cmp.set("v.data", data);
    },
    sortBy: function (field, reverse, primer) {
        var key = primer
        ? function(x) {
            return primer(x[field]);
        }
        : function(x) {
            return x[field];
        };
        
        return function (a, b) {
            var A = key(a);
            var B = key(b);
            return reverse * ((A > B) - (B > A));
        };
    },
    
    getSelectedRows : function (cmp, event, selectedRows) {
        var setRows = [];
        // try {
        //     component.set("v.subsType", selectedRows.get("Subscription_Type__c"));
        // } catch (error) {
        //     component.set("v.subsType", "noVNO");
        // }
        
        for ( var i = 0; i < selectedRows.length; i++ ) {
            setRows.push(selectedRows[i]);
        }
        cmp.set("v.SelectedRowsList",JSON.stringify(setRows));
        
    },
    
})