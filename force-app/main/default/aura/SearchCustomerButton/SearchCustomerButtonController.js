({

    /**
     * handleNext
     * @param {*} component 
     * @param {*} event 
     * @param {*} helper 
     */
    handleNext : function(component, event, helper) {
        var navigate = component.get("v.navigateFlow");
        navigate("NEXT");
    }


})