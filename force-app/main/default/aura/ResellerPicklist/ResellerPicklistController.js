({
    doInit: function(component, event, helper) {
        console.log('start do init');
        helper.fetchPickListVal(component);
    },
    
    onPicklistChange: function(component, event, helper) {
        console.log('Start onPicklistChange Reseller');
        component.set("v.selectedReseller",event.getSource().get("v.value"));
		var compEvent = component.getEvent("RefreshIndirectCommissionModel");
        console.log('EVENTO : '+compEvent);
        compEvent.fire();
        console.log('EVENTO PARTITO');
    }
})