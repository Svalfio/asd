({
    fetchPickListVal: function(component) {
        var action = component.get("c.getResellerList");
        var opts = [];
        action.setParams({ 
            accountId : component.get("v.accountId")
        }); 
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                opts.push({
                        class: "All",
                        label: "All",
                        value: "All"
                    });
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                    
                    console.log('allValues['+i+']: '+allValues[i]);
                }
                component.find("Reseller").set("v.options", opts);
            }
        });
        $A.enqueueAction(action);
    },
})