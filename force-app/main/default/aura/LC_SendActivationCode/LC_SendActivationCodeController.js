({
    doinit : function(component, event, helper) {
        var action = component.get('c.SendActivationCode'); 
        action.setParams({
            "AccountId" : component.get('v.AccountId'),
            "ActivationCode" : component.get('v.ActivationCode')
        });
        action.setCallback(this, function(a){
            var state = a.getState(); 
            if(state == 'SUCCESS') {
                console.log(state);
            }
        });
        $A.enqueueAction(action);
    }
})