({
    doinit: function (component, event, helper) {
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        console.log('user' + userId);
        component.set('v.loading', true);
        component.set('v.columns', [
            { label: 'Statement Name', type: 'button', initialWidth: 275, typeAttributes: { label: { fieldName: 'Monthly_Statement_Name__c' }, name: 'view_details', variant: 'base' } },
            { label: 'Commission Payment Date', fieldName: 'Commission_Payment_Date__c', type: 'date', format: 'DD/MM/YYYY' },
            { label: 'Commissioning Amount', fieldName: 'Commissioning_Amount__c', type: 'currency' },
            { label: 'Month of Reference', fieldName: 'Month_of_Reference__c', type: 'number' },
            { label: 'Year of Reference', fieldName: 'Year__c', type: 'string' },
            { label: 'Payment Method', fieldName: 'Commission_Payment_Method__c', type: 'string' },
            {
                type: "button", typeAttributes: {
                    iconName: 'utility:download',
                    label: '',
                    name: 'download',
                    title: 'download',
                    disabled: false,
                    variant: { fieldName: 'variantValue' }
                }
            },
        ]);

        var action = component.get('c.getMyStatements');

        action.setParams({
            "UserId": userId,
            "NameFilter": null,
            "DateFilter": null,
            "DatePicklistFilter": component.get('v.selectDate'),
            "AmountFilter": null,
            "AmountPicklistFilter": component.get('v.selectAmount'),
            "MonthFilter": null,
            "MonthPicklistFilter": component.get('v.selectMonth')
        });
        action.setCallback(this, function (a) {
            var state = a.getState(); // get the response state
            if (state == 'SUCCESS') {
                console.log('success');
                component.set('v.data', a.getReturnValue());
                component.set('v.numberOfRecords', a.getReturnValue().length)
                component.set('v.loading', false);
            }
        });
        $A.enqueueAction(action);
    },

    openModel: function (component, event, helper) {
        // Set isModalOpen attribute to true
        component.set("v.isModalOpen", true);
    },

    closeModel: function (component, event, helper) {
        // Set isModalOpen attribute to false  
        component.set("v.isModalOpen", false);
    },

    openModelURL: function (component, event, helper) {
        // Set isModalOpen attribute to true
        component.set("v.isModalURLOpen", true);
    },

    closeModelURL: function (component, event, helper) {
        // Set isModalOpen attribute to false  
        component.set("v.isModalURLOpen", false);
    },

    filter: function (component, event, helper) {
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        console.log('user' + userId);
        component.set('v.loading', true);
        component.set('v.columns', [
            { label: 'Statement Name', type: 'button', initialWidth: 275, typeAttributes: { label: { fieldName: 'Monthly_Statement_Name__c' }, name: 'view_details', variant: 'base' } },
            { label: 'Commission Payment Date', fieldName: 'Commission_Payment_Date__c', type: 'date', format: 'DD/MM/YYYY' },
            { label: 'Commissioning Amount', fieldName: 'Commissioning_Amount__c', type: 'currency' },
            { label: 'Month of Reference', fieldName: 'Month_of_Reference__c', type: 'number' },
            { label: 'Year of Reference', fieldName: 'Year__c', type: 'string' },
            { label: 'Payment Method', fieldName: 'Commission_Payment_Method__c', type: 'string' },
            {
                type: "button", typeAttributes: {
                    iconName: 'utility:download',
                    label: '',
                    name: 'download',
                    title: 'download',
                    disabled: false,
                    variant: { fieldName: 'variantValue' }
                }
            },
        ]);
        console.log(component.get('v.nameFiltered'));
        console.log(component.get('v.dateFiltered'));
        console.log(component.get('v.amountFiltered'));
        console.log(component.get('v.monthFiltered'));
        console.log(component.get('v.statusFiltered'));
        var action = component.get('c.getMyStatements');

        action.setParams({
            "UserId": userId,
            "NameFilter": component.get('v.nameFiltered'),
            "DateFilter": component.get('v.dateFiltered'),
            "DatePicklistFilter": component.get('v.selectDate'),
            "AmountFilter": component.get('v.amountFiltered'),
            "AmountPicklistFilter": component.get('v.selectAmount'),
            "MonthFilter": component.get('v.monthFiltered'),
            "MonthPicklistFilter": component.get('v.selectMonth'),
        });
        action.setCallback(this, function (a) {
            var state = a.getState(); // get the response state
            if (state == 'SUCCESS') {
                console.log('success');
                component.set('v.data', a.getReturnValue());
                component.set('v.numberOfRecords', a.getReturnValue().length)
                component.set('v.loading', false);
            } else {
                console.log(state);
                component.set('v.loading', false);
            }
        });
        $A.enqueueAction(action);

        component.set("v.isModalOpen", false);

    },
    cancelFilter: function (component, event, helper) {
        component.set('v.nameFiltered', null);
        component.set('v.amountFiltered', null);
        component.set('v.monthFiltered', null);
        component.set('v.dateFiltered', null);
    },

    handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');

        switch (action.name) {
            case 'view_details':
                var obj = JSON.parse(JSON.stringify(row));
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": obj.Id
                });
                navEvt.fire();
                //window.open('/flow/Lock_Unlock?recordId={!Account.Id}&retURL={!Account.Id}');
                break;
            case 'download':
                var obj = JSON.parse(JSON.stringify(row));
                if (obj.Statement_Url__c != null) {
                    window.open(obj.Statement_Url__c);
                } else {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type": "warning",
                        "message": "Download not available for this item"
                    });
                    toastEvent.fire();
                }
        }
    }

})