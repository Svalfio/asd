({
    doInit: function(component, event, helper) {
        console.log('start do init Commission Model Status');
        helper.fetchPickListVal(component);
    },
    
    onPicklistChange: function(component, event, helper) {
        console.log('Start onPicklistChange Status');
        component.set("v.selectedStatus",event.getSource().get("v.value"));
        var father = component.get("v.father");
        if(father == "Direct")
			var compEvent = component.getEvent("RefreshCommissionModel");
        else
            var compEvent = component.getEvent("RefreshIndirectCommissionModel");
        console.log('EVENTO : '+compEvent);
        compEvent.fire();
        console.log('EVENTO PARTITO');
    }
})