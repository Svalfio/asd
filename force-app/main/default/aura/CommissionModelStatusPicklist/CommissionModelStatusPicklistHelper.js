({
    fetchPickListVal: function(component) { 
          var opts = [];

        opts.push({
            class: "Active",
            label: "Active",
            value: "Active"
        });
        
        opts.push({
            class: "Inactive",
            label: "Inactive",
            value: "Inactive"
        });

        component.find("Status").set("v.options", opts);
    }
})