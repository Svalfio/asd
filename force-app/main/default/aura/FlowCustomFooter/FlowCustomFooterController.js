({
   init : function(cmp, event, helper) {
      // Figure out which buttons to display
      var availableActions = cmp.get('v.availableActions');
      var array = JSON.parse("[" + availableActions + "]");;
              
      for (var i = 0; i < array.length; i++) {
         if (array[i] == "PAUSE") {
            cmp.set("v.canPause", true);
         } else if (array[i] == "BACK") {
            cmp.set("v.canBack", true);
         } else if (array[i] == "NEXT") {
            cmp.set("v.canNext", true);
         } else if (array[i] == "FINISH") {
            cmp.set("v.canFinish", true);
         }
      }
   },
        
   onButtonPressed: function(cmp, event, helper) {
      // Figure out which action was called
      var actionClicked = event.getSource().getLocalId();
      // Fire that action
      var navigate = cmp.get('v.navigateFlow');
      navigate(actionClicked);
   }
})