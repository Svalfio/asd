({    invoke : function(component, event, helper) {
    	
    	var account = component.get("v.accountId");
		var urlString = window.location.href;
 		var baseURL = urlString.substring(0, urlString.indexOf("/s"));
 		component.set("v.cbaseURL", baseURL);
    	//var baseurl = 'https://eutelsat--dev01.lightning.force.com';
    	var baseurl = window.location.hostname;
    	var url = baseurl+'/apex/zqu__QuoteOption?&crmAccountId='+account;
        
    	var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": url
        });
    
        urlEvent.fire();
     }
})