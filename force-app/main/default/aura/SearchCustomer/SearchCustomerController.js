({

    /**
     * doInit
     * initializes component
     * 
     * @param {*} component 
     * @param {*} event 
     * @param {*} helper 
     */
    doInit: function(component, event, helper){       
        helper.getColumn(component);
        helper.resetForm(component);
        helper.checkCurrentUser(component, helper);
        helper.getPlans(component);
        helper.getStatus(component);
        helper.getCountries(component);
    },

    /**
     * clickSearch
     * manages click on search button
     * 
     * @param {*} component 
     * @param {*} event 
     * @param {*} helper 
     */
    clickSearch : function(component, event, helper) {
        helper.getAccounts(component, helper, false);
    },

    /**
     * clickCancel
     * manages click on cancel button
     * 
     * @param {*} component 
     * @param {*} event 
     * @param {*} helper 
     */
    clickCancel : function(component, event, helper) {
        console.log('clickCancel');
        helper.resetForm(component);
    },

    /**
     * handleRowAction
     * manages actions
     * 
     * @param {*} component 
     * @param {*} event 
     * @param {*} helper 
     */
    handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');

        switch (action.name) {
            // case 'show_details':
            //     var obj = JSON.parse(JSON.stringify(row));
            //     var navEvt = $A.get("e.force:navigateToSObject");
            //     navEvt.setParams({
            //         "recordId": obj.AccountId
            //     });
            //     navEvt.fire();
            //     break;
            case 'view_details':
                var obj = JSON.parse(JSON.stringify(row));
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": obj.AccountId
                });
                navEvt.fire();
                break;
        }
    },

    /**
     * handleNameChange
     * manages name field change event
     * 
     * @param {*} component 
     * @param {*} event 
     * @param {*} helper 
     */
    handleNameChange: function (component, event, helper) {
        var value = component.get("v.customerName");
        if (value.length >=3) {
            helper.getSuggestions(component, "customerName");
        }
    },

    /**
     * handleNameFocus
     * 
     * @param {*} component 
     * @param {*} event 
     * @param {*} helper 
     */
    handleNameFocus: function (component, event, helper) {
        component.set('v.customerNameSuggestionsShow', true);

        var value = component.get("v.customerName");
        if (value.length >=3) {
            helper.getSuggestions(component, "customerName");
        }
    },
    
    /**
     * handleNameBlur
     * 
     * @param {*} component 
     * @param {*} event 
     * @param {*} helper 
     */
    handleNameBlur: function (component, event, helper) {
        setTimeout(function (){
            component.set('v.customerNameSuggestionsShow', false);
        },125);
    },

    /**
     * clickNameSuggestion
     * 
     * @param {*} component 
     * @param {*} event 
     * @param {*} helper 
     */
    clickNameSuggestion: function(component, event, helper) {
        var name = event.target.name;
        component.set("v.customerName", name);
    },

    /**
     * handlePhoneChange
     * manages phone field change event
     * 
     * @param {*} component 
     * @param {*} event 
     * @param {*} helper 
     */
    handlePhoneChange: function (component, event, helper) {
        var value = component.get("v.phoneNumber");
        if (value.length >=5) {
            helper.getSuggestions(component, "phoneNumber");
        } else {
            component.set("v.customerPhoneSuggestions", []);
        }
    },

    /**
     * handlePhoneFocus
     * 
     * @param {*} component 
     * @param {*} event 
     * @param {*} helper 
     */
    handlePhoneFocus: function (component, event, helper) {
        component.set('v.customerPhoneSuggestionsShow', true);

        var value = component.get("v.phoneNumber");
        if (value.length >=5) {
            helper.getSuggestions(component, "phoneNumber");
        } else {
            component.set("v.customerPhoneSuggestions", []);
        }
    },
    
    /**
     * handlePhoneBlur
     * 
     * @param {*} component 
     * @param {*} event 
     * @param {*} helper 
     */
    handlePhoneBlur: function (component, event, helper) {
        setTimeout(function (){
            component.set('v.customerPhoneSuggestionsShow', false);
        },125);
    },

    /**
     * clickPhoneSuggestion
     * 
     * @param {*} component 
     * @param {*} event 
     * @param {*} helper 
     */
    clickPhoneSuggestion: function(component, event, helper) {
        var name = event.target.name;
        component.set("v.phoneNumber", name);
    },

    /**
     * handleEmailChange
     * manages email field change event
     * 
     * @param {*} component 
     * @param {*} event 
     * @param {*} helper 
     */
    handleEmailChange: function (component, event, helper) {
        var value = component.get("v.emailAddress");
        if (value.length >=3) {
            helper.getSuggestions(component, "emailAddress");
        }
    },

    /**
     * handleEmailFocus
     * 
     * @param {*} component 
     * @param {*} event 
     * @param {*} helper 
     */
    handleEmailFocus: function (component, event, helper) {
        component.set('v.customerEmailSuggestionsShow', true);

        var value = component.get("v.emailAddress");
        if (value.length >=3) {
            helper.getSuggestions(component, "emailAddress");
        }
    },
    
    /**
     * handleEmailBlur
     * 
     * @param {*} component 
     * @param {*} event 
     * @param {*} helper 
     */
    handleEmailBlur: function (component, event, helper) {
        setTimeout(function (){
            component.set('v.customerEmailSuggestionsShow', false);
        },125);
    },

    /**
     * clickEmailSuggestion
     * 
     * @param {*} component 
     * @param {*} event 
     * @param {*} helper 
     */
    clickEmailSuggestion: function(component, event, helper) {
        var name = event.target.name;
        component.set("v.emailAddress", name);
    },

    /**
     * clickSearchFilter
     * 
     * @param {*} component 
     * @param {*} event 
     * @param {*} helper 
     */
    clickSearchFilter: function (component, event, helper) {
        component.set("v.isModalOpen", false);
        helper.getAccounts(component, helper, true);
    },

    /**
     * openModel
     * 
     * @param {*} component 
     * @param {*} event 
     * @param {*} helper 
     */
    openModel: function(component, event, helper) {
        component.set("v.isModalOpen", true);
    },

    /**
     * closeModel
     * 
     * @param {*} component 
     * @param {*} event 
     * @param {*} helper 
     */
    closeModel: function(component, event, helper) {
        component.set("v.isModalOpen", false);
    },
    
    /**
     * clearFilters
     * 
     * @param {*} component 
     * @param {*} event 
     * @param {*} helper 
     */
    clearFilters: function(component, event, helper) {
        component.set("v.filterPlan", '');
        component.set("v.filterStatus", '');
        component.set("v.filterCountry", '');
        component.set("v.filterDate", null);
    }

})