({
    /**
     * getColumn
     * sets datatable columns
     * 
     * @param {*} component 
     */
    getColumn: function(component) {
        var actions = [
            {label: 'Show details', name: 'show_details'}
        ];
        component.set('v.columns', [
            // {label: 'Customer Name', fieldName: 'AccountName', type: 'text'},
            {
                label: 'Customer Account', type: 'button', initialWidth: 275,
                typeAttributes: { label: { fieldName: 'AccountName' }, name: 'view_details', variant: 'base'}
            },
            {label: 'Account ID', fieldName: 'AccountId', type: 'text'},
            {label: 'Phone number', fieldName: 'AccountPhone', type: 'text'},
            {label: 'Email Address', fieldName: 'AccountEmail', type: 'text'},
            {label: 'Serial Number', fieldName: 'SerialNumber', type: 'text'}
            // {type: 'action', typeAttributes: { rowActions: actions } }
        ]);
    },

    /**
     * getColumnNoTerminal
     * sets datatable columns without terminal
     * 
     * @param {*} component 
     */
    getColumnNoTerminal: function(component) {
        var actions = [
            {label: 'Show details', name: 'show_details'}
        ];
        component.set('v.columns', [
            // {label: 'Customer Name', fieldName: 'AccountName', type: 'text'},
            // {
            //     label: 'Customer Name', fieldName: 'AccountName', type: 'url',
            //     typeAttributes: { label: { fieldName: 'AccountName' }, name: 'URL', target: '_blank' }
            // },
            {
                label: 'Customer Account', type: 'button', initialWidth: 275,
                typeAttributes: { label: { fieldName: 'AccountName' }, name: 'view_details', variant: 'base' }
            },
            {label: 'Account ID', fieldName: 'AccountId', type: 'text'},
            {label: 'Phone number', fieldName: 'AccountPhone', type: 'text'},
            {label: 'Email Address', fieldName: 'AccountEmail', type: 'text'},
           // {type: 'action', typeAttributes: { rowActions: actions } }
        ]);

    },

    /**
     * resetForm
     * reset search form
     * 
     * @param {*} component 
     */
    resetForm: function(component) {
        component.set("v.customerName", "");
        component.set("v.customerAccountId", "");
        component.set("v.phoneNumber", "");
        component.set("v.emailAddress", "");
        component.set("v.terminalId", "");
    },

    /**
     * checkCurrentUser
     */
    checkCurrentUser: function(component, helper) {
        var action = component.get("c.getRecordTypeCurrentUser");
        action.setParams({
            "id": $A.get("$SObjectType.CurrentUser.Id")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var recordType = response.getReturnValue();
                component.set('v.isRequiredCustomerName', recordType === 'Reseller');
                component.set('v.isReseller', recordType === 'Reseller');
                component.set('v.isDistributor', recordType === 'Distributor');

                // for distributor it performs search on page load
                if (recordType === 'Distributor') {
                    this.getAccounts(component, helper, false);
                }
            }
        });
        $A.enqueueAction(action);
    },

    /**
     * getAccounts
     * Permorms search
     * 
     * @param {*} component 
     * @param {*} helper
     * @param {Boolean} filterActive 
     */
    getAccounts : function(component, helper, filterActive) {
        component.set("v.requiredError", false);
        component.set("v.noRowError", false);
        component.set("v.oneFieldAtLeast", false);
        component.set("v.tooManyRowsError", false);
        
        if ( component.get('v.terminalId') !== "" ) {
            helper.getColumn(component);
        } else {
            helper.getColumnNoTerminal(component);
        }

        if ( component.get('v.customerName') === "" && component.get('v.customerAccountId') === "" && 
                component.get('v.phoneNumber') === "" && component.get('v.emailAddress') === "" && 
                component.get('v.terminalId') === "" && !component.get('v.isDistributor')) {
            component.set("v.oneFieldAtLeast", true);
        } /*else if ( (component.get('v.isRequiredCustomerName') && component.get('v.customerName') === "") ||
            !component.get('v.isRequiredCustomerName'))*/
    
        else if ((component.get('v.isRequiredCustomerName') && (component.get('v.customerName') === "" &&
            (component.get('v.customerAccountId') !== "" || component.get('v.phoneNumber') !== "" ||
                component.get('v.emailAddress') !== "" || component.get('v.terminalId') !== ""))) ||
            (component.get('v.isRequiredCustomerName') && (component.get('v.customerName') !== "" ))||
            !component.get('v.isRequiredCustomerName') ){
            component.set('v.loading', true);

            // Create the action
            var action = component.get("c.searchAccount");
            action.setParams({
                "customerName": component.get('v.customerName'),
                "customerAccountId": component.get('v.customerAccountId'),
                "phoneNumber": component.get('v.phoneNumber'),
                "emailAddress": component.get('v.emailAddress'),
                "terminalId": component.get('v.terminalId'),
                "filterPlan": component.get('v.filterPlan'),
                "filterStatus": component.get('v.filterStatus'),
                "filterCountry": component.get('v.filterCountry'),
                "filterDate": component.get('v.filterDate'),
                "filterActive": filterActive,
                "isReseller": component.get('v.isRequiredCustomerName')
            });
            // Add callback behavior for when response is received
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var rows = response.getReturnValue();

                    if (rows.length == 0) {
                        component.set("v.noRowError", true);
                        component.set("v.accounts", []);
                    } else if (rows.length <= 10 || !component.get('v.isRequiredCustomerName')) {

                        var row = null;
                        for (var i = 0; i < rows.length; i++) {
                            row = rows[i];
                            if (row.Account){
                                row.AccountName = row.Account.Name;
                                row.AccountId = row.Account.Id;
                                row.AccountPhone = row.Account.Phone;
                                row.AccountEmail = row.Account.Email_address__c;
                                row.TerminalId = row.SerialNumber;
                            }else{
                                row.AccountName = row.Name;
                                row.AccountId = row.Id;
                                row.AccountPhone = row.Phone;
                                row.AccountEmail = row.Email_address__c;
                                row.TerminalId = '';
                            }
                        }
                        
                        component.set("v.accounts", rows);
                    } else {
                        component.set("v.tooManyRowsError", true);
                        component.set("v.accounts", []);
                    }
                } else {
                    console.log("Failed with state: " + state);
                    component.set("v.noRowError", true);
                    component.set("v.accounts", []);
                }
                component.set('v.loading', false);
            });
            $A.enqueueAction(action);
        
        
        } else {
            component.set("v.requiredError", true);
        }
    },

    /**
     * getSuggestions
     * shows suggestion
     * 
     * @param {*} component 
     * @param {String} field
     */
    getSuggestions: function (component, field) {
        // Create the action
        var action = component.get("c.searchAccount");
        var params = {};
        params[field] = component.get('v.'+field);
        params["isReseller"] = component.get('v.isRequiredCustomerName');
        params["filterActive"] = false;
        action.setParams(params);

        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var rows = response.getReturnValue();

                rows = rows.slice(0, 5); // limit results

                var row = null;
                for (var i = 0; i < rows.length; i++) {
                    row = rows[i];
                    if (row.Account){
                        row.AccountName = row.Account.Name;
                        row.AccountId = row.Account.Id;
                        row.AccountPhone = row.Account.Phone;
                        row.AccountEmail = row.Account.Email_address__c;
                        row.TerminalId = row.Id;
                    }else{
                        row.AccountName = row.Name;
                        row.AccountId = row.Id;
                        row.AccountPhone = row.Phone;
                        row.AccountEmail = row.Email_address__c;
                        row.TerminalId = '';
                    }
                }
                if (field == "phoneNumber") {
                    component.set("v.customerPhoneSuggestions", rows);
                } else if (field == "emailAddress") {
                    component.set("v.customerEmailSuggestions", rows);
                } else {
                    component.set("v.customerNameSuggestions", rows);
                }
            } else {
                console.log("Failed with state: " + state);
            }
            //component.set('v.loading', false);
        });
        $A.enqueueAction(action);
    },
    
    /**
     * getPlans
     * 
     * @param {*} component 
     */
    getPlans: function(component) {
        var action = component.get("c.getPlans");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.plans", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    /**
     * getStatus
     * 
     * @param {*} component 
     */
    getStatus: function(component) {
        var action = component.get("c.getStatus");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.status", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    /**
     * getCountries
     * 
     * @param {*} component 
     */
    getCountries: function(component) {
        var action = component.get("c.getCountries");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.countries", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }
})