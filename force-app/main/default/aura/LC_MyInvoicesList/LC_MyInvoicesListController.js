({
    doinit : function(component, event, helper) {
        var userId = $A.get( "$SObjectType.CurrentUser.Id" );
        console.log('user'+userId);
        component.set('v.loading', true);
        component.set('v.columns', [
            {label: 'Name',  type: 'button', initialWidth: 275, typeAttributes: {label: { fieldName: 'Name' }, name: 'view_details', variant: 'base'}}, 
            {label: 'Invoice Date', fieldName: 'Zuora__Generated_Date__c', type: 'date', format: 'DD/MM/YYYY'},
            {label: 'Amount', fieldName: 'Zuora__TotalAmount__c', type: 'currency'},
            {label: 'Balance', fieldName: 'Zuora__Balance2__c', type: 'number'},
            {label: 'Status', fieldName: 'Zuora__Status__c', type: 'text'},
            {type: "button", typeAttributes: {
                iconName: 'utility:download',
                label: '',
                name: 'selectRecord',
                title: 'selectRecord',
                disabled: false,
                value: 'test',
                variant: {fieldName: 'variantValue'}
            }},
        ]);
            
            var action = component.get('c.getMyInvoices'); 
            
            action.setParams({
            "UserId" : userId,
            "NameFilter" : null,
            "DateFilter" : null,
            "DatePicklistFilter" : component.get('v.selectDate'),
            "AmountFilter" : null,
            "AmountPicklistFilter" : component.get('v.selectAmount'),
            "BalanceFilter" : null,
            "BalancePicklistFilter" : component.get('v.selectBalance'),
            "StatusFilter" : null
            });
            action.setCallback(this, function(a){
            var state = a.getState(); // get the response state
            if(state == 'SUCCESS') {
            console.log('success');
            component.set('v.data', a.getReturnValue());
            component.set('v.numberOfRecords', a.getReturnValue().length)
            component.set('v.loading', false);
            }
            });
            $A.enqueueAction(action);
            },
            handleRowAction: function (component, event, helper) {
                var action = event.getParam('action');
                var row = event.getParam('row');
                var text = '';
                switch (action.name) {
                    case 'selectRecord':
                        console.log(row.Id);
                        console.log(row.Name);
                        console.log('Showing Details: ' + JSON.stringify(row));
                        var actionPDF = component.get('c.DownloadInvoice');
                            console.log(actionPDF);
                            actionPDF.setParams({
                            "InvoiceName" : row.Name
                        });
                        actionPDF.setCallback(this, function(a){
                            console.log('finito');
                            var state = a.getState(); // get the response state
                            if(state == 'SUCCESS') {
                                console.log('success');
                                console.log(a.getReturnValue());
                                text = a.getReturnValue();
                                console.log('text'+text);
                                window.open('/partner/s/sfsites/c'+text);
                            }
                        });
                        $A.enqueueAction(actionPDF);
                        break;
                    case 'view_details':
                        var obj = JSON.parse(JSON.stringify(row));
                        var navEvt = $A.get("e.force:navigateToSObject");
                        navEvt.setParams({
                            "recordId": obj.Id
                        });
                        navEvt.fire();
                        //window.open('/flow/Lock_Unlock?recordId={!Account.Id}&retURL={!Account.Id}');
                        break;
            }
            
            },
            openModel: function(component, event, helper) {
            // Set isModalOpen attribute to true
            component.set("v.isModalOpen", true);
            },
            
            closeModel: function(component, event, helper) {
            // Set isModalOpen attribute to false  
            component.set("v.isModalOpen", false);
            },
            filter: function(component, event, helper) {
            var userId = $A.get( "$SObjectType.CurrentUser.Id" );
            console.log('user'+userId);
            component.set('v.loading', true);
            component.set('v.columns', [
            {label: 'Name',  type: 'button', initialWidth: 275, typeAttributes: {label: { fieldName: 'Name' }, name: 'view_details', variant: 'base'}}, 
            {label: 'Invoice Date', fieldName: 'Zuora__Generated_Date__c', type: 'date', format: 'DD/MM/YYYY'},
            {label: 'Amount', fieldName: 'Zuora__TotalAmount__c', type: 'currency'},
            {label: 'Balance', fieldName: 'Zuora__Balance2__c', type: 'number'},
            {label: 'Status', fieldName: 'Zuora__Status__c', type: 'text'},
            {type: "button", typeAttributes: {
            iconName: 'utility:download',
            label: '',
            name: 'selectRecord',
            title: 'selectRecord',
            disabled: false,
            value: 'test',
            variant: {fieldName: 'variantValue'}
            }},
        ]);
        console.log(component.get('v.nameFiltered'));
        console.log(component.get('v.dateFiltered'));
        console.log(component.get('v.amountFiltered'));
        console.log(component.get('v.balanceFiltered'));
        console.log(component.get('v.statusFiltered'));
        var action = component.get('c.getMyInvoices'); 
        
        action.setParams({
            "UserId" : userId,
            "NameFilter" : component.get('v.nameFiltered'),
            "DateFilter" : component.get('v.dateFiltered'),
            "DatePicklistFilter" : component.get('v.selectDate'),
            "AmountFilter" : component.get('v.amountFiltered'),
            "AmountPicklistFilter" : component.get('v.selectAmount'),
            "BalanceFilter" : component.get('v.balanceFiltered'),
            "BalancePicklistFilter" : component.get('v.selectBalance'),
            "StatusFilter" : component.get('v.statusFiltered')
        });
        action.setCallback(this, function(a){
            var state = a.getState(); // get the response state
            if(state == 'SUCCESS') {
                console.log('success');
                component.set('v.data', a.getReturnValue());
                component.set('v.numberOfRecords', a.getReturnValue().length)
                component.set('v.loading', false);
            }else{
                console.log(state);
                component.set('v.loading', false);
            }
        });
        $A.enqueueAction(action);
        
        component.set("v.isModalOpen", false);
        
    },
    cancelFilter: function(component, event, helper){
        component.set('v.nameFiltered', null);
        component.set('v.amountFiltered', null);
        component.set('v.balanceFiltered', null);
        component.set('v.dateFiltered', null);
        component.set('v.statusFiltered', null);
    }
})