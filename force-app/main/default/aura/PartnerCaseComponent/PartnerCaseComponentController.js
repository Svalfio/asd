({
	CheckChange : function(component, event, helper) {
        var action = component.get('c.VerifyChange'); 
        var userId = $A.get( "$SObjectType.CurrentUser.Id" );
        console.log('user id' + userId);
        action.setParams({
            "UserId" : userId
        });
        action.setCallback(this, function(a){
            var state = a.getState(); // get the response state
            if(state == 'SUCCESS') {
                component.set('v.bool', a.getReturnValue());
                console.log('ritorno' + a.getReturnValue());
                if (String(component.get('v.bool')) == 'true'){
                    toastEvent.fire();
                }
            }
        });
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Your case is assigned!",
            "message": "Your case has been assigned to our operator!",
            "type" : "Success"
        });
        
        $A.enqueueAction(action);
    }
})