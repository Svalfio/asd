({
	sendEmail: function (component) {
        console.log('a');
        var quoteId = component.get("v.quoteId");
        var emailAddress = component.get("v.emailAddress");
        console.log(quoteId);
        console.log(emailAddress);
        var action = component.get("c.sendMail");
        action.setParams({
            "quoteId": quoteId,
            "emailAddress": emailAddress
        });
        action.setCallback(this, function (a) {
            var state = a.getState(); // get the response state
            if (state == 'SUCCESS') {
                
            }
        });

        $A.enqueueAction(action);
    }
})