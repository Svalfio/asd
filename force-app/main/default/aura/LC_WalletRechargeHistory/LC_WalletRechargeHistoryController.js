({
	doinit : function(component, event, helper) {
		var action = component.get('c.getRecharges'); 
        var recordId = component.get('v.recordId');
        console.log(component.get('v.filterId'));
        action.setParams({
            "recordId" : recordId
        });
        action.setCallback(this, function(a){
            var state = a.getState(); // get the response state
            if(state == 'SUCCESS') {
                component.set('v.recharges', a.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	}
})