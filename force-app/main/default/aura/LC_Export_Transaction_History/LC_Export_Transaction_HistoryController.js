({
	handleClick : function(component, event, helper) {
        
        var sDate = component.get("v.dataInValue");
        console.log(sDate);
        
        var eDate = component.get("v.dataEndValue");
        var rId = component.get('v.recordId');
        console.log(eDate);
        console.log(component.get("v.selectPicklistFormat"));
        if(component.get("v.selectPicklist") == 'Transaction'){
            if(component.get("v.selectPicklistFormat") == 'PDF'){
                window.open('/partner/s/sfsites/c/apex/VP_ExportToPDF_Transaction?recordId='
                            +component.get('v.recordId')+'&startDate='+sDate+'&endDate='+eDate);

            }else{
                window.open('/partner/s/sfsites/c/apex/VP_ExportToExcel_Transaction?recordId='
                            +component.get('v.recordId')+'&startDate='+sDate+'&endDate='+eDate);
            }
        }else{
            if(component.get("v.selectPicklistFormat") == 'PDF'){
                window.open('/partner/s/sfsites/c/apex/VP_ExportToPDF_History?recordId='
                            +component.get('v.recordId')+'&startDate='+sDate+'&endDate='+eDate);
            }else{
                window.open('/partner/s/sfsites/c/apex/VP_ExportToExcel_History?recordId='
                            +component.get('v.recordId')+'&startDate='+sDate+'&endDate='+eDate);
            }
            
        }
	},
    openModel: function(component, event, helper) {
      // Set isModalOpen attribute to true
      component.set("v.isModalOpen", true);
   },
  
    closeModel: function(component, event, helper) {
        // Set isModalOpen attribute to false  
        component.set("v.isModalOpen", false);
    },
    
    downloadDocument : function(component, event, helper){
        var sendDataProc = component.get("v.sendData");
        var dataToSend = {
            "label" : "This is test"
        }; //this is data you want to send for PDF generation
        //invoke vf page js method
        sendDataProc(dataToSend, function(){
            //handle callback
        });
    }
})