({
	showErrorToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Warning!",
            "message": "No information found",
            "type": "warning"
        });
        toastEvent.fire();
    }
})