({
	clickSearch : function(component, event, helper) {
        if(component.get('v.valueRadioButton') == 'Case'){
            
            console.log('case');
            var action = component.get('c.getCaseById');
            action.setParams({
                'CaseId' : component.get('v.caseId')
            });
            
            action.setCallback(this, function(response){
                var state = response.getState();
                if(state == 'SUCCESS'){
                    if(response.getReturnValue() == 'No information found'){
                        helper.showErrorToast();
                    }else{
                        window.open('/lightning/r/Case/'+response.getReturnValue()+'/view')
                    }
                }else{
                    console.log('Failed with state: '+state);
                }
            });
            $A.enqueueAction(action);
            
        }else if(component.get('v.valueRadioButton') == 'Partner'){
            console.log('partner');
            var action = component.get('c.getPartnerByCaseId');
            action.setParams({
                'CaseId' : component.get('v.caseId')
            });
            
            action.setCallback(this, function(response){
                var state = response.getState();
                if(state == 'SUCCESS'){
					if(response.getReturnValue() == 'No information found'){
                        helper.showErrorToast();
                    }else{
                        window.open('/lightning/r/Account/'+response.getReturnValue()+'/view')
                    }
                }else{
                    console.log('Failed with state: '+state);
                }
            });
            $A.enqueueAction(action);
            
        }else if(component.get('v.valueRadioButton') == 'Customer'){
             console.log('customer');
            var action = component.get('c.getCustomerByCaseId');
            action.setParams({
                'CaseId' : component.get('v.caseId')
            });
            
            action.setCallback(this, function(response){
                var state = response.getState();
                if(state == 'SUCCESS'){
					if(response.getReturnValue() == 'No information found'){
                        helper.showErrorToast();
                    }else{
                        window.open('/lightning/r/Account/'+response.getReturnValue()+'/view')
                    }
                }else{
                    console.log('Failed with state: '+state);
                }
            });
            $A.enqueueAction(action);
        }
		
	}
})