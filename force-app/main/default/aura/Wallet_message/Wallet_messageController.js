({
	CheckAmount : function(component, event, helper) {
        var action = component.get('c.VerifyAmount'); 
        var userId = $A.get( "$SObjectType.CurrentUser.Id" );
        console.log('user id' + userId);
        action.setParams({
            "UserId" : userId
        });
        action.setCallback(this, function(a){
            var state = a.getState(); // get the response state
            if(state == 'SUCCESS') {
                component.set('v.boolean', a.getReturnValue());
                console.log('ritorno' + a.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }
})