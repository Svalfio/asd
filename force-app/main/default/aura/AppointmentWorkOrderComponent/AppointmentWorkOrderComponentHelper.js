({
	createWorkOrder : function(component,event) {
        
		console.log("DO INIT - Create Work Order");
         var action = component.get("c.createTask");
        console.log('OrderID :'+component.get("v.recordId"));
         action.setParams({
             'orderId' : component.get("v.recordId")
            });
        
            action.setCallback(this,function(res){
            	var state = res.getState();
           		if(state === "SUCCESS"){
                    console.log("SUCCESS CreateOrder");
                    var response = res.getReturnValue();
                    component.set("v.esito", response);
                
                } else {
                	console.log('error');
                }
            });
            
            $A.enqueueAction(action);
	}
})