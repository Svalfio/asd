({
    helperFunc: function (component) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.getAccountRating");
        action.setParams({
            recordId: recordId
        });
        action.setCallback(this, function (response) {
            component.set("v.accountRating" ,response.getReturnValue());
        });
        $A.enqueueAction(action);

    }
})