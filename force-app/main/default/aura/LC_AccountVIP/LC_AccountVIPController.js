({


    doInit: function (component, event, helper) {

        helper.helperFunc(component);

    },

    handleSubmit: function (component, event, helper) {
        component.set('v.disabled', true);
        component.set('v.provaRefresh', true);
        component.set('v.saved', true);
        location.reload();
    },

    handleCancel: function (component, event, helper) {
        component.set('v.provaRefresh', true);
    },

    refresh: function (component, event, helper) {
        component.set('v.provaRefresh', false);

    }

})