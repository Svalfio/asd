({
     doInit: function (component, event, helper) {
         
        var accountId = component.get("v.recordId");
        var selectedRecTypeName = component.get("v.selectedRecTypeName");
         
        console.log('accountId: '+accountId);
        console.log('selectedRecType: '+selectedRecTypeName);
         
        var action = component.get("c.retrieveInfo");
        action.setParams({
            accountId : accountId,
            recordTypeName : selectedRecTypeName
        });
         
        action.setCallback(this, function(response) {  
            var state = response.getState();  
            if (state === "SUCCESS") {
                console.log(JSON.stringify(response.getReturnValue()));
                component.set( "v.wrapper", response.getReturnValue());
                var recordTypeId = response.getReturnValue().recordTypeId;
                var result = response.getReturnValue().countryList;
                console.log(JSON.stringify(result));
                var opts = [];
                for (var j = 0; j < result.length; j++) {
                    opts.push({
                        label: result[j],
                        value: result[j]
                    });
                
                }
                console.log('*** opts *** '+JSON.stringify(opts));
                component.set("v.listOptions", opts);
                component.set("v.recordTypeId",recordTypeId);
                console.log('*** recordTypeId *** '+recordTypeId);
            }  
        });  
        $A.enqueueAction(action);  
        
        
     },
    
     handleChange: function (component, event) {
        var selectedOptionsList = event.getParam("value");
        console.log(selectedOptionsList);
        component.set("v.selectedOptions", selectedOptionsList);
        
     },
    
    closeModal: function (component, event) {
       	console.log('Start closeModal');
        $A.get("e.force:closeQuickAction").fire();
     },
    
    saveRecord: function (component, event) {
       var subresellerid = ''; 
       var startdate =  component.find("startdate").get("v.value");
       var enddate =  component.find("enddate").get("v.value"); 
       var accountId = component.get("v.recordId");
       var selectedcountry = component.get("v.selectedOptions");
       var recordtypeid = component.get("v.recordTypeId");
       var selectedRecTypeName = component.get("v.selectedRecTypeName");
       
       if(component.get("v.selectedLookUpRecord").Id != undefined && selectedRecTypeName == 'Ad-hoc'){
           subresellerid = component.get("v.selectedLookUpRecord").Id;
       }
        
       console.log('Selected country at save:'+selectedcountry);
       console.log('Selected record type name at save:'+selectedRecTypeName);
       console.log('Selected subreseller at save:'+subresellerid);
        
       if(startdate == null || startdate == undefined || startdate == '' 
          || enddate == null || enddate == undefined || enddate == ''
          || selectedcountry == null || selectedcountry == undefined || selectedcountry == ''){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type": "Error",
                "title": "Error!",
                "message": "These required fields must be completed: Country, Start Date, End Date"
            });
            toastEvent.fire();
       }
       else if(selectedRecTypeName == 'Ad-hoc' && (subresellerid == '' || subresellerid == null)){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type": "Error",
                "title": "Error!",
                "message": "These required fields must be completed: Reseller"
            });
            toastEvent.fire();
       } 
       else{	
            console.log('*** Start creazione ***');
           
            var action = component.get("c.createCommissionModel");
            action.setParams({ 
                accountId : accountId,
                recordTypeName : selectedRecTypeName,
                subresellerid : subresellerid,
                startdate : startdate,
                enddate   : enddate,
                selectedcountry : selectedcountry.toString()
            }); 
           
           console.log(JSON.stringify(action.getParams()));
           
           action.setCallback(this, function(response) {  
                var state = response.getState();
                console.log('state '+ state);
                if (state === "SUCCESS") {          
                    var result = response.getReturnValue();
                    console.log('Error result '+ result);
                    
                    var toastEvent = $A.get("e.force:showToast");
                    
                    if(result != 'OK'){
                        toastEvent.setParams({
                            "type": "Error",
                            "title": "Error!",
                            "message": "It is not possible to create New Commission Model with inserted information. Please, check: Country,Start Date,End Date"
                        });
                    	toastEvent.fire();
                    }
                    else{
                        toastEvent.setParams({
                            "type": "Success",
                            "title": "Success!",
                            "message": "Record successfully created"
                        });
                    	toastEvent.fire();
                        
                        console.log('Start closeModal OK');
       					$A.get("e.force:closeQuickAction").fire();
                    }
                }  
            });  
            $A.enqueueAction(action); 
       }
     }
})