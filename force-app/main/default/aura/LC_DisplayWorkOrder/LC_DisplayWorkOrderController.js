({
    
    /**
     * doInit
     * Loads items from Salesforce
     *  
     * @param {*} component 
     * @param {*} event 
     * @param {*} helper 
     */
    doInit: function(component, event, helper) {
        var urlParams = helper.getUrlVars();
        component.set("v.quoteId", urlParams.id);

        // Create the action
        var action = component.get("c.retrieveContactInfo");
        action.setParams({
            "quoteId": urlParams.id
        });
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.contact", response.getReturnValue());
            } else {
                console.log("Failed with state: " + state);
            }
        });
        // Send action off to be executed
        $A.enqueueAction(action);

        //check if the invoice is present
        var action6 = component.get("c.retrieveAttachmentInfo");
        action6.setParams({
            "quoteId": urlParams.id
        });
        // Add callback behavior for when response is received
        action6.setCallback(this, function (response1) {
            var state = response1.getState();
            if (state === "SUCCESS") {
                helper.invoiceId = response1.getReturnValue()[2];
                component.set("v.attachmentPresent", Boolean.valueOf(response1.getReturnValue()[0]));
                component.set("v.invoiceName", response1.getReturnValue()[1]);
            } else {
                console.log("Failed with state: " + state);
            }
        });
        // Send action off to be executed
        $A.enqueueAction(action6);

    },

    /**
     * handleClickDownloadI
     * Manages download invoice button click
     *  
     * @param {*} component 
     * @param {*} event 
     * @param {*} helper 
     */
    handleClickDownloadI: function(component, event, helper) {
        console.log("handleClickDownloadI");
        // var urlParams = helper.getUrlVars();
        var id = component.get("v.invoiceId");
        window.open('/partner/servlet/servlet.FileDownload?file=' + helper.invoiceId , '_blank');
                /*var link = document.getElementById('downloadLink');
                link.download = "fileName.pdf";
                link.href = 'https://dev01-eutelsat.cs85.force.com/partner/servlet/servlet.FileDownload?file=' + response2.getReturnValue()[2];

                link.click();
                */
        //     }else{
        //         console.log("Failed with state: " + state);
        //     }
        // });
        // // Send action off to be executed
        // $A.enqueueAction(action7);

        // var token = "";
        // // Create the action
        // var action = component.get("c.getAccessToken");
        // // Add callback behavior for when response is received
        // action.setCallback(this, function(response) {
        //     var state = response.getState();
        //     if (state === "SUCCESS") {
        //         token = response.getReturnValue();
        //         var action2 = component.get("c.getInvoiceId");
        //         action2.setParams({
        //             "quoteId": urlParams.id,
        //             "token": token
        //         });
        //         action2.setCallback(this, function(response2) {
        //             var state2 = response2.getState();
        //             if (state2 === "SUCCESS") {
        //                 var action3 = component.get("c.getPDFUrl")
        //                 action3.setParams({
        //                     "token": token,
        //                     "invoiceId": response2.getReturnValue()
        //                 });
        //                 action3.setCallback(this, function(response3) {
        //                     var state3 = response3.getState();
        //                     if (state3 === "SUCCESS") {
        //                         var action4 = component.get("c.getPDF")
        //                         action4.setParams({
        //                             "token": token,
        //                             "pdfUrl": response3.getReturnValue()
        //                         });
        //                         action4.setCallback(this, function(response4) {
        //                             var state4 = response4.getState();
        //                             if (state4 === "SUCCESS") {
        //                                 var pdfBlob = response4.getReturnValue();
        //                                 /*var file = new Blob([pdfBlob], {type: 'application/pdf'});
        //                                 var fileURL = URL.createObjectURL(file);
        //                                 window.open(fileURL, '_blank');
        //                                 */
        //                                 var myBlob = new Blob([pdfBlob], {type: "application/pdf"});
        //                                 /*var reader = new FileReader();
        //                                 reader.onload = function(event) {
        //                                     var URL = event.target.result;
        //                                     var a = document.createElement('a');
        //                                     a.href = URL;
        //                                     a.click();
        //                                 };
        //                                 reader.readAsDataURL(myBlob);*/
        //                                 var link = document.getElementById('downloadLink');
        //                                 link.href = URL.createObjectURL(myBlob);
        //                                 link.download = "fileName.pdf";
        //                                 //document.body.append(link);
        //                                 link.click();
        //                                 //component.find('downloadLink').getElement().click();
        //                                 //link.remove();
        //                             } else {
        //                                 console.log("Failed with state: " + state4);
        //                             }
        //                         });
        //                         // Send action off to be executed
        //                         $A.enqueueAction(action4);
        //                     } else {
        //                         console.log("Failed with state: " + state3);
        //                     }
        //                 });
        //                 // Send action off to be executed
        //                 $A.enqueueAction(action3);
        //             } else {
        //                 console.log("Failed with state: " + state2);
        //             }
        //         });
        //         // Send action off to be executed
        //         $A.enqueueAction(action2);
        //     } else {
        //         console.log("Failed with state: " + state);
        //     }
        // });
        // // Send action off to be executed
        // $A.enqueueAction(action);
    },

    handleClickDownloadWO: function (component, event, helper) {
        
        var urlParams = helper.getUrlVars();

        window.open('/partner/s/sfsites/c/apex/DownloadWorkOrderVfp?id=' + urlParams.id, '_blank');
        
        //  var url = 'https://dev01-eutelsat.cs85.force.com/apex/DownloadWorkOrderVfp?id=' + urlParams.id;
        // var urlEvent = $A.get("e.force:navigateToURL");
        // urlEvent.setParams({
        //     "url": url,
        //     "target": "_blank"
        // });
        // urlEvent.fire();

    },
    
    handleClickSendWO: function (component, event, helper) {
        console.log('test send');
        var urlParams = helper.getUrlVars();
        window.open('/partner/s/send-workorder-pdf?recordId='+urlParams.id)
        
    }
    
})