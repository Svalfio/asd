({
    doInit : function(component, event, helper) {
        var invoiceId = component.get("v.recordId");
        var action = component.get("c.DownloadInvoice");
        action.setParams({
            "invoiceId": invoiceId
        });
        action.setCallback(this, function(a) {
            var response = a.getState();
            if (response == 'SUCCESS') {
                component.set("v.url", a.getReturnValue());
                window.open('/partner/s/sfsites/c' + a.getReturnValue(), '_blank');
                
            }
        })
        $A.enqueueAction(action);
    }
})