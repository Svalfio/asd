({
	doInit: function(component, event, helper) {
		var action = component.get("c.getAccount");
        action.setParams({"accountId": component.get("v.recordId")});
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var account = component.get("v.account");
                account = response.getReturnValue();
                component.set("v.account", account);
            } else {
                console.log("Failed with state: " + state);
            }
        });
        // Send action off to be executed
        $A.enqueueAction(action);
	}
})