({
    fetchPickListVal: function(component) {
        var action = component.get("c.getCountryList");
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                /*
                opts.push({
                        class: "None",
                        label: "None",
                        value: "None"
                    });*/
                opts.push({
                        class: "All",
                        label: "All",
                        value: "All"
                    });
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                component.find("Country").set("v.options", opts);
            }
        });
        $A.enqueueAction(action);
    },
})