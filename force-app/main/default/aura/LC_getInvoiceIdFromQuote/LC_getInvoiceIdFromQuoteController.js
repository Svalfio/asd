({
	doInit : function(component, event, helper) {
        console.log('partito');
        console.log(component.get('v.QuoteId'));
		var action = component.get('c.getInvoice');
        action.setParams({
            'QuoteId' : component.get('v.QuoteId')
        });
        
        action.setCallback(this, function(response){
        	var state = response.getState();
            if(state == 'SUCCESS'){
                var InvoiceId = response.getReturnValue();
                console.log('InvoiceId' + InvoiceId);
                component.set('v.InvoiceId', response.getReturnValue());
            }else{
                console.log('Failed with state: '+state);
            }
        });
        $A.enqueueAction(action);
	}
})