({    
    invoke : function(component, event, helper) {
    	var URL = component.get("v.URL");
    	var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": URL
        });
        urlEvent.fire();
     }
})