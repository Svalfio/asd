trigger WalletTransactionTrigger on Wallet_Transaction__c (before insert) {
    
     try {   
        WalletTransactionTriggerHandler handler = new WalletTransactionTriggerHandler();
        
        if(Trigger.isInsert && Trigger.isBefore){
            
            handler.OnBeforeInsert(Trigger.new);
        }      
    } 
    catch(Exception e){
        
        System.debug('WalletTransactionTrigger '+e.getLineNumber()+' '+e.getMessage() +' '+ e.getStackTraceString());
        
    }

}