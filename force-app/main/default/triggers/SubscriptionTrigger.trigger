trigger SubscriptionTrigger on Zuora__Subscription__c (before insert,after insert,before update, after update) {
    
    try { 
        
        SubscriptionTriggerHandler handler = new SubscriptionTriggerHandler();
        
        if(Trigger.isInsert && Trigger.isBefore){
            handler.OnBeforeInsert(trigger.New);   
            
        }
        if(Trigger.isInsert && Trigger.isAfter){
            handler.onAfterInsert(trigger.NewMap); 
        }
        if(Trigger.isBefore && Trigger.isUpdate){
            handler.onBeforeUpdate(trigger.OldMap,trigger.NewMap);
        }
        
        if(Trigger.isAfter && Trigger.isUpdate){
            handler.onAfterUpdate(trigger.NewMap,trigger.OldMap);
        }
        
        
    }
    
    
    catch(Exception e) {
        
        System.debug('SubscriptionTrigger '+e.getLineNumber()+' '+e.getMessage() +' '+ e.getStackTraceString());
        
    }
    
}