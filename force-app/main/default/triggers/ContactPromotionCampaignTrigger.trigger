trigger ContactPromotionCampaignTrigger on CampaignMember (after insert,before delete) {
    
    
    try{
        ContactPromotionCampaignTriggerHandler handler = new ContactPromotionCampaignTriggerHandler();
        if(Trigger.isInsert && Trigger.isAfter)
            handler.OnAfterInsert(trigger.new);
        else {
            if(Trigger.isBefore && Trigger.isDelete){
                handler.onBeforeDelete(trigger.old);
            }
        }
        
    }
    catch(Exception e){
        System.debug('ContactPromotionCampaignTrigger '+e.getLineNumber()+' '+e.getMessage() +' '+ e.getStackTraceString());
    }
    
}