trigger ContactTrigger on Contact (after update) {
    
    try{
        ContactTriggerHandler handler = new ContactTriggerHandler();
        
        if(Trigger.isUpdate && Trigger.isAfter){
            handler.onAfterUpdate(Trigger.new);
            handler.onAfterUpdateAccount(Trigger.new);
        }
        
    }catch(Exception e) {
        System.debug('ContactTrigger '+e.getLineNumber()+' '+e.getMessage() +' '+ e.getStackTraceString());
    }

}