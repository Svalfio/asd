trigger ModifyCustomSettingTrigger on zqu__Quote__c (after insert) {
    
    
        try { 
    system.debug('ModifyCustomSettingTrigger-------------START');
        ModifyCustomSettingTriggerHandler handler = new ModifyCustomSettingTriggerHandler();
          
       if(Trigger.isInsert && Trigger.isAfter)
            handler.OnAfterInsert(trigger.new);
    
    }

     catch(Exception e) {
    
        System.debug('ModifyCustomSettingTrigger '+e.getLineNumber()+' '+e.getMessage() +' '+ e.getStackTraceString());
    
    }
    

}