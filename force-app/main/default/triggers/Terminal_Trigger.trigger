trigger Terminal_Trigger on Asset (after insert, after update) {

    Map<Asset,Id> Asset_RelatedTerminal = new Map<Asset,Id>();
    
    for(Asset singleTerminal : Trigger.new){
        if(singleTerminal.Terminal_Activation_Date__c !=null && singleTerminal.Subscription_ID__c!=null){
            if(Trigger.isInsert || (Trigger.isUpdate && 
                                    ((singleTerminal.Terminal_Activation_Date__c != trigger.oldMap.get(singleTerminal.Id).Terminal_Activation_Date__c) ||
                                     (singleTerminal.Subscription_ID__c!= trigger.oldMap.get(singleTerminal.Id).Subscription_ID__c)))){
                         Asset_RelatedTerminal.put(singleTerminal, singleTerminal.Subscription_ID__c);                
                                     }
        }
    }
    /*
    if(!Asset_RelatedTerminal.isEmpty())
    SubscriptionDate_Management.StartDateSetup(Asset_RelatedTerminal);*/
}