trigger AccountTrigger on Account (before insert,after insert,before update,after update, before delete) {

    
        try { 
        
            AccountTriggerHandler handler = new AccountTriggerHandler();
            
        
            if(Trigger.isInsert && Trigger.isAfter) {
                
                handler.OnAfterInsert(Trigger.new);
            }
            
            if(Trigger.isUpdate && Trigger.isBefore){
                handler.OnBeforeUpdate(trigger.OldMap,trigger.NewMap);  
                
            }
            
            if(Trigger.isUpdate && Trigger.isAfter){
                handler.OnAfterUpdate(trigger.OldMap,trigger.NewMap);  
                
            }
        
        }
    
    
    catch(Exception e) {
    
        System.debug('AccountTrigger '+e.getLineNumber()+' '+e.getMessage() +' '+ e.getStackTraceString());
    
    }
    
    
}