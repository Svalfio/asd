trigger CommissionModelTrigger on Commission_Model__c (before insert,after insert,before delete,before update) {
    
        
    try{
        CommissionModelTriggerHandler handler = new CommissionModelTriggerHandler();
        if(Trigger.isInsert && Trigger.isAfter)
            handler.OnAfterInsert(trigger.new);
        
        if(Trigger.isInsert && Trigger.isBefore)
            handler.OnBeforeInsert(trigger.new);
        
        if(Trigger.isUpdate && Trigger.isBefore)
            handler.OnBeforeUpdate(trigger.new);

        
    }
    catch(Exception e){
        System.debug('CommissionModelTrigger '+e.getLineNumber()+' '+e.getMessage() +' '+ e.getStackTraceString());
    }

}