trigger NotificationItemTrigger on Notification_Item__c (before insert) {
	
    try { 
        
            NotificationItemTriggerHandler handler = new NotificationItemTriggerHandler();
            
             if(Trigger.isInsert && Trigger.isBefore) {
            
                handler.checkEventType(Trigger.new);
            
            }   
            
            if(Trigger.isUpdate && Trigger.isBefore) {
            
               
            
            }   
            
          
        
            else if(Trigger.isInsert && Trigger.isAfter) {
            
                
            
            } 
            
        
        }
    
    
    catch(Exception e) {
    
        System.debug('CaseTrigger '+e.getLineNumber()+' '+e.getMessage() +' '+ e.getStackTraceString());
    
    }
    
}