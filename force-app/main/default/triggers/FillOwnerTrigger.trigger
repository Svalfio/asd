trigger FillOwnerTrigger on Wallet_Transaction__c (after insert) {

    try { 
    
        FillOwnerTriggerHandler handler = new FillOwnerTriggerHandler();
        
        if(Trigger.isInsert && Trigger.isAfter){
            handler.OnAfterInsert(trigger.new);
        }  
            
    }

     catch(Exception e) {
    
        System.debug('FillOwnerTriggerHandler '+e.getLineNumber()+' '+e.getMessage() +' '+ e.getStackTraceString());
    
    }

}