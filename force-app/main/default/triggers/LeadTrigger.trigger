trigger LeadTrigger on Lead (after update) {
    
    try{
        LeadTriggerHandler handler = new LeadTriggerHandler();
        
        if(Trigger.isUpdate && Trigger.isAfter) {
            
            handler.OnAfterUpdate(Trigger.new);
        }
    }
    catch(Exception e) {
        
        System.debug('CaseTrigger '+e.getLineNumber()+' '+e.getMessage() +' '+ e.getStackTraceString());
        
    }
    
}