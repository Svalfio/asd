trigger CampaignMemberHistoryTrigger on CampaignMember (after insert, before delete) {
    try { 
        CampaignMemberHistoryTriggerHandler handler = new CampaignMemberHistoryTriggerHandler();
        if(Trigger.isDelete && Trigger.isBefore) {
            handler.OnBeforeDelete(Trigger.old);
        }else if(Trigger.isInsert && Trigger.isAfter) { 
            handler.OnAfterInsert(Trigger.new);
        }    
    }catch(Exception e) {
        System.debug('CaseTrigger '+e.getLineNumber()+' '+e.getMessage() +' '+ e.getStackTraceString());  
    } 
}