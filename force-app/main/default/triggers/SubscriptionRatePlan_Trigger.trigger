trigger SubscriptionRatePlan_Trigger on Zuora__SubscriptionRatePlan__c (before insert, before update) {

    if(trigger.isBefore){
        if(trigger.isInsert) SubscriptionRatePlan_Handler.UpdateSubscription(trigger.new);
        if(trigger.isUpdate) SubscriptionRatePlan_Handler.getSubscriptionRatePlan(trigger.newMap, trigger.oldMap);
    }
}