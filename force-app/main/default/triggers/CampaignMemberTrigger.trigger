trigger CampaignMemberTrigger on CampaignMember (before insert,after insert,before update,after update) {
    
    try { 
    
        CampaignMemberTriggerHandler handler = new CampaignMemberTriggerHandler();
          
       if(Trigger.isInsert && Trigger.isAfter)
            handler.OnAfterInsert(trigger.new);
    
    }

     catch(Exception e) {
    
        System.debug('CampaignTrigger '+e.getLineNumber()+' '+e.getMessage() +' '+ e.getStackTraceString());
    
    }

}