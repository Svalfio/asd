trigger CampaignTrigger on Campaign (before insert, before update, after insert, after update) {
    
    try { 
    
        CampaignTriggerHandler handler = new CampaignTriggerHandler();
        
        if(Trigger.isInsert && Trigger.isBefore){
            handler.OnBeforeInsert(trigger.new);  
            
        }else if(Trigger.isUpdate && Trigger.isAfter){
            handler.onAfterUpdate(trigger.oldMap, trigger.newMap);
 
        }
    }

     catch(Exception e) {
    
        System.debug('CampaignTrigger '+e.getLineNumber()+' '+e.getMessage() +' '+ e.getStackTraceString());
    
    }
}