trigger ProductTrigger on Product2 (before insert) {
    
    if(trigger.isInsert && trigger.isBefore){
        
        Map<String,Decimal> ProductLastVersion = new Map<String,Decimal>();
        Map<String, List<Product2>> ProductToUpdate = new Map<String, List<Product2>>();
        List<Product2> OldVersions = new List<Product2>();
        List<String> ProductNames = new List<String>();
        for(Product2 prod : trigger.new){
            ProductNames.add(prod.Name);
        }
        
        try{
            for(product2 p : [SELECT Name,Version__c FROM Product2 WHERE Name=:ProductNames]){
                if(!ProductLastVersion.containskey(p.Name) || ProductLastVersion.get(p.Name)<p.Version__c)
                    ProductLastVersion.put(p.Name,p.Version__c);
                if(!ProductToUpdate.containskey(p.Name)) ProductToUpdate.put(p.Name, new list<Product2>());
                list<Product2> listprod = new list<Product2>();
                listprod = ProductToUpdate.get(p.Name);
                listprod.add(p);
                ProductToUpdate.put(p.Name,listprod); 
            }
        }catch(exception e){
            system.debug('Error in existing product retrive: '+ e.getMessage());
        }        
        
        for(product2 newProduct : trigger.new){
            try{
                if(!ProductLastVersion.isEmpty() && ProductLastVersion.containskey(newProduct.Name)) 
                    newProduct.version__c= ProductLastVersion.get(newProduct.Name) + 1;
                else newProduct.version__c=1;
                
                for(product2 Oldprod : ProductToUpdate.get(newProduct.Name)){
                    Oldprod.new_version__c = newProduct.version__c;
                    OldVersions.add(Oldprod);
                }
            }catch(exception e){
                system.debug('Error in version update for Product: '+ newProduct.Name);
            }
            
        }
        
        Database.SaveResult[] srList = Database.update(OldVersions, false);
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                
                System.debug('Successfully updated Product: ' + sr.getId());
            }
            else {
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Product fields that affected this error: ' + err.getFields());
                }
            }
            
        }
        
        
    }
}