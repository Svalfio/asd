trigger CaseTrigger on Case (before insert, before update, after insert, after update) {
    
        try { 
        
            CaseTriggerHandler handler = new CaseTriggerHandler();
            
             if(Trigger.isInsert && Trigger.isBefore) {
            
                handler.OnBeforeInsert(Trigger.new);
            
            }   
            
            if(Trigger.isUpdate && Trigger.isBefore) {
            
                handler.OnBeforeUpdate(Trigger.NewMap,Trigger.OldMap);
            
            }   
            
          
        
            else if(Trigger.isInsert && Trigger.isAfter) {
            
                handler.OnAfterInsert(Trigger.new);
            
            } 
            
        
        }
    
    
    catch(Exception e) {
    
        System.debug('CaseTrigger '+e.getLineNumber()+' '+e.getMessage() +' '+ e.getStackTraceString());
    
    }


}