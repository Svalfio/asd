trigger NotificationSettingTrigger on Notification_Setting__c (before insert, before update, after insert, after update) {
    
        try { 
        
            NotificationSettingTriggerHandler handler = new NotificationSettingTriggerHandler();
            
             if(Trigger.isInsert && Trigger.isBefore) {
            
                handler.checkCountry(Trigger.new);
            
            }   
            
            if(Trigger.isUpdate && Trigger.isBefore) {
            
               
            
            }   
            
          
        
            else if(Trigger.isInsert && Trigger.isAfter) {
            
                
            
            } 
            
        
        }
    
    
    catch(Exception e) {
    
        System.debug('CaseTrigger '+e.getLineNumber()+' '+e.getMessage() +' '+ e.getStackTraceString());
    
    }

}