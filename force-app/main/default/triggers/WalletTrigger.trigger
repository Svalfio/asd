trigger WalletTrigger on Wallet__c (before update, after update) {
    
    try{
        WalletTriggerHandler handler = new WalletTriggerHandler();
        if(trigger.isBefore && trigger.isUpdate){
            handler.OnBeforeUpdate(Trigger.newMap, Trigger.oldMap);
        }

        if(trigger.isAfter && trigger.isUpdate){
            handler.OnAfterUpdate(Trigger.newMap, Trigger.oldMap);
        }

        
        
    }catch(Exception e){
		System.debug('WalletTrigger '+e.getLineNumber()+' '+e.getMessage() +' '+ e.getStackTraceString());
    }
}