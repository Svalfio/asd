trigger TaskTrigger on Task (before insert, before update, after insert, after update) {
    
    try {   
        TaskTriggerHandler handler = new TaskTriggerHandler();
        
        if(Trigger.isUpdate && Trigger.isAfter){
            
            handler.OnAfterUpdate(Trigger.newMap, Trigger.oldMap);
        }      
    } 
    catch(Exception e){
        
        System.debug('TaskTrigger '+e.getLineNumber()+' '+e.getMessage() +' '+ e.getStackTraceString());
        
    }
}