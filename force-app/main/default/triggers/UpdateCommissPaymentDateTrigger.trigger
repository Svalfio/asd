trigger UpdateCommissPaymentDateTrigger on Monthly_Statement__c (before update) {

        try { 
    
        UpdateCommissPaymentDateTriggerHandler handler = new UpdateCommissPaymentDateTriggerHandler();
          
       if(Trigger.isUpdate && Trigger.isBefore)
            handler.OnBeforeUpdate(trigger.OldMap,trigger.NewMap);
    
    }

     catch(Exception e) {
    
        System.debug('UpdatePaymentStatusTrigger '+e.getLineNumber()+' '+e.getMessage() +' '+ e.getStackTraceString());
    
    }
    
    
}