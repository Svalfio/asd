trigger CommissionItemTrigger on Commission_Item__c (before insert,after insert, before update,after update) {
	
   
      try { 
            CommissionItemTriggerHandler handler = new CommissionItemTriggerHandler();
        
            if(Trigger.isInsert && Trigger.isbefore) {
                
                handler.OnbeforeInsert(Trigger.new);
            } 
            
           if(Trigger.isUpdate && Trigger.isbefore) 
        
               handler.onBeforeUpdate(Trigger.oldMap, Trigger.newMap);
        }
    
    catch(Exception e) {
    
        System.debug('CommissionItemTrigger '+e.getLineNumber()+' '+e.getMessage() +' '+ e.getStackTraceString());
    
    }
}