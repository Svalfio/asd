global class Invocable_CreateUserOnIam {
    global with sharing class CreateUserInputs {
        @InvocableVariable(Label = 'accountId' Required = false) global Integer accountId;
        @InvocableVariable(Label = 'username' Required = false) global String username;
        @InvocableVariable(Label = 'mainContactPhone' Required = false) global String mainContactPhone;
        @InvocableVariable(Label = 'mainContactMail' Required = false) global String mainContactMail;
        @InvocableVariable(Label = 'enabled' Required = false) global Boolean enabled;    
        @InvocableVariable(Label = 'Esito' Required = false) global List<String> Esito = new List<String>();
    }
        @InvocableMethod(Label = 'API_OSS_CreateUserOnIam')
	global static List<String> makePostCallout(CreateUserInputs[] requestActionInList) 
    {
    	requestActionInList[0].Esito.add('KO');
    	return requestActionInList[0].Esito;   
    }
}