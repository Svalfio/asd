@isTest
public class Test_WalletTransactionTriggerHandler {
    
    @testSetup
    static void createData(){
        
        AllMySmsCustomSetting__c testCustomMySms = new AllMySmsCustomSetting__c(Name='AllMySms',Endpoint__c='urlFalse');
        insert testCustomMySms;
        
        Wallet__c testWallet = new Wallet__c(Amount__c=199);
        insert testWallet;
        
    }

    
    @isTest
    static void onBeforeInsert(){
        
        Wallet__c testWallet = [Select id from Wallet__c limit 1];
               
        Wallet_Transaction__c testWT = new Wallet_Transaction__c();
        testWT.Wallet__c = testWallet.Id;
        testWT.Transaction_Category__c = 'Top-up';
        testWT.Amount__c = 200;
        testWT.Payment_Method__c = 'Cash';
        testWT.Sub_Category__c = 'Standard Top-up';
        testWT.Description__c = 'test';
        
        test.startTest();
        insert testWT;
        test.stopTest();
        
    }
}