@isTest
public class AccountActivationCode_Ctrl_test {
    
    @testSetup static void createData() {

        Id DistributorRTId =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_DISTRIBUTOR).getRecordTypeId();
		account Distributor = new Account(Name='Distributor1',RecordTypeId=DistributorRTId);
        insert Distributor;
                
        List<zqu__Quote__c> testQuoteList = new List<zqu__Quote__c>();
        integer i=0;
        do{
            zqu__Quote__c quot = new zqu__Quote__c(Name ='TestQuote'+1,zqu__Account__c =Distributor.id);
            testQuoteList.add(quot);
            i++;
        }while(i<10);
        
        insert testQuoteList;
        system.debug('testQuoteList: '+testQuoteList);
    }
    
    @isTest
    public static void getAccountId_Test(){
        List<zqu__Quote__c> QuoteList = new List<zqu__Quote__c>();
        id QuoteId =null;
        QuoteList = [Select id, zqu__Account__c from zqu__Quote__c];
        if(QuoteList.size()>0) QuoteId=QuoteList[0].id;
        AccountActivationCode_Controller.getAccountId(QuoteId);
    }

}