global class SfToHerokuAPIManager {
    
    global class CustomerAccountCreationRequest{
        @TestVisible public String externalId;
        @TestVisible public Integer serviceProviderId;
        @TestVisible public Integer partnerId;
        @TestVisible public Integer subPartnerId;
        @TestVisible public String description;
        @TestVisible public List<PhoneInfo> phoneList;
        @TestVisible public List<EmailInfo> emailList;
        @TestVisible public Integer categoryId;
    }
    global class CustomerAccountCreationResponse{
        @TestVisible public String externalId;
        @TestVisible public Integer serviceProviderId;
        @TestVisible public Integer partnerId;
        @TestVisible public Integer subPartnerId;
        @TestVisible public String description;
        @TestVisible public List<PhoneInfo> phoneList;
        @TestVisible public List<EmailInfo> emailList;
        @TestVisible public Integer categoryId;
        @TestVisible public Integer id;
    }
    global class PhoneInfo{
        @TestVisible public String phone;
        @TestVisible public String category;
        @TestVisible public String phoneType;
    }
    global class EmailInfo{
        @TestVisible public String address;
        @TestVisible public String category;
    }
    global class TechnicalAccountCreationRequest{
        @TestVisible public TechnicalAccountInfo account;
        @TestVisible public LocationInfo location;
        @TestVisible public List<Integer> optionItemsIds;
    }
    global class TechnicalAccountInfo{
        @TestVisible public Integer subPartnerId;
        @TestVisible public Integer customerId;
        @TestVisible public Integer techServiceId;
        @TestVisible public Integer satelliteId;
        @TestVisible public Integer technologyId;
        @TestVisible public Integer productId;
        @TestVisible public String name;
    }
    global class sendSingleNotificationRequest{
        @TestVisible public String technicalAccountId;
        @TestVisible public String accountId;
        @TestVisible public String contactId;
        @TestVisible public String eventCode;
        @TestVisible public String channel;
        @TestVisible public Map<String, String> parametersMap;
    }
    global class TechnicalServiceCreationRequest{
        @TestVisible public String name;
        @TestVisible public Integer partnerId;
        @TestVisible public List<Integer> componentIds;
    }
    global class TechnicalServiceEditRequest{
        @TestVisible public Integer techServiceId;
        @TestVisible public String name;
        @TestVisible public String status;
    }
    global class CycleDates{
        @TestVisible public Date dateFrom;
        @TestVisible public Date dateTo;
    }
    global class TechnicalAccountChangeItemRequest{
        @TestVisible public Integer techAccountId;
        @TestVisible public Integer planId;
        @TestVisible public String subnetIpOrIp;
        @TestVisible public Boolean resetCycle;
        @TestVisible public Boolean resetCounters;
        @TestVisible public List<CycleDates> cycleDates;
        @TestVisible public List<Integer> components;
    }
    global class TechnicalAccountAssignAddonRequest{
        @TestVisible public Integer techAccountId;
        @TestVisible public Integer addonId;
        @TestVisible public Integer addonTypeId;
        @TestVisible public Integer quantity;
    }
    global class HerokuResponse{
        @TestVisible public Integer errorCode;
        @TestVisible public String message;
        @TestVisible public Object data;
    }
    global class TechnicalAccountCreationResponse{
        @TestVisible public Integer id;
        @TestVisible public String activationCode;
        @TestVisible public String name;
        @TestVisible public String status;
    }
    global class SatelliteDetails{
        @TestVisible public Integer id;
        @TestVisible public String name;
        @TestVisible public String shortName;
        @TestVisible public String position;
        @TestVisible public Double azimuth;
        @TestVisible public Double elevation;
    }
	global class TechnologyServiceDetail{
        @TestVisible public String technology;
        @TestVisible public String globalService;
    }
    global class BeamPolylineDetails{
        @TestVisible public Integer id;
        @TestVisible public Decimal downlinkSNR;
        @TestVisible public Integer antennaSize;
    }
    global class BeamDetails{
        @TestVisible public Integer id;
        @TestVisible public String name;
        @TestVisible public Double latitude;
        @TestVisible public Double longitude;
        @TestVisible public Double maxRadius;
        @TestVisible public Integer beamStatusId;
        @TestVisible public Integer recommendedAntennaSize;
        @TestVisible public SatelliteDetails satellite;
        @TestVisible public List<TechnologyServiceDetail> technologyServices;
        @TestVisible public List<BeamPolylineDetails> polylines;
    }
    global class ServiceCoverageResponse{        
        @TestVisible public List<BeamDetails> beams;
    }
    global class CustomerUserCreationRequest{
        @TestVisible public Integer accountId;
        @TestVisible public String username;
        @TestVisible public String mainContactPhone;
        @TestVisible public String mainContactMail;
        @TestVisible public boolean enabled;
    }
    global class CustomerUserCreationResponse{
        @TestVisible public Integer id;
        @TestVisible public Integer accountId;
        @TestVisible public String username;
        @TestVisible public String mainContactPhone;
        @TestVisible public String mainContactMail;
        @TestVisible public boolean enabled;
        @TestVisible public String generatedPassword;
    }
    global class TechAccountInfo{
        @TestVisible public String status;
        @TestVisible public Integer customerId;
        @TestVisible public Integer productId;
        @TestVisible public Integer techServiceId;
        @TestVisible public Datetime creationDate;
        @TestVisible public String activationCode;
        @TestVisible public String name;
        @TestVisible public LocationInfo location;
        @TestVisible public Integer technologyId;
        @TestVisible public Integer id;
    }
    global class LocationInfo{
        @TestVisible public String country;
        @TestVisible public Integer latitude;
        @TestVisible public Integer longitude;
        @TestVisible public String timezone;
        @TestVisible public List<CoverageInfo> coverage;
    }
    global class CoverageInfo{
        @TestVisible public Integer satelliteId;
        @TestVisible public Integer beamId;
    }
    global class IpSubnetInfo{
        @TestVisible public String ipv4Address;
        @TestVisible public Integer ipv4Cidr;
        @TestVisible public String 	ipv6Address;
        @TestVisible public Integer ipv6Cidr;
    }
    global class RetrieveTechnicalAccountResponse{
        @TestVisible public TechAccountInfo account;
        @TestVisible public LocationInfo location;
        @TestVisible public IpSubnetInfo ipSubnet;
        @TestVisible public List<Integer> optionItemIds;
        @TestVisible public List<Integer> planItemIds;
    }
    global class PreActivationCheckRequest{
        @TestVisible public Integer techServiceId;
        @TestVisible public String ipv4;
        @TestVisible public Integer antennaSize;
        @TestVisible public List<String> hwTypeList;
    }
    global class PreActivationCheckResponse{
        @TestVisible public boolean valid;
        @TestVisible public Integer downlink;
        @TestVisible public Integer uplink;
        @TestVisible public boolean newTerminal;
    }
    global class TechnicalServiceInfo{
        @TestVisible public Integer id;
        @TestVisible public String name;
        @TestVisible public Integer partnerId;
        @TestVisible public List<Integer> componentIds;
        @TestVisible public String status;
    }
    global class TechnicalServiceResponse{
        @TestVisible public TechnicalServiceInfo technicalService;
    }
    global class TerminalInfo{
        @TestVisible public String physicalId;
        @TestVisible public Integer technologyId;
        @TestVisible public String status;
        @TestVisible public String owner;
        @TestVisible public Double latitude;
        @TestVisible public Double longitude;
        @TestVisible public String partnerIdLock;
        @TestVisible public String techServiceIdLock;
        @TestVisible public Datetime firstTimeViewed;
        @TestVisible public Integer antennaSize;
    }
    global class GetTerminalDetailsResponse{
        @TestVisible public TerminalInfo terminalDetail;
    }
    
    private static HerokuResponse callOut(String request, String url, String reqType){
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
                
        req.setEndpoint(url); 
        req.setTimeout(60000);
        req.setMethod(reqType);
        
        if(!String.isEmpty(request)){
            req.setHeader('Content-type', 'application/json');
            req.setBody(request);
        }
        
        HttpResponse resHttp = h.send(req);
        
        Map<String,Object> jsonMap = (Map<String,Object>)JSON.deserializeUntyped(resHttp.getBody());
        HerokuResponse resp = new HerokuResponse();
        resp.errorCode = (Integer)jsonMap.get('errorCode');
        resp.message = (String)jsonMap.get('message');
        resp.data = jsonMap.get('data');
        return resp;
    }
    
    @RemoteAction
    global static HerokuResponse createCustomerAccount(CustomerAccountCreationRequest request){
        
        SfToHK_Urls__c urls = SfToHK_Urls__c.getValues(UserInfo.getOrganizationId());
        
        String url = urls.Base_Url__c + urls.Create_Customer_Account__c; 
        
        HerokuResponse resp = callOut(JSON.serialize(request), url, 'POST');
        
        if(resp.errorCode==201){
        	resp.data = Json.deserialize((String)resp.data, CustomerAccountCreationResponse.class);
        }
        
        return resp;
    }
    
    @RemoteAction
    global static HerokuResponse createTechnicalAccount(TechnicalAccountCreationRequest request){
        
        SfToHK_Urls__c urls = SfToHK_Urls__c.getValues(UserInfo.getOrganizationId());
        
        String url = urls.Base_Url__c + urls.Create_Tech_Account__c;
        
        HerokuResponse resp = callOut(JSON.serialize(request), url, 'POST');
        
        if(resp.errorCode==201){
            resp.data = Json.deserialize((String) resp.data, TechnicalAccountCreationResponse.class);
        }
        
        return resp;
    }
    
    @RemoteAction
    global static HerokuResponse serviceCoverageCheck(Double latitude, Double longitude, String country, String globalService, Integer preferenceBeamId){
        
        SfToHK_Urls__c urls = SfToHK_Urls__c.getValues(UserInfo.getOrganizationId());
        
        String url = urls.Base_Url__c + urls.Service_Coverage_Check__c+'?latitude='+latitude
                        											  +'&longitude='+longitude
                        											  +(String.isNotBlank(country) ? '&country='+country : '')
                        											  +(String.isNotBlank(country) ? '&globalService='+globalService : '')
                        											  +(String.isNotBlank(country) ? '&preferenceBeamId='+preferenceBeamId : '');
        
        HerokuResponse resp = callOut(null, url, 'GET');
        
        if(resp.errorCode==200){
            resp.data = Json.deserialize((String)resp.data, ServiceCoverageResponse.class);
        }
        
        return resp;
    }
    
    @RemoteAction
    global static HerokuResponse createCustomerUser(CustomerUserCreationRequest request){
        
        SfToHK_Urls__c urls = SfToHK_Urls__c.getValues(UserInfo.getOrganizationId());
        
        String url = urls.Base_Url__c + urls.Create_Customer_User__c;
        
        HerokuResponse resp = callOut(JSON.serialize(request), url, 'POST');
        
        if(resp.errorCode==201){
            resp.data = Json.deserialize((String) resp.data, CustomerUserCreationResponse.class);
        }
        
        return resp;
    }
    
    @RemoteAction
    global static HerokuResponse retrieveTechAccount(Integer techAccountId){
        
        SfToHK_Urls__c urls = SfToHK_Urls__c.getValues(UserInfo.getOrganizationId());
        
        String url = urls.Base_Url__c + urls.Create_Tech_Account__c+'/'+techAccountId;
        
        HerokuResponse resp = callOut(null, url, 'GET');
        
        if(resp.errorCode==200){
            resp.data = Json.deserialize((String) resp.data, RetrieveTechnicalAccountResponse.class);
        }
        
        return resp;
    }
    
    @RemoteAction
    global static HerokuResponse sendSingleNotification(sendSingleNotificationRequest request){
        
        SfToHK_Urls__c urls = SfToHK_Urls__c.getValues(UserInfo.getOrganizationId());
        
        String url = urls.Base_Url__c + urls.Send_Single_Notification__c; 
        
        return callOut(JSON.serialize(request), url, 'POST');
    }
    
    @RemoteAction
    global static HerokuResponse preActivationCheck(Integer techAccountId, PreActivationCheckRequest request){
        
        SfToHK_Urls__c urls = SfToHK_Urls__c.getValues(UserInfo.getOrganizationId());
        
        String url = urls.Base_Url__c + urls.Create_Tech_Account__c+'/'+techAccountId+'/pre-activation-check';
        
        HerokuResponse resp = callOut(JSON.serialize(request), url, 'POST');
        
        if(resp.errorCode==200){
        	resp.data = Json.deserialize((String) resp.data, PreActivationCheckResponse.class);
        }
        
        return resp;
    }
    
    @RemoteAction
    global static HerokuResponse cancellationTechAccount(Integer techAccountId){
        
        SfToHK_Urls__c urls = SfToHK_Urls__c.getValues(UserInfo.getOrganizationId());
        
        String url = urls.Base_Url__c + urls.Create_Tech_Account__c+'/'+techAccountId; 
        
        return callOut(null, url, 'DELETE');
    }
    
    @RemoteAction
    global static HerokuResponse technicalServiceCreation(TechnicalServiceCreationRequest request){
        
        SfToHK_Urls__c urls = SfToHK_Urls__c.getValues(UserInfo.getOrganizationId());
        
        String url = urls.Base_Url__c + urls.Tech_Service_Creation__c;
        
        HerokuResponse resp = callOut(JSON.serialize(request), url, 'POST');
        
        if(resp.errorCode==201){
        	resp.data = Json.deserialize((String) resp.data, TechnicalServiceResponse.class);
        }
        
        return resp;
    }
    
    @RemoteAction
    global static HerokuResponse technicalServiceEdit(Integer techServiceId, TechnicalServiceEditRequest request){
        
        SfToHK_Urls__c urls = SfToHK_Urls__c.getValues(UserInfo.getOrganizationId());
        
        String url = urls.Base_Url__c + urls.Tech_Service_Creation__c+'/';
        
        HerokuResponse resp = callOut(JSON.serialize(request), url, 'PATCH');
        
        if(resp.errorCode==200){
        	resp.data = Json.deserialize((String) resp.data, TechnicalServiceResponse.class);
        }
        
        return resp;
    }
    
    @RemoteAction
    global static HerokuResponse retrieveTechService(Integer techServiceId){
        
        SfToHK_Urls__c urls = SfToHK_Urls__c.getValues(UserInfo.getOrganizationId());
        
        String url = urls.Base_Url__c + urls.Tech_Service_Creation__c+'/'+techServiceId; 
        
        HerokuResponse resp = callOut(null, url, 'GET');
        
        if(resp.errorCode==200){
        	resp.data = Json.deserialize((String) resp.data, TechnicalServiceResponse.class);
        }
        
        return resp;
    }
    
    @RemoteAction
    global static HerokuResponse technicalAccountChangeItem(TechnicalAccountChangeItemRequest request){
        
        SfToHK_Urls__c urls = SfToHK_Urls__c.getValues(UserInfo.getOrganizationId());
        
        String url = urls.Base_Url__c + urls.Tech_Account_Change_Item__c;
        
        return callOut(JSON.serialize(request), url, 'POST');
    }
    
    @RemoteAction
    global static HerokuResponse technicalAccountAssignAddon(TechnicalAccountAssignAddonRequest request){
        
        SfToHK_Urls__c urls = SfToHK_Urls__c.getValues(UserInfo.getOrganizationId());
        
        String url = urls.Base_Url__c + urls.Tech_Account_Assign_Addon__c;
        
        return callOut(JSON.serialize(request), url, 'POST');
    }
    @RemoteAction
    global static HerokuResponse getTerminalDetails(String terminalId){
        
        SfToHK_Urls__c urls = SfToHK_Urls__c.getValues(UserInfo.getOrganizationId());
        
        String url = urls.Base_Url__c + urls.Get_Terminal_Details__c +'/'+terminalId;
        
        HerokuResponse resp = callOut(null, url, 'GET');
        
        if(resp.errorCode==200){
        	resp.data = Json.deserialize((String) resp.data, GetTerminalDetailsResponse.class);
        }
        
        return resp;
    }
}