@IsTest
public class Test_MyInvoicesListController {
    
    @testSetup
    public static void setup(){
		Id crcAgentId = [SELECT Id FROM Profile WHERE Name = 'CRC Agent' LIMIT 1].id;
        Id RecordTypeQuoteId = Schema.SObjectType.zqu__Quote__c.getRecordTypeInfosByDeveloperName().get('Order_Validated').getRecordTypeId();
        User user_test = new User(LastName = 'Sub1', alias = 'sub1', Email= 'sureseller1@gmail.com', Username = 'sureseller1@gmail.com', TimeZoneSidKey='Europe/Rome'	,LocaleSidKey = 'en_US', EmailEncodingKey='ISO-8859-1'	,ProfileId=crcAgentId 	 ,LanguageLocaleKey = 'en_US' );
    	insert user_test;
        zqu__Quote__c quote = new zqu__Quote__c(OwnerId = user_test.Id, RecordTypeId = RecordTypeQuoteId, zqu__ZuoraSubscriptionID__c = 'test', Renew_Settings__c ='Data full consumption', Number_of_Cycles__c = '5');
        insert quote;
    }
	
    @IsTest
    public static void Test_DownloadInvoice(){
        Test.startTest();
        MyInvoicesListController.DownloadInvoice('Test invoice');
        Test.stopTest();
    }
}