global class GuidedSellingFlowPluginImpl implements zqu.SelectProductComponentOptions.IGuidedSellingFlowPlugin{   
   public List<ID> getFlows(Id quoteId){
      List < Id > flowIds = new List < Id >();

      if(String.isBlank(quoteId)) return flowIds;

      // Retrieve the quote based on Id
      List < zqu__Quote__c > quoteList = [SELECT Name, zqu__SubscriptionType__c,zqu__Account__r.Billing__c FROM zqu__Quote__c WHERE Id = :quoteId];
      if(quoteList.isEmpty()) return flowIds;
      
      // Determine the string to query flows based on the subscription type of the quote
      //String flowName = quoteList[0].zqu__SubscriptionType__c == 'New Subscription' ? 'New %' : '%grade Order';
      
      List<String> flowNames = new List<String>();
      String subType = quoteList[0].zqu__SubscriptionType__c;
      String accBillingType = quoteList[0].zqu__Account__r.Billing__c;
      if (subType == 'New Subscription'){ 
             if(accBillingType=='Retail'){
              flowNames.add('New Order'); 
              //flowNames.add('Equipment Selection'); 
          }else if(accBillingType=='Wholesale'){
              flowNames.add('New Order Wholesale');
          }
      }
      if (subType == 'Renew Subscription'){ 
        if(accBillingType=='Retail'){
            flowNames.add('Renew Order'); 
        }
      }
      if (subType == 'Amend Subscription'){ 
        if (accBillingType == 'Retail'){
            flowNames.add('Upgrade Order'); 
            flowNames.add('Downgrade Order');
            flowNames.add('Manage Options / Add-On');
            //flowNames.add('Equipment Selection'); modificato da gdalberto 02/12/2019
           	//flowNames.add('Change Subscription Wholesale');
        }
        else if (accBillingType == 'Wholesale'){
            flowNames.add('Upgrade Order Wholesale');
            flowNames.add('Downgrade Order Wholesale');
            flowNames.add('Manage Options / Add-On Wholesale');
          }
      }
      

      // Query for flows based on the name and IncludeInProductSelector flag
      List < zqu__GuidedSellingFlow__c > flows = [SELECT Id FROM zqu__GuidedSellingFlow__c WHERE zqu__IncludeInProductSelector__c = true AND (Name IN :flowNames) ORDER BY CreatedDate];
      if(flows.isEmpty()) return flowIds; 

      // If flows were found, add their ids to the result list
      for(zqu__GuidedSellingFlow__c flow : flows) {
         flowIds.add(flow.Id);
      }
      return flowIds;
    }
 }