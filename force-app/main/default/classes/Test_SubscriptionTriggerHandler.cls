@isTest
public class Test_SubscriptionTriggerHandler {
    
   @testSetup 
    static void setup() {
        
        Id resellerRTId=  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_RESELLER).getRecordTypeId();
        Account reseller =  new Account(Name = 'Reseller',RecordTypeId=resellerRTId);    
        insert reseller;
        
        List<Zuora__Subscription__c> subscriptionList= new List<Zuora__Subscription__c>();
        Zuora__Subscription__c testSubscription = new Zuora__Subscription__c(Name='Subscription1',wallet_mode__c='PostPaid',Zuora__Account__c = reseller.id);
        Zuora__Subscription__c testSubscription2 = new Zuora__Subscription__c(Name='Subscription2',Zuora__Account__c = reseller.id);
        subscriptionList.add(testSubscription);
        subscriptionList.add(testSubscription2);
        
        insert subscriptionList;
        
        Id idRTRes = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Residential').getRecordTypeId();
        Account residential =  new Account(Name = 'Residential',RecordTypeId=idRTRes);    
        insert residential;
        
        Contact contact = new Contact(LastName = 'Sempronio',AccountId=reseller.id);
        insert contact;

        zqu__Quote__c qt = new zqu__Quote__c(zqu__Account__c = residential.Id,zqu__ZuoraSubscriptionID__c = 'test'); 
        insert qt;
        
        
    }
    
    @isTest
    static void onAfterInsertTest(){
        List<Zuora__Subscription__c> subscriptionList= new List<Zuora__Subscription__c>();
        Zuora__Subscription__c testSubscription = new Zuora__Subscription__c(Name='testSubscription',wallet_mode__c='PostPaid');
        Zuora__Subscription__c testSubscription2 = new Zuora__Subscription__c(Name='testSubscription2');
        subscriptionList.add(testSubscription);
        subscriptionList.add(testSubscription2);
        
        Test.startTest();
        insert subscriptionList;
        Test.stopTest();
          
    }
    
    
    @isTest
    static void onBeforeUpdateTest(){
        System.debug('onBeforeUpdateTest');
        List<Zuora__Subscription__c> subUpdateList = new List<Zuora__Subscription__c>();
        Zuora__Subscription__c sub1 = [SELECT id,wallet_mode__c FROM Zuora__Subscription__c WHERE Name='Subscription1'  LIMIT 1];
        Zuora__Subscription__c sub2 = [SELECT id,wallet_mode__c FROM Zuora__Subscription__c WHERE Name='Subscription2'  LIMIT 1];
        
        sub1.wallet_mode__c='PrePaid';
        sub2.wallet_mode__c='PostPaid';
        
        subUpdateList.add(sub1);
        subUpdateList.add(sub2);
        
        Test.startTest();
        update subUpdateList;
        Test.stopTest();
          
    }
    
    @isTest
    static void onAfterInsertTest2(){
        
        zqu__Quote__c qt = [SELECT Id,zqu__Number__c FROM zqu__Quote__c LIMIT 1]; 
        
        List<Zuora__Subscription__c> subscriptionList= new List<Zuora__Subscription__c>();
        Zuora__Subscription__c testSubscription = new Zuora__Subscription__c(Name='testSubscription',wallet_mode__c='PostPaid',Zuora__QuoteNumber__c = qt.zqu__Number__c);
        subscriptionList.add(testSubscription);
        
        Test.startTest();
		insert subscriptionList;
        Test.stopTest();
          
    }
    
    @isTest
    static void beforeinsert(){
        
        zqu__Quote__c qt = [SELECT Id,zqu__Number__c FROM zqu__Quote__c LIMIT 1]; 
        
        List<Zuora__Subscription__c> subscriptionList= new List<Zuora__Subscription__c>();
        Zuora__Subscription__c testSubscription = new Zuora__Subscription__c(Name='testSubscription',wallet_mode__c='PostPaid',Zuora__QuoteNumber__c = qt.zqu__Number__c,Zuora__External_Id__c = 'test');
        subscriptionList.add(testSubscription);
        
        Test.startTest();
		insert subscriptionList;
        Test.stopTest();
          
    }
    
    
    @isTest
    static void onBeforeUpdateTest2(){
      	
        List<Wallet__c> wList = [SELECT Id FROM Wallet__c ];
        for(Wallet__c w : wList){
            w.Account__c = null; 
        }
        
        update wList;
        
        List<Zuora__Subscription__c> subUpdateList = new List<Zuora__Subscription__c>();
        Zuora__Subscription__c sub1 = [SELECT id,wallet_mode__c FROM Zuora__Subscription__c WHERE Name='Subscription1'  LIMIT 1];
        Zuora__Subscription__c sub2 = [SELECT id,wallet_mode__c FROM Zuora__Subscription__c WHERE Name='Subscription2'  LIMIT 1];
        
        sub1.wallet_mode__c='PrePaid';
        sub2.wallet_mode__c='PostPaid';
        
        subUpdateList.add(sub1);
        subUpdateList.add(sub2);
        
        Test.startTest();
        update subUpdateList;
        Test.stopTest();
        
    }

}