public class TaskTriggerHandler {
    
	public void OnAfterUpdate( Map<Id,Task> triggerNewMap, Map<Id,Task> triggerOldMap){
        
        System.debug('*** START OnAfterUpdate in TaskTriggerHandler ***');
        
        //StaticResource sr = [select Id,Name,LastModifiedDate from StaticResource where Name = 'Wallet_Recharge_Invoice' LIMIT 1];
        
        Set<String> wallIdCmp = new Set<String>();
        Set<String> caseIdCmp = new Set<String>();
        Map<String,Double> mapWalletNewCurr = new Map<String,Double>();
        
        for(Id taskId : triggerNewMap.KeySet()){
            Task newTask = triggerNewMap.get(taskId);
            if(triggerOldMap.containsKey(taskId)){
                Task oldTask = triggerOldMap.get(taskId);          
                if(newTask.Status == 'Completed' && oldTask.Status != 'Completed' && newTask.Type == 'Manage Recharge Wallet'){
                    wallIdCmp.add(newTask.WhatId);
                    caseIdCmp.add(newTask.Case__c);
                }
            }    
        }
        
        List<Case> caseLst = [SELECT Id,Status,Wallet__c,Recharge_Amount__c FROM Case WHERE Id IN : caseIdCmp];
        List<Case> caseLstComp = new List<Case>();
        for(Case caseToUpd : caseLst){
            caseToUpd.Status = 'Completed';
            caseLstComp.add(caseToUpd);
            mapWalletNewCurr.put(caseToUpd.Wallet__c,caseToUpd.Recharge_Amount__c);
        }

       List<Wallet__c> wallList = [SELECT Id,Amount__c,Account__c FROM Wallet__c WHERE Id IN :wallIdCmp];
       List<Wallet__c> wallToUpd = New List<Wallet__c>();
       List<Wallet_Transaction__c> wlTransToInsert = new List<Wallet_Transaction__c>();
       
        for(Wallet__c w : wallList){
            if(mapWalletNewCurr.containsKey(w.Id)){
                w.Amount__c += mapWalletNewCurr.get(w.Id);
                wallToUpd.add(w);
                
                Wallet_Transaction__c wallTrans = new Wallet_Transaction__c();
                wallTrans.Wallet__c = w.Id;
                wallTrans.Transaction_Category__c = 'Recharge';
                wallTrans.Date__c = DateTime.now();
                wallTrans.Payment_Method__c = 'Cash';
                wallTrans.Sub_Category__c = 'Standard_Recharge';
                //wallTrans.Owner__c = w.Account__c;
                wallTrans.Amount__c = mapWalletNewCurr.get(w.Id);
                //wallTrans.Recharge_Document_ID__c = '/resource/'+sr.LastModifiedDate.getTime()+'/'+sr.Name ;
				wlTransToInsert.add(wallTrans);
            }
        }
        
        if(caseLstComp.size() > 0)
            update caseLstComp;
        
        if(wallToUpd.size() > 0)
            update wallToUpd;
        
        if(wlTransToInsert.size() > 0)
            insert wlTransToInsert;
    }
    
}