global with sharing class MockRedirectToInvoice implements HttpCalloutMock {
	global HttpResponse respond(HttpRequest req){
		HttpResponse res = new HttpResponse();
		String accessToken = '{\"access_token\": \"test\", \"invoiceFiles\": [{\"pdfFileUrl\": \"test url\"}]}';
		res.setHeader('content-Type', 'application/JSON');
		res.setBody(accessToken);
		return res;
}
}