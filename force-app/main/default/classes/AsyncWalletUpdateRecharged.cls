public class AsyncWalletUpdateRecharged implements Queueable, Database.AllowsCallouts{
    
	private Set<Id> pcIdResponse;
    
    public AsyncWalletUpdaterecharged(Set<Id> WalletIds){
        this.pcIdResponse=WalletIds;
    }
    
    public void execute(QueueableContext cntx) {
        
       	List<Wallet__c> wallets = new List<Wallet__c>();
        List<String> subreseller_Ids = new List<String>();
        List<String> subreseller_account_Ids = new List<String>();
        List<User> subresellers = new List<User>();
        List<Order> subreseller_orders = new List<Order>();
        List<String> notification_sSubResIds = new List<String>();
        List<Notification_Item__c> notificationsSubRes = new List<Notification_Item__c>();
        Map<Id, User> subreseller_map = new Map<Id, User>();
        Map<Id, List<Order>> subreseller_order_map = new Map<Id, List<Order>>();
        Map<Id, Notification_Item__c> notificationSubRes_map = new Map<Id, Notification_Item__c>();
        
        List<FeedItem> feeds = new List<FeedItem>();
        
        Id communityId = [Select id from Network where Name='Eutelsat Partner Community'].id;
        
        wallets = [SELECT id, Name, Amount__c, Account__c, LastModifiedBy.Name FROM Wallet__c WHERE Id IN :pcIdResponse];
        
        for(Wallet__c w: wallets){
            subreseller_Ids.add(w.Account__c);
        }
        
        subresellers = [SELECT id, Name, Account.Notifications_Web__c, Account.Notifications_Email__c, Account.Notifications_SMS__c,
                        AccountId, Account.Notification_Setting__c, Email FROM User WHERE AccountId IN:subreseller_Ids];
        
        for(User u: subresellers){
            if(!subreseller_map.containsKey(u.AccountId)){
            	subreseller_map.put(u.AccountId, u);
                system.debug('ID User '+ u.Id);
            }
            subreseller_account_Ids.add(u.AccountId);
            notification_sSubResIds.add(u.Account.Notification_Setting__c);
        }
        
        notificationsSubRes = [SELECT id, Notification_Setting__c, Event_Type__r.Event_Code__c, Event_Type__r.Name FROM Notification_Item__c WHERE Notification_Setting__c IN :notification_sSubResIds];
        
        for(Notification_Item__c n: notificationsSubRes){
            system.debug(n.Event_Type__r.Name.split('_0')[1]);
            if(!notificationSubRes_map.containsKey(n.Notification_Setting__c+'_0'+ n.Event_Type__r.Name.split('_0')[1])){
                system.debug('test');
                notificationSubRes_map.put(n.Notification_Setting__c+'_0'+ n.Event_Type__r.Name.split('_0')[1], n);
            }
        }  
        
        //Order con status Pending
       	
        subreseller_orders = [SELECT id, Name, AccountId, Status FROM Order WHERE AccountId IN :subreseller_account_Ids AND Status = 'Pending'];
        
        for(Order o: subreseller_orders){
            if(!subreseller_order_map.containsKey(o.AccountId)){
            	subreseller_order_map.put(o.AccountId,new List<Order>{o});
            }else{
                subreseller_order_map.get(o.AccountId).add(o);
            }
        }
        
        AllMySMSPartner__c mc = AllMySMSPartner__c.getInstance('SMSPartner');
        String endpoint = mc.endpoint__c;
        if(endpoint==null)
            System.debug('Endpoint vuota');
        
        for(Wallet__c w: wallets){
            //gestione notifiche
            //
           if(subreseller_map.get(w.Account__c).Account.Notifications_SMS__c == true){
                if(subreseller_map.get(w.Account__c).Account.Notification_Setting__c != null){
                    if(notificationSubRes_map.get(subreseller_map.get(w.Account__c).Account.Notification_Setting__c+'_004').Event_Type__r.Event_Code__c != null){
                        Map<String, String> parametersMap = new Map<String, String>{'subject' => String.valueOf(w.Name), 'comments' => String.valueOf(w.Amount__c)};
                        SfToHerokuAPIManager.sendSingleNotificationRequest richiesta = new SfToHerokuAPIManager.sendSingleNotificationRequest();
                        richiesta.accountId = String.valueOf(w.Account__c);
                        richiesta.channel = 'SMS';
                        richiesta.eventCode = notificationSubRes_map.get(subreseller_map.get(w.Account__c).Account.Notification_Setting__c+'_004').Event_Type__r.Event_Code__c;
                        richiesta.parametersMap = parametersMap;
                    
                        SFtoHerokuAPIManager.HerokuResponse response = SfToHerokuAPIManager.sendSingleNotification(richiesta);
                        System.debug(response.errorCode);
                    }
                }
            }
            
            
            for(id key: subreseller_map.keySet()){
                system.debug(key);
			}
            
            if(subreseller_map.get(w.Account__c).Account.Notifications_Web__c == true){
                FeedItem feedSubRes = new FeedItem();
                feedSubRes.Title = 'Your walles has been recharged';
                feedSubRes.Body = 'Your wallet '+ w.Name+ ' has been recharged by '+w.LastModifiedBy.Name+'. New amount: '+ w.Amount__c;
                system.debug('Your wallet '+ w.Name+ ' has been recharged by '+w.LastModifiedBy.Name+'. New amount: '+ w.Amount__c);
                feedSubRes.ParentId = subreseller_map.get(w.Account__c).Id;
                feedSubRes.NetworkScope=communityId;
                feeds.add(feedSubRes);
            }
            
            if(subreseller_map.get(w.Account__c).Account.Notifications_Email__c == true){
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.toAddresses = new String[] {subreseller_map.get(w.Account__c).Email};
                message.subject = 'Your walles has been recharged';
                message.plainTextBody =  'Your wallet '+ w.Name+ ' has been recharged by '+w.LastModifiedBy.Name+'. New amount: ' + w.Amount__c;
                Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                
                if (results[0].success) {
                    System.debug('The email to subreseller was sent successfully.');
                } else {
                    System.debug('The email to subreseller failed to send: ' + results[0].errors[0].message);
                }
            }
            
            //notifiche se ci sono ordini in pending
            
            if(subreseller_order_map.get(subreseller_map.get(w.Account__c).Account.Id) == null){
                system.debug('NULLO');
            }

            /*if(subreseller_map.get(w.Account__c).Account.Marketing_Opt_in_Portal__c == true && subreseller_order_map.get(subreseller_map.get(w.Account__c).Account.Id) != null){
                FeedItem feedSubRes = new FeedItem();
                feedSubRes.Title = 'You can proceed with order(s)';
                feedSubRes.Body = 'Dear Partner, \nPlease note that you can now proceed with the order(s) which were on hold.';
                system.debug('Dear Partner, \nPlease note that you can now proceed with the order(s) which were on hold.');
                feedSubRes.ParentId = subreseller_map.get(w.Account__c).Id;
                feedSubRes.NetworkScope=communityId;
                feeds.add(feedSubRes);
            }
            
            if(subreseller_map.get(w.Account__c).Account.Marketing_Opt_in_Email__c == true && subreseller_order_map.get(subreseller_map.get(w.Account__c).Account.Id) != null){
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.toAddresses = new String[] {subreseller_map.get(w.Account__c).Email};
                message.subject = 'You can proceed with order(s)';
                message.plainTextBody = 'Dear Partner, \nPlease note that you can now proceed with the order(s) which were on hold.\nKind regards';
                Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                
                if (results[0].success) {
                    System.debug('The email to subreseller was sent successfully.');
                } else {
                    System.debug('The email to subreseller failed to send: ' + results[0].errors[0].message);
                }
            }*/
        }
        if(feeds.size() > 0){
            insert feeds;
        }
    }
}