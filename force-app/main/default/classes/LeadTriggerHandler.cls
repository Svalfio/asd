public class LeadTriggerHandler {
    
    public void OnAfterUpdate(List<Lead> triggerNew){

        List<String> leadIds= new List<String>();
        List<CampaignMember> campaignMembers = new List<CampaignMember>();
        Map<Id,List<CampaignMember>> campaignMemberToLeadMap = new Map<Id,List<CampaignMember>>();
		List<CampaignMember> campaignMembersInsert = new List<CampaignMember>();
        
        for(lead l : triggerNew){
            leadIds.add(l.Id);
        }
        
        campaignMembers = [Select id,name, LeadId, CampaignId from CampaignMember where LeadId IN : leadIds ] ;
        
        for(CampaignMember member : campaignMembers){
            if(!campaignMemberToLeadMap.containsKey(member.LeadId))
                campaignMemberToLeadMap.put(member.LeadId,new List<CampaignMember>{member});
            else
                campaignMemberToLeadMap.get(member.LeadId).add(member);
        }
        
        for(Lead l : triggerNew){

            for(CampaignMember cm : campaignMemberToLeadMap.get(l.Id)){
  
                cm.Opt_out_Email__c = l.Opt_out_Email__c;
                cm.Opt_out_SMS__c = l.Opt_out_SMS__c;
                cm.Opt_out_Phone__c = l.Opt_out_Phone__c;
                cm.Opt_out_Web__c = l.Opt_out_Web__c;
                campaignMembersInsert.add(cm);
            }    
        }
        
        update campaignMembersInsert;
         
    }

}