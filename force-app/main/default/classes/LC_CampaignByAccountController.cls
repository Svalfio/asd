public class LC_CampaignByAccountController {
    
    @AuraEnabled
    public static List<Campaign> getCampaigns(String AccountId){
        List<Campaign> campaigns = new List<Campaign>();
        List<Contact> contacts = new List<Contact>();
        List<String> contactIds = new List<String>();
        List<CampaignMember> campaignMembers = new List<CampaignMember>();
        List<String> campaignIds = new List<String>();
        
        contacts = [Select id From Contact WHERE AccountId =: AccountId];
        for(Contact c: contacts){
            contactIds.add(c.Id);
        }
        
        campaignMembers = [Select id, CampaignId FROM CampaignMember WHERE ContactId IN :contactIds];
        for(CampaignMember cm: campaignMembers){
            campaignIds.add(cm.CampaignId);
        }
        
        campaigns = [SELECT Id, Name, Type, Status, Channel__c, NumberOfResponses, StartDate FROM Campaign WHERE Id IN :campaignIds ORDER BY StartDate];
        
        
        return campaigns;
    }

}