public class CampaignTriggerHandler {

    public  void OnBeforeInsert(List<Campaign> campaignList){
        
        
    }
    
    
    public void onAfterUpdate(Map<Id, Campaign> triggerOld, Map<Id, Campaign> triggerNew){
        
        System.debug('onAfterUpdate----------START');
        
        Set<ID> CampaignIds = new Set<Id>();
        Map<Id,Campaign> phoneCampaignMap = new Map<Id,Campaign>();
        Set<ID> phoneCampaignSet = new Set<Id>();
        
        for(Id key: triggerOld.keySet()){
            System.debug('Old status : '+triggerOld.get(key).Status +' ' + 'New status: ' +triggerNew.get(key).Status);
            if(triggerNew.get(key).Channel__c!=null){
                if((triggerOld.get(key).Status == 'Planned' && triggerNew.get(key).Status == 'In Progress') && ((triggerNew.get(key).Channel__c.contains('SMS') ||triggerNew.get(key).Channel__c.contains('Phone') || triggerNew.get(key).Channel__c.contains('Partner_Portal') || triggerNew.get(key).Channel__c.contains('Customer_Portal')))){
                    CampaignIds.add(key);
                    if(triggerNew.get(key).Channel__c.contains('Phone')){
                        if(!phoneCampaignMap.containsKey(key))
                            phoneCampaignMap.put(key,triggerNew.get(key));
                        system.debug('phoneCampaignMap: '+JSON.serialize(phoneCampaignMap));
                    }       
                    
                }
            }
            
        }
        
		System.debug('phoneCampaignMap : '+JSON.serialize(phoneCampaignMap));
        System.debug('CampaignIds : '+ CampaignIds);
        CampaignTriggerHandler.createPhoneCalendarEvent(phoneCampaignMap);
        	System.enqueueJob(new AsyncCampaignUpdate(CampaignIds));
     
    }    
    
    
    
    
    public static void createPhoneCalendarEvent(Map<Id,Campaign> phoneCampaignMap){
        
        system.debug('createPhoneCalendarEvent-------------START');
        system.debug('phoneCampaignMap: '+JSON.serialize(phoneCampaignMap));
        
        List<CampaignMember> campaignMemberList = [Select id,ContactId,CampaignId,LeadId from CampaignMember where CampaignId In : phoneCampaignMap.keyset()];
        Map<Id,Set<Id>> campaignContactsMap = new Map<Id,Set<Id>>();
        Map<Id,Set<Id>> campaignLeadMap =new Map<Id,Set<Id>>();
        
        
        for(CampaignMember member : campaignMemberList){
            if(!campaignContactsMap.containsKey(member.CampaignId)){
                campaignContactsMap.put(member.CampaignId,new Set<Id>{member.ContactId});
                campaignLeadMap.put(member.CampaignId,new Set<Id>{member.LeadId});
            }
            else{
                campaignContactsMap.get(member.CampaignId).add(member.contactId);
                campaignLeadMap.get(member.CampaignId).add(member.LeadId);
            }
            
        }
        
        system.debug('campaignContactsMap: '+JSON.serialize(campaignContactsMap));
        system.debug('campaignLeadMap: '+JSON.serialize(campaignLeadMap));
        
        
        Id crcCalendar = [SELECT Id FROM Calendar WHERE Name = 'CRC Calendar' LIMIT 1].id;   
        List<Event> phoneEventList = new List<Event>();
        
        for(Campaign c : phoneCampaignMap.values()){
            Integer d = c.StartDate.day();
            Integer mo = c.StartDate.month();
            Integer yr = c.StartDate.year();
            DateTime DTStart = DateTime.newInstance(yr, mo, d);
            d = c.EndDate.day();
            mo = c.EndDate.month();
            yr = c.EndDate.year();
            DateTime DTEnd = DateTime.newInstance(yr, mo, d);
            
            //Gestione dei Contact
            for(Id contactId  : CampaignContactsMap.get(c.id)){
                Event e = new Event();
                e.StartDateTime = DTStart;
                if(DTStart.date().daysBetween(DTEnd.date())<=14){
                    e.EndDateTime = DTEnd;
                }
                else{
                    e.EndDateTime = DTStart.date().addDays(13);
                }
                e.Subject = 'Reminder related to '+c.Name+' Call : ';
                e.WhoId=contactId;
                e.Campaign__c =c.Id;
                e.OwnerId = crcCalendar;
                e.WhatId=c.id;
                phoneEventList.add(e);
            }
            
            //Gestione dei Lead
            for(Id leadId : campaignLeadMap.get(c.id)){
                Event e = new Event();
                e.StartDateTime = DTStart;
                if(DTStart.date().daysBetween(DTEnd.date())<=14){
                    e.EndDateTime = DTEnd;
                }
                else{
                    e.EndDateTime = DTStart.date().addDays(13);
                }
                e.Subject = 'Reminder related to '+c.Name+' Call : ';
                e.WhoId=leadId;
                e.Campaign__c =c.Id;
                e.OwnerId = crcCalendar;
                e.WhatId=c.id;
                phoneEventList.add(e);
            }
            
            if(phoneEventList.size()>0)
                insert phoneEventList;
            else
                system.debug('No Events Created');
            
        } 
    }     

}