public with sharing class VF_ViewPrefilledInvoice {
   
    public LC_PrefilledInvoiceController.AccountWrapper Accwr {get;set;}
    public List<LC_PrefilledInvoiceController.RecordWrapper> ListRecwr {get;set; }
    private String prefilledInvoiceId;
    private String resellerId;
    private String country;
    private String month;
    private String year;

    public VF_ViewPrefilledInvoice() {
        prefilledInvoiceId=ApexPages.currentPage().getParameters().get('prefilledInvoiceId');
        resellerId = prefilledInvoiceId.split('-').get(0);
		country  = prefilledInvoiceId.split('-').get(1);
        month  = prefilledInvoiceId.split('-').get(2);
        year  = prefilledInvoiceId.split('-').get(3);
        Accwr = LC_PrefilledInvoiceController.getAccwr(resellerId,month,year);
        ListRecwr = LC_PrefilledInvoiceController.getListRecwr(resellerId,country,month,year);
    }
}