public class SubscriptionDate_Management {
    
    /*Prevedere la possibilità per usarlo nel Wholesale PostPaid
     * 
    public static void ActivationDateSetup(Set<Id> SubscriptionToManage){
        
        List<Zuora__Subscription__c> SubscriptionToUpdate = new List<Zuora__Subscription__c>();
        Zuora__SubscriptionRatePlan__c MinStartDatePlan;
        List<Id> SubscriptionOwnersId = new List<Id>();
        Map<Zuora__Subscription__c,Zuora__SubscriptionRatePlan__c> Subscription_ActivationInfoPlan = new Map<Zuora__Subscription__c,Zuora__SubscriptionRatePlan__c>();
        Map<Id,List<Database.Error>> Errors_Map = new Map<Id, List<Database.Error>>();
        String ErrorMessage='';
        List<String> QuoteOwner_Username = new List<String>();
        List<Zuora__Subscription__c> subscriptionList = new List<Zuora__Subscription__c>();
        List<zqu__Quote__c> Subscription_QuoteOwner = new List<zqu__Quote__c>();
        Map<Id, Account> SubscriptionId_RelatedAccount_Map = new Map<Id, Account>();
        List<Account> SubscriptionOwnerRelatedAccount = new List<Account>();
        List<Zuora__Subscription__c> Wholesale_Subscription = new List<Zuora__Subscription__c>();
        Set<String> SubscriptionZuoraId = new Set<String>();
        Map<String,String> SubscriptionZuoraId_QuoteUsername = new Map<String,String>();
        
        try{
            subscriptionList = new List<Zuora__Subscription__c>([SELECT id, Wallet_Mode__c, Zuora__External_Id__c,Zuora__Status__c,End_Date__c,Owner.Username,
                                                                 Zuora__ServiceActivationDate__c,Grace_Period__c, Grace_Period_Type__c, 
                                                                 (SELECT id, Start_Date__c, End_Date__c FROM Zuora__Subscription_Rate_Plans__r WHERE 
                                                                  Start_Date__c!=null AND Status__c='Active') FROM 
                                                                 Zuora__Subscription__c WHERE Zuora__Status__c!='Active' AND Id=:SubscriptionToManage]);
            
            for(Zuora__Subscription__c singleSubscription : subscriptionList){
                SubscriptionZuoraId.add(singleSubscription.Zuora__External_Id__c);
            }
            
            Subscription_QuoteOwner = [SELECT id,zqu__ZuoraSubscriptionID__c, owner.Username from zqu__Quote__c where zqu__ZuoraSubscriptionID__c=:SubscriptionZuoraId];
                
            for(zqu__Quote__c singleQuote : Subscription_QuoteOwner){
                QuoteOwner_Username.add(singleQuote.Owner.Username);
            }
            
            for(Zuora__Subscription__c singleSubscription : subscriptionList){
                for(zqu__Quote__c singleQuote : Subscription_QuoteOwner){
                    if(singleSubscription.Zuora__External_Id__c == singleQuote.zqu__ZuoraSubscriptionID__c){
                        SubscriptionZuoraId_QuoteUsername.put(singleSubscription.Zuora__External_Id__c,singleQuote.owner.Username);
                    }
                }
            }
			            
            SubscriptionOwnerRelatedAccount = [SELECT id, Username__c from Account where Username__c=:QuoteOwner_Username and Billing__c='Wholesale'];
            
            for(Zuora__Subscription__c singleSubscription : subscriptionList) {
                for(Account singleAccount : SubscriptionOwnerRelatedAccount){
                    if(SubscriptionZuoraId_QuoteUsername.get(singleSubscription.Zuora__External_Id__c) == singleAccount.Username__c)
                        Wholesale_Subscription.add(singleSubscription);
                }
            }       
            
            
            for(Zuora__Subscription__c singleSubscription : Wholesale_Subscription){
                MinStartDatePlan = null;
                for(Zuora__SubscriptionRatePlan__c singlePlan : singleSubscription.Zuora__Subscription_Rate_Plans__r){
                    if(MinStartDatePlan == null || (MinStartDatePlan.Start_Date__c > singlePlan.Start_Date__c)){
                        MinStartDatePlan = singlePlan;
                    }
                }
                if(MinStartDatePlan!=null)
                    Subscription_ActivationInfoPlan.put(singleSubscription,MinStartDatePlan);
            } 
        }catch(Exception e){
            ErrorMessage = 'Exception during the research of the minumum Start Date in Plans related to each Subscription - '+
                'APEX CLASS: SubscriptionDate_Management Class - APEX TRIGGER: Zuora_SubscriptionRatePlan_Trigger - '+
                'ERROR: ' + e.getMessage() + ' at Line: '+ e.getLineNumber();
        }
        
        if(ErrorMessage.equalsIgnoreCase('')){
            try{
                for(Zuora__Subscription__c singleSubscription : Subscription_ActivationInfoPlan.keySet()){
                   Zuora__Subscription__c singleSubscription2 = new Zuora__Subscription__c();
                   singleSubscription2 = singleSubscription;
                   Date EndDate = null;
                   Date GracePeriodEndDate = null;
                    
                    EndDate = Subscription_ActivationInfoPlan.get(singleSubscription).End_Date__c;
                    singleSubscription2.Zuora__ServiceActivationDate__c = Subscription_ActivationInfoPlan.get(singleSubscription).Start_Date__c;
                    singleSubscription2.Zuora__Status__c = 'Active';
                    if(singleSubscription.Zuora__ServiceActivationDate__c != null && singleSubscription.Wallet_Mode__c=='PrePaid'){
                        singleSubscription2.End_Date__c = EndDate;
                        singleSubscription2.Grace_Period_EndDate__c = EndDate;
                    }

                    SubscriptionToUpdate.add(singleSubscription2);
                }
                //SubscriptionToUpdate.addAll(Subscription_ActivationInfoPlan.keySet());
            }catch(exception e){
                ErrorMessage = 'Exception during Activation and End Date assignment to each Subscription - '+
                    'APEX CLASS: SubscriptionDate_Management Class - APEX TRIGGER: Zuora_SubscriptionRatePlan_Trigger - '+
                    'ERROR: ' + e.getMessage() + ' at Line: '+ e.getLineNumber();
            }
        }
        
        if(ErrorMessage.equalsIgnoreCase('') && SubscriptionToUpdate.size()>0){
            List<Database.SaveResult> UpdateDML_Result = Database.update(SubscriptionToUpdate,false);
            for(Database.SaveResult singleResult : UpdateDML_Result){
                if(!singleResult.isSuccess()){
                    Errors_Map.put(singleResult.getId(),singleResult.getErrors());
                }
            }
            
            if(!Errors_Map.isEmpty()){
                for(id ErrorId : Errors_Map.keySet()){
                    ErrorMessage += 'ERROR ID: ' + ErrorId + ' - ';
                    for(Database.Error D_Error : Errors_Map.get(ErrorId)){
                        ErrorMessage += 'ERROR MESSAGE: ' + D_Error + '\r';
                    }
                }
            }else{
                ErrorMessage = 'No Errors occurred';
            }
        }
        
        system.debug(ErrorMessage);
    }
    public static void DeactivationDateSetup(Set<Id> SubscriptionToManage){
        List<Zuora__Subscription__c> SubscriptionToUpdate = new List<Zuora__Subscription__c>();
        Map<Zuora__Subscription__c, Boolean> Subscription_ActivePlan = new Map<Zuora__Subscription__c, Boolean>();
        Map<Id,List<Database.Error>> Errors_Map = new Map<Id, List<Database.Error>>();
        String ErrorMessage='';
        List<Zuora__Subscription__c> subscriptionList = new List<Zuora__Subscription__c>();
        Map<Id, Account> SubscriptionId_RelatedAccount_Map = new Map<Id, Account>();
        List<Account> SubscriptionOwnerRelatedAccount = new List<Account>();
        List<Zuora__Subscription__c> Wholesale_Subscription = new List<Zuora__Subscription__c>();
        Set<String> SubscriptionZuoraId = new Set<String>();
        List<zqu__Quote__c> Subscription_QuoteOwner = new List<zqu__Quote__c>();
        List<String> QuoteOwner_Username = new List<String>();
        Map<String,String> SubscriptionZuoraId_QuoteUsername = new Map<String,String>();
        
        try{
            
            subscriptionList = new List<Zuora__Subscription__c>([SELECT id,Zuora__External_Id__c, Zuora__Status__c,(SELECT id FROM Zuora__Subscription_Rate_Plans__r WHERE 
                                                                                          Status__c='Active') FROM 
                                                             		Zuora__Subscription__c WHERE Zuora__Status__c='Active' AND Id=:SubscriptionToManage]);
            
            for(Zuora__Subscription__c singleSubscription : subscriptionList){
                SubscriptionZuoraId.add(singleSubscription.Zuora__External_Id__c);
            }
            
            Subscription_QuoteOwner = [SELECT id, zqu__ZuoraSubscriptionID__c,owner.Username from zqu__Quote__c where zqu__ZuoraSubscriptionID__c=:SubscriptionZuoraId];
                
            for(zqu__Quote__c singleQuote : Subscription_QuoteOwner){
                QuoteOwner_Username.add(singleQuote.Owner.Username);
            }
            
            for(Zuora__Subscription__c singleSubscription : subscriptionList){
                for(zqu__Quote__c singleQuote : Subscription_QuoteOwner){
                    if(singleSubscription.Zuora__External_Id__c == singleQuote.zqu__ZuoraSubscriptionID__c){
                        SubscriptionZuoraId_QuoteUsername.put(singleSubscription.Zuora__External_Id__c,singleQuote.owner.Username);
                    }
                }
            }
			      
            SubscriptionOwnerRelatedAccount = [SELECT id, Username__c from Account where Username__c=:QuoteOwner_Username and Billing__c='Wholesale'];
            
            for(Zuora__Subscription__c singleSubscription : subscriptionList) {
                for(Account singleAccount : SubscriptionOwnerRelatedAccount){
                    if(SubscriptionZuoraId_QuoteUsername.get(singleSubscription.Zuora__External_Id__c)  == singleAccount.Username__c)
                        Wholesale_Subscription.add(singleSubscription);
                }
            }       
            for(Zuora__Subscription__c singleSubscription : Wholesale_Subscription){
                                                                 Boolean ActivePlan_Exist = false;
                                                                 if(singleSubscription.Zuora__Subscription_Rate_Plans__r.size()>0){
                                                                     ActivePlan_Exist = true;
                                                                 }
                                                                 Subscription_ActivePlan.put(singleSubscription,ActivePlan_Exist);
                                                             } 
            for(Zuora__Subscription__c singleSubscription : Subscription_ActivePlan.keySet()){
                if(!Subscription_ActivePlan.get(singleSubscription)){
                    Date GracePeriodEndDate=null;
                    singleSubscription.Zuora__Status__c='Expired';
                    singleSubscription.Grace_Period__c = 90;
                    singleSubscription.Grace_Period_Type__c='Days';
                    if(singleSubscription.Grace_Period_Type__c!='' && 
                       String.valueOf(singleSubscription.Grace_Period__c)!='' && singleSubscription.Wallet_Mode__c=='PrePaid'){
                           if(singleSubscription.Grace_Period_Type__c=='Days') 
                               GracePeriodEndDate = Date.today().addDays((Integer)singleSubscription.Grace_Period__c);
                           else if(singleSubscription.Grace_Period_Type__c=='Months')
                               GracePeriodEndDate = Date.today().addMonths((Integer)singleSubscription.Grace_Period__c);
                           else if(singleSubscription.Grace_Period_Type__c=='Years')
                               GracePeriodEndDate = Date.today().addYears((Integer)singleSubscription.Grace_Period__c);
                      singleSubscription.Grace_Period_EndDate__c=GracePeriodEndDate;
                       }
                    
                    SubscriptionToUpdate.add(singleSubscription);
                } 
            }
        }catch(Exception e){
            ErrorMessage = 'Exception during Expire Status assignment to each Subscription - '+
                'APEX CLASS: SubscriptionDate_Management Class - APEX TRIGGER: Zuora_SubscriptionRatePlan_Trigger - '+
                'ERROR: ' + e.getMessage() + ' at Line: '+ e.getLineNumber();
        }
        
        
        if(SubscriptionToUpdate.size()>0 && ErrorMessage.equals('')){
            List<Database.SaveResult> UpdateDML_Result = Database.update(SubscriptionToUpdate,false);
            for(Database.SaveResult singleResult : UpdateDML_Result){
                if(!singleResult.isSuccess()){
                    Errors_Map.put(singleResult.getId(),singleResult.getErrors());
                }
            }
            
            
            if(!Errors_Map.isEmpty()){
                for(id ErrorId : Errors_Map.keySet()){
                    ErrorMessage += 'ERROR ID: ' + ErrorId + ' - ';
                    for(Database.Error D_Error : Errors_Map.get(ErrorId)){
                        ErrorMessage += 'ERROR MESSAGE: ' + D_Error + '\r';
                    }
                }
            }else{
                ErrorMessage = 'No Errors occurred';
            }
        }
        system.debug(ErrorMessage);
        
    }
    public static void StartDateSetup(Map<Asset,Id> Asset_RelatedTerminal){
        
        List<Zuora__Subscription__c> SubscriptionToManage = new List<Zuora__Subscription__c>();
        List<Zuora__Subscription__c> SubscriptionToUpdate = new List<Zuora__Subscription__c>();
        Map<Id,Zuora__Subscription__c> SubscriptionToUpdateMap = new Map<Id,Zuora__Subscription__c>();
        Map<Id,List<Database.Error>> Errors_Map = new Map<Id, List<Database.Error>>();
        Id relatedSubscriptionId = null;
        String ErrorMessage='';
        List<Account> WholesaleAccountList = new List<Account>();
        Set<String> SubscriptionZuoraId = new Set<String>();
        List<zqu__Quote__c> Subscription_QuoteOwner = new List<zqu__Quote__c>();
        List<String> QuoteOwner_Username = new List<String>();
        Map<String,String> SubscriptionZuoraId_QuoteUsername = new Map<String,String>();
        
        
        try{
            SubscriptionToManage = [SELECT Id, Start_Date__c, Zuora__External_Id__c,Owner.Username from Zuora__Subscription__c 
                                                                          WHERE Id =: Asset_RelatedTerminal.values()];
            
             for(Zuora__Subscription__c singleSubscription : SubscriptionToManage){
                SubscriptionZuoraId.add(singleSubscription.Zuora__External_Id__c);
            }
            
            Subscription_QuoteOwner = [SELECT id, zqu__ZuoraSubscriptionID__c,owner.Username from zqu__Quote__c where zqu__ZuoraSubscriptionID__c=:SubscriptionZuoraId];
                
            for(zqu__Quote__c singleQuote : Subscription_QuoteOwner){
                QuoteOwner_Username.add(singleQuote.Owner.Username);
            }
            
            for(zqu__Quote__c singleQuote : Subscription_QuoteOwner){
                QuoteOwner_Username.add(singleQuote.Owner.Username);
            }
            
            for(Zuora__Subscription__c singleSubscription : SubscriptionToManage){
                for(zqu__Quote__c singleQuote : Subscription_QuoteOwner){
                    if(singleSubscription.Zuora__External_Id__c == singleQuote.zqu__ZuoraSubscriptionID__c){
                        SubscriptionZuoraId_QuoteUsername.put(singleSubscription.Zuora__External_Id__c,singleQuote.owner.Username);
                    }
                }
            }
            
            WholesaleAccountList = [SELECT Id, Username__c FROM Account WHERE Username__c=:QuoteOwner_Username AND Billing__c='Wholesale'];
            
            for(Zuora__Subscription__c singleSubscription : SubscriptionToManage){
                for(Account singleAccount : WholesaleAccountList){
                    if(SubscriptionZuoraId_QuoteUsername.get(singleSubscription.Zuora__External_Id__c)  == singleAccount.Username__c) 
                        SubscriptionToUpdateMap.put(singleSubscription.Id, singleSubscription);
                }
            }
            
            for(Asset singleTerminal : Asset_RelatedTerminal.keySet()){
                relatedSubscriptionId = Asset_RelatedTerminal.get(singleTerminal);
                if(SubscriptionToUpdateMap.containsKey(relatedSubscriptionId)) 
                    SubscriptionToUpdateMap.get(relatedSubscriptionId).Start_Date__c = singleTerminal.Terminal_Activation_Date__c;
            }
            SubscriptionToUpdate.addAll(SubscriptionToUpdateMap.values());
        	}catch(exception e){
            ErrorMessage = 'Exception during Start Date assignment to each Subscription - '+
                'APEX CLASS: SubscriptionDate_Management Class - APEX TRIGGER: Terminal_Trigger - '+
                'ERROR: ' + e.getMessage() + ' at Line: '+ e.getLineNumber();
        }
        
        if(SubscriptionToUpdate.size()>0 && ErrorMessage.equals('')){
            List<Database.SaveResult> UpdateDML_Result = Database.update(SubscriptionToUpdate,false);
            for(Database.SaveResult singleResult : UpdateDML_Result){
                if(!singleResult.isSuccess()){
                    Errors_Map.put(singleResult.getId(),singleResult.getErrors());
                }
            }
        }
        system.debug(ErrorMessage);
    }
*/
}