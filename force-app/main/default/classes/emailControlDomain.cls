public class emailControlDomain{
    
    @InvocableMethod(label='Invoke control email domain')
    public static List<Boolean> controlEmail(List<String> email) {
       /* List<String> uncorrectDomains = new List<String>{
            'gmai.com',
            'gnail.com',
            'yaho.com',
            'hotmai.com',
            'hotnail.com',
            'yahooo.com'
                };
         */         
        List<String> uncorrectDomains = new List<String>();
        for(Domain__mdt currentCustomMTD : [SELECT MasterLabel FROM Domain__mdt]){
            uncorrectDomains.add(currentCustomMTD.MasterLabel);
        }
        
        List<Boolean> results = new List<Boolean>();
        system.debug(email);
        for(String e: email){
            system.debug(e);
            String domain = e.split('@')[1];
            Boolean res = false;
            for(String uD: uncorrectDomains){
                system.debug('UD '+ uD);
                if(domain.equals(uD)){
                    
                    res = true;
                    break;
				}
                
            }
            system.debug('risultato ' + res);
            results.add(res);
        }
        
        system.debug(results);
        return results;
    }

}