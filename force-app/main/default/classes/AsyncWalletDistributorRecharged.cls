public class AsyncWalletDistributorRecharged implements Queueable, Database.AllowsCallouts{
    
    private Set<Id> pcIdResponse;
    
    public AsyncWalletDistributorRecharged(Set<Id> WalletIds){
        this.pcIdResponse=WalletIds;
    }
    
    public void execute(QueueableContext cntx) {
        
       	List<Wallet__c> wallets = new List<Wallet__c>();
        List<String> distributor_Ids = new List<String>();
        List<String> distributor_account_Ids = new List<String>();
        List<User> distributors = new List<User>();
        List<String> notification_sIds = new List<String>();
        List<Notification_Item__c> notifications = new List<Notification_Item__c>();
        
        Map<Id, User> distributor_map = new Map<Id, User>();
        Map<String, Notification_Item__c> notification_map = new Map<String, Notification_Item__c>();
        
        List<FeedItem> feeds = new List<FeedItem>();
        
        Id communityId = [Select id from Network where Name='Eutelsat Partner Community'].id;
        
        wallets = [SELECT id, Name, Amount__c, Account__c, Old_Amount__c, CurrencyIsoCode FROM Wallet__c WHERE Id IN :pcIdResponse];
        
        for(Wallet__c w: wallets){
            distributor_Ids.add(w.Account__c);
        }
        
        
        distributors = [SELECT id, Name, Account.Notifications_Web__c, Account.Notifications_Email__c, Account.Notifications_SMS__c,
                        Account.Id, Email, Account.Notification_Setting__c FROM User WHERE AccountId IN:distributor_Ids];
       
        
        for(User u: distributors){
            if(!distributor_map.containsKey(u.AccountId)){
            	distributor_map.put(u.AccountId, u);
            }
            distributor_account_Ids.add(u.AccountId);
            notification_sIds.add(u.Account.Notification_Setting__c);
        }
        
        notifications = [SELECT id, Notification_Setting__c, Event_Type__r.Event_Code__c, Event_Type__r.Name FROM Notification_Item__c WHERE Notification_Setting__c IN :notification_sIds];
        
        for(Notification_Item__c n: notifications){
            system.debug(n.Event_Type__r.Name.split('_0')[1]);
            if(!notification_map.containsKey(n.Notification_Setting__c+'_0'+ n.Event_Type__r.Name.split('_0')[1])){
                system.debug('test');
                notification_map.put(n.Notification_Setting__c+'_0'+ n.Event_Type__r.Name.split('_0')[1], n);
            }
        }
        system.debug('test');
        AllMySMSPartner__c mc = AllMySMSPartner__c.getInstance('SMSPartner');
        String endpoint = mc.endpoint__c;
        if(endpoint==null)
            System.debug('Endpoint vuota');
        for(Wallet__c w: wallets){
            //gestione notifiche
            system.debug('ENTRATO');
            if(distributor_map.get(w.Account__c).Account.Notifications_SMS__c == true){
                if(distributor_map.get(w.Account__c).Account.Notification_Setting__c != null){
                    if(notification_map.get(distributor_map.get(w.Account__c).Account.Notification_Setting__c+'_001').Event_Type__r.Event_Code__c != null){
                        Map<String, String> parametersMap = new Map<String, String>{'subject' => String.valueOf(w.Name), 'comments' => String.valueOf(w.Amount__c)};
                        SfToHerokuAPIManager.sendSingleNotificationRequest richiesta = new SfToHerokuAPIManager.sendSingleNotificationRequest();
                        richiesta.accountId = String.valueOf(w.Account__c);
                        richiesta.channel = 'SMS';
                        richiesta.eventCode = notification_map.get(distributor_map.get(w.Account__c).Account.Notification_Setting__c+'_001').Event_Type__r.Event_Code__c;
                        richiesta.parametersMap = parametersMap;
                        /*JSONGenerator gen = JSON.createGenerator(true);
                        gen.writeStartObject(); 
                        gen.writeStringField('accountId', w.Account__c);
                        gen.writeStringField('channel', 'SMS');
                        gen.writeStringField('eventCode', notification_map.get(distributor_map.get(w.Account__c).Account.Notification_Setting__c+'_001').Event_Type__r.Event_Code__c);
                        gen.writeFieldName('parametersMap');
                        gen.writeStartObject();
                        gen.writeStringField('subject', String.valueOf(w.Name));
                        gen.writeStringField('comments', String.valueOf(w.Amount__c));
                        gen.writeEndObject(); 
                        gen.writeEndObject(); 
                        String jsonS = gen.getAsString();
                        system.debug('Queuealbe --------- FINE JSON ---------');
                        System.debug('jsonAPINotification'+jsonS);*/
                        
                        SFtoHerokuAPIManager.HerokuResponse response = SfToHerokuAPIManager.sendSingleNotification(richiesta);
                        System.debug(response.errorCode);
                        /*Map<String, Object> res1 = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
                        Map<String, Object> res2 = (Map<String, Object>)JSON.deserializeUntyped((String)res1.get('data'));*/
                    }
                }
        	}
            
            
            for(id key: distributor_map.keySet()){
                system.debug(key);
			}
            if(distributor_map.get(w.Account__c).Account.Notifications_Web__c == true){
                system.debug('NOTIFICA WEB');
                Decimal difference = w.Amount__c - w.Old_Amount__c;
                FeedItem feedRes = new FeedItem();
                feedRes.Title = 'Your wallet has been topped up';
                feedRes.Body = 'Your wallet ' + w.Name+ ' has been topped up with '+difference + w.CurrencyIsoCode + '. Your new balance is: '+w.Amount__c + w.CurrencyIsoCode;  
                system.debug('Your wallet '+ w.Name+ ' has been recharged. New amount: '+ w.Amount__c);
                feedRes.ParentId = distributor_map.get(w.Account__c).Id;
                feedRes.NetworkScope=communityId;
                feeds.add(feedRes);
            }
            
            if(distributor_map.get(w.Account__c).Account.Notifications_Email__c == true){
                Decimal difference = w.Amount__c - w.Old_Amount__c;
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.toAddresses = new String[] {distributor_map.get(w.Account__c).Email};
                message.subject = 'Your walles has been topped up';
                message.plainTextBody =  'Your wallet ' + w.Name+ ' has been topped up with '+difference + w.CurrencyIsoCode + '. Your new balance is: '+w.Amount__c + w.CurrencyIsoCode; 
                Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                
                if (results[0].success) {
                    System.debug('The email to distributor was sent successfully.');
                } else {
                    System.debug('The email to distributor failed to send: ' + results[0].errors[0].message);
                }
            }
            
           
           
        }
        if(feeds.size() > 0){
            insert feeds;
        }
    }

}