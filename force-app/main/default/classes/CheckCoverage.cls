public with sharing class CheckCoverage {
@AuraEnabled
public static Account getAccount(String accountId) {
	String[] fieldsToCheck = new String[] {
		'Id', 'Name', 'BillingAddress'
	};

	Map<String,Schema.SObjectField> fieldDescribeTokens =
		Schema.SObjectType.Account.fields.getMap();

	for(String field : fieldsToCheck) {
		if( !fieldDescribeTokens.get(field).getDescribe().isAccessible()) {
			throw new System.NoAccessException();
		}
	}

	// OK, they're cool, let 'em through
	return [SELECT Id, Name, BillingAddress FROM Account WHERE Id = : accountId LIMIT 1];
}

@AuraEnabled
public static Profile getProfileInfo(){
	try{
		String profileId = UserInfo.getProfileId();
		Profile profile = [SELECT Id, Name FROM Profile WHERE Id =:profileId];
		return profile;
	}catch(Exception e) {
		throw new AuraHandledException(e.getMessage());
	}
}
}