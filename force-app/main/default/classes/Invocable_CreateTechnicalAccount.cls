global class Invocable_CreateTechnicalAccount {
    global with sharing class CreateTechnicalAccountInputs {
        @InvocableVariable(Label = 'name' Required = false) global String name;
        @InvocableVariable(Label = 'techServiceId' Required = false) global Integer techServiceId; //non arriva dal flow
        @InvocableVariable(Label = 'technologyId' Required = false) global Integer technologyId; //non arriva dal flow
        @InvocableVariable(Label = 'customerId' Required = false) global Integer customerId;
        @InvocableVariable(Label = 'subPartnerId' Required = false) global Integer subPartnerId;
        @InvocableVariable(Label = 'productId' Required = false) global Integer productId;
        @InvocableVariable(Label = 'satelliteId' Required = false) global String satelliteId; //non arriva dal flow
        @InvocableVariable(Label = 'country' Required = false) global String country;
        @InvocableVariable(Label = 'latitude' Required = false) global Integer latitude;
        @InvocableVariable(Label = 'longitude' Required = false) global Integer longitude;    
        @InvocableVariable(Label = 'timezone' Required = false) global Integer timezone;
        @InvocableVariable(Label = 'quoteId' Required = false) global String quoteId;
        @InvocableVariable(Label = 'optionItems' Required = false) global List<Integer> optionItems = new List<Integer>();
        @InvocableVariable(Label = 'Esito' Required = false) global List<String> Esito = new List<String>();
    }
    @InvocableMethod(Label = 'API_OSS_CreateTechnicalAccount')
	global static List<String> makePostCallout(CreateTechnicalAccountInputs[] requestActionInList) 
    {
		SfToHerokuAPIManager.TechnicalAccountInfo accountInfo = new SfToHerokuAPIManager.TechnicalAccountInfo();
       	accountInfo.subPartnerId = requestActionInList[0].subPartnerId;
        accountInfo.customerId = requestActionInList[0].customerId;
        accountInfo.techServiceId = 1;
        accountInfo.satelliteId = 1;
        accountInfo.technologyId = 1;
        accountInfo.productId = requestActionInList[0].productId;
        accountInfo.name = requestActionInList[0].name;

        SfToHerokuAPIManager.CoverageInfo coverage = new SfToHerokuAPIManager.CoverageInfo();
       	coverage.satelliteId = 1;
       	coverage.beamId = 1;
        List<SfToHerokuAPIManager.CoverageInfo> coverageList = new List<SfToHerokuAPIManager.CoverageInfo>();
        coverageList.add(coverage);
        
        SfToHerokuAPIManager.LocationInfo newLocation = new SfToHerokuAPIManager.LocationInfo();
       	newLocation.country = requestActionInList[0].country;
       	newLocation.latitude = requestActionInList[0].latitude;
        newLocation.longitude = requestActionInList[0].longitude;
        newLocation.timezone = 'blank';
        newLocation.coverage = coverageList;

        SfToHerokuAPIManager.TechnicalAccountCreationRequest creationRequest = new SfToHerokuAPIManager.TechnicalAccountCreationRequest();
       	creationRequest.account = accountInfo;
        creationRequest.location = newLocation;
		creationRequest.optionItemsIds = requestActionInList[0].optionItems;
        
		SfToHerokuAPIManager.HerokuResponse responseNewTechAccount = SfToHerokuAPIManager.createTechnicalAccount(creationRequest);
        
        if(responseNewTechAccount != null){
            Integer errCode = responseNewTechAccount.errorCode;
        	String descEsito = responseNewTechAccount.message;
        
            if(errCode == 200 || errCode == 201){
            	SfToHerokuAPIManager.TechnicalAccountCreationResponse dateNewTechnicalAccount = (SfToHerokuAPIManager.TechnicalAccountCreationResponse)responseNewTechAccount.data;         
                Integer techinicalAccountId = dateNewTechnicalAccount.id;
                String techinicalAccountIdString = String.valueOf(techinicalAccountId);
                String activationCode = dateNewTechnicalAccount.activationCode;
                zqu__Quote__c quoteToUpdate = [SELECT Id FROM zqu__Quote__c where Id =:requestActionInList[0].quoteId];
                quoteToUpdate.OSSTechAccountId__c = techinicalAccountIdString;
                quoteToUpdate.OSSActivationCode__c = activationCode;
                update quoteToUpdate;
                requestActionInList[0].Esito.add('OK');
                // da sviluppare process builder che setta il Technical Account ID sulla Subscription a partire dal Quote.
            }else{
                String bodyRequest = JSON.serialize(creationRequest);
                String bodyResponse = JSON.serialize(responseNewTechAccount);
                ApexLog__c writeErrorLog = new ApexLog__c();
                writeErrorLog.ClassName__c = 'Invocable_CreateTechnicalAccount';
                writeErrorLog.MethodName__c = 'API_OSS_CreateTechnicalAccount';
                writeErrorLog.ProcessName__c = 'CreateTechnicalAccount';
                writeErrorLog.Body__c = bodyRequest;
                writeErrorLog.Response__c = bodyResponse;
                writeErrorLog.ProcessId__c = requestActionInList[0].quoteId;
                insert writeErrorLog;
                requestActionInList[0].Esito.add('KO');
            }
        }
    	return requestActionInList[0].Esito;  
    }
}