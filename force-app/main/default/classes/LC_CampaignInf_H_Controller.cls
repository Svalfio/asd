public class LC_CampaignInf_H_Controller {
    public List<CampaignMember> campaignMember{get;set;}
    
    public pageReference DownloadInformations(){
        String recordId = apexpages.currentpage().getparameters().get('recordId');
        System.debug(recordId);
        campaignMember = [SELECT Name FROM CampaignMember WHERE CampaignId =:recordId];
        return null;
    }

}