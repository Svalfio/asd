public class UpdateCommissPaymentDateTriggerHandler {
    
    public void OnBeforeUpdate(Map<Id,Monthly_Statement__c> triggerOld,Map<Id,Monthly_Statement__c> triggerNew){
        
        List<Monthly_Statement__c> monthlyStatementList = new List<Monthly_Statement__c>();
        
        for(Id key: triggerOld.keySet()){
            if(triggerNew.get(key).Payment_Status__c!=null && triggerOld.get(key).Payment_Status__c!=null){
                if((triggerOld.get(key).Payment_Status__c == 'Unpaid' && triggerNew.get(key).Payment_Status__c == 'Paid') ){
                    triggerNew.get(key).Commission_Payment_Date__c = Date.today();
                    system.debug(Date.today());
                }        
            }
        }
        
    }
    
    
}