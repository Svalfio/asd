public class LC_WalletRechargeHistoryController {
    @AuraEnabled
    public static List<Wallet_Transaction__c> getRecharges(String recordId){
        List<Wallet_Transaction__c> walletRecharges = new List<Wallet_Transaction__c>();
        walletRecharges = [SELECT Id,Name, Date__c, Amount__c, Payment_Method__c FROM Wallet_Transaction__c WHERE
                           Wallet__c =:recordId AND Transaction_Category__c = 'Top-up' ORDER BY Date__c DESC LIMIT 5];
        return walletRecharges;
    }

}