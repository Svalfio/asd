public class LC_SearchByCaseId_Controller {
    
    @AuraEnabled
    public static String getPartnerByCaseId(String CaseId){
        String result;
        try{
            Case c = [SELECT id, AccountId FROM Case WHERE CaseNumber =: CaseId AND (Account.RecordType.DeveloperName = 'Distributor'
                                                                             OR Account.RecordType.DeveloperName = 'Reseller'
                                                                             OR Account.RecordType.DeveloperName = 'Sub_Reseller') LIMIT 1];
            result = c.Id;
        }catch(Exception e){
            result = 'No information found';
        }
        return result;
        
    }
    
    @AuraEnabled
    public static String getCustomerByCaseId(String CaseId){
        
        String result;
        try{
            Case c = [SELECT id, AccountId FROM Case WHERE CaseNumber =: CaseId AND (Account.RecordType.DeveloperName = 'Residential'
                                                                             OR Account.RecordType.DeveloperName = 'Professional') LIMIT 1];
            result = c.Id;
        }catch(Exception e){
            result = 'No information found';
        }
        return result;
        
    }
    
    @AuraEnabled
    public static String getCaseById(String CaseId){
        system.debug('###########'+CaseId);
        
        String result;
        try{
            Case c = [SELECT id FROM Case WHERE CaseNumber =: CaseId LIMIT 1];
            result = c.Id;
        }catch(Exception e){
            result = 'No information found';
        }
        return result;
        
    }
    
}