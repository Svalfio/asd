@isTest
public class Test_ContactPromotionCampaignTrigger {
    
    
    @testSetup
    static void createData(){
        
        Date myDate = date.newinstance(2019, 2, 17);
        Date myDate2 = date.newinstance(2019, 2, 20);
        
        Id RecordTypeIdCampaign = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Promotion').getRecordTypeId();
        Campaign PromotionCampaign = new Campaign(Name='PromotionCampaign',
                                             Channel__c='SMS;Partner_Portal;Phone;Customer_Portal',
                                             Status='Planned',
                                             startDate=myDate,
                                             endDate=myDate2,
                                             SMS_Body__c='SMSTest',
                                             Web_Body__c='webTest',
                                             RecordTypeId=RecordTypeIdCampaign);
        insert PromotionCampaign;
        
        
        Id RecordTypeIdCampaign2 = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Campaign').getRecordTypeId();
        Campaign testCampaign2 = new Campaign(Name='Campaign',
                                             Channel__c='SMS;Partner_Portal;Phone;Customer_Portal',
                                             Status='Planned',
                                             startDate=myDate,
                                             endDate=myDate2,
                                             SMS_Body__c='SMSTest',
                                             Web_Body__c='webTest',
                                             RecordTypeId=RecordTypeIdCampaign2);
        
        insert testCampaign2;
        
        
        Campaign PromotionCampaign2 = new Campaign(Name='PromotionCampaign2',
                                             Channel__c='SMS;Partner_Portal;Phone;Customer_Portal',
                                             Status='Planned',
                                             startDate=myDate,
                                             endDate=myDate2,
                                             SMS_Body__c='SMSTest',
                                             Web_Body__c='webTest',
                                             RecordTypeId=RecordTypeIdCampaign);
        insert PromotionCampaign2;

        
        Account acc = new Account(Name='Acme');
        insert acc;
        
        Contact testContact = new Contact(FirstName='test',LastName='contact',AccountId=acc.id);
        insert testContact;
        
        Contact testContact2 = new Contact(FirstName='test2',LastName='contact2',AccountId=acc.id);
        insert testContact2;
        
        ///This three campaigns are used in onBeforeDeleteTest method
        
       Campaign PromotionCamp = new Campaign(Name='PromotionCamp',
                                             Channel__c='SMS;Partner_Portal;Phone;Customer_Portal',
                                             Status='Planned',
                                             startDate=myDate,
                                             endDate=myDate2,
                                             SMS_Body__c='SMSTest',
                                             Web_Body__c='webTest',
                                             RecordTypeId=RecordTypeIdCampaign);
        insert PromotionCamp;
        
        Campaign PromotionCamp2 = new Campaign(Name='PromotionCamp2',
                                             Channel__c='SMS;Partner_Portal;Phone;Customer_Portal',
                                             Status='Planned',
                                             startDate=myDate,
                                             endDate=myDate2,
                                             SMS_Body__c='SMSTest',
                                             Web_Body__c='webTest',
                                             RecordTypeId=RecordTypeIdCampaign);
        insert PromotionCamp2;
        
        
        Campaign Camp = new Campaign(Name='Camp',
                                             Channel__c='SMS;Partner_Portal;Phone;Customer_Portal',
                                             Status='Planned',
                                             startDate=myDate,
                                             endDate=myDate2,
                                             SMS_Body__c='SMSTest',
                                             Web_Body__c='webTest',
                                             RecordTypeId=RecordTypeIdCampaign2);
        
        insert Camp;
        
        //This members are used in onBeforeDeleteTest method
        
        CampaignMember member1 = new CampaignMember(CampaignId=PromotionCamp2.id,ContactId=testContact.id);
        CampaignMember member2 = new CampaignMember(CampaignId=PromotionCamp.id,ContactId=testContact.id);
        CampaignMember member3 = new CampaignMember(CampaignId=PromotionCamp.id,ContactId=testContact2.id);
 		CampaignMember member4 = new CampaignMember(CampaignId=PromotionCamp2.id,ContactId=testContact2.id);

        CampaignMember member5 = new CampaignMember(CampaignId=Camp.id,ContactId=testContact2.id);
        CampaignMember member6 = new CampaignMember(CampaignId=Camp.id,ContactId=testContact.id);
        
        List<CampaignMember> campaignMembers = new List<CampaignMember>();
        campaignMembers.add(member1);
        campaignMembers.add(member2);
        campaignMembers.add(member3);
        campaignMembers.add(member4);
        campaignMembers.add(member5);
        campaignMembers.add(member6);
        insert campaignMembers;
        
    }
    
    
    @isTest
    static void onAfterInsertTest(){
        system.debug('Test_ContactPromotionCampaignTrigger---------onAfterInsertTest--------START');
         
        id PromotionCampaignId = [Select id from Campaign where Name='PromotionCampaign' limit 1].id;
        id PromotionCampaignId2 = [Select id from Campaign where Name='PromotionCampaign2' limit 1].id;
        
        id CampaignId = [Select id from Campaign where Name='Campaign' limit 1].id;

        Contact testContact2Id = [select id,In_promotion_campaign__c from Contact where LastName='contact2' limit 1];
        Contact testContactId = [select id,In_promotion_campaign__c from Contact where LastName='contact' limit 1];

        CampaignMember member1 = new CampaignMember(CampaignId=PromotionCampaignId,ContactId=testContactId.id);
        CampaignMember member2 = new CampaignMember(CampaignId=PromotionCampaignId,ContactId=testContact2Id.id);
        CampaignMember member3 = new CampaignMember(CampaignId=PromotionCampaignId2,ContactId=testContact2Id.id);
        CampaignMember member4 = new CampaignMember(CampaignId=CampaignId,ContactId=testContact2Id.id);
        
        List<CampaignMember> campaignMembers = new List<CampaignMember>();
        campaignMembers.add(member1);
        campaignMembers.add(member2);
        campaignMembers.add(member3);
        campaignMembers.add(member4);
        insert campaignMembers;

        Contact testContact2Id2 = [select id,In_promotion_campaign__c from Contact where LastName='contact2' limit 1];
        Contact testContactId2 = [select id,In_promotion_campaign__c from Contact where LastName='contact' limit 1];
        system.debug('onAfterInsertTest - testContactId in promotion : '+testContactId2.In_promotion_campaign__c+', '+'testContact2Id in promotion : '+testContact2Id2.In_promotion_campaign__c);
        
        
    }
    
    
    @isTest
    static void onBeforeDeleteTest(){
        system.debug('Test_ContactPromotionCampaignTrigger---------onBeforeDeleteTest--------START');
        
        CampaignMember member1 = [SELECT id,CampaignId,Contact.LastName  FROM CampaignMember WHERE Campaign.Name='PromotionCamp' AND Contact.LastName='contact2'];
        CampaignMember member2 = [SELECT id,CampaignId,Contact.LastName  FROM CampaignMember WHERE Campaign.Name='PromotionCamp2' AND Contact.LastName='contact2'];
        CampaignMember member3 = [SELECT id,CampaignId,Contact.LastName  FROM CampaignMember WHERE Campaign.Name='Camp' AND Contact.LastName='contact2'];
        CampaignMember member4 = [SELECT id,CampaignId,Contact.LastName  FROM CampaignMember WHERE Campaign.Name='PromotionCamp' AND Contact.LastName='contact'];
        CampaignMember member5 = [SELECT id,CampaignId,Contact.LastName  FROM CampaignMember WHERE Campaign.Name='PromotionCamp2' AND Contact.LastName='contact'];
        CampaignMember member6 = [SELECT id,CampaignId,Contact.LastName  FROM CampaignMember WHERE Campaign.Name='Camp' AND Contact.LastName='contact'];
        
        List<CampaignMember> deleteList = new List<CampaignMember>();
        List<CampaignMember> member = [SELECT id,CampaignId,Contact.LastName  FROM CampaignMember WHERE Contact.LastName='contact2'];
        System.debug('before delete : contact2 associated promotion campaigns : '+member.size());
        deleteList.add(member1);
        deleteList.add(member2);
        deleteList.add(member3);
        deleteList.add(member4);
        deleteList.add(member5);
        deleteList.add(member6);
        delete deleteList;
        
        List<CampaignMember> memberAfter = [SELECT id,CampaignId,Contact.LastName  FROM CampaignMember WHERE Contact.LastName='contact2'];
        System.debug('after delete : contact2 associated promotion campaigns : '+memberAfter.size());
        
        Contact testContact2Id2 = [select id,In_promotion_campaign__c from Contact where LastName='contact2' limit 1];
        Contact testContactId2 = [select id,In_promotion_campaign__c from Contact where LastName='contact' limit 1];
        system.debug('onBeforeDeleteTest - testContactId in promotion : '+testContactId2.In_promotion_campaign__c+', '+'testContact2Id in promotion : '+testContact2Id2.In_promotion_campaign__c);
        
    }
    
    
    
    
}