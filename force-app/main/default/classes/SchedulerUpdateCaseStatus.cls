global class SchedulerUpdateCaseStatus implements Schedulable{

    global void execute(SchedulableContext sc) {
        
        BatchUpdateStatusCase b1 = new BatchUpdateStatusCase();
        ID batchprocessid = Database.executeBatch(b1,50);      
    }
}