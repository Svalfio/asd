public class AsyncWalletUpdate implements Queueable, Database.AllowsCallouts{
    
	private Set<Id> pcIdResponse;
    
    public AsyncWalletUpdate(Set<Id> WalletIds){
        this.pcIdResponse=WalletIds;
    }
    
    public void execute(QueueableContext cntx) {
        
        system.debug('EXECUTE');
        
       	List<Wallet__c> wallets = new List<Wallet__c>();
        List<String> subreseller_Ids = new List<String>();
        List<String> reseller_Ids = new List<String>();
        List<User> subresellers = new List<User>();
        List<User> resellers = new List<User>();
        List<String> notification_sResIds = new List<String>();
        List<Notification_Item__c> notificationsRes = new List<Notification_Item__c>();
        List<String> notification_sSubResIds = new List<String>();
        List<Notification_Item__c> notificationsSubRes = new List<Notification_Item__c>();
        Map<Id, User> subreseller_map = new Map<Id, User>();
        Map<Id, User> reseller_map = new Map<Id, User>();
        Map<String, Notification_Item__c> notificationRes_map = new Map<String, Notification_Item__c>();
        Map<String, Notification_Item__c> notificationSubRes_map = new Map<String, Notification_Item__c>();
        
        List<FeedItem> feeds = new List<FeedItem>();
        
        Id communityId = [Select id from Network where Name='Eutelsat Partner Community'].id;
        
        wallets = [SELECT id, Name, Reseller_Wallet__r.Account__c, Account__c, Threshold_Amount__c FROM Wallet__c WHERE Id IN :pcIdResponse];
        
        for(Wallet__c w: wallets){
            subreseller_Ids.add(w.Account__c);
            reseller_Ids.add(w.Reseller_Wallet__r.Account__c);
        }
        
        subresellers = [SELECT id, Name, Account.Notifications_Web__c, Account.Notifications_Email__c,
         Account.Notifications_SMS__c,  Account.Notification_Setting__c, AccountId, Email FROM User WHERE AccountId IN:subreseller_Ids];
        resellers = [SELECT id, Name, Account.Notifications_Web__c, Account.Notifications_Email__c,
         Account.Notifications_SMS__c,  Account.Notification_Setting__c, AccountId, Email FROM User WHERE AccountId IN:reseller_Ids];
        
        for(User u: resellers){
            if(!reseller_map.containsKey(u.AccountId)){
            	reseller_map.put(u.AccountId, u);
            }
            notification_sResIds.add(u.Account.Notification_Setting__c);
        }
        
        for(User u: subresellers){
            system.debug('AAA');
            if(!subreseller_map.containsKey(u.AccountId)){
            	subreseller_map.put(u.AccountId, u);
                system.debug('Aggiunto');
            }
            notification_sSubResIds.add(u.Account.Notification_Setting__c);
        }

        notificationsRes = [SELECT id, Notification_Setting__c, Event_Type__r.Event_Code__c, Event_Type__r.Name FROM Notification_Item__c WHERE Notification_Setting__c IN :notification_sResIds];
        notificationsSubRes = [SELECT id, Notification_Setting__c, Event_Type__r.Event_Code__c, Event_Type__r.Name FROM Notification_Item__c WHERE Notification_Setting__c IN :notification_sSubResIds];
        
        for(Notification_Item__c n: notificationsRes){
            system.debug(n.Event_Type__r.Name.split('_0')[1]);
            if(!notificationRes_map.containsKey(n.Notification_Setting__c+'_0'+ n.Event_Type__r.Name.split('_0')[1])){
                system.debug('test');
                notificationRes_map.put(n.Notification_Setting__c+'_0'+ n.Event_Type__r.Name.split('_0')[1], n);
            }
        }
        
        for(Notification_Item__c n: notificationsSubRes){
            system.debug(n.Event_Type__r.Name.split('_0')[1]);
            if(!notificationSubRes_map.containsKey(n.Notification_Setting__c+'_0'+ n.Event_Type__r.Name.split('_0')[1])){
                system.debug('test');
                notificationSubRes_map.put(n.Notification_Setting__c+'_0'+ n.Event_Type__r.Name.split('_0')[1], n);
            }
        }  
        
        AllMySMSPartner__c mc = AllMySMSPartner__c.getInstance('SMSPartner');
        String endpoint = mc.endpoint__c;
        if(endpoint==null)
            System.debug('Endpoint vuota');
        
        for(Wallet__c w: wallets){
            //gestione notifiche
            //
            if(subreseller_map.get(w.Account__c).Account.Notifications_SMS__c  == true){
                if(subreseller_map.get(w.Account__c).Account.Notification_Setting__c != null){
                    if(notificationSubRes_map.get(subreseller_map.get(w.Account__c).Account.Notification_Setting__c+'_006').Event_Type__r.Event_Code__c != null){
                        Map<String, String> parametersMap = new Map<String, String>{'subject' => String.valueOf(w.Name), 'comments' => String.valueOf(w.Amount__c)};
                        SfToHerokuAPIManager.sendSingleNotificationRequest richiesta = new SfToHerokuAPIManager.sendSingleNotificationRequest();
                        richiesta.accountId = String.valueOf(w.Account__c);
                        richiesta.channel = 'SMS';
                        richiesta.eventCode = notificationSubRes_map.get(subreseller_map.get(w.Account__c).Account.Notification_Setting__c+'_006').Event_Type__r.Event_Code__c;
                        richiesta.parametersMap = parametersMap;
                    
                        SFtoHerokuAPIManager.HerokuResponse response = SfToHerokuAPIManager.sendSingleNotification(richiesta);
                        System.debug(response.errorCode);
                    }
                }
            }

            if(reseller_map.get(w.Reseller_Wallet__r.Account__c).Account.Notifications_SMS__c  == true){
                if(reseller_map.get(w.Reseller_Wallet__r.Account__c).Account.Notification_Setting__c != null){
                    if(notificationRes_map.get(reseller_map.get(w.Reseller_Wallet__r.Account__c).Account.Notification_Setting__c+'_006').Event_Type__r.Event_Code__c != null){
                        Map<String, String> parametersMap = new Map<String, String>{'subject' => String.valueOf(w.Name), 'comments' => String.valueOf(w.Amount__c)};
                        SfToHerokuAPIManager.sendSingleNotificationRequest richiesta = new SfToHerokuAPIManager.sendSingleNotificationRequest();
                        richiesta.accountId = String.valueOf(w.Account__c);
                        richiesta.channel = 'SMS';
                        richiesta.eventCode = notificationRes_map.get(reseller_map.get(w.Reseller_Wallet__r.Account__c).Account.Notification_Setting__c+'_006').Event_Type__r.Event_Code__c;
                        richiesta.parametersMap = parametersMap;
                    
                        SFtoHerokuAPIManager.HerokuResponse response = SfToHerokuAPIManager.sendSingleNotification(richiesta);
                        System.debug(response.errorCode);
                    }
                }
            }
            
            for(id key: subreseller_map.keySet()){
                system.debug(key);
			}
            if(subreseller_map.get(w.Account__c).Account.Notifications_Web__c  == true){
                FeedItem feedSubRes = new FeedItem();
                feedSubRes.Title = 'Not enough wallet amount';
                feedSubRes.Body = 'Your Wallet '+w.Name+' amount is below the minimum threshold('+w.Threshold_Amount__c+'). Please recharge your Wallet.';
                system.debug('Your Wallet '+w.Name+' amount is below the minimum threshold('+w.Threshold_Amount__c+'). Please recharge your Wallet.');
                feedSubRes.NetworkScope=communityId;
                feedSubRes.ParentId = subreseller_map.get(w.Account__c).Id;
                feeds.add(feedSubRes);
            }
            
            if(reseller_map.get(w.Reseller_Wallet__r.Account__c).Account.Notifications_Web__c  == true){
                FeedItem feedRes = new FeedItem();
                feedRes.Title = 'Not enough sub-wallet amount';
                feedRes.Body = 'The amount of your Sub-reseller’s Wallet ('+w.Name+') is below the minimum threshold('+w.Threshold_Amount__c+').';
                system.debug('The amount of your Sub-reseller’s Wallet ('+w.Name+') is below the minimum threshold('+w.Threshold_Amount__c+').');
                feedRes.ParentId = reseller_map.get(w.Reseller_Wallet__r.Account__c).Id;
                feedRes.NetworkScope=communityId;
                feeds.add(feedRes);
            }
            
            if(subreseller_map.get(w.Account__c).Account.Notifications_Email__c  == true){
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.toAddresses = new String[] {subreseller_map.get(w.Account__c).Email};
                message.subject = 'Not enough wallet amount';
                message.plainTextBody =  'Your Wallet '+w.Name+' amount is below the minimum threshold ('+w.Threshold_Amount__c+'). Please recharge your Wallet.';
                Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                
                if (results[0].success) {
                    System.debug('The email to subreseller was sent successfully.');
                } else {
                    System.debug('The email to subreseller failed to send: ' + results[0].errors[0].message);
                }
            }
            
            if(reseller_map.get(w.Reseller_Wallet__r.Account__c).Account.Notifications_Email__c == true){
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.toAddresses = new String[] {reseller_map.get(w.Reseller_Wallet__r.Account__c).Email};
                message.subject = 'Not enough sub-wallet amount';
                message.plainTextBody =  'The amount of your Sub-reseller’s Wallet ('+w.Name+') is below the minimum threshold('+w.Threshold_Amount__c+').';
                Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                
                if (results[0].success) {
                    System.debug('The email to reseller was sent successfully.');
                } else {
                    System.debug('The email to reseller failed to send: ' + results[0].errors[0].message);
                }
            }
            
        }
        
        if(feeds.size() > 0){
            insert feeds;
        }
    }
}