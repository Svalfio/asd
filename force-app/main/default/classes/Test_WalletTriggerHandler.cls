@IsTest
public class Test_WalletTriggerHandler {
   
    @testSetup
    static void setup(){
        
        AllMySMSPartner__c testCustomMySms = new AllMySMSPartner__c(Name='SMSPartner',endpoint__c='urlFalse');
        insert testCustomMySms;
        
        
        
        List<Wallet__c> wallets = new List<Wallet__c>();
        List<Wallet__c> walletsUpdate = new List<Wallet__c>();
        List<User> users = new List<User>();
        List<Account> accounts = new List<Account>();
        List<Contact> contacts = new List<Contact>();
        List<Notification_Setting__c> notification_settings = new List<Notification_Setting__c>();
        List<Notification_Item__c> notification_items = new List<Notification_Item__c>();
        List<Event_Type__c> events = new List<Event_Type__c>();
        
        Event_Type__c evdistr = new Event_Type__c(Name = 'Ev_001');
        Event_Type__c evres = new Event_Type__c(Name = 'Ev_003');
        Event_Type__c evsubres = new Event_Type__c(Name = 'Ev_004');
        Event_Type__c evres2 = new Event_Type__c(Name = 'Ev_005');
        Event_Type__c evsubres2 = new Event_Type__c(Name = 'Ev_006');
        Event_Type__c evsubres3 = new Event_Type__c(Name = 'Ev_007');
        
        events.add(evdistr);
        events.add(evres);
        events.add(evsubres);
        events.add(evres2);
        events.add(evsubres2);
        events.add(evsubres3);
        
        insert events;
        
        Id RecordTypeSubWalletId = Schema.SObjectType.Wallet__c.getRecordTypeInfosByDeveloperName().get('Sub_Reseller_Wallet').getRecordTypeId();
        Id RecordTypeWalletId = Schema.SObjectType.Wallet__c.getRecordTypeInfosByDeveloperName().get('Reseller_Wallet').getRecordTypeId();
        Id RecordTypeDistrWalletId = Schema.SObjectType.Wallet__c.getRecordTypeInfosByDeveloperName().get('Distributor_Wallet').getRecordTypeId();
        
        Id RecordTypeSubResId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Sub_Reseller').getRecordTypeId();
        Id RecordTypeResId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Reseller').getRecordTypeId();
        Id RecordTypeDistrId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Distributor').getRecordTypeId();
        
        Notification_Setting__c n_s_res = new Notification_Setting__c();
        Notification_Setting__c n_s_subres1 = new Notification_Setting__c();
        Notification_Setting__c n_s_subres2 = new Notification_Setting__c();
        Notification_Setting__c n_s_distr = new Notification_Setting__c();
        
        notification_settings.add(n_s_res);
        notification_settings.add(n_s_subres1);
        notification_settings.add(n_s_subres2);
        notification_settings.add(n_s_distr);
        
        insert notification_settings;
        
        Account account_res =  new Account(Name = 'Reseller', RecordTypeId=RecordTypeResId, Notifications_Email__c = true, Notifications_Web__c= true, Notifications_SMS__c= true, Notification_Setting__c = n_s_res.id);
        Account account_subres1 = new Account(Name = 'SubReseller1', recordTypeId = RecordTypeSubResId, Notifications_Email__c = true, Notifications_Web__c= true, Notifications_SMS__c= true, Notification_Setting__c = n_s_subres1.id);
        Account account_subres2 = new Account(Name = 'SubReseller2', recordTypeId = RecordTypeSubResId, Notifications_Email__c = true, Notifications_Web__c= true, Notifications_SMS__c= true, Notification_Setting__c = n_s_subres2.id);
		Account account_distr = new Account(Name = 'Distributor', recordTypeId = RecordTypeSubResId, Notifications_Email__c = true, Notifications_Web__c= true, Notifications_SMS__c= true, Notification_Setting__c = n_s_distr.id);

        accounts.add(account_res);
        accounts.add(account_subres1);
        accounts.add(account_subres2);
        accounts.add(account_distr);
        
        insert accounts;
        
        Notification_Item__c n_i_distr = new Notification_Item__c(Notification_Setting__c = n_s_distr.id, Event_Type__c=evdistr.Id);
        Notification_Item__c n_i_res = new Notification_Item__c(Notification_Setting__c = n_s_res.id, Event_Type__c=evres.Id);
        Notification_Item__c n_i_subres = new Notification_Item__c(Notification_Setting__c = n_s_subres1.id, Event_Type__c=evsubres.Id);
        Notification_Item__c n_i_res2 = new Notification_Item__c(Notification_Setting__c = n_s_res.id, Event_Type__c=evres2.Id);
        Notification_Item__c n_i_subres2 = new Notification_Item__c(Notification_Setting__c = n_s_subres2.id, Event_Type__c=evsubres2.Id);
        Notification_Item__c n_i_subres3 = new Notification_Item__c(Notification_Setting__c = n_s_subres1.id, Event_Type__c=evsubres3.Id);
        
        notification_items.add(n_i_distr);
        notification_items.add(n_i_res);
        notification_items.add(n_i_subres);
        notification_items.add(n_i_res2);
        notification_items.add(n_i_subres2);
        notification_items.add(n_i_subres3);
        
        insert notification_items;
        
        Id account_resId = [SELECT id, Name From Account WHERE Name = 'Reseller'].id;
        Id account_subres1Id = [SELECT id, Name From Account WHERE Name = 'SubReseller1'].id;
        Id account_subres2Id = [SELECT id, Name From Account WHERE Name = 'SubReseller2'].id;
        Id account_distrId = [SELECT id, Name From Account WHERE Name = 'Distributor'].id;
        
        Contact contact_subres1 = new Contact(LastName = 'Sub1', AccountId= account_subres1Id);
        Contact contact_subres2 = new Contact(LastName = 'Sub2', AccountId= account_subres2Id);
        Contact contact_res = new Contact(LastName = 'Res', AccountId= account_resId);
        Contact contact_distr = new Contact(LastName = 'Distr', AccountId= account_distrId);
        
        contacts.add(contact_subres1);
        contacts.add(contact_subres2);
        contacts.add(contact_res);
        contacts.add(contact_distr);
        
        insert contacts;
        
        
        Id contact_resId = [SELECT id, LastName From Contact WHERE LastName = 'Res'].id;
        Id contact_subres1Id = [SELECT id, LastName From Contact WHERE LastName = 'Sub1'].id;
        Id contact_subres2Id = [SELECT id, LastName From Contact WHERE LastName = 'Sub2'].id;
        Id contact_distrId = [SELECT id, LastName From Contact WHERE LastName = 'Distr'].id;
        
       /* Id resellerId = [SELECT Id FROM Profile WHERE Name = 'Reseller' LIMIT 1].id;
		Id subresellerId = [SELECT Id FROM Profile WHERE Name = 'Sub-Reseller' LIMIT 1].id;
        
        Id subresellerRoleId = [SELECT Id FROM UserRole WHERE Name='Sub Reseller Test Utente Partner' LIMIT 1].id;
        Id resellerRoleId = [SELECT Id FROM UserRole WHERE Name='Partner Utente Partner' LIMIT 1].id;*/
        
        
     /*   User user_sub1 = new User(LastName = 'Sub1', alias = 'sub1', Email= 'sureseller1@gmail.com', Username = 'sureseller1@gmail.com', TimeZoneSidKey='Europe/Rome'	,LocaleSidKey = 'en_US', EmailEncodingKey='ISO-8859-1'	,ProfileId=subresellerId 	,UserRoleId=subresellerRoleId ,LanguageLocaleKey = 'en_US', ContactId=contact_subres1Id );
        User user_sub2 = new User(LastName = 'Sub2', alias = 'sub2', Email= 'sureseller2@gmail.com', Username = 'sureseller2@gmail.com', TimeZoneSidKey='Europe/Rome'	,LocaleSidKey = 'en_US', EmailEncodingKey='ISO-8859-1'	,ProfileId=subresellerId 	,UserRoleId=subresellerRoleId ,LanguageLocaleKey = 'en_US', ContactId=contact_subres2Id );
        User user_res = new User(LastName = 'ResTest', alias = 'restest', Email= 'resellerTest@gmail.com', Username = 'resellerTest@gmail.com', TimeZoneSidKey='Europe/Rome'	,LocaleSidKey = 'en_US', EmailEncodingKey='ISO-8859-1'	,ProfileId=resellerId 	,UserRoleId=resellerRoleId ,LanguageLocaleKey = 'en_US', ContactId=contact_resId );
        users.add(user_sub1);
        users.add(user_sub2);
        users.add(user_res);
        insert users;*/
        
        Id user_sub1Id = [Select Id from User WHERE Profile.Name='Sub-Reseller' AND isActive=true LIMIT 1].Id;
        Id user_sub2Id = [Select Id from User WHERE Profile.Name='Sub-Reseller' AND isActive=true LIMIT 1 OFFSET 1].Id;
        Id user_resId = [Select Id from User WHERE Profile.Name='Reseller' AND isActive=true LIMIT 1].Id;
        Id user_distId = [Select Id from User WHERE Profile.Name='Distributor' AND isActive=true LIMIT 1].Id;
        
        Contract contract = new Contract(AccountId = account_subres1Id);
        insert contract;
        
        Id contractId = [SELECT id FROM Contract WHERE AccountId =:account_subres1Id].id;
        
        Order order = new Order(ContractId = contractId, AccountId = account_subres1Id, Status = 'Pending', EffectiveDate = date.today());
        
        insert order;
          
        Wallet__c wallet_sub1 = new Wallet__c(RecordTypeId = RecordTypeSubWalletId, Amount__c = 11, Threshold_Amount__c = 200, OwnerId = user_sub1Id);
        Wallet__c wallet_sub2 = new Wallet__c(RecordTypeId = RecordTypeSubWalletId, Amount__c = 301, Threshold_Amount__c = 200, OwnerId = user_sub2Id);
        Wallet__c wallet_res = new Wallet__c(RecordTypeId = RecordTypeWalletId, Amount__c = 13, Threshold_Amount__c = 220, OwnerId = user_resId);
        Wallet__c wallet_distr = new Wallet__c(RecordTypeId = RecordTypeDistrWalletId, Amount__c = 14, Threshold_Amount__c = 221, OwnerId = user_distId);
        
        wallets.add(wallet_sub1);
        wallets.add(wallet_sub2);
        wallets.add(wallet_res);
        wallets.add(wallet_distr);
        
        insert wallets;
        
        Id wallet_resId = [SELECT id From Wallet__c WHERE OwnerId = :user_resId].Id;
        
        wallet_sub1.Reseller_Wallet__c = wallet_resId;
        wallet_sub2.Reseller_Wallet__c = wallet_resId;
        
        walletsUpdate.add(wallet_sub1);
        walletsUpdate.add(wallet_sub2);
        
        update walletsUpdate;
	}
       
    
    @isTest
    static void OnAfterUpdateTest(){
        
       	
        
        
        Wallet__c wallet_sub1 = [SELECT id, Name, Amount__c, Threshold_Amount__c FROM Wallet__c WHERE  Amount__c = 11 AND Threshold_Amount__c = 200];
        Wallet__c wallet_sub2 = [SELECT id, Name, Amount__c, Threshold_Amount__c FROM Wallet__c WHERE Amount__c = 301 AND Threshold_Amount__c = 200];
        Wallet__c wallet_res = [SELECT id, Name, Amount__c, Threshold_Amount__c FROM Wallet__c WHERE Amount__c = 13 AND Threshold_Amount__c = 220];
        Wallet__c wallet_distr = [SELECT id, Name, Amount__c, Threshold_Amount__c FROM Wallet__c WHERE Amount__c = 14 AND Threshold_Amount__c = 221];
       
        
        List<Wallet__c> wallets = new List<Wallet__c>();
        
        Test.startTest();
        wallet_sub1.Amount__c = 300;
        wallet_sub2.Amount__c = 10;
        wallet_res.Amount__c = 1000;
        wallet_distr.Amount__c = 1000;
        
        wallets.add(wallet_sub1);
        wallets.add(wallet_sub2);
        wallets.add(wallet_res);
        wallets.add(wallet_distr);
        
        update wallets;
		
        Test.stopTest();
        
      
    }

}