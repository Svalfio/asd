public class getInvoiceIdFromQuote {
    
    @InvocableMethod(label='Get Invoice Id')
    public static List<String> getInvoiceId(List<String> QuoteId){
        ZuoraRestCalls restCall = new ZuoraRestCalls();
        List<String> results = new List<String>();
        String token;
        String InvoiceId;
        String url;
        String IdAttach;
        String InvoiceNumber;
        token = restCall.getAccessToken();
        for(String q: QuoteId){
            
            if(token != null){
                InvoiceId = restCall.getInvoiceId(q, token);
                if(InvoiceId != null){
                     url = restCall.getPDFUrl(token, InvoiceId);
					 InvoiceNumber = restCall.getInvoiceNumber(token, InvoiceId);
                    if (url != null) {
                        idAttach = restCall.AttachPDF(token, url, InvoiceNumber, q);

                        if (idAttach != null) {
        
                            results.add(idAttach);
                        }else{
                            results.add('attach file');
                		}
                	}else{
                		results.add('url');
            		}
                }else{
                	results.add('invoice');
        		}
            }else{
                results.add('token');
            }
        }        
        for(String r: results){
            system.debug('###############'+r);
        }
        return results;
    }
    
}