public with sharing class RedirectToInvoicePDF {

@AuraEnabled
public static String DownloadInvoice(String invoiceId){
	Zuora__ZInvoice__c invoice = [SELECT Id, Name, Zuora__Zuora_Id__c FROM Zuora__ZInvoice__c WHERE id =:invoiceId];
	ZuoraRestCalls zuoraCalls = new ZuoraRestCalls();
	String page;
	String token = zuoraCalls.getAccessToken();
	system.debug('#### TOKEN'+ token);
	String url = zuoraCalls.getPDFUrl(token, invoice.Zuora__Zuora_Id__c);
	if(url != null) {
		system.debug('#### URL'+ url);
		page = zuoraCalls.DownloadPdf(token, url, invoice.Name, invoice.Id);
	}
	return page;
}
}