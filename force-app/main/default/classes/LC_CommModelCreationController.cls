public class LC_CommModelCreationController {
   
    public class Wrapper{
       	@AuraEnabled
        public String ownerName {get;set;}
  		@AuraEnabled
        public String accountName {get;set;}
        @AuraEnabled
        public List<String> countryList {get;set;}
        @AuraEnabled
        public String recordTypeId {get;set;}
    	
        Public Wrapper(String ownerName,String accountName,List<String> countryList,String recordTypeId) {
            this.ownerName = ownerName;
            this.accountName = accountName;
            this.countryList = countryList;
            this.recordTypeId = recordTypeId;
    	}    
    }
    
    @AuraEnabled
    public static Wrapper retrieveInfo(String accountId,String recordTypeName){
        String accountName = '';
        
        String recordTypeId = Schema.SObjectType.Commission_Model__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
 		
        accountName = [SELECT Id,Name FROM Account WHERE Id = :accountId LIMIT 1].Name;          
        
        String userName = UserInfo.getName();
        
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Commission_Model__c.Country__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }    
        
        Wrapper result = new Wrapper(userName,accountName,pickListValuesList,recordTypeId);
        
        system.debug('*** wrapper ***' + result);
        
        return result;
    }

	@AuraEnabled
    public static String createCommissionModel(String accountId,String recordTypeName,String subresellerid,String startdate,String enddate, String selectedcountry){
        system.debug('START createCommissionModel'); 
        String result = 'OK';
        
        String recordTypeId = Schema.SObjectType.Commission_Model__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        
        List<Commission_Model__c> listToInsert = new List<Commission_Model__c>();
        
        List<String> countryList = selectedcountry.split(',');
        
        for(String cnt : countryList){
            Commission_Model__c tmpComm = new Commission_Model__c();
            tmpComm.Reseller_Id__c = accountId;
            if(subresellerid != '' && subresellerid != null)
            	tmpComm.Sub_Reseller_Id__c = subresellerid;
            tmpComm.Country__c = cnt;
            tmpComm.Start_Date__c = Date.valueOf(startdate);
            tmpComm.End_Date__c = Date.valueOf(enddate);
            tmpComm.RecordTypeId = recordTypeId;
            listToInsert.add(tmpComm);
        }
        
        system.debug('listToInsert: '+ listToInsert);
        
        if(listToInsert.size() > 0){
            try{
            	insert listToInsert;
            }
            catch(DmlException e){
                result = e.getMessage();
            }
        }
		
        return result;
    }    
    
}