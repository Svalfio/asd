@isTest
public class Test_CommissionTriggerHandler {

    @testSetup
    static void createDataSet(){
        
        Test_CreateDataSetup.createAccountHierarchy();
        Test_CreateDataSetup.createCommissionItemHierarchy();
    }
    
    @isTest
    static void onBeforeInsertTest(){
        
        Commission_Model__c commissionModel = [Select id from Commission_Model__c limit 1];
        Commission_Item__c testCommissionItem = new Commission_Item__c(Name='testItem',Commission_Model__c=commissionModel.Id,Transaction_Type__c='ADD_ON_FEE');
        
        try{
        insert testCommissionItem;
        }
        catch(Exception e){ e.getMessage();}
        
    }
    
    @isTest
    static void onAfterUpdate(){
        
        Commission_Item__c commItem = [Select id,Commission_Model__c,Transaction_Type__c from Commission_Item__c limit 1];
        
        commItem.Transaction_Type__c='ADD_ON_FEE';
        try{
        update commItem;
        }
        catch(Exception e){e.getMessage();}
        
        
    }
    
}