public class SubscriptionTriggerHandler{
    
    public Map<String, Zuora__Subscription__c> GetSubscriptions(List<Zuora__Subscription__c> subscriptionList){
        Map<String, Zuora__Subscription__c> NewSubscriptions_Map = new Map<String,Zuora__Subscription__c>();
        Map<String,Zuora__Subscription__c> SubscriptionZuoraId_Map = new Map<String,Zuora__Subscription__c>();
        Set<User> QuoteOwnerUser_Set = new Set<User>();
        Map<String,String> QuoteOwnerUsername_Map = new Map<String,String>();
        List<Account> QuoteOwnerAccountList = new List<Account>();
        
        for(Zuora__Subscription__c singleSubscription : subscriptionList){
            SubscriptionZuoraId_Map.put(singleSubscription.Zuora__External_Id__c,singleSubscription);
        }
        
        for(zqu__Quote__c singleQuote : [SELECT id,zqu__ZuoraSubscriptionID__c, Owner.Username FROM zqu__Quote__c WHERE 
                                         zqu__ZuoraSubscriptionID__c=:SubscriptionZuoraId_Map.keySet()]){
                                             QuoteOwnerUsername_Map.put(singleQuote.zqu__ZuoraSubscriptionID__c ,singleQuote.Owner.Username);
                                         }
        for(User singleUser : [SELECT id, Username FROM User WHERE Id=:QuoteOwnerUsername_Map.values()]){
            QuoteOwnerUser_Set.add(singleUser);
        }
        
        QuoteOwnerAccountList = [SELECT id, Username__c, Billing__c from Account  WHERE/*Billing__c = 'Wholesale' and*/ Username__c=:QuoteOwnerUsername_Map.values()];
        
        for(String singleSubscriptionZuoraId : SubscriptionZuoraId_Map.keySet()) {
            for(Account singleAccount : QuoteOwnerAccountList){
                if(singleAccount.Billing__c == 'Wholesale'){
                if(QuoteOwnerUsername_Map.get(singleSubscriptionZuoraId)==singleAccount.Username__c)
                    NewSubscriptions_Map.put(singleSubscriptionZuoraId, SubscriptionZuoraId_Map.get(singleSubscriptionZuoraId));
                }
                
            }
        }
        
        return NewSubscriptions_Map;
    }
    
    public void ManageSubscriptionStatus(Map<String, Zuora__Subscription__c> NewSubscriptions_Map){
        for(String ZuoraSubscriptionID : NewSubscriptions_Map.keySet()){
            if(NewSubscriptions_Map.get(ZuoraSubscriptionID).Wallet_Mode__c=='PostPaid'){
                if(NewSubscriptions_Map.get(ZuoraSubscriptionID).Zuora__ServiceActivationDate__c <= Date.today() &&
                   NewSubscriptions_Map.get(ZuoraSubscriptionID).End_Date__c > Date.today()){
                       NewSubscriptions_Map.get(ZuoraSubscriptionID).put('Zuora__Status__c','Active');
                   }else if(NewSubscriptions_Map.get(ZuoraSubscriptionID).End_Date__c <= Date.today()){
                       NewSubscriptions_Map.get(ZuoraSubscriptionID).put('Zuora__Status__c','Cancelled'); 
                   }else{
                       NewSubscriptions_Map.get(ZuoraSubscriptionID).put('Zuora__Status__c','Free'); 
                   }
            }
            
        }
    }
    
    public void ManageSubscriptionAddress(Map<String, Zuora__Subscription__c> NewSubscriptions_Map){
        for(zqu__Quote__c singleQuote : [SELECT id, Installation_Street__c, Installation_PostalCode__c, Installation_City__c, Installation_State__c,
                                         Installation_Country__c, Installation_Latitude__c, Installation_Longitude__c, zqu__ZuoraSubscriptionID__c 
                                         FROM zqu__Quote__c WHERE zqu__ZuoraSubscriptionID__c =: NewSubscriptions_Map.keySet()]){
                                             for(String ZuoraSubscriptionID : NewSubscriptions_Map.keySet()){
                                                 if(singleQuote.zqu__ZuoraSubscriptionID__c.equals(ZuoraSubscriptionID)){
                                                     NewSubscriptions_Map.get(ZuoraSubscriptionID).put('Installation_Street__c',singleQuote.Installation_Street__c);
                                                     NewSubscriptions_Map.get(ZuoraSubscriptionID).put('Installation_City__c',singleQuote.Installation_City__c);
                                                     NewSubscriptions_Map.get(ZuoraSubscriptionID).put('Installation_Zip_Postal_Code__c',singleQuote.Installation_PostalCode__c);
                                                     NewSubscriptions_Map.get(ZuoraSubscriptionID).put('Installation_State_Province__c',singleQuote.Installation_State__c);
                                                     NewSubscriptions_Map.get(ZuoraSubscriptionID).put('Installation_Country__c',singleQuote.Installation_Country__c);
                                                     NewSubscriptions_Map.get(ZuoraSubscriptionID).put('Installation_Longitude__c',singleQuote.Installation_Longitude__c);
                                                     NewSubscriptions_Map.get(ZuoraSubscriptionID).put('Installation_Latitude__c',singleQuote.Installation_Latitude__c);
                                                 }
                                             }
                                         }
    }
    
    
    public void onBeforeInsert(List<Zuora__Subscription__c> subscriptionList){
        Map<String, Zuora__Subscription__c> NewSubscriptions_Map = new Map<String,Zuora__Subscription__c>();
        NewSubscriptions_Map = GetSubscriptions(subscriptionList);
        ManageSubscriptionStatus(NewSubscriptions_Map); 
        ManageSubscriptionAddress(NewSubscriptions_Map);
    }
    
    public void onAfterInsert(Map<Id,Zuora__Subscription__c> subscriptionMap){
        System.debug('SubscriptionTriggerHandler--------onAfterInsert-------');
        
        Set<String> quoteNumb = new Set<String>();
        Set<String> accnIdSet = new Set<String>();
        Map<String,String> accountUserMapRes = new Map<String,String>();
        Map<String,String> accountUserMapSubRes = new Map<String,String>();
        
        for(Id key : subscriptionMap.KeySet()){
            quoteNumb.add(subscriptionMap.get(key).Zuora__QuoteNumber__c);
        }
        
        Id idRTReseller = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Reseller').getRecordTypeId();
        Id idRTSubRes = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Sub_Reseller').getRecordTypeId();
        Id idRTRes = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Residential').getRecordTypeId();
        Id idRTProf = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Professional').getRecordTypeId(); 
        
        List<zqu__Quote__c> quoteList = [SELECT Id,zqu__Number__c,CreatedBy.Name,CreatedBy.Profile.Name,CreatedBy.Contact.Account.Id,
                                         CreatedBy.Contact.Account.RecordTypeId,zqu__Account__r.RecordTypeId,zqu__Account__c
                                         FROM zqu__Quote__c WHERE zqu__Number__c IN :quoteNumb];
        
        for(zqu__Quote__c tmpQuote : quoteList){
            if((tmpQuote.CreatedBy.Profile.Name == 'Reseller' || tmpQuote.CreatedBy.Profile.Name == 'Sub-Reseller')
               && (tmpQuote.CreatedBy.Contact.Account.RecordTypeId == idRTReseller || tmpQuote.CreatedBy.Contact.Account.RecordTypeId == idRTSubRes)
               && (tmpQuote.zqu__Account__r.RecordTypeId == idRTRes || tmpQuote.zqu__Account__r.RecordTypeId == idRTProf)){
                   accnIdSet.add(tmpQuote.zqu__Account__c);
                   if(tmpQuote.CreatedBy.Contact.Account.RecordTypeId == idRTReseller)
                       accountUserMapRes.put(tmpQuote.zqu__Account__c,tmpQuote.CreatedBy.Contact.Account.Id);
                   else
                       accountUserMapSubRes.put(tmpQuote.zqu__Account__c,tmpQuote.CreatedBy.Contact.Account.Id);
               }
        }
        
        List<Account> accnList = [SELECT Id,ParentId,Initial_Subscription_Partner__c,Initial_Subscription_Sub_Partner__c 
                                  FROM Account 
                                  WHERE Id IN :accnIdSet];
        
        List<Account> accnListUpd = new List<Account>();
        
        for(Account tmpAcc : accnList){
            
            if(accountUserMapRes.containskey(tmpAcc.id)){
                String userId = accountUserMapRes.get(tmpAcc.id);
                if(tmpAcc.Initial_Subscription_Partner__c == '' || tmpAcc.Initial_Subscription_Partner__c == null)
                    tmpAcc.Initial_Subscription_Partner__c = userId;
                tmpAcc.ParentId = userId;
                accnListUpd.add(tmpAcc);
            }
            else if(accountUserMapSubRes.containskey(tmpAcc.id)){
                String userId = accountUserMapSubRes.get(tmpAcc.id);
                if(tmpAcc.Initial_Subscription_Sub_Partner__c == '' || tmpAcc.Initial_Subscription_Sub_Partner__c == null)
                    tmpAcc.Initial_Subscription_Sub_Partner__c = userId;
                tmpAcc.ParentId = userId;
                accnListUpd.add(tmpAcc);
            }
        }
        
        if(accnListUpd.size() > 0 )
            update accnListUpd;
        
        /*********************************************************/
        List<Id> accList = new List<Id>();
        
        for(Id key : subscriptionMap.KeySet()){
            accList.add(subscriptionMap.get(key).Zuora__Account__c);
        }
        
        List<Wallet__c> wallets = [SELECT Id,Account__c FROM Wallet__c WHERE Account__c IN :accList];
        Map<Id,Wallet__c> accWallMap= new Map<Id,Wallet__c>();
        
        for(Wallet__c w : wallets){
            if(!accWallMap.containsKey(w.Account__c))
                accWallMap.put(w.Account__c,w);
        }
        
        List<Zuora__Subscription__c> subsToUpdate = new List<Zuora__Subscription__c>();
        List<Wallet__c> walletList = new List<Wallet__c>();
        
        Id idRT = Schema.SObjectType.Wallet__c.getRecordTypeInfosByDeveloperName().get('Distributor_Wallet').getRecordTypeId();
        
        for(Id key : subscriptionMap.KeySet()){
            System.debug(accWallMap.isEmpty());
            if(subscriptionMap.get(key).wallet_mode__c!=null && subscriptionMap.get(key).wallet_mode__c.equals('PrePaid')){
                if(accWallMap.get(SubscriptionMap.get(key).Zuora__Account__c)==null){
                    System.debug('Creation of wallet');
                    Wallet__c distribWallet= new Wallet__c();   
                    distribWallet.RecordTypeId=idRT;
                    distribWallet.Account__c=subscriptionMap.get(key).Zuora__Account__c;
                    distribWallet.Amount__c=0;
                    distribWallet.Subscription_Id__c = subscriptionMap.get(key).id;
                    distribWallet.Status__c='Active';
                    distribWallet.Activation_Start_Date__c=Date.today();
                    walletList.add(distribWallet);
                }
                else{
                    subsToUpdate.add(subscriptionMap.get(key));
                }
            }
            
        }
        
        if(!subsToUpdate.isEmpty() && subsToUpdate.size()>0){
            List<Zuora__Subscription__c> subList1 = [SELECT id,Wallet__c,Zuora__Account__c FROM Zuora__Subscription__c Where id IN :subsToUpdate];
            for(Zuora__Subscription__c s : subList1){
                if(accWallMap.get(s.Zuora__Account__c)!=null){
                    s.Wallet__c = accWallMap.get(s.Zuora__Account__c).Id;
                }
            }
            update subList1;
        }
        
        if(!walletList.isEmpty() && walletList.size()>0){
            System.debug('Wallets inserted');
            insert walletList;
            
            
            
            List<Id> subscriptionIdList = new List<Id>();
            for(Wallet__c wall : walletList){
                subscriptionIdList.add(wall.Subscription_Id__c);
            }
            
            List<Zuora__Subscription__c> subList = [SELECT id,Wallet__c FROM Zuora__Subscription__c Where id IN :subscriptionIdList ];
            
            
            Map<Id, Zuora__Subscription__c> subMap = new Map<Id, Zuora__Subscription__c>();
            
            for(Zuora__Subscription__c sub : subList){
                if(!subMap.containsKey(sub.Id))
                    subMap.put(sub.Id,sub);
            }
            
            for(Wallet__c wall : walletList){
                if(subMap.get(wall.Subscription_Id__c)!=null){
                    subMap.get(wall.Subscription_Id__c).Wallet__c=wall.id;
                }
            }
            update subList;
            
            
        }
        
        
        
        
    }
    
    
    public void onBeforeUpdate(Map<Id,Zuora__Subscription__c> OldMap,Map<Id,Zuora__Subscription__c> NewMap){
        
        System.debug('SubscriptionTriggerHandler--------onBeforeUpdate-------');
        List<Wallet__c> walletList = new List<Wallet__c>();
        Id idRT = Schema.SObjectType.Wallet__c.getRecordTypeInfosByDeveloperName().get('Distributor_Wallet').getRecordTypeId();
        
        List<Id> accList = new List<Id>();
        
        for(Id singleSubId : NewMap.keyset()){
            if(NewMap.get(singleSubId).Wallet_Mode__c=='PrePaid' && NewMap.get(singleSubId).Zuora__Status__c=='Free' &&
               NewMap.get(singleSubId).Grace_Period_Type__c!='' && String.valueOf(NewMap.get(singleSubId).Grace_Period__c)!='' &&
               (NewMap.get(singleSubId).Grace_Period_Type__c!=OldMap.get(singleSubId).Grace_Period_Type__c ||
                NewMap.get(singleSubId).Grace_Period__c!=OldMap.get(singleSubId).Grace_Period__c )){ 
                    if(NewMap.get(singleSubId).Grace_Period_Type__c=='Days') 
                        NewMap.get(singleSubId).put('Grace_Period_EndDate__c',Date.today().addDays((Integer)NewMap.get(singleSubId).Grace_Period__c)); 
                    if(NewMap.get(singleSubId).Grace_Period_Type__c=='Months') 
                        NewMap.get(singleSubId).put('Grace_Period_EndDate__c',Date.today().addMonths((Integer)NewMap.get(singleSubId).Grace_Period__c)); 
                    if(NewMap.get(singleSubId).Grace_Period_Type__c=='Years') 
                        NewMap.get(singleSubId).put('Grace_Period_EndDate__c',Date.today().addYears((Integer)NewMap.get(singleSubId).Grace_Period__c)); 
                    
                }
        }
        
        for(Id key : NewMap.KeySet()){
            accList.add(NewMap.get(key).Zuora__Account__c);
        }
        
        List<Wallet__c> wallets = [SELECT Id,Account__c,Account__r.Name FROM Wallet__c WHERE Account__c IN :accList];
        Map<Id,Wallet__c> accWallMap= new Map<Id,Wallet__c>();
        system.debug('**** wallets: '+wallets);
        for(Wallet__c w : wallets){
            system.debug('**** wallets name: '+w.Account__r.Name);
            if(!accWallMap.containsKey(w.Account__c))
                accWallMap.put(w.Account__c,w);
        }
        
        List<Zuora__Subscription__c> subsToUpdate = new List<Zuora__Subscription__c>();
        
        
        for(id key : OldMap.KeySet()){
            if(NewMap.get(key).wallet_mode__c!=null && OldMap.get(key).wallet_mode__c!=null && NewMap.get(key).wallet_mode__c.equals('PrePaid') && OldMap.get(key).wallet_mode__c.equals('PostPaid')){
                if(accWallMap.get(OldMap.get(key).Zuora__Account__c)==null){
                    if(NewMap.get(key).Wallet__c==null){
                        Wallet__c distribWallet= new Wallet__c();   
                        distribWallet.RecordTypeId=idRT;
                        distribWallet.Account__c=NewMap.get(key).Zuora__Account__c;
                        distribWallet.Amount__c=0;
                        distribWallet.Subscription_Id__c = NewMap.get(key).id;
                        distribWallet.Status__c='Active';
                        distribWallet.Activation_Start_Date__c=Date.today();
                        walletList.add(distribWallet);
                    }
                }
                else{
                    subsToUpdate.add(OldMap.get(key));
                }
            }
        }
        
        if(!subsToUpdate.isEmpty() && subsToUpdate.size()>0){
            List<Zuora__Subscription__c> subList1 = [SELECT id,Wallet__c,Zuora__Account__c FROM Zuora__Subscription__c Where id IN :subsToUpdate];
            for(Zuora__Subscription__c s : subsToUpdate){
                if(accWallMap.get(s.Zuora__Account__c)!=null){
                    NewMap.get(s.Id).Wallet__c = accWallMap.get(s.Zuora__Account__c).Id;
                }
            }/*
if(subList1.size() > 0)
update subList1;
*/
        }
        
        if(!walletList.isEmpty() && walletList.size()>0){
            System.debug('Wallets inserted');
            insert walletList;
            
            
            List<Id> subscriptionIdList = new List<Id>();
            for(Wallet__c wall : walletList){
                subscriptionIdList.add(wall.Subscription_Id__c);
            }
            
            for(Wallet__c wall : walletList){
                if(NewMap.get(wall.Subscription_Id__c)!=null){
                    NewMap.get(wall.Subscription_Id__c).Wallet__c=wall.id;
                }
            }
            
            
        }
        
        
        
        
    }
    
    public void onAfterUpdate(Map<Id,Zuora__Subscription__c> subscriptionNewMap, Map<Id,Zuora__Subscription__c> subscriptionOldMap){
        
        String ErrorMessage='';
        Map<Id,List<Database.Error>> Errors_Map = new Map<Id, List<Database.Error>>();
        Set<Id> UpdateSubscriptionSet = new Set<Id>();
        List<Zuora__SubscriptionRatePlan__c> SubscriptionRatePlans = new List<Zuora__SubscriptionRatePlan__c>();
        for(Zuora__Subscription__c singleSubscription : subscriptionNewMap.values()){
            if(((singleSubscription.Zuora__Status__c).toLowerCase() == 'suspended' || (singleSubscription.Zuora__Status__c).toLowerCase() =='cancelled') && singleSubscription.Zuora__Status__c != subscriptionOldMap.get(singleSubscription.id).Zuora__Status__c){
                UpdateSubscriptionSet.add(singleSubscription.Id);
            }
            System.debug('UpdateSubscriptionSet: '+UpdateSubscriptionSet);
        }   
        
        try{
            SubscriptionRatePlans = [SELECT Id, Status__c, Zuora__Subscription__r.Zuora__Status__c FROM Zuora__SubscriptionRatePlan__c WHERE Zuora__Subscription__c =: UpdateSubscriptionSet];
            system.debug('SubscriptionRatePlans: '+SubscriptionRatePlans);
            for(Zuora__SubscriptionRatePlan__c singleSubscriptionRatePlan : SubscriptionRatePlans){
                singleSubscriptionRatePlan.Status__c = singleSubscriptionRatePlan.Zuora__Subscription__r.Zuora__Status__c;
            } 
        }catch(exception e){
            ErrorMessage='Exception during Subscription\'s Rate Plans Suspended Status update  - '+
                'APEX CLASS: SubscriptionTriggerHandler Class - APEX TRIGGER: SubscriptionTrigger - '+
                'ERROR: ' + e.getMessage() + ' at Line: '+ e.getLineNumber();
            system.debug('Error message: '+ErrorMessage);
        }
        
        system.debug('List: '+SubscriptionRatePlans);
        
        if(SubscriptionRatePlans.size()>0 && ErrorMessage.equals('')){
            List<Database.SaveResult> UpdateDML_Result = Database.update(SubscriptionRatePlans,false);
            for(Database.SaveResult singleResult : UpdateDML_Result){
                if(!singleResult.isSuccess()){
                    Errors_Map.put(singleResult.getId(),singleResult.getErrors());
                }
            }
        }
    }
}