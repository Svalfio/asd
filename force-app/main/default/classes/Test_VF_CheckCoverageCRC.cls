@isTest
public with sharing class Test_VF_CheckCoverageCRC {

    @TestSetup
    static void makeData(){
        Account a = new Account(Name='Test Account');
        insert a;
    }

    @isTest
    public static void test1() {
        Account a = [SELECT Id FROM Account WHERE Name = 'Test Account'];
        VF_CheckCoverageCRC testClass = new VF_CheckCoverageCRC(null);
        testClass.recordId = a.Id;
        testClass.retrieveAccount();
    }

}