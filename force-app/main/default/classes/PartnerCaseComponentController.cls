public class PartnerCaseComponentController {
	@AuraEnabled public static Boolean VerifyChange(String UserId){
       List<Case> caseUpdate = new List<Case>();
       Boolean ritorno  = false;
       List<Case> cases = [SELECT Id, OwnerId, Status_Changed__c FROM Case WHERE CreatedById =: UserId];
        for(Case c: cases){
            if(c.Status_Changed__c == True){
                c.Status_Changed__c = False;
                caseUpdate.add(c);
                ritorno = true;
            }
        }
        System.debug(caseUpdate.size());
        System.debug(caseUpdate[0].Status_Changed__c);
        update caseUpdate;
        return ritorno;
    }
}