@isTest
public class Test_TaskTriggerHandler {
    
    @testSetup
    static void createData(){
        
        Test_CreateDataSetup.createAllMySmsCustomSetting();
        
        Wallet__c testWallet = new Wallet__c(Amount__c=199);
        insert testWallet;
        
        Id recordTypeWallet = [Select id from RecordType where Name='Recharge Wallet'].id;
        Case caseWallet = new Case(Wallet__c=testWallet.id,
                                  Recharge_Amount__c=100,
                                  RecordTypeId=recordTypeWallet);
        insert caseWallet;
        
        Task testTask = new Task(Case__c=caseWallet.id,
                                 whatId=testWallet.id);
        insert testTask;
    }

    
    @isTest
    static void onAfterUpdate(){
        
        Wallet__c testWallet = [Select id from Wallet__c limit 1 ]; 
        
        Task testTask = [Select id from Task limit 1];
        testTask.Status='Completed';
        testTask.Type= 'Manage Recharge Wallet';
        
        test.startTest();
        update testTask;
        test.stopTest();
        
    }
}