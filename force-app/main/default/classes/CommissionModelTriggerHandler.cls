public class CommissionModelTriggerHandler {
    
    public void onBeforeInsert(List<Commission_Model__c> triggerNew){
        
        Id directRecordTypeId = Schema.SObjectType.Commission_Model__c.getRecordTypeInfosByDeveloperName().get('Direct').getRecordTypeId();
        Id indirectRecordTypeId = Schema.SObjectType.Commission_Model__c.getRecordTypeInfosByDeveloperName().get('Indirect').getRecordTypeId();
        
        Set<String> resellerIdSet = new Set<String>();
        Set<String> accnIdSet = new Set<String>();
        
        for(Commission_Model__c model : triggerNew){
   			resellerIdSet.add(model.Reseller_Id__c);
            accnIdSet.add(model.Reseller_Id__c); 
            accnIdSet.add(model.Sub_Reseller_Id__c); 
        }
        
        List<Account> accn = [SELECT Id,CurrencyIsoCode FROM Account WHERE Id IN :accnIdSet];
        Map<String,String> accnCurrMap = new  Map<String,String>(); 
        for(Account tmpAcn : accn){
            accnCurrMap.put(tmpAcn.Id,tmpAcn.CurrencyIsoCode);
        }
        
        
        List<Commission_Model__c> existentModel = [SELECT Id,Country__c,Reseller_Id__c,Start_Date__c,End_Date__c,RecordTypeId,Sub_Reseller_Id__c  FROM Commission_Model__c WHERE Reseller_Id__c IN : resellerIdSet];
        Map<String,List<Commission_Model__c>> existentModelMap = new Map<String,List<Commission_Model__c>>();
        
        for(Commission_Model__c exModel : existentModel){
            if(existentModelMap.containsKey(exModel.Reseller_Id__c)){
                List<Commission_Model__c> tmpComm = existentModelMap.get(exModel.Reseller_Id__c);
                tmpComm.add(exModel);
                existentModelMap.put(exModel.Reseller_Id__c,tmpComm);
            }
            else{
                List<Commission_Model__c> tmpComm = new List<Commission_Model__c>();
                tmpComm.add(exModel);
                existentModelMap.put(exModel.Reseller_Id__c,tmpComm);
            }
        }
        
        for(Commission_Model__c modelCountry : triggerNew){
            
            Boolean currencyCheck = true;
            
            if(modelCountry.RecordTypeId == directRecordTypeId){//Direct
                if(accnCurrMap.containsKey(modelCountry.Reseller_Id__c)){
                    modelCountry.CurrencyIsoCode = accnCurrMap.get(modelCountry.Reseller_Id__c);
                }
            }
            else{//Indirect
                String resCurr = '';
                String subResCurr = '';
                if(accnCurrMap.containsKey(modelCountry.Reseller_Id__c)){
                    resCurr = accnCurrMap.get(modelCountry.Reseller_Id__c);
                }
                
                if(accnCurrMap.containsKey(modelCountry.Sub_Reseller_Id__c)){
                    subResCurr = accnCurrMap.get(modelCountry.Sub_Reseller_Id__c);
                }
                
                if(resCurr != subResCurr){
                    modelCountry.addError('Please check the currency of the selected Reseller');
                    currencyCheck = false;
                }
                else{
                    if(accnCurrMap.containsKey(modelCountry.Reseller_Id__c))
                    modelCountry.CurrencyIsoCode = accnCurrMap.get(modelCountry.Reseller_Id__c);      
                }
            }
            
            if(currencyCheck){   
                if(modelCountry.Start_Date__c < modelCountry.End_Date__c ){
                    if(existentModelMap.containsKey(modelCountry.Reseller_Id__c)){
                        Boolean checkResult = true;
                        List<Commission_Model__c> tmpCommCheck = existentModelMap.get(modelCountry.Reseller_Id__c);
                        for(Commission_Model__c check : tmpCommCheck){
                            if(check.Country__c == modelCountry.Country__c && check.Sub_Reseller_Id__c == modelCountry.Sub_Reseller_Id__c){
                                if(!((modelCountry.Start_Date__c < check.Start_Date__c  && modelCountry.End_Date__c < check.Start_Date__c)
                                  || (modelCountry.Start_Date__c > check.End_Date__c  && modelCountry.End_Date__c > check.End_Date__c)
                                  ) && check.RecordTypeId == modelCountry.RecordTypeId){
                                  modelCountry.addError('It is not possible to create New Commission Model with inserted information. Please, check: Country,Start Date,End Date');                          
                                    checkResult = false;
                                }         
                            }
                        }
                        
                        if(checkResult){
                            tmpCommCheck.add(modelCountry);
                            existentModelMap.put(modelCountry.Reseller_Id__c,tmpCommCheck);
                        }
                        
                    }
                    else{
                        List<Commission_Model__c> tmpCommCountry = new List<Commission_Model__c>();
                        tmpCommCountry.add(modelCountry);
                        existentModelMap.put(modelCountry.Reseller_Id__c,tmpCommCountry);
                    }
                }
                else
                    modelCountry.addError('It is not possible to create New Commission Model with inserted information. Please, check: Country,Start Date,End Date');
            }
        }
    }
    
    public void onBeforeUpdate(List<Commission_Model__c> triggerNew){
        
        Id directRecordTypeId = Schema.SObjectType.Commission_Model__c.getRecordTypeInfosByDeveloperName().get('Direct').getRecordTypeId();
        Id indirectRecordTypeId = Schema.SObjectType.Commission_Model__c.getRecordTypeInfosByDeveloperName().get('Indirect').getRecordTypeId();
        
        Set<String> resellerIdSet = new Set<String>();
        Set<String> accnIdSet = new Set<String>();
        
        for(Commission_Model__c model : triggerNew){
   			resellerIdSet.add(model.Reseller_Id__c);
            accnIdSet.add(model.Reseller_Id__c); 
            accnIdSet.add(model.Sub_Reseller_Id__c); 
        }
        
        List<Account> accn = [SELECT Id,CurrencyIsoCode FROM Account WHERE Id IN :accnIdSet];
        Map<String,String> accnCurrMap = new  Map<String,String>(); 
        for(Account tmpAcn : accn){
            accnCurrMap.put(tmpAcn.Id,tmpAcn.CurrencyIsoCode);
        }
        
        
        List<Commission_Model__c> existentModel = [SELECT Id,Country__c,Reseller_Id__c,Start_Date__c,End_Date__c,RecordTypeId,Sub_Reseller_Id__c  FROM Commission_Model__c WHERE Reseller_Id__c IN : resellerIdSet];
        Map<String,List<Commission_Model__c>> existentModelMap = new Map<String,List<Commission_Model__c>>();
        
        for(Commission_Model__c exModel : existentModel){
            if(existentModelMap.containsKey(exModel.Reseller_Id__c)){
                List<Commission_Model__c> tmpComm = existentModelMap.get(exModel.Reseller_Id__c);
                tmpComm.add(exModel);
                existentModelMap.put(exModel.Reseller_Id__c,tmpComm);
            }
            else{
                List<Commission_Model__c> tmpComm = new List<Commission_Model__c>();
                tmpComm.add(exModel);
                existentModelMap.put(exModel.Reseller_Id__c,tmpComm);
            }
        }
        
        for(Commission_Model__c modelCountry : triggerNew){
            
            Boolean currencyCheck = true;
            
            if(modelCountry.RecordTypeId == directRecordTypeId){//Direct
                if(accnCurrMap.containsKey(modelCountry.Reseller_Id__c)){
                    modelCountry.CurrencyIsoCode = accnCurrMap.get(modelCountry.Reseller_Id__c);
                }
            }
            else{//Indirect
                String resCurr = '';
                String subResCurr = '';
                if(accnCurrMap.containsKey(modelCountry.Reseller_Id__c)){
                    resCurr = accnCurrMap.get(modelCountry.Reseller_Id__c);
                }
                
                if(accnCurrMap.containsKey(modelCountry.Sub_Reseller_Id__c)){
                    subResCurr = accnCurrMap.get(modelCountry.Sub_Reseller_Id__c);
                }
                
                if(resCurr != subResCurr){
                    modelCountry.addError('Please check the currency of the selected Reseller');
                    currencyCheck = false;
                }
                else{
                    if(accnCurrMap.containsKey(modelCountry.Reseller_Id__c))
                    modelCountry.CurrencyIsoCode = accnCurrMap.get(modelCountry.Reseller_Id__c);      
                }
            }
            
            if(currencyCheck){   
                if(modelCountry.Start_Date__c < modelCountry.End_Date__c ){
                    if(existentModelMap.containsKey(modelCountry.Reseller_Id__c)){
                        Boolean checkResult = true;
                        List<Commission_Model__c> tmpCommCheck = existentModelMap.get(modelCountry.Reseller_Id__c);
                        for(Commission_Model__c check : tmpCommCheck){
                            if(check.Country__c == modelCountry.Country__c && check.Sub_Reseller_Id__c == modelCountry.Sub_Reseller_Id__c){
                                if(!((modelCountry.Start_Date__c < check.Start_Date__c  && modelCountry.End_Date__c < check.Start_Date__c)
                                  || (modelCountry.Start_Date__c > check.End_Date__c  && modelCountry.End_Date__c > check.End_Date__c)
                                  ) && modelCountry.Id != check.Id && check.RecordTypeId == modelCountry.RecordTypeId
                                  ){
                                  modelCountry.addError('It is not possible to update New Commission Model with inserted information. Please, check: Country,Start Date,End Date');                          
                                    checkResult = false;
                                }         
                            }
                        }
                        
                        if(checkResult){
                            tmpCommCheck.add(modelCountry);
                            existentModelMap.put(modelCountry.Reseller_Id__c,tmpCommCheck);
                        }
                        
                    }
                    else{
                        List<Commission_Model__c> tmpCommCountry = new List<Commission_Model__c>();
                        tmpCommCountry.add(modelCountry);
                        existentModelMap.put(modelCountry.Reseller_Id__c,tmpCommCountry);
                    }
                }
                else
                    modelCountry.addError('It is not possible to update New Commission Model with inserted information. Please, check: Country,Start Date,End Date');
            }
        }
        
    }
    
    public void onAfterInsert( List<Commission_Model__c> triggerNew){
        system.debug('CommissionItemsTriggerHandler---------onAfterInsert--------START');
        Set<String> accnId = new Set<String>();
        Map<String,String> accnCurrency = new Map<String,String>();
        
        for(Commission_Model__c tmpCommModel : triggerNew){
        	accnId.add(tmpCommModel.Reseller_Id__c);
        }
        
        List<Account> accnList = [SELECT Id,CurrencyIsoCode FROM Account WHERE Id IN :accnId];
        
        for(Account tmpAccn : accnList){
            accnCurrency.put(tmpAccn.Id,tmpAccn.CurrencyIsoCode);
        }
        
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Commission_Item__c.Transaction_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getValue());
        }
        
        List<Commission_Item__c> commissionItemList = new List<Commission_Item__c>();
        for(Commission_Model__c commissionModel : triggerNew){
            String curreIsoCode = '';
            if(accnCurrency.containsKey(commissionModel.Reseller_Id__c)){
                curreIsoCode = accnCurrency.get(commissionModel.Reseller_Id__c);
            }
            
         	for(String s : pickListValuesList){
            	Commission_Item__c commissionItem = new Commission_Item__c();
                if(curreIsoCode != '' && curreIsoCode != null)
                    commissionItem.CurrencyIsoCode = curreIsoCode;
                commissionItem.Commission_Model__c = commissionModel.Id;
                commissionItem.Transaction_Type__c = s;
                switch on s {
                    when 'FIRST_EQUIPMENT_ACTIVATION_FEE'  {commissionItem.Name = 'First Equipment Activation Fee';}
                    when 'FIRST_OPTION_FEE'  {commissionItem.Name = 'First Option Fee';}
                    when 'ADD_ON_FEE'  {commissionItem.Name = 'Add-On Fee';}
                    when 'FIRST_PLAN_FEE'  {commissionItem.Name = 'First Plan Fee';}
                    when 'PLAN_RENEWAL_FEE'  {commissionItem.Name = 'Plan Renewal Fee';}
                    when 'OPTION_RENEWAL_FEE'  {commissionItem.Name = 'Option Renewal Fee';}
        		}
            	if(s=='FIRST_EQUIPMENT_ACTIVATION_FEE') commissionItem.Commission_Amount__c = 5;
                else if(s=='FIRST_PLAN_FEE') commissionItem.Commission_Percentage__c = 25;
                else commissionItem.Commission_Percentage__c = 20;
                commissionItemList.add(commissionItem);
            }
        }
		if(commissionItemList.size()>0)
            insert commissionItemList;
  
    }
}