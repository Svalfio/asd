@isTest
public class Test_VF_ViewPrefilledInvoice {
    
    @testSetup
    static void createDataSet(){
		
		Id resellerRTId=  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_RESELLER).getRecordTypeId();  
        Address address = new Address();
        Account reseller =  new Account(Name = 'Reseller',
                                        RecordTypeId=resellerRTId,
                                        //billingCountry = 'Country',
                                        billingCountry = 'Congo',
                                        //billingCountryCode = 'CountryCode',
                                        billingState = 'State',
                                        //billingStateCode = 'StateCode',
                                        billingStreet = 'Street',
                                        billingPostalCode = 'PostalCode'
                                        //billingGeocodeAccuracy = 'getGeocodeAccuracy'
                                        );  
        
        insert reseller;
        
        Id customerRTId=  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER).getRecordTypeId();
        Account customer = new Account(Name='Customer', RecordTypeId=customerRTId);
        insert customer;
        
        Commission_Model__c commissionModelReseller= new Commission_Model__c(Reseller_Id__c=reseller.id,Country__c='Congo');
        insert commissionModelReseller;
        
        Product2 Product = new Product2(Name='TestProduct',Family='Option');
        Product2 ProductTerminal = new Product2(Name='TestProduct',Family='Plan');
        
        insert Product;
        insert ProductTerminal;
        
        zqu__ProductRatePlan__c TestPRPPercentage = new zqu__ProductRatePlan__c(zqu__Product__c=Product.Id);
        zqu__ProductRatePlan__c TestPRPAmount = new zqu__ProductRatePlan__c(zqu__Product__c=ProductTerminal.Id);
        
        insert TestPRPPercentage;
        insert TestPRPAmount;
        
                
        Zuora__Subscription__c subPercentage = new Zuora__Subscription__c();
        insert subPercentage;
        
        Zuora__Subscription__c subAmount = new Zuora__Subscription__c();
        insert subAmount;
        
        zqu__Quote__c TestQuotePercentage = new zqu__Quote__c(zqu__subscriptiontype__c='ADD_ON_FEE',zqu__Account__c=reseller.Id,zqu__zuorasubscriptionid__c=subPercentage.Id);
        zqu__Quote__c TestQuoteAmount = new zqu__Quote__c(zqu__subscriptiontype__c='FIRST_EQUIPMENT_ACTIVATION_FEE',zqu__Account__c=reseller.Id,zqu__zuorasubscriptionid__c=subAmount.Id);
        
        insert TestQuotePercentage;
        insert TestQuoteAmount;
        
        zqu__QuoteAmendment__c amPerc = new zqu__QuoteAmendment__c(zqu__Quote__c=TestQuotePercentage.Id);
		insert amPerc;
        
        zqu__QuoteAmendment__c amAm = new zqu__QuoteAmendment__c(zqu__Quote__c=TestQuoteAmount.Id);
		insert amAm;
        
        Order TestOrderPercentage = new Order(Zuora_Quote__c=TestQuotePercentage.Id,AccountId=customer.Id,EffectiveDate=System.today(),Status='Draft',Subscription_Id__c=subPercentage.Id);
        Order TestOrderAmount = new Order(Zuora_Quote__c=TestQuoteAmount.Id,AccountId=customer.Id,EffectiveDate=System.today(),Status='Draft',Subscription_Id__c=subAmount.Id);
        
        insert TestOrderPercentage;
        insert TestOrderAmount;

        zqu__QuoteRatePlan__c TestQRPPercentage = new zqu__QuoteRatePlan__c(zqu__Quote__c=TestQuotePercentage.Id,zqu__QuoteAmendment__c=amPerc.Id);
        zqu__QuoteRatePlan__c TestQRPAmount = new zqu__QuoteRatePlan__c(zqu__Quote__c=TestQuoteAmount.Id,zqu__QuoteAmendment__c=amAm.Id);
        
        insert TestQRPPercentage;
        insert TestQRPAmount;
        
        zqu__QuoteRatePlanCharge__c TestQRPCPercentage = new zqu__QuoteRatePlanCharge__c(zqu__QuoteRatePlan__c=TestQRPPercentage.Id,zqu__ProductName__c='TestProduct',zqu__total__c=1000,zqu__Quantity__c=1);
        zqu__QuoteRatePlanCharge__c TestQRPCAmount = new zqu__QuoteRatePlanCharge__c(zqu__QuoteRatePlan__c=TestQRPAmount.Id,zqu__ProductName__c='TestProduct',zqu__total__c=1000,zqu__Quantity__c=1);
        
		insert TestQRPCPercentage;
        insert TestQRPCAmount;
        
        zqu__ProductRatePlanCharge__c TestPRPCPercentage = new zqu__ProductRatePlanCharge__c(zqu__ProductRatePlan__c=TestPRPPercentage.Id,VAT_Rate__c=5);
        zqu__ProductRatePlanCharge__c TestPRPCAmount = new zqu__ProductRatePlanCharge__c(zqu__ProductRatePlan__c=TestPRPAmount.Id,VAT_Rate__c=5);
        
        insert TestPRPCPercentage;
        insert TestPRPCAmount;
        
    }
	@isTest
    static void test1(){
        
        test.startTest();
        Account reseller = [Select id from Account limit 1];
        ApexPages.currentPage().getParameters().put('prefilledInvoiceId',reseller.Id+'-'+'Congo'+'-'+'11'+'-'+'2019');
    	VF_ViewPrefilledInvoice t = new VF_ViewPrefilledInvoice();
        test.stopTest();
    }    
}