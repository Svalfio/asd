public class LC_CampaignInf_H_ContactController {
    public List<Campaign_Member_History__c> campaignMemberHistory{get;set;}
    
    public pageReference DownloadInformations(){
        String recordId = apexpages.currentpage().getparameters().get('recordId');
        System.debug(recordId);
        campaignMemberHistory = [SELECT Campaign_Id__c, Campaign_Name__c, Join_Campaign_Date__c, Leave_Campaign_Date__c FROM Campaign_Member_History__c WHERE Contact_Id__c =:recordId];
        return null;
    }


    
    
}