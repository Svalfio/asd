global class PlanCustomFilter implements zqu.QuickListController.IHierarchyAllowedRecordPlugin{
    
    global zqu.QuickListController.PluginResult getAllowedRecords
        (Map<Object, Object> contextIds, Map<String, String> pluginParams){
            system.debug('contextIds: '+contextIds);
            zqu.QuickListController.PluginResult result = new zqu.QuickListController.PluginResult();    
            result.records = new List<zqu.QuickListController.PluginRecord>();
            zqu.QuickListController.PluginRecord record;
            
            List<zqu__Quote__c> QuoteList = new List<zqu__Quote__c>();
            List<Account> ParentAccount = new List<Account>();
            List<String> RatePlanSubscription = new List<String>();
            //Id quoteId = (Id) contextIds.get('qid');
            Id quoteId = (contextIds.containskey('qid') && contextIds.get('qid')!=null)? (Id) contextIds.get('qid'):null;
            Map<Id, List<Id>> Product_RatePlans = new Map<Id, List<Id>>();
            
            system.debug('QuoteiD= '+quoteId);
            if(quoteId!=null){
                try{
                    QuoteList = [SELECT zqu__Account__c, Customer_Subscription_ID__c,zqu__Account__r.Parent.SIP_Option__c, 
                                 zqu__Account__r.Billing__c, Owner.Username FROM zqu__Quote__c WHERE Id =: quoteId]; 
                    
                    system.debug('QuoteList= '+QuoteList);
                    if(QuoteList.size()>0){
                        
                        ParentAccount = [SELECT id, Headquarter_Country__c FROM Account where username__c=: QuoteList[0].Owner.Username]; //da modificare per prendere l'account associato direttamente dal campo sul partner user
                        
                        system.debug('Parent Account:'+ParentAccount);
                        if(ParentAccount.size()>0){
                            String billingType = QuoteList[0].zqu__Account__r.Billing__c;                   
                            if(billingType=='Retail'){
                                List<zqu__ProductRatePlan__c> ProdRatePlanList = new List<zqu__ProductRatePlan__c>();
                                List<Eligible_Partner__c> Eligible_Partner_Product = new List<Eligible_Partner__c>();
                                
                                ProdRatePlanList = [Select id, zqu__Product__c FROM zqu__ProductRatePlan__c WHERE zqu__Product__r.Status__c='Active' 
                                                    AND zqu__Product__r.Family='Plan' AND zqu__Product__r.Product_Type__c=: QuoteList[0].zqu__Account__r.Billing__c AND
                                                    Partner__c='All']; //all rateplan related to active, retail, plan product eligible for all partner
                                Eligible_Partner_Product = [Select id, Product_Rate_Plan__c, Product_Rate_Plan__r.zqu__Product__c from Eligible_Partner__c where Partner__c=: ParentAccount AND
                                                            Product_Rate_Plan__r.zqu__Product__r.Status__c='Active' AND Product_Rate_Plan__r.zqu__Product__r.Family='Plan'
                                                            AND Product_Rate_Plan__r.zqu__Product__r.Product_Type__c=: QuoteList[0].zqu__Account__r.Billing__c ];
                                for(zqu__ProductRatePlan__c pr : ProdRatePlanList){
                                    List<id> RatePlanIds = new List<id>();
                                    if(Product_RatePlans.containskey(pr.zqu__Product__c)){
                                        RatePlanIds.addAll(Product_RatePlans.get(pr.zqu__Product__c));
                                    }
                                    RatePlanIds.add(pr.id);
                                    Product_RatePlans.put(pr.zqu__Product__c,RatePlanIds);
                                }
                                for(Eligible_Partner__c ep : Eligible_Partner_Product){
                                    List<id> RatePlanIds = new List<id>();
                                    if(Product_RatePlans.containskey(ep.Product_Rate_Plan__r.zqu__Product__c)){
                                        RatePlanIds.addAll(Product_RatePlans.get(ep.Product_Rate_Plan__r.zqu__Product__c));
                                    }
                                    RatePlanIds.add(ep.Product_Rate_Plan__c);
                                    Product_RatePlans.put(ep.Product_Rate_Plan__r.zqu__Product__c,RatePlanIds);
                                }
                                system.debug('Product_RatePlans map: '+Product_RatePlans); 
                            }
                            if(billingType=='Wholesale'){
                                
                                WrapperCustomSettingObject wrapperObject = getAreaManagement();
                                System.debug('wrapperObject: '+wrapperObject);
                                Map<String,String> AreaMngmnt_Map = new Map<String,String>();
                                String ManagedArea ='';
                                if(String.isNotBlank(wrapperObject.Error_Message)){
                                    QuoteList[0].addError(wrapperObject.Error_Message);
                                }else{
                                    AreaMngmnt_Map = wrapperObject.AreaManagement_Map;
                                }
                                system.debug('AreaMngmnt_Map'+AreaMngmnt_Map.keySet());
                                if(AreaMngmnt_Map.containsKey(ParentAccount[0].Headquarter_Country__c)){
                                    ManagedArea = AreaMngmnt_Map.get(ParentAccount[0].Headquarter_Country__c);
                                }else ManagedArea='Other';
                                
                                system.debug('ManagedArea:'+ManagedArea);
                                for(Zuora__SubscriptionRatePlan__c sub : [SELECT id, Zuora__ProductRatePlanId__c FROM Zuora__SubscriptionRatePlan__c WHERE
                                                                          Zuora__Subscription__c=: QuoteList[0].Customer_Subscription_ID__c AND
                                                                          Zuora__ProductRatePlanId__c!='']){
                                                                              system.debug('RatePlanSubscription:'+sub.Zuora__ProductRatePlanId__c);
                                                                              RatePlanSubscription.add(sub.Zuora__ProductRatePlanId__c);
                                                                          }
                                
                                if(RatePlanSubscription.size()>0){
                                    for(zqu__ProductRatePlan__c ProdRatePlan : [SELECT id, zqu__Product__c,Area__c FROM zqu__ProductRatePlan__c 
                                                                                WHERE zqu__ZuoraId__c =:RatePlanSubscription AND zqu__Product__r.Status__c != 'Inactive' 
                                                                                AND zqu__Product__r.Family='Plan' AND zqu__Product__r.Product_Type__c=: QuoteList[0].zqu__Account__r.Billing__c 
                                                                                AND zqu__Product__c!=null AND Area__c=:ManagedArea ]){
                                                                                    List<Id> RatePlansList = new List<Id>();
                                                                                    if(Product_RatePlans.containsKey(ProdRatePlan.zqu__Product__c)){
                                                                                        RatePlansList.addAll(Product_RatePlans.get(ProdRatePlan.zqu__Product__c));
                                                                                    }
                                                                                    RatePlansList.add(ProdRatePlan.id);
                                                                                    Product_RatePlans.put(ProdRatePlan.zqu__Product__c,RatePlansList);
                                                                                }
                                    
                                    system.debug('Product_RatePlans: '+Product_RatePlans);
                                    
                                    
                                }
                                
                            }
                            
                            if(!Product_RatePlans.IsEmpty()) {
                                
                                for(id ProdId : Product_RatePlans.keySet()){
                                    record = new zqu.QuickListController.PluginRecord();
                                    record.recordId = ProdId;
                                    record.relatedObjectIds = new Map<String, List<ID> >{'zqu__productrateplan__c' => Product_RatePlans.get(ProdId)};
                                        result.records.add(record); 
                                }
                            }
                        }
                    }
                }catch(exception e){
                    System.debug('Error in PlanCustomFilter Class: '+e.getMessage()+' Line Number: '+e.getLineNumber());
                }
                
            }
            system.debug('Result: '+result);
            return result;
            
        }
    
    public static WrapperCustomSettingObject getAreaManagement(){
        List<GeographicArea_Manage__c> AreaManagementList = new List<GeographicArea_Manage__c>();
        WrapperCustomSettingObject wrapperObj;
        Map<String,String> AreaManagement_Map = new Map<String,String>();
        String ErrorMessage = '';
        try{
            for(GeographicArea_Manage__c singleobj : [SELECT Id, Name, Area__c, Distributor_Country__c FROM GeographicArea_Manage__c]){
                AreaManagement_Map.put(singleobj.Distributor_Country__c,singleobj.Area__c );
                system.debug('AreaManagement_Map: '+AreaManagement_Map);
            }
            wrapperObj = new WrapperCustomSettingObject(AreaManagement_Map,ErrorMessage);
            system.debug('wrapperObj: '+wrapperObj);
        }catch(Exception e){
            ErrorMessage = 'Error in Area Management Table. Error message: '+e.getMessage();
            System.debug('ErrorMessage: '+ErrorMessage);
            wrapperObj = new WrapperCustomSettingObject(AreaManagement_Map,ErrorMessage);
        }
        
        return wrapperObj;
    }
    
    
    public class WrapperCustomSettingObject{
        Map<String,String> AreaManagement_Map {get;set;}
        String Error_Message {get;set;}
        public WrapperCustomSettingObject (Map<String,String> AreaManagement_Map,String Error_Message){
            this.AreaManagement_Map = AreaManagement_Map;
            this.Error_Message = Error_Message;
        }
    }
}