public class CampaignMemberHistoryTriggerHandler {

    public void onBeforeDelete(List<CampaignMember> triggerOld){
        List<String> campaignMemberDeletedIds = new List<String>();
        for(CampaignMember cm: triggerOld ){
            campaignMemberDeletedIds.add(cm.Id);    
        }
        List<Campaign_Member_History__c> campaignMemberHistoryDeleted = new List<Campaign_Member_History__c>();
        campaignMemberHistoryDeleted = [SELECT Id, Name, Leave_Campaign_Date__c FROM Campaign_Member_History__c WHERE Id IN :campaignMemberDeletedIds];
        for(Campaign_Member_History__c cmh: campaignMemberHistoryDeleted){
            cmh.Leave_Campaign_Date__c = Date.today();
        }
        update campaignMemberHistoryDeleted;
    }
    
    public void onAfterInsert(List<CampaignMember> triggerNew){
        List<Campaign_Member_History__c> newCampaignMemberHistory = new List<Campaign_Member_History__c>();
        for(CampaignMember cm: triggerNew ){
            Campaign_Member_History__c cmh =  new Campaign_Member_History__c();
            cmh.Campaign_Id__c = cm.CampaignId;
            cmh.Campaign_Member_Id__c = cm.Id;
            cmh.Campaign_Name__c = cm.Campaign.Name;
            cmh.Join_Campaign_Date__c = Date.today();
            if(cm.ContactId != null){
                cmh.Contact_Id__c = cm.ContactId;
            }else{
                cmh.Lead_Id__c=cm.LeadId;
            }
            newCampaignMemberHistory.add(cmh);
        }
        insert newCampaignMemberHistory;
    }
}