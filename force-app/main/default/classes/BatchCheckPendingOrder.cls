global class BatchCheckPendingOrder  implements  Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts   {

    global List<Order> orders= new List<Order>();
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        OrderPendingMaxDays__c mc = OrderPendingMaxDays__c.getInstance('MaxDays');
        Integer maxDays = (Integer)(mc.MaxDays__c);
        Datetime d=system.today();
        d = d - maxDays;      
        return Database.getQueryLocator('SELECT id, CreatedDate, Status FROM Order WHERE Status = \'Pending\' AND  CreatedDate <:d');
    }
    
    
    global void execute(Database.BatchableContext bc, List<sObject> scope){
        Order[] orders = (Order[]) scope;
        delete orders;
    }
    
    global void finish(Database.BatchableContext bc){
        
        System.debug(('Codice eseguito'));
        
    }    
}