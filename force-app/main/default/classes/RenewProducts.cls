global class RenewProducts implements zqu.QuickListController.IHierarchyAllowedRecordPlugin {
    global zqu.QuickListController.PluginResult getAllowedRecords (Map<Object, Object> contextIds, Map<String, String> pluginParams){
            
        zqu.QuickListController.PluginResult result = new zqu.QuickListController.PluginResult();    
        result.records = new List<zqu.QuickListController.PluginRecord>();
		
        Id quoteId = null;
        List<zqu__Quote__c> QuoteList = new List<zqu__Quote__c>();
        List<String> ZuoraLiveRatePlanIds = new List<String>();
		List<zqu__ProductRatePlan__c> listProductRatePlanToView = new List<zqu__ProductRatePlan__c>();  
      	for (Object k : contextIds.keySet()) {quoteId = (Id) contextIds.get(k);}              
        
        if(quoteId != null){
            try{
                    QuoteList = [SELECT zqu__Account__c, zqu__ExistSubscriptionID__c FROM zqu__Quote__c WHERE Id =: quoteId]; 
                    if(QuoteList.size()>0){
                        for(Zuora__SubscriptionRatePlan__c subRatePlan : [SELECT Zuora__OriginalProductRatePlanId__c, Status_Calc__c
                                                                  FROM Zuora__SubscriptionRatePlan__c 
                                                                  WHERE Zuora__Subscription__r.Zuora__Zuora_Id__c =: QuoteList[0].zqu__ExistSubscriptionID__c]){
                                                                      
                            	if(subRatePlan.Status_Calc__c == 'Active')
                        			ZuoraLiveRatePlanIds.add(subRatePlan.Zuora__OriginalProductRatePlanId__c);
                        }
                            
                        if(ZuoraLiveRatePlanIds.size()>0){
                            for(zqu__ProductRatePlan__c prodRatePlan : [SELECT Id, zqu__Product__r.Volume_GB__c, zqu__Product__r.Family, zqu__Product__r.zqu__Type__c, zqu__ZuoraId__c, zqu__Product__c 
                                                                        FROM zqu__ProductRatePlan__c 
                                                                        WHERE zqu__ZuoraId__c =:ZuoraLiveRatePlanIds]){
                                                                            
                                    if(prodRatePlan.Id != null && 
                                       prodRatePlan.zqu__Product__r.Family != 'Bonus' && 
									   prodRatePlan.zqu__Product__r.Family != 'Fee' && 
                                       prodRatePlan.zqu__Product__r.Family != 'Promotion'){
                                		listProductRatePlanToView.add(prodRatePlan);
                                    }  
                            }
                        }
                    }
            	}   	
            	catch(exception e){
                	System.debug('Error in PlanCustomFilter Class: '+e.getMessage());
            	}
            
            for(zqu__ProductRatePlan__c tempProducts : listProductRatePlanToView){     
                String tProductId = tempProducts.zqu__Product__c;
                List<Id> ratePlanIdOk = new List<Id>();
                    
                for(zqu__ProductRatePlan__c tempRatePlan : listProductRatePlanToView){
                    if (tempRatePlan.zqu__Product__c == tProductId){
                        ratePlanIdOk.add(tempRatePlan.Id);
                    }   
                }
                
                zqu.QuickListController.PluginRecord record = new zqu.QuickListController.PluginRecord();
                record.recordId = tProductId;
                record.relatedObjectIds = new Map<String, List<ID>
                >{'zqu__productrateplan__c' => ratePlanIdOk};
                result.records.add(record);  
        	}         
		}
        return result;
    }
}