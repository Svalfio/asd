@isTest
public class ObjectRecordTableController_test {
    
    @testSetup static void createData() {

        Id DistributorRTId =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_DISTRIBUTOR).getRecordTypeId();
		account Distributor = new Account(Name='Distributor1',RecordTypeId=DistributorRTId);
        insert Distributor;
        
        List<Zuora__Subscription__c> testSubscriptionList = new List<Zuora__Subscription__c>();
        integer i=0;
        do{
            Zuora__Subscription__c sub = new Zuora__Subscription__c(Name ='TestSub'+1,Country__c='Cote d\'Ivoire', Zuora__Account__c=Distributor.id);
            testSubscriptionList.add(sub);
            i++;
        }while(i<10);
        
        insert testSubscriptionList;
        system.debug('testSubscriptionList: '+testSubscriptionList);
    }
    
    @isTest
    public static void getMetadata2_test(){
        String MetadataName='GenericLightningTableInfo__mdt';
        String sObjectType='Zuora__Subscription__c';
        String OrderField='Order__c';
        String UsedField='{\'fieldName__c\',\'iconName__c\',\'label__c\',\'type__c\'}';
        ObjectRecordTableController.getMetadata2(MetadataName,sObjectType,OrderField,UsedField);
    }
    
    @isTest
    public static void getColumns_test(){
        String MetadataName='GenericLightningTableInfo__mdt';
        String sObjectType='Zuora__Subscription__c';
        String UsedField='{\'fieldName__c\',\'iconName__c\',\'label__c\',\'type__c\'}';
        String Metadata2='';
        List<GenericLightningTableInfo__mdt> TableInfoList = new List<GenericLightningTableInfo__mdt>();
        TableInfoList = [Select id, fieldName__c,iconName__c,label__c,type__c from GenericLightningTableInfo__mdt];
        Metadata2=JSON.serialize(TableInfoList);
        
        ObjectRecordTableController.getColumns(Metadata2, sObjectType, MetadataName, UsedField);
    }
    
    @isTest
    public static void getData_test(){
        String MetadataName='GenericLightningTableInfo__mdt';
        String sObjectType='Zuora__Subscription__c';
        String UsedField='{\'fieldName__c\',\'iconName__c\',\'label__c\',\'type__c\'}';
        String MetaData2='';
        String MetadataFilterField='fieldName__c';
        String whereConditions='';
        List<GenericLightningTableInfo__mdt> TableInfoList = new List<GenericLightningTableInfo__mdt>();
        TableInfoList = [Select id, fieldName__c,iconName__c,label__c,type__c from GenericLightningTableInfo__mdt];
        MetaData2=JSON.serialize(TableInfoList);
        List<Account> Accounts = new List<Account>([select id, name from account limit 1]);
        if(Accounts.size()>0) whereConditions=' Zuora__Account__c = \''+Accounts[0].Id+'\'';
      

        ObjectRecordTableController.getData(sObjectType, MetaData2, whereConditions, MetadataFilterField, MetadataName);
    }
}