public class SendGeneratedPDF {

@AuraEnabled
public static void sendMail(String attachmentId, String emailAddress, String name){
    system.debug('Send email');
	// Attachment attachment = [SELECT id, name FROM Attachment WHERE ParentId = 'a1J6E000002G2WHUA0'];
	Attachment attachment = [SELECT id, name, ContentType, body FROM Attachment WHERE Id =: attachmentId];
	system.debug(attachment.id);
	Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
	attach.setContentType('application/pdf');
	attach.setFileName(attachment.Name);
	attach.setInline(false);
	// attach.setBody(Blob.valueOf(attachment.ContentType));
	attach.setBody(attachment.body);
    
    String nameAttach = attachment.Name;
    nameAttach = nameAttach.split('.pdf')[0];
    system.debug('name file: '+ nameAttach);

	Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
	message.toAddresses = new String[] {emailAddress};
	message.subject = '[KONNECT] Thank you for your order! Invoice ' + nameAttach;
	message.plainTextBody =  'Hello '+name+',\nPlease find attached your invoice number '+nameAttach+'.\n\nKind regards,\nKonnect Team';
	message.setFileAttachments(new Messaging.EmailFileAttachment[] { attach });
	Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
	Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
	if (results[0].success) {
		System.debug('The email was sent successfully.');
	} else {
		System.debug('The email failed to send: ' + results[0].errors[0].message);
	}
}

}