public with sharing class LC_SendActivationCodeController {
    public LC_SendActivationCodeController() {

    }

    @AuraEnabled
    public static void SendActivationCode(String AccountId, String ActivationCode){
        List<Notification_Item__c> notification_items = new List<Notification_Item__c>();
        Account acc = new Account();
        Map<String, Notification_Item__c> notification_map = new Map<String, Notification_Item__c>();

        acc = [SELECT Id, Name, ParentId, Parent.Notification_Setting__c FROM Account WHERE Id =: AccountId];
        notification_items = [SELECT id, Notification_Setting__c, Event_Type__r.Event_Code__c, Event_Type__r.Name FROM Notification_Item__c WHERE Notification_Setting__c =: acc.Parent.Notification_Setting__c];

        for(Notification_Item__c n: notification_items){
            system.debug(n.Event_Type__r.Name.split('_0')[1]);
            if(!notification_map.containsKey(n.Notification_Setting__c+'_0'+ n.Event_Type__r.Name.split('_0')[1])){
                notification_map.put(n.Notification_Setting__c+'_0'+ n.Event_Type__r.Name.split('_0')[1], n);
            }
        }

        if(notification_map.get(acc.Parent.Notification_Setting__c+'_008').Event_Type__r.Event_Code__c != null){
            Map<String, String> parametersMap = new Map<String, String>{'subject' => ActivationCode};
            SfToHerokuAPIManager.sendSingleNotificationRequest richiesta = new SfToHerokuAPIManager.sendSingleNotificationRequest();
            richiesta.accountId = String.valueOf(AccountId);
            richiesta.channel = 'SMS';
            richiesta.eventCode = notification_map.get(acc.Parent.Notification_Setting__c+'_008').Event_Type__r.Event_Code__c;
            richiesta.parametersMap = parametersMap;
            SFtoHerokuAPIManager.HerokuResponse response = SfToHerokuAPIManager.sendSingleNotification(richiesta);
            System.debug(response.errorCode);
        }
    }
}