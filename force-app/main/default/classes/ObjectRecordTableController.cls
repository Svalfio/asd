public class ObjectRecordTableController {

@AuraEnabled
public static String getMetadata2(String MetadataName, String sObjectType, String OrderField, String UsedField){

	Map<String, Schema.SObjectField> MetaData_Field_Map = new Map<String, Schema.SObjectField>();
	Map<string,string> MapselectFields = new Map<string,string>(); //field used to query metadata record
	List<sObject> MetaDataList = (List<sObject>)Type.forName('List<'+MetadataName+'>').newInstance();//list of metadata records existing inside the database
	List<string> ListselectFields = new List<string>();

	String queryString = '';
	String JSONMetadata='';

	try{
		MetaData_Field_Map = Schema.getGlobalDescribe().get(MetadataName.toLowerCase()).getDescribe().Fields.getMap();
	}catch(exception e) {
		system.debug('Error during metadata retrieving. Metadata Name: '+MetadataName +'/r Error Message: '+e.getMessage());
	}

	if (MetaData_Field_Map != null) {
		for (Schema.SObjectField ft : MetaData_Field_Map.values()) {
			if(ft.getDescribe().getName().contains('__c') && UsedField.contains(ft.getDescribe().getName())) MapselectFields.put(ft.getDescribe().getName(),ft.getDescribe().getLabel());
		}
		ListselectFields.addAll(MapselectFields.keyset());
		queryString= String.join(ListselectFields,',');
	}

	if(queryString!='') {
		String WhereClause = 'sObjectType__c =\''+sObjectType+'\'';
		String OrderClause ='';
		if(OrderField!='') OrderClause = ' order by '+OrderField;
		queryString = 'select '+ queryString.removeEnd(',')+' from '+ MetadataName +' where '+WhereClause+OrderClause;
		try{
			MetaDataList = Database.query(queryString);
			system.debug('Object Type: '+((List<SObject>)MetaDataList).getSObjectType().getDescribe().getName()+'');
			JSONMetadata= JSON.serialize(MetaDataList);
		}catch(exception e) {
			System.debug('Error in metadata '+ MetadataName+' query: '+e.getMessage());
		}
	}
	system.debug('JSONMetadata: '+JSONMetadata);
	return JSONMetadata;

}

@AuraEnabled
public static String getColumns(String Metadata2, String sObjectType, String MetadataName, String UsedField){
	system.debug('Metadata2: '+Metadata2);
	List<sObject> MetaDataList = (List<sObject>)Type.forName('List<'+MetadataName+'>').newInstance();
	system.debug('Metadata get: '+Metadata2);
	MetaDataList = (List<sObject>)Json.deserialize(MetaData2,List<sObject>.class);
	list<map<string,object> > mappa_final =new list<map<string,object> >();
	Map<string,string> MapselectFields = new Map<string,string>(); //field used to query metadata record
	List<string> ListselectFields = new List<string>();
	String queryString='';
	Map<String, Schema.SObjectField> MetaData_Field_Map = new Map<String, Schema.SObjectField>();
	string JSONcolumns;

	try{
		MetaData_Field_Map = Schema.getGlobalDescribe().get(MetadataName.toLowerCase()).getDescribe().Fields.getMap();
	}catch(exception e) {
		system.debug('Error during metadata retrieving. Metadata Name: '+MetadataName +'/r Error Message: '+e.getMessage());
	}

	if (MetaData_Field_Map != null) {
		for (Schema.SObjectField ft : MetaData_Field_Map.values()) {
			if(ft.getDescribe().getName().contains('__c') && UsedField.contains(ft.getDescribe().getName())) MapselectFields.put(ft.getDescribe().getName(),ft.getDescribe().getLabel());
		}
		ListselectFields.addAll(MapselectFields.keyset());
		queryString= String.join(ListselectFields,',');
	}

	try{
		for(sobject MD_Record : MetaDataList) {
			map<string,object> tmp = new map<string,object>();
			for (string f : MapselectFields.keyset()) {
				tmp.put(MapselectFields.get(f),MD_Record.get(f));
			}
			mappa_final.add(tmp);
		}
		JSONcolumns = JSON.serialize(mappa_final);
		System.debug('JSON String for Header:'+JSONcolumns);

	}catch(exception e) {
		system.debug('Error message: '+e.getMessage());
	}

	return JSONcolumns;
}

@AuraEnabled
// , String fieldSet
public static String getData(String sObjectType, String MetaData2, String whereConditions, String MetadataFilterField, String MetadataName){
	List<sObject> MetaDataList = (List<sObject>)Type.forName('List<'+MetadataName+'>').newInstance();
	List<sObject> DataList = (List<sObject>)Type.forName('List<'+sObjectType+'>').newInstance();


	//String fieldSetObj = Schema.getGlobalDescribe().get(sObjectType).getDescribe().FieldSets.getMap().get(fieldSet).getFields().get(0).getFieldPath();

	MetaDataList = (List<sObject>)Json.deserialize(MetaData2,List<sObject>.class);
	String data;
	list<map<string,object> > mappa_final =new list<map<string,object> >();
	list<string> fieldToquery = new list<string>();
	system.debug('JSON Deserialize: '+MetaDataList);
	for(sobject s : MetaDataList) {
		fieldToquery.add((String)s.get(MetadataFilterField));
		System.debug('fieldToQuery ' + (String)s.get(MetadataFilterField));
	}

	if(whereConditions!='') whereConditions =' where '+whereConditions;
	system.debug('WHERECONDITION ' + whereConditions);
	//String queryString= 'select id,' + fieldSetObj + ',' +String.join(fieldToquery, ',')+' from '+sObjectType+whereConditions;
    String queryString= 'select id,' +String.join(fieldToquery, ',')+' from '+sObjectType+whereConditions;
	system.debug('queryString: '+queryString);
	DataList = Database.query(queryString);
	data = JSON.serialize(DataList);

	return data;
}

@AuraEnabled
public static String getMetadata2(String sObjectType, String ListSelectedRows){
	String selectedEntities='';
	return selectedEntities;
}


}