global class CallOutMockSetter implements HttpCalloutMock{
	
    String requestedCode;
    Object objectResp;
    
    global CallOutMockSetter(String code, Object obj){
        requestedCode = code;
        objectResp = obj;
    }
    
    global HTTPResponse respond(HTTPRequest req) {
        
        SfToHerokuAPIManager.HerokuResponse resp = new SfToHerokuAPIManager.HerokuResponse();
        
        if(requestedCode=='200'){
        resp.errorCode = 200;
        resp.message = 'SUCCESS';
        }
        else if(requestedCode=='201'){
            resp.errorCode = 201;
            resp.message = 'CREATED';
        }
        else if(requestedCode=='204'){
            resp.errorCode = 204;
            resp.message = 'NO_CONTENT';
        }
        else if(requestedCode=='400'){
            resp.errorCode = 400;
            resp.message = 'BAD_REQUEST';
        }
        else if(requestedCode=='400'){
            resp.errorCode = 400;
            resp.message = 'BAD_REQUEST';
        }
        resp.data = JSON.serialize(objectResp);
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(JSON.serialize(resp));
        res.setStatusCode(200);
        return res;
    }
}