public class ApexConstants {

    //Task Constants
    public static final String TASK_TYPE_WORK_ORDER='Work Order';
    
    
    //Product Constants
    public static final String PRODUCT_FAMILY='Equipment';
    
    //Order Constants
    public static final String ORDER_STATUS_APPROVED='Approved';
    
    //RecordTypes
    public static final STRING ACCOUNT_RECORDTYPE_DEVELOPERNAME_RESELLER='Reseller';
    public static final String ACCOUNT_RECORDTYPE_DEVELOPERNAME_SUBRESELLER='Sub_Reseller';
    public static final String ACCOUNT_RECORDTYPE_DEVELOPERNAME_DISTRIBUTOR='Distributor';
    public static final String ACCOUNT_RECORDTYPE_DEVELOPERNAME_CUSTOMER='Customer_User';
    
}