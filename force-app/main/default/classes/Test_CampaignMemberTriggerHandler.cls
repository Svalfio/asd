@isTest
public class Test_CampaignMemberTriggerHandler {
    
    @testSetup
    static void createData(){
        
        AllMySmsCustomSetting__c testCustomMySms = new AllMySmsCustomSetting__c(Name='AllMySms',Endpoint__c='urlFalse');
        insert testCustomMySms;
        
        Date myDate = date.newinstance(2019, 2, 17);
        Date myDate2 = date.newinstance(2019, 12, 20);
        
        Campaign testCampaign = new Campaign(Name='testCampaign',
                                            Channel__c='SMS;Partner_Portal;Phone;Customer_Portal',
                                            Status='In Progress',
                                            startDate=myDate,
                                            endDate=myDate2,
                                            SMS_Body__c='SMSTest',
                                            Web_Body__c='webTest');
        insert testCampaign;
        
        Account acc = new Account(Name='Acme');
        insert acc;
        
        Contact testContact = new Contact(FirstName='test',LastName='contact',AccountId=acc.id);
        insert testContact;
        
        Lead testLead = new Lead(LastName='testLead',Company='testACME',Status='New',CurrencyIsoCode = 'EUR');
       insert testLead;
    }
    
    @isTest
    static void onAfterInsertTest(){
        
        Id testCampaignId = [Select id from Campaign limit 1].id;
        Id testLeadId =[Select id from Lead limit 1].id;
        Id testContactId = [select id from Contact limit 1].id;
        
        CampaignMember cmLead = new CampaignMember(CampaignId=testCampaignId, LeadId= testLeadId);
        CampaignMember cmContact = new CampaignMember(CampaignId=testCampaignId,ContactId=testContactId);
        
        List<CampaignMember> campaignMembers = new List<CampaignMember>();
        campaignMembers.add(cmContact);
        
        // Use StaticResourceCalloutMock built-in class to
        // specify fake response and include response body 
        // in a static resource.
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('Test_CampaignMember');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        
        
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, mock);
        insert campaignMembers;
        Test.stopTest();
    }
    
    
    
    

}