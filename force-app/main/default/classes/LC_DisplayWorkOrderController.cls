public with sharing class LC_DisplayWorkOrderController {

	@AuraEnabled
	public static string getQuoteId(String subscriptionId){
        System.debug('call getQuoteId');
        System.debug(subscriptionId);
		Zuora__Subscription__c s = [SELECT id, Zuora__QuoteNumber__c FROM Zuora__Subscription__c WHERE Id =: subscriptionId];
		return [SELECT Id FROM zqu__Quote__c WHERE zqu__Number__c = : s.Zuora__QuoteNumber__c].Id;
}

	@AuraEnabled
	public static Contact retrieveContactInfo(String quoteId) {
		// Check to make sure all fields are accessible to this user
		String[] fieldsToCheck = new String[] {
			'Name', 'Phone', 'MobilePhone', 'MailingStreet', 'MailingPostalCode',
			'MailingCity', 'MailingCountry', 'MailingLatitude', 'MailingLongitude'
		};

		Map<String,Schema.SObjectField> fieldDescribeTokens =
			Schema.SObjectType.Contact.fields.getMap();

		for (String field : fieldsToCheck) {
			if (!fieldDescribeTokens.get(field).getDescribe().isAccessible()) {
				throw new System.NoAccessException();
			}
		}

		// Execute queries
		zqu__Quote__c q = [SELECT Id, zqu__Account__c FROM zqu__Quote__c WHERE Id = : quoteId];

		return [SELECT Name, Phone, MobilePhone, MailingStreet, MailingPostalCode, MailingCity, MailingCountry,  MailingLatitude, MailingLongitude
		        FROM Contact
		        WHERE AccountId = : q.zqu__Account__c
		                          LIMIT 1];
	}

	@AuraEnabled
	public static List<String> retrieveAttachmentInfo(String quoteId){

		Attachment a;
		List<String> invId = new List<String>();
		Boolean attFound = true;
		try {
			 a = [SELECT Id, Name, ParentId FROM Attachment WHERE Name LIKE 'INV%' AND ParentId =:quoteId];

		} catch (Exception e) {
			System.debug('No attachment found.');
			attFound = false;
		}
		invId.add(String.valueOf(attFound));
		invId.add(a.Name);
        invId.add(a.Id);

		return invId;
     
}

@AuraEnabled
public static String getAccessToken() {
	ZuoraRestCalls restCall = new ZuoraRestCalls();
	return restCall.getAccessToken();
}

@AuraEnabled
public static String getInvoiceId(String quoteId, String token) {
	ZuoraRestCalls restCall = new ZuoraRestCalls();
	return restCall.getInvoiceId(quoteId, token);
}

@AuraEnabled
public static String getPDFUrl(String token, String invoiceId) {
	ZuoraRestCalls restCall = new ZuoraRestCalls();
	return restCall.getPDFUrl(token, invoiceId);
}

@AuraEnabled
public static String getPDF(String token, String pdfUrl) {
	System.debug('retrieveContactInfo.getPDF');
	ZuoraRestCalls restCall = new ZuoraRestCalls();
	return restCall.getPDF(token, pdfUrl);
}
}