public with sharing class MyMonthlyStatementController {
    
    @AuraEnabled
    public static List<Monthly_Statement__c> getMyStatements(String UserId, String NameFilter, DateTime DateFilter, String DatePicklistFilter, Integer AmountFilter, String AmountPicklistFilter, Integer MonthFilter, String MonthPicklistFilter){
        List<Monthly_Statement__c> statements = new List<Monthly_Statement__c>();
        Id AccountId = [SELECT id, AccountId from User WHERE id =: UserId].AccountId;

        String q = 'SELECT Id, Monthly_Statement_Name__c, Month_of_Reference__c, Commission_Payment_Date__c, Commissioning_Amount__c, Statement_Url__c, Commission_Payment_Method__c, Year__c  FROM Monthly_Statement__c WHERE Account_ID__c = \''+AccountId+'\'';
        String order = ' ORDER BY Commission_Payment_Date__c DESC';
        List<String> filters = new List<String>();
        system.debug('nome' + NameFilter + ' data '+ DateFilter + ' amount ' + AmountFilter + ' balance '+ MonthFilter );
        if(NameFilter != null){
            filters.add('Monthly_Statement_Name__c = \''+NameFilter+'\'');
        }
        if(DateFilter != null){
            if(DatePicklistFilter == 'Equals'){
                filters.add('Commission_Payment_Date__c = '+DateFilter.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\''));
            }else if(DatePicklistFilter == 'GreaterThan'){
                filters.add('Commission_Payment_Date__c > '+DateFilter.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\''));
            }else{
                filters.add('Commission_Payment_Date__c < '+DateFilter.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\''));
            }
        }
        if(AmountFilter != null){
            if(AmountPicklistFilter == 'Equals'){
                filters.add('Commissioning_Amount__c = '+AmountFilter );
            }else if(AmountPicklistFilter == 'GreaterThan'){
                filters.add('Commissioning_Amount__c > '+AmountFilter );
            }else{
                filters.add('Commissioning_Amount__c < '+AmountFilter );
            }
        }
        if(MonthFilter != null){
            if(MonthPicklistFilter == 'Equals'){
                filters.add('Month_of_Reference__c = '+MonthFilter );
            }else if(MonthPicklistFilter == 'GreaterThan'){
                filters.add('Month_of_Reference__c > '+MonthFilter );
            }else{
                filters.add('Month_of_Reference__c < '+MonthFilter );
            }
        }

        String filter = String.join(filters, ' AND ');
        if(filter != ''){
            q = q +' AND ' + filter + order;
            
        }else{
            q = q + order;
        }
        system.debug('	QUERY FINALE'+ q);

        statements = Database.query(q);
        return statements;
    }
}