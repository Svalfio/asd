public class AccountActivationCode_Controller {

    @AuraEnabled
    public static String getAccountId(String QuoteId){
        String response= '';
        List<zqu__quote__c> RelatedAccounts_Id = new List<zqu__quote__c>();
        RelatedAccounts_Id = [Select zqu__Account__c from zqu__quote__c where id=: QuoteId];
        if(RelatedAccounts_Id.size()>0){
            response = String.valueOf(RelatedAccounts_Id[0].zqu__Account__c);
        }
        return response;
    }
}