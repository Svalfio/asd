public class LC_MonthlyStatementController {

    @AuraEnabled
    public static List<String> retrieveInformation(String accnId){
        
        List<String> stringLst = new List<String>();
        Id indirectRecordTypeId = Schema.SObjectType.Commission_Model__c.getRecordTypeInfosByDeveloperName().get('Indirect').getRecordTypeId();
        Map<String,Commission_Model__c> mapSubRes = new Map<String,Commission_Model__c>();      
     
        Schema.DescribeFieldResult fieldResult = Commission_Model__c.Country__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry pickListVal : ple){
            String stringInfo = 'Country'+'|'+''+'|'+pickListVal.getLabel();
			stringLst.add(stringInfo);
        }  
        
        
        List<Commission_Model__c> commModel = [SELECT Id,Sub_Reseller_Id__c,Sub_Reseller_Id__r.Name FROM Commission_Model__c 
                                                   WHERE RecordTypeId = :indirectRecordTypeId
                                                   AND Reseller_Id__c = :accnId];
        
        for(Commission_Model__c model : commModel){
            mapSubRes.put(model.Sub_Reseller_Id__c,model);
        } 
      
        for(String key : mapSubRes.KeySet()){
            Commission_Model__c tmpModel = mapSubRes.get(key);
            String stringInfo = 'SubReseller'+'|'+tmpModel.Sub_Reseller_Id__c+'|'+tmpModel.Sub_Reseller_Id__r.Name;
			stringLst.add(stringInfo);
        }
    	
        system.debug('stringLst :' + stringLst);
        
        return stringLst;
    }
    
    @AuraEnabled
    public static String callMonthly(String accnId,String country, String subResellerId){
        
        MonthlyOnDemand__c mc = MonthlyOnDemand__c.getInstance('MSOnDemand');
        String endpoint = mc.Endpoint__c;

        endpoint += '?country='+country+'&resellerId='+accnId;
        
        if(subResellerId != '' && subResellerId != null)
        	endpoint += '&subResellerId='+subResellerId;      
       
        system.debug('endpoint: '+endpoint);
        
        return endpoint;
    }    
}