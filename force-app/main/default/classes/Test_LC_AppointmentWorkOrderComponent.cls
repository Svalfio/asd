@isTest
public class Test_LC_AppointmentWorkOrderComponent {
    
    
    @testSetup
    static void createData(){
        
        Date myDate = date.newinstance(2019, 2, 17);
        
        Account acc= new Account (Name='ACME',Installation_County__c= 'United States', 
                                  Installation_Street__c='New Avenue' ,
                                  Installation_State_Province__c='New York' ,
                                  Installation_City__c='New York');
        insert acc;
        
        Contact cont = new Contact (Lastname = 'Black',accountid=acc.id);
        insert cont;
       
        
        Product2 zquProduct = new Product2(Family='Equipment',Name = 'TestClasse');
        insert zquProduct;
        
        
        zqu__Quote__c zquQuote = new zqu__Quote__c(zqu__BillToContact__c=cont.id);
        insert zquQuote;
        

        zqu__ProductRatePlan__c ProductRatePlan = new zqu__ProductRatePlan__c(zqu__Product__c = zquProduct.id);
        insert ProductRatePlan;
        
        zqu__QuoteAmendment__c zquQuoteAmendment = new zqu__QuoteAmendment__c(zqu__Quote__c = zquQuote.id );
        insert zquQuoteAmendment;
        
        
        zqu__QuoteRatePlan__c zquQuoteRatePlan = new zqu__QuoteRatePlan__c(zqu__QuoteAmendment__c =zquQuoteAmendment.id,
                                                                           zqu__Product__c = zquProduct.id,
                                                                           zqu__ProductRatePlan__c = ProductRatePlan.id ,
                                                                           zqu__Quote__c= zquQuote.id,
                                                                           zqu__QuoteRatePlanFullName__c = 'QuoteRatePlan1'
                                                                          );
    	insert zquQuoteRatePlan;
        

        
		Order SelectedOrder = new Order(Zuora_Quote_Id__c = zquQuote.id,AccountId = acc.id,Status = 'Approved',EffectiveDate=myDate);
        insert SelectedOrder;
        
        
    }
    
    
    @isTest
    static void createWorkOrder(){
        
        id orderId = [Select id from Order ].id;
        LC_AppointmentWorkOrderComponent.createTask(orderId);
        
    }

}