public with sharing class LC_SearchCustomerController {

    @AuraEnabled
    public static Object[] searchAccount(
            String customerName,
            String customerAccountId,
            String phoneNumber,
            String emailAddress,
            String terminalId,
            String filterPlan,
            String filterStatus,
            String filterCountry,
            Date filterDate,
            Boolean filterActive,
            Boolean isReseller){
        /*String[] fieldsToCheck = new String[] {
            'Id', 'Name'
        };
        
        Map<String,Schema.SObjectField> fieldDescribeTokens = 
            Schema.SObjectType.Account.fields.getMap();
        
        for(String field : fieldsToCheck) {
            if( ! fieldDescribeTokens.get(field).getDescribe().isAccessible()) {
                throw new System.NoAccessException();
            }
        }*/

        if (customerName != null){
            customerName = String.escapeSingleQuotes(customerName);
        } else {
            customerName = '';
        }
        if (customerAccountId != null){
            customerAccountId = String.escapeSingleQuotes(customerAccountId);
        } else {
            customerAccountId = '';
        }
        if (phoneNumber != null){
            phoneNumber = String.escapeSingleQuotes(phoneNumber);
        } else {
            phoneNumber = '';
        }
        if (emailAddress != null){
            emailAddress = String.escapeSingleQuotes(emailAddress);
        } else {
            emailAddress = '';
        }
        if (terminalId != null){
            terminalId = String.escapeSingleQuotes(terminalId);
        } else {
            terminalId = '';
        }

        String query = '';

        if (terminalId != null && terminalId != '') {
            System.debug('Perform Asset query');
            // performs search asset
            query += 'SELECT Id, SerialNumber, Name, Account.Id, Account.Name, Account.Phone, Account.Email_address__c';
            query += ' FROM Asset';
            query += ' WHERE (Account.RecordType.DeveloperName = \'Residential\' OR Account.RecordType.DeveloperName = \'Professional\')';
            //query += ' WHERE Id != \'\'';
            query += ' AND Account.Name LIKE \'%' + customerName + '%\'';
            if (customerAccountId != '') {
                query += ' AND Account.Id = \'' + customerAccountId + '\'';
            }
            query += ' AND Account.Phone LIKE \'%' + phoneNumber + '%\'';
            query += ' AND Account.Email_address__c LIKE \'%' + emailAddress + '%\'';
            if (terminalId != '') {
                query += ' AND SerialNumber LIKE \'%' + terminalId + '%\'';
            }
        } else {
            System.debug('Perform Account query');
            // performs search account
            query += 'SELECT Id, Name, Phone, Email_address__c';
            query += ' FROM Account';
            query += ' WHERE (RecordType.DeveloperName = \'Residential\' OR RecordType.DeveloperName = \'Professional\')';
            //query += ' WHERE Id != \'\'';
            query += ' AND Name LIKE \'%' + customerName + '%\'';
            if (customerAccountId != '') {
                query += ' AND Id = \'' + customerAccountId + '\'';
            }
            query += ' AND Phone LIKE \'%' + phoneNumber + '%\'';
            query += ' AND Email_address__c LIKE \'%' + emailAddress + '%\'';
        }

        System.debug('1----------------' + query);

        // filter plan
        if (filterActive && filterPlan != '') {
            List<String> listIdPlan = new List<String>();

            zuora__subscriptionrateplan__c[] plans = [SELECT Zuora__Subscription__r.Zuora__Account__r.id FROM zuora__subscriptionrateplan__c WHERE Name = :filterPlan];
            for (zuora__subscriptionrateplan__c plan : plans) {
                listIdPlan.add(plan.Zuora__Subscription__r.Zuora__Account__r.id);
            }
            query += ' AND ID IN (\'' + String.join(listIdPlan, '\',\'') + '\')';
        }

        // filter status
        if (filterActive && filterStatus != '') {
            List<String> listIdStatus = new List<String>();

            Zuora__Subscription__c[] subscriptions = [SELECT Zuora__Account__r.id FROM Zuora__Subscription__c WHERE Zuora__Status__c = :filterStatus];
            for (Zuora__Subscription__c subscription : subscriptions) {
                listIdStatus.add(subscription.Zuora__Account__r.id);
            }
            query += ' AND ID IN (\'' + String.join(listIdStatus, '\',\'') + '\')';
        }

        // filter date
        if (filterActive && filterDate != null) {
            List<String> listIdDate = new List<String>();

            Zuora__Subscription__c[] subscriptions = [SELECT Zuora__Account__r.id FROM Zuora__Subscription__c WHERE Zuora__ServiceActivationDate__c = :filterDate];
            for (Zuora__Subscription__c subscription : subscriptions) {
                listIdDate.add(subscription.Zuora__Account__r.id);
            }
            query += ' AND ID IN (\'' + String.join(listIdDate, '\',\'') + '\')';
        }

        // filter country
        if (filterActive && filterCountry != '') {
            if (terminalId != null && terminalId != '') {
                query += ' AND Account.Billing_Country_Name__c = :filterCountry';
            } else {
                query += ' AND Billing_Country_Name__c = :filterCountry';
            }
        }

        if (isReseller) {
            query += ' LIMIT 15';
        }

        System.debug('2----------------' + query);

        Object[] result = Database.query(query);
        
        
        
        return result;
    }

    @AuraEnabled
    public static String getRecordTypeCurrentUser(String id){
        return [SELECT Account.RecordType.Name FROM User WHERE Id = :id].Account.RecordType.Name;
    }

    @AuraEnabled
    public static Object[] getPlans(){
        return [SELECT name FROM zuora__subscriptionrateplan__c GROUP BY name ORDER BY name];
    }

    @AuraEnabled
    public static Object[] getStatus(){
        return [SELECT Zuora__Status__c FROM Zuora__Subscription__c WHERE Zuora__Status__c != '' GROUP BY Zuora__Status__c];
    }

    @AuraEnabled
    public static Object[] getCountries(){
        return [SELECT Billing_Country_Name__c FROM Account WHERE Billing_Country_Name__c != '' GROUP BY Billing_Country_Name__c];
    }
    
}