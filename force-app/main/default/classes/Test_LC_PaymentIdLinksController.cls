@isTest
public class Test_LC_PaymentIdLinksController {
    
    @testSetup
    static void createDataSet(){
        Wallet__c test = new Wallet__c();
        insert test;
    }
    
    @isTest
    static void testGetReceipts(){
    	Wallet__c testId = [SELECT Id FROM Wallet__c LIMIT 1];
        String testString = testId.Id;
        test.startTest();
        LC_PaymentIdLinksController.getReceipts(testString);
        test.stopTest();
    }

}