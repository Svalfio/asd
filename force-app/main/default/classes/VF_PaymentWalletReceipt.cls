public with sharing class VF_PaymentWalletReceipt {
	
    
    public ReceiptWrapper wallTrans {get;set;}
    private String walletTransId ;
    
	public class ReceiptWrapper{
        public String transcategory {get;set;}
       	public Decimal previousbalance {get;set;}
        public Decimal topupamount {get;set;}
        public Decimal newbalance {get;set;}
        public String topupdate {get;set;}
        public String accnname {get;set;}
        public String country {get;set;}
        public String vat {get;set;}
        public String curr {get;set;}
        public String paymethod {get;set;}
        public String receiptnumb {get;set;}
        
        Public ReceiptWrapper(String transcategory,Decimal previousbalance, Decimal topupamount,Decimal newbalance, String topupdate, String accnname, String country, String vat, String curr,String paymethod,String receiptnumb ) {
            this.transcategory = transcategory;
            this.previousbalance = previousbalance;
            this.topupamount = topupamount;
            this.newbalance = newbalance;
            this.topupdate = topupdate;
            this.accnname = accnname;
            this.country = country;
            this.vat = vat;
            this.curr = curr;
            this.paymethod = paymethod;
            this.receiptnumb = receiptnumb;
        }
    }

	public VF_PaymentWalletReceipt() {
        walletTransId = ApexPages.currentPage().getParameters().get('walletTransId');
        wallTrans = retrieveTransactionInfo(walletTransId);
    }    
    
    public static ReceiptWrapper retrieveTransactionInfo(String walletTransId){
        
        Wallet_Transaction__c tmpWall = [SELECT Id,Wallet__r.Account__r.Name, Wallet__r.Account__r.VAT_Number__c,Wallet__r.Account__r.Headquarter_Country__c,
                                         Transaction_Category__c,Amount__c, Date__c, Wallet_Balance__c,Wallet__r.CurrencyIsoCode,Payment_Method__c,Name
                                         FROM Wallet_Transaction__c 
                                         WHERE Id = :walletTransId 
                                         LIMIT 1];
        Decimal prevCalcBal = 0;
        if(tmpWall.Wallet_Balance__c != null)
			prevCalcBal = tmpWall.Wallet_Balance__c - tmpWall.Amount__c;
        
        String strMonth = '';
        String finalNUmber = '';
        List<String> tmpNumber = tmpWall.Name.split('-');
        if(tmpNumber.size() > 0)
        	finalNUmber = tmpNumber[1];
        
        switch on tmpWall.Date__c.month() {
            when 1 {
                strMonth = 'January';
            }	
            when 2 {
                strMonth = 'February';
            }
            when 3 {
                strMonth = 'March';
            }
            when 4 {		 
                strMonth = 'April';
            }
            when 5 {		 
                strMonth = 'May';
            }
            when 6 {		 
                strMonth = 'June';
            }
            when 7 {		 
                strMonth = 'July';
            }
            when 8 {		 
                strMonth = 'August';
            }
            when 9 {		 
                strMonth = 'September';
            }
            when 10 {		 
                strMonth = 'October';
            }
            when 11 {		 
                strMonth = 'November';
            }
            when 12 {		 
                strMonth = 'December';
            }
		} 
        
        ReceiptWrapper result = new ReceiptWrapper(tmpWall.Transaction_Category__c,
                                                   prevCalcBal,
                                                   tmpWall.Amount__c,
                                                   tmpWall.Wallet_Balance__c,
                                                   //String.valueOf(tmpWall.Date__c.day())+'/'+String.valueOf(tmpWall.Date__c.month())+'/'+ String.valueOf(tmpWall.Date__c.year()),
                                                   strMonth + ' ' +String.valueOf(tmpWall.Date__c.day())+', '+ String.valueOf(tmpWall.Date__c.year()),
                                                   tmpWall.Wallet__r.Account__r.Name, 
                                                   tmpWall.Wallet__r.Account__r.Headquarter_Country__c,
                                                   tmpWall.Wallet__r.Account__r.VAT_Number__c,
                                                   tmpWall.Wallet__r.CurrencyIsoCode,
                                                   tmpWall.Payment_Method__c,
                                                   tmpWall.Wallet__r.Account__r.Name.substring(0,3)+'-'+finalNumber
                                                 );
        return result;
    }
    
}