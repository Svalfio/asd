@isTest
public class Test_ContactTriggerHandler {
    
    @testSetup static void setup() {
        
        AllMySmsCustomSetting__c testCustomMySms = new AllMySmsCustomSetting__c(Name='AllMySms',Endpoint__c='urlFalse');
        insert testCustomMySms;
        
        Date myDate = date.newinstance(2019, 2, 17);
        Date myDate2 = date.newinstance(2019, 2, 20);
        
        Campaign testCampaign = new Campaign(Name='testCampaign',
                                            Channel__c='SMS;Partner_Portal;Phone;Customer_Portal',
                                            Status='In Progress',
                                            startDate=myDate,
                                            endDate=myDate2,
                                            SMS_Body__c='SMSTest',
                                            Web_Body__c='webTest');
        insert testCampaign;
        
        Account acc = new Account(Name='Acme');
        insert acc;
        
        
        Contact contact = new Contact(LastName = 'Sempronio',AccountId=acc.id);
        insert contact;

    }
    
    @isTest
    static void onAfterUpdateTest(){

        Contact contact = [SELECT Id, Email_Opt_out__c, Phone_Opt_out__c, Web_Opt_out__c, SMS_Opt_out__c FROM Contact WHERE LastName = 'Sempronio' LIMIT 1];
        system.debug(contact.Id);
        id ContId=contact.Id;
		Campaign camp = [SELECT Id,Name FROM Campaign WHERE Name = 'testCampaign' LIMIT 1];

        CampaignMember campaignMember = new CampaignMember(ContactId= contact.Id, CampaignId = camp.id);
        
        contact.Email_Opt_out__c = true;
        contact.Phone_Opt_out__c = false;
        contact.Web_Opt_out__c = true;
        contact.SMS_Opt_out__c = false;
        
        Test.startTest();
        insert campaignMember;
        update contact;
        Test.stopTest();
    }

}