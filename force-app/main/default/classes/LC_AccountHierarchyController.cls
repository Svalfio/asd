public class LC_AccountHierarchyController {

    @AuraEnabled  
    public static List<items> getAccountHierarchy(Id accountId) {  
        
        //Wrapper instance  
        List<items> finalWrp = new List<items>();  
        
        List<Account> accountList =  [ Select Id, Name, RecordType.DeveloperName,ParentId 
                                      From Account 
                                      Where RecordType.DeveloperName IN ('Reseller','Sub_Reseller')
                                      AND (Id =: accountId OR ParentId =:accountId)];
        
        system.debug('*** '+accountList);
        
        List<items> trP1 = new List<items>();
        
        String idRes = '';
        String nameRes = '';

        for(Account acc : accountList){
            
            if(acc.RecordType.DeveloperName == 'Sub_Reseller'){
                idRes = acc.ParentId;
                trP1.add(new items(acc.Id, 'Reseller: '+acc.Name, false, null));
            }
            else if(acc.RecordType.DeveloperName == 'Reseller'){
                idRes = acc.Id;
                nameRes = acc.Name;
            }
        }
        
        if(idRes != '' && nameRes == ''){
            Account accRes = [SELECT Id,Name FROM Account WHERE Id = :idRes LIMIT 1];
            nameRes = accRes.Name;
        }
            
        
        List<items> trP2 = new List<items>{new items(idRes, 'Main Reseller: '+nameRes, false, trP1)};
        
       	if(trP2.size() > 0)
            return trP2;
            
        return finalWrp;    
    }  

	public class items {  

	@AuraEnabled  
	public string label { get; set; } 

	@AuraEnabled  
	public string name { get; set; }  

	@AuraEnabled  
	public Boolean expanded { get; set; }  

	@AuraEnabled  
	public List<items> items { get; set; }  

	public items( String name, String label, Boolean expanded,  List<items> items) {  
		this.label = label;  
		this.name = name;  
		this.expanded = expanded;  
		this.items = items;   
		}  
	} 
}