public with sharing class DownloadWorkOrderApxc {

public Contact con {get; set;}
public zqu__Quote__c q {get; set;}
public Id quoteId {get; set;}

public DownloadWorkOrderApxc(ApexPages.StandardController controller){



	quoteId = ApexPages.CurrentPage().getparameters().get('id');

	q = [SELECT Id, zqu__Account__c FROM zqu__Quote__c WHERE Id = :quoteId];

	con = [SELECT Name, Phone, MobilePhone, MailingStreet, MailingPostalCode, MailingCity, MailingCountry,  MailingLatitude, MailingLongitude
	       FROM Contact
	       WHERE AccountId = : q.zqu__Account__c
	                         LIMIT 1];

	String pdfName = 'Installation_work_order_' + con.Name + '.pdf';
	Apexpages.currentPage().getHeaders().put('content-disposition', 'inline; filename=' + pdfName);

}

}