@isTest
public class Test_SubscriptionJSON {
	@isTest
	static void SubscriptionJSON(){

        List<Account> accountList = new List<Account>();
		List<String> AccountQuery = new List<String>();
		List<String> AccountIds = new List<String>();

		Account accountTest1 = new Account (Name='Test Account 1');
		accountList.add(accountTest1);
		
		Account accountTest2 = new Account (Name='Test Account 2');
		accountList.add(accountTest2);
		
		Account accountTest3 = new Account (Name='Test Account 3');
		accountList.add(accountTest3);
        
        insert accountList;
        
        AccountIds.add(String.valueOf(accountTest1.Id));
        AccountIds.add(String.valueOf(accountTest2.Id));
		AccountIds.add(String.valueOf(accountTest3.Id));

		for (String currentString : AccountIds) {
			String s = 'SELECT id, Name, Zuora__ServiceActivationDate__c, Zuora__ContractEffectiveDate__c, '+
			           'Zuora__SubscriptionEndDate__c, Zuora__Status__c, Zuora__SubscriptionStartDate__c, Subscription_Type__c, '+
			           'Zuora__Version__c, Country__c FROM Zuora__Subscription__c WHERE Zuora__Account__c = \'' + currentString+'\'';
		           AccountQuery.add(s);
	}
        

	Test.startTest();
	List<String> result = SubscriptionJSON.getJSONSubscription(AccountIds);
	Test.stopTest();

	system.assertEquals(result.size(), AccountQuery.size());
}
}