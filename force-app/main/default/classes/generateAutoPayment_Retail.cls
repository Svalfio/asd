global class generateAutoPayment_Retail {
    global static void makePostCallout(String zuoraInvoiceId, Decimal amount, String invoiceCurrency, Date createdDate, String accountId) 
    {
        ZuoraRestCalls zuoraRestCall = new ZuoraRestCalls();
        String token = zuoraRestCall.getAccessToken();
		
        String payDate;
        payDate = string.valueOfGmt(createdDate);  
        payDate = payDate.replace(' ','T');
        
		string jsons = 
		'{'+
		'  "accountId": "'+accountId+'",'+
		'  "amount": '+amount+','+
		'  "comment": "Invoice Payment - Retail Pre Paid model",'+
		'  "currency": "'+invoiceCurrency+'",'+
		'  "effectiveDate": "'+payDate+'",'+
		'  "invoices": ['+
		'    {'+
		'      "amount": '+amount+','+
		'      "invoiceId": "'+zuoraInvoiceId+'"'+
		'    }  ],'+
		'  "type": "External"'+
		'}';
		
        if (token != null) {
            generateCashPayment(jsonS,token);
        }
    }
    
    public static void generateCashPayment(String body, String token){//(String token, String zuoraAccountId, String invoiceCurrency, Decimal amount, String invoiceId, String paymentDate){
        ZuoraInvoiceCustomSetting__c zcs = ZuoraInvoiceCustomSetting__c.getOrgDefaults();
        HttpRequest req = new HttpRequest();
        
        req.setEndpoint(zcs.Zuora_Endpoint__c + '/v1/payments/');
        req.setHeader('authorization', 'bearer ' + token);
        req.setHeader('content-type', 'application/json');
        req.setTimeout(6000);
        req.setMethod('POST');
		req.setBody(body);
		
        HttpResponse resp = new Http().send(req);
        System.debug('@@@@resp:  '+resp);
		system.debug(body);
        //return resp.getBody();
	}
}