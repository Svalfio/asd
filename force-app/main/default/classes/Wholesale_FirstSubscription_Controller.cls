global class Wholesale_FirstSubscription_Controller {
    
    @InvocableMethod(Label = 'Check Subscription')
        public static List<WrapperSubscription> ParseSubscription(List<String> JsonSubscription){
            List<Zuora__Subscription__c> subscriptions = new List<Zuora__Subscription__c>();
            List<WrapperSubscription> subscriptions_Wrapper = new List<WrapperSubscription>();
            WrapperSubscription wrappSubscription;
            subscriptions= (List<Zuora__Subscription__c>) JSON.deserialize(JsonSubscription[0], List<Zuora__Subscription__c>.class);
            for(Zuora__Subscription__c sub : subscriptions){
                wrappSubscription = new WrapperSubscription();
                wrappSubscription.subscription_ID = sub.id;
                wrappSubscription.subscription_Name = sub.Name;
                wrappSubscription.subscription_Country = sub.Country__c;
                subscriptions_Wrapper.add(wrappSubscription);
            }
            system.debug('Return: '+subscriptions_Wrapper);
            return subscriptions_Wrapper;
        }

    global class WrapperSubscription{
    
    @AuraEnabled
    @ InvocableVariable 
    global Id subscription_ID;
        
    @AuraEnabled
    @ InvocableVariable 
    global String subscription_Name;
        
    @AuraEnabled
    @ InvocableVariable 
    global String subscription_Country;
    }
}