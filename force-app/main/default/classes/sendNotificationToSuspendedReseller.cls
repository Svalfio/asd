public class sendNotificationToSuspendedReseller {
	@InvocableMethod(label='Send notification to suspended reseller')
    public static void sendNotification(List<String> idReseller) {
		
        Id communityId = [Select id from Network where Name='Eutelsat Partner Community'].id;
        List<FeedItem> feeds = new List<FeedItem>();
        system.debug(idReseller);
        for(String idRes: idReseller){
            FeedItem feed = new FeedItem();
            feed.Title = 'Your account has been suspended';
            feed.Body = 'You are currently unable to sell new items. Please contact your reseller.';
            feed.ParentId = idRes;
            feed.NetworkScope = communityId;
            feeds.add(feed);
        }
        insert feeds;
        
    }
}