global class NewProductFlow_Handler {

        @ InvocableMethod
         public static List<String> getRatePlan_RecordType(List<String> ParentRecordTypeDeveloperName){
            @TestVisible String Id_RatePlan_RT ='';
            List<String> Ids = new List<String>();
            try{
                @TestVisible String ChildRecordTypeDeveloperName = [Select Dependent_Field_Value__c from Dependent_RecordType__mdt where 
                                     Controlling_Object_API_Name__c='Product2' and
                                     Controlling_Field_Value__c=:ParentRecordTypeDeveloperName].Dependent_Field_Value__c;
                
            	 Id_RatePlan_RT = Schema.SObjectType.zqu__ProductRatePlan__c.getRecordTypeInfosByDeveloperName().get(ChildRecordTypeDeveloperName).getRecordTypeId();
				Ids.add(Id_RatePlan_RT);
           
            } catch(exception e){
                system.debug('An Error Occurred in ProductRatePlan Record Type assignment: '+e.getMessage());
            }          
            
            return Ids	;
        }
}