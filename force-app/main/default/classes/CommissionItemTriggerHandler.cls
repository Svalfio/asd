public class CommissionItemTriggerHandler {
    
    public void onBeforeInsert(List<Commission_Item__c> newCommissionList){
        
        system.debug('CommissionTriggerHandler----onBeforeInsert--------START');
        
        Set<String> commissionModelSet = new Set<String>();
        
        for(Commission_Item__c item : newCommissionList)
            commissionModelSet.add(item.Commission_Model__c);
        
        List<Commission_Item__c> existingCommissionItems = [SELECT Commission_Model__c,Transaction_Type__c FROM Commission_Item__c where Commission_Model__c in : commissionModelSet];
        
        for(Commission_Item__c newCommItems : newCommissionList){
            for (Commission_Item__c exCommItem : existingCommissionItems){
                if(exCommItem.Commission_Model__c == newCommItems.Commission_Model__c && exCommItem.Transaction_Type__c == newCommItems.Transaction_Type__c)
                    newCommItems.addError('The Selected Transaction Type already exist in this Commission Model');
            }
        }
    }  
    
    public void onBeforeUpdate(Map<Id,Commission_Item__c> oldCommissionMap,Map<Id,Commission_Item__c> updatedCommissionMap){
        
        System.debug('CommissionTriggerHandler-------OnBeforeUpdate------START');
        
        Set<String> commissionModelSet = new Set<String>();
        for(Commission_Item__c item : updatedCommissionMap.values())
            commissionModelSet.add(item.Commission_Model__c);
        
        
        List<Commission_Item__c> existingCommissionItems = [SELECT Commission_Model__c,Transaction_Type__c FROM Commission_Item__c where Commission_Model__c in : commissionModelSet];
        List<String> commissionTypeList = new List<String>();
         for(Commission_Item__c existingCommission : existingCommissionItems)
            commissionTypeList.add(existingCommission.Commission_Model__c+'_'+existingCommission.Transaction_Type__c);
        
        system.Debug('Existing Commission Model List: '+commissionTypeList);
        
        
        for(Id CommissionId : updatedCommissionMap.keySet()){
            if(!updatedCommissionMap.get(commissionId).Commission_Model__c.equals(oldCommissionMap.get(commissionId).Commission_Model__c) || !updatedCommissionMap.get(commissionId).Transaction_Type__c.equals(oldCommissionMap.get(commissionId).Transaction_Type__c) )
             if(commissionTypeList.contains(updatedCommissionMap.get(commissionId).Commission_Model__c+'_'+updatedCommissionMap.get(commissionId).Transaction_Type__c))
                updatedCommissionMap.get(commissionId).addError('The Selected Transaction Type already exist in this Commission Model');
        }
        
     
    }
    
}