@isTest
global class OSS_OK_HttpCalloutMock implements HttpCalloutMock{
    global HTTPResponse respond(HTTPRequest request) { 
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"coverage": ["ok"]}');
        response.setStatusCode(200);
        return response; 
    }
}