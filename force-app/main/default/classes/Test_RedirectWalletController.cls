@isTest
public class Test_RedirectWalletController {
    @isTest
    static void RedirectWalletController(){
        
       Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User userTest = new User(Alias = 'standt', Email='eutelsatTestUser@eutelsat.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='eutelsatTestUser@eutelsat.com');
        insert userTest;
        
        String UserId = String.valueOf(userTest.Id);
        
        Wallet__c walletTest = new Wallet__c(OwnerId=userTest.Id);
        insert walletTest;
        
        Test.startTest();
        String result = LC_RedirectWalletController.getWalletId(UserId);
        Test.stopTest();
        
        System.assertEquals(String.valueOf(walletTest.Id), result);
    }
}