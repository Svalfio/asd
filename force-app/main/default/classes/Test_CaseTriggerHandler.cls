@isTest
public class Test_CaseTriggerHandler {
    
    @isTest
    static void onBeforeAfterInsertTest(){
        
        Case testCase = new Case();
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        /*User u = new User(Alias = 'test', Email='testuser@test.it', 
            EmailEncodingKey='ISO-8859-1', LastName='testUser', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='Europe/Rome', UserName='test@user.it');
        insert u;*/
        
        Contact testContact = new Contact(FirstName='testFirst', LastName='testLast',OwnerId=UserInfo.getUserId());
        insert testContact;
        
        Test.startTest();
        insert testCase;
        Test.stopTest();
                
    }
    
    /*
    @isTest
    static void OnAfterInsertTest(){
        
    }
    */
    
    @isTest
    static void OnBeforeUpdateTest(){
        
        Case testCase = new Case(Status='New');
        insert testCase;
        testCase.Status = 'Assigned';
        
        Test.startTest();
        update testCase;
        Test.stopTest();
        
    }

}