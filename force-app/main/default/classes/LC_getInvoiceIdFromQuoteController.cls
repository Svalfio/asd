public class LC_getInvoiceIdFromQuoteController {
    @AuraEnabled
    public static String getInvoiceId(String QuoteId){
        
        system.debug('1');
        zqu__Quote__c quote = [SELECT id, name, zqu__ZuoraSubscriptionID__c FROM zqu__Quote__c WHERE id =: QuoteId];
        system.debug('2');
		ZuoraInvoiceCustomSetting__c zcs = ZuoraInvoiceCustomSetting__c.getOrgDefaults();

		Map<String, Object> jsonToken = new Map<String, Object>();
		Map<String, Object> jsonLink = new Map<String, Object>();

		String token = '';
		String pdfUrl = '';
		String payloadToken = 'client_id=' + EncodingUtil.urlEncode(zcs.Client_Id__c, 'UTF-8') + '&client_secret=' + EncodingUtil.urlEncode(zcs.Client_Secret__c, 'UTF-8') +
		                      '&grant_type=' + EncodingUtil.urlEncode('client_credentials', 'UTF-8');
		system.debug('First call');
        Http h = new Http();
        
        // First call
		HttpRequest reqToken = new HttpRequest();

		reqToken.setEndpoint('https://rest.sandbox.eu.zuora.com/oauth/token');


		System.debug('@@@User ID: '+zcs.User_Id__c);
		System.debug('@@@User PSW: '+zcs.User_Password__c);
		System.debug('@@@Client ID: '+zcs.Client_Id__c);
		System.debug('@@@Client SECRET: '+zcs.Client_Secret__c);
		System.debug('@@@payloadToken: '+payloadToken);


		reqToken.setHeader('apiAccessKeyId', zcs.User_Id__c);
		reqToken.setHeader('apiSecretAccessKey', zcs.User_Password__c);
		reqToken.setHeader('grant_type', 'client_credentials');
		reqToken.setHeader('Content-Type', 'application/x-www-form-urlencoded');

		reqToken.setTimeout(6000);

		reqToken.setMethod('POST');

		reqToken.setBody(payloadToken);

		HttpResponse respToken = h.send(reqToken);
		System.debug('@@@@respToken:  '+respToken.getBody());

		System.debug('HTTP callout failed:'
		             + '\nendPoint=' + reqToken.getEndPoint()
		             + '\nstatusCode=' + respToken.getStatusCode()
		             + '\nstatus=' + respToken.getStatus()
		             + '\nbody=' + respToken.getBody());

		jsonToken = (Map<String, Object>)JSON.deserializeUntyped(respToken.getBody());

		token = (String)jsonToken.get('access_token');
		System.debug('@@@TOKEN: ' +token);
        
        
        system.debug('Second call');
        
        HttpRequest httpQuery = new HttpRequest();
        httpQuery.setEndpoint('https://rest.sandbox.eu.zuora.com/v1/action/query');
        httpQuery.setHeader('authorization', 'bearer ' + token);
        httpQuery.setHeader('content-type', 'application/json');
        httpQuery.setBody('{"queryString":"select id, InvoiceId from InvoiceItem where SubscriptionId =\'8adce4216ec97e24016ecbe46ad57127\' "}');
        httpQuery.setMethod('POST');
        httpQuery.setTimeout(6000);
        HttpResponse respQuery = h.send(httpQuery);
        system.debug(respQuery.getStatus());
		System.debug('@@@@respToken:  '+respQuery.getBody());
        
        jsonToken = (Map<String, Object>)JSON.deserializeUntyped(respQuery.getBody());
        

		List<Object> risultati = (List<Object>)jsonToken.get('records');
        String s;
        for(Object o: risultati){
            s = String.valueof(o);
            system.debug(s);
            s= s.split('InvoiceId')[1];
            s= s.split('}')[0];
            system.debug(s);
        }
        
        
        return s;
    }

}