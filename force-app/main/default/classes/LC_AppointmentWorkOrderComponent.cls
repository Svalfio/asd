public class LC_AppointmentWorkOrderComponent {
    
    @AuraEnabled
    public static String createTask(Id orderId){
        String esito;
        
        Order selectedOrder = [Select id,Account.Name,Zuora_Quote_Id__c,Status from Order where Id =: orderId];
        System.debug('selectedOrder ' +selectedOrder.id);
        
        String zuoraAssociatedQuote = selectedOrder.Zuora_Quote_Id__c;
        System.debug('zuoraAssociatedQuote ' +zuoraAssociatedQuote);
        
        if(!String.isBlank(zuoraAssociatedQuote) && zuoraAssociatedQuote!=null){
            
            List<zqu__QuoteRatePlan__c> zquRatePlanList = [select id,zqu__Quote__r.zqu__BillToContact__c,
                                                           zqu__Quote__r.zqu__BillToContact__r.AccountId,
                                                           zqu__Quote__r.zqu__BillToContact__r.Name,
                                                           zqu__ProductRatePlan__r.zqu__Product__r.Family,
                                                           zqu__ProductRatePlan__r.zqu__Product__r.Unit_Type__c,
                                                           zqu__QuoteRatePlanFullName__c,
                                                           zqu__Quote__r.zqu__BillToContact__r.Account.Installation_County__c,
                                                           zqu__Quote__r.zqu__BillToContact__r.Account.Installation_Street__c, 
                                                           zqu__Quote__r.zqu__BillToContact__r.Account.Installation_State_Province__c,
                                                           zqu__Quote__r.zqu__BillToContact__r.Account.Installation_City__c
                                                           
                                                           from zqu__QuoteRatePlan__c 
                                                           where zqu__Quote__c =: zuoraAssociatedQuote];
            
            system.debug('RatePlanList: '+(zquRatePlanList));
            List<Task> taskList = new List<Task>();
            
            if(!zquRatePlanList.isEmpty() && selectedOrder.Status.equalsIgnoreCase(ApexConstants.ORDER_STATUS_APPROVED)){
                SYstem.debug('Status Approved');
                for(zqu__QuoteRatePlan__c RatePlan : zquRatePlanList){
                    if(RatePlan.zqu__ProductRatePlan__c!=null && RatePlan.zqu__ProductRatePlan__r.zqu__Product__c!= null ){
                        if((RatePlan.zqu__ProductRatePlan__r.zqu__Product__r.Family).equalsIgnoreCase(ApexConstants.PRODUCT_FAMILY)){
                            SYstem.debug('Family Equipment');
                            Task workOrder = new Task();
                            if(RatePlan.zqu__Quote__r.zqu__BillToContact__c!=null  && RatePlan.zqu__Quote__r.zqu__BillToContact__r.Account!=null){
                                if(RatePlan.zqu__Quote__r.zqu__BillToContact__r.Account.Installation_County__c!=null){
                                    workOrder.Address__c=RatePlan.zqu__Quote__r.zqu__BillToContact__r.Account.Installation_County__c;
                                }
                                if(RatePlan.zqu__Quote__r.zqu__BillToContact__r.Account.Installation_State_Province__c!=null){
                                    workOrder.Address__c=workOrder.Address__c+','+RatePlan.zqu__Quote__r.zqu__BillToContact__r.Account.Installation_State_Province__c;
                                }
                                if(RatePlan.zqu__Quote__r.zqu__BillToContact__r.Account.Installation_City__c!=null){
                                    workOrder.Address__c=workOrder.Address__c+','+RatePlan.zqu__Quote__r.zqu__BillToContact__r.Account.Installation_City__c;
                                }
                                if(RatePlan.zqu__Quote__r.zqu__BillToContact__r.Account.Installation_Street__c!=null){
                                    workOrder.Address__c=workOrder.Address__c+','+RatePlan.zqu__Quote__r.zqu__BillToContact__r.Account.Installation_Street__c;
                                }
                            }
                            workOrder.Customer_name__c = RatePlan.zqu__Quote__r.zqu__BillToContact__r.Name;
                            workOrder.Plan__c = RatePlan.zqu__QuoteRatePlanFullName__c;
                            workOrder.Equipment_type__c = RatePlan.zqu__ProductRatePlan__r.zqu__Product__r.Unit_Type__c;
                            workOrder.Subject='Installation Appointment';
                            workOrder.Type=ApexConstants.TASK_TYPE_WORK_ORDER;
                            workOrder.WhoId = RatePlan.zqu__Quote__r.zqu__BillToContact__c;
                            workOrder.WhatId = RatePlan.zqu__Quote__r.zqu__BillToContact__r.AccountId;
                            taskList.add(workOrder);
                        }
                    }
                }
            }
            else{
                if(zquRatePlanList.isEmpty()){
                    System.debug('There is no quote rate plan for the associated quote');
                    esito = 'There is no quote rate plan for the associated quote';
                }else{
                    System.debug('Order is not approved');
                    esito = 'Order is not approved';
                }
            }
            
            if(taskList.size()>0){
                insert taskList;
                system.debug('createTask: '+Json.serialize(taskList));
                esito = 'Appointment related to '+selectedOrder.Account.Name+' has been created !';
            }
            else{
                if(String.isBlank(esito)){
                    System.debug('There is no Equipment related to this Quote');
                    esito = 'There is no equipment related to this quote';
                }
            }
            
            
        }
        else{
            System.debug('No associated quote for this order');
            esito = 'No associated quote for this order';
        }
        
        return esito;
    } 
    
}