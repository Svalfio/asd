public class TestPlanUpgrade {
    public void getMapbyQuoteId (String quoteId){
             
        List<zqu__Quote__c> tQ = [SELECT zqu__Account__c, zqu__ExistSubscriptionID__c FROM zqu__Quote__c WHERE Id =: quoteId];   
        String accountId = tQ[0].zqu__Account__c; 
        String existingSubId = tQ[0].zqu__ExistSubscriptionID__c;
        List<Account> Acc = [SELECT Billing__c, BillingCountry, BillingState FROM Account WHERE Id =: accountId];
        String billingType = Acc[0].Billing__c; 
        String billingCountry = Acc[0].BillingCountry;
        String billingState = Acc[0].BillingState;
            
 		System.debug('Quote ID = '+quoteId);    
        System.debug('Billing Type = '+billingType);
        System.debug('Account ID = '+accountId);
        System.debug('existingSubId = '+existingSubId);
        System.debug('Billing Country = '+billingCountry);
        System.debug('Billing State = '+billingState);

        List<zqu__QuoteRatePlan__c> qrp = [SELECT zqu__ProductRatePlan__r.Volume_GB__c
                                           FROM zqu__QuoteRatePlan__c 
                                           WHERE zqu__Quote__r.zqu__ZuoraSubscriptionID__c=: existingSubId 
                                           AND zqu__ProductRatePlan__r.zqu__Product__r.Family = 'Plan']; 
            
        Decimal liveVolume = qrp[0].zqu__ProductRatePlan__r.Volume_GB__c;
            
        System.debug('Live Volume = '+liveVolume);
        //List<Product2> allowedProducts = new List<Product2>();
            
        if (billingType == 'B2B'){   
            List<zqu__ProductRatePlan__c> allowedProdRatePlans = [SELECT Id, zqu__Product__c
                                                                  FROM zqu__ProductRatePlan__c  
                                                                  WHERE zqu__Product__r.Product_Type__c =: billingType 
                                                                  AND zqu__Product__r.Family = 'Plan'
                                                                  AND zqu__Product__r.isActive = true
                                                                  AND Volume_GB__c >: liveVolume
                                                                  ORDER BY zqu__Product__c];
            System.debug('Allowedproducts ='+allowedProdRatePlans);

        }else if(billingType == 'B2C'){   
            List<zqu__ProductRatePlan__c> allowedProdRatePlans = [SELECT Id, zqu__Product__c
                                                                  FROM zqu__ProductRatePlan__c  
                                                                  WHERE zqu__Product__r.Product_Type__c =: billingType 
                                                                  AND zqu__Product__r.Family = 'Plan'
                                                                  AND zqu__Product__r.isActive = true
                                                                  AND State__c =: billingState
                                                                  AND Country__c =: billingCountry
                                                                  AND Volume_GB__c >: liveVolume
                                                                  ORDER BY zqu__Product__c];   
            System.debug('Allowedproducts ='+allowedProdRatePlans);
            for(zqu__ProductRatePlan__c tempProducts : allowedProdRatePlans){     
                String tProductId = '\''+tempProducts.zqu__Product__c+'\'';
                System.debug('tProductId ='+tProductId);
                List<String> ratePlanIdOk = new List<String>();
                
                    for(zqu__ProductRatePlan__c tempRatePlan : allowedProdRatePlans){
                        if ('\''+tempRatePlan.zqu__Product__c+'\'' == tProductId){
                            ratePlanIdOk.add('\''+tempRatePlan.Id+'\'');  
                        }   
                    }
               	System.debug('ratePlanIdOk ='+ratePlanIdOk);
            }
        }
		//System.debug('Results ='+result);            
		//return result;
     }
}