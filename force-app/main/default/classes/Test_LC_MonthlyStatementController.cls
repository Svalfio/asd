@isTest
public class Test_LC_MonthlyStatementController {
	
    @testSetup
    static void createDataSet(){
		
        Id resellerRTId=  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_RESELLER).getRecordTypeId();
        Account reseller =  new Account(Name = 'Reseller',
                                        RecordTypeId=resellerRTId);
        insert reseller;
        
        Id subresellerRTId=  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_SUBRESELLER).getRecordTypeId();
        Account subreseller =  new Account(Name = 'SubReseller',
                                        RecordTypeId=subresellerRTId);
        
        insert subreseller;

        Commission_Model__c commissionModel = new Commission_Model__c();
        Id idRT =  Schema.SObjectType.Commission_Model__c.getRecordTypeInfosByDeveloperName().get('Direct').getRecordTypeId();
        commissionModel.RecordTypeId = idRT;
        commissionModel.Reseller_Id__c = reseller.Id;
        commissionModel.Country__c = reseller.Country__c;
        commissionModel.Start_Date__c = Date.today().addDays(-10);
        commissionModel.End_Date__c = Date.today().addDays(10);
        insert commissionModel;
        
        Commission_Model__c commissionModel2 = new Commission_Model__c();
        Id indRT =  Schema.SObjectType.Commission_Model__c.getRecordTypeInfosByDeveloperName().get('Indirect').getRecordTypeId();
        commissionModel2.RecordTypeId = indRT;
        commissionModel2.Reseller_Id__c = reseller.Id;
        commissionModel2.Country__c = reseller.Country__c;
        commissionModel2.Start_Date__c = Date.today().addDays(-10);
        commissionModel2.End_Date__c = Date.today().addDays(10);
        commissionModel2.Sub_Reseller_Id__c = subreseller.Id;
        insert commissionModel2;
        
        MonthlyOnDemand__c testCustom = new MonthlyOnDemand__c(Name='MSOnDemand',Endpoint__c='urlFalse');
        insert testCustom;

    }
    
    @isTest
    static void test1(){
        Id resellerRTId=  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_RESELLER).getRecordTypeId();
		Account reseller = [SELECT id FROM Account WHERE RecordTypeId=:resellerRTId LIMIT 1];
        
        Test.startTest();
        LC_MonthlyStatementController.retrieveInformation(reseller.id);
        Test.stopTest();
        
    }
    
    @isTest
    static void test2(){
        Id resellerRTId=  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_RESELLER).getRecordTypeId();
		Account reseller = [SELECT id FROM Account WHERE RecordTypeId=:resellerRTId LIMIT 1];
        
        Id subresellerRTId=  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_SUBRESELLER).getRecordTypeId();
		Account subreseller = [SELECT id FROM Account WHERE RecordTypeId=:subresellerRTId LIMIT 1];
        
        Test.startTest();
        LC_MonthlyStatementController.callMonthly(reseller.id,'COG',subreseller.Id);
        Test.stopTest();
        
    }
}