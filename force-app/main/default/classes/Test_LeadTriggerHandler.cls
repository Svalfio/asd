@isTest
public class Test_LeadTriggerHandler {
    
        @testSetup static void setup() {
        // Create common test accounts
        Lead testLead = new Lead(LastName = 'TestAcct',Company='TestSRL');
        
        insert testLead;        
    }
    
    @isTest
    static void OnAfterUpdateTest(){
        Lead lead= [SELECT Id,Opt_out_Email__c,Opt_out_Phone__c,Opt_out_Web__c,Opt_out_SMS__c FROM Lead WHERE LastName = 'TestAcct'];
        CampaignMember campaignMember =new CampaignMember(LeadId=lead.Id,CampaignId='7016E000000d1p4QAA');
        Lead.Opt_out_Email__c = true;
        Lead.Opt_out_Phone__c = false;
        Lead.Opt_out_Web__c = true;
        Lead.Opt_out_SMS__c = false;
        
        Test.startTest();
        insert campaignMember;
        update lead;
        Test.stopTest();
    }
    
}