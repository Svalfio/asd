@isTest
public class Test_ModifyCustomSettingTriggerHandler {
        @testSetup
    static void createData(){
        
        QuoteAutoNumber__c testCustomSetting = new QuoteAutoNumber__c(Name='Quote Code',Quote_Code__c='00000001');
        insert testCustomSetting;
    }

    
    @isTest
    static void onAfterInsertTest(){
        
        zqu__Quote__c quote1 = new zqu__Quote__c(Name='55');
         zqu__Quote__c quote2 = new zqu__Quote__c(Name='56');
        List<zqu__Quote__c> listQuote = new List<zqu__Quote__c>();
        //listQuote.add(quote1);
        listQuote.add(quote2);
        
        Test.startTest();

        insert listQuote;

        Test.stopTest(); 
        QuoteAutoNumber__c quote= [SELECT Id,Name,CreatedDate,Quote_Code__c FROM QuoteAutoNumber__c];
        SYstem.debug('Updated custom setting : '+quote.Quote_Code__c);
    }

}