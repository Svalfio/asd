public class ContactPromotionCampaignTriggerHandler {
    
    public void onAfterInsert( List<CampaignMember> triggerOld){
        system.debug('ContactPromotionCampaignTriggerHandler---------onAfterInsert--------START');
        
        Map<id,Set<id>> campaignToContactMap = new Map<id,Set<id>>();
        
        
        for(CampaignMember member: triggerOld){
            
            if(!campaignToContactMap.containsKey(member.campaignId)){
                if(member.contactId!=null)
                    campaignToContactMap.put(member.campaignId,new Set<Id>{member.ContactId});
            }   
            else{
                if(member.contactId!=null)
                    campaignToContactMap.get(member.CampaignId).add(member.ContactId);
            }     
        }
        
        
        List<Campaign> campaigns = [SELECT id,RecordType.Name FROM campaign where id IN :campaignToContactMap.KeySet()];
        List<Campaign> promotionCampaigns = new List<Campaign>();
        
        if(campaigns!=null){
            for(Campaign camp : campaigns){
                if(camp.RecordType.Name!=null && camp.RecordType.Name.equals('Promotion')){
                    promotionCampaigns.add(camp);
                }
                
            }
        }
        
        List<id> contactIds = new List<id>();
        if(promotionCampaigns!=null && promotionCampaigns.size()>0){
            for(Campaign campaignId : promotionCampaigns){
                if(campaignToContactMap.containsKey(campaignId.id)){
                    if(campaignToContactMap.get(campaignId.id)!=null){
                        for(id contact : campaignToContactMap.get(campaignId.id)){
                            contactIds.add(contact);
                        }
                    }
                }   
            }
        }
        
        List<Contact> contacts = new List<Contact>();
        if(contactIds!= null && contactIds.size()>0){
            contacts = [Select id,In_promotion_campaign__c FROM contact where id IN :contactIds];
            for(Contact con : contacts){
                con.In_promotion_campaign__c = True;
            }
        }
        
        if(contacts.size()>0){
            System.debug('Contacts updated');
            update contacts;
        }else{
            System.debug('No contacts to update');
        }
        
        
    }
    
    public void onBeforeDelete( List<CampaignMember> triggerOld){
        system.debug('ContactPromotionCampaignTriggerHandler---------onBeforeDelete--------START');
        List<id> contactsIds = new List<id>();
        
        for(CampaignMember member: triggerOld){
            if(member.ContactId!=null && !contactsIds.contains(member.ContactId)){
                contactsIds.add(member.ContactId);
            }
        }
        
        
        Map<id,Set<id>> contactToCampaignMap = new Map<id,Set<id>>();
        
        
        for(CampaignMember member: triggerOld){
            
            if(!contactToCampaignMap.containsKey(member.ContactId)){
                if(member.contactId!=null)
                    contactToCampaignMap.put(member.ContactId,new Set<Id>{member.CampaignId});
            }   
            else{
                if(member.contactId!=null)
                    contactToCampaignMap.get(member.ContactId).add(member.CampaignId);
            }     
        }
        
        
        //List<CampaignMember> campaignMembers = [SELECT id,campaignId,Contactid FROM CampaignMember where Contactid IN :contactsIds];
        //prendo gli id delle campagne coinvolte nel trigger
        Set<id> TriggerCampaignIds = new Set<id>();
        for(id conId : contactToCampaignMap.KeySet()){
            for(id campID : contactToCampaignMap.get(conId)){
                if(!TriggerCampaignIds.contains(campID))
                    TriggerCampaignIds.add(campID);
                
            }
        }
        
        
        
        //vedo tra le campagne coinvolte nella delete, quali sono quelle di tipo 'Promotion'
        List<Campaign> promotionCampaigns=[SELECT id,RecordType.Name FROM Campaign WHERE id IN :TriggerCampaignIds AND RecordType.Name='Promotion'];
        
        // se promotionCampaigns è null, significa che tra i contatti su cui sto facendo la delete,
        //  io non sto cancellando nessun contatto da una campagna di tipo Promotion
        System.debug('promotionCampaigns size : '+promotionCampaigns.size());
        Set<id> contactIdsToUpdate = new Set<id>();
        
        if(promotionCampaigns!=null){
            for(id con : contactsIds){
                if(contactToCampaignMap.containsKey(con)){
                    for(id camp : contactToCampaignMap.get(con)){
                        for(Campaign promotionCamp :promotionCampaigns){
                            if(promotionCamp.id==camp){
                                System.debug('contactIdsToUpdate : ' + con);
                                contactIdsToUpdate.add(con);
                            }
                        }
                    }
                }   
            } 
        }
        //////////
        // ora devo prendere una seconda mappa con i contact come chiave e le campagne promotion associate ad ogni contatto.
        // Se il numero di campagne associate ad ogni contatto è 1 allora, il campo del contatto deve essere settato a false
        ////////////////
        
        
        //mi prendo tutti i campaign member relativi ai contact su cui devo verificare quante campagne promotion ci sono,
        // in modo da vedere quante campagne di tipo promotion sono collegate e li metto in una mappa
        List<CampaignMember> members2 = new List<CampaignMember>();
        List<id> campaignIds2= new List<id>();
        if(members2!=null){
            members2= [SELECT id,campaignId,ContactId FROM CampaignMember where ContactId IN : contactIdsToUpdate ];
            
            //ora mi faccio una lista con tutti gli id delle campagne contenute in members2  
            for(CampaignMember mem: members2){
                campaignIds2.add(mem.campaignId);
            }
        }
        System.debug('campaignIds2 :'+campaignIds2.isEmpty());
        //controllo quante campagne Promotion sono collegate in generale ad ogni contact
        List<Campaign> campaigns = new List<Campaign>();
        List<id> PromotionCampaigns2 = new List<id>();
        
        campaigns = [SELECT id FROM Campaign where id IN : campaignIds2 AND RecordType.Name='Promotion' ];
        
        for(Campaign camp :campaigns){
            if(!PromotionCampaigns2.contains(camp.id)){
                PromotionCampaigns2.add(camp.id);
            }
        }
        System.debug('PromotionCampaigns2 :'+PromotionCampaigns2.isEmpty());
        
        //ora faccio un ciclo su members2 e mi setto la mappa con Contact coinvolto nel trigger
        //e campagne promotion associate in generale ad esso
        
        Map<id,Set<id>> contactToPromoCampaignMap = new Map<id,Set<id>>();
        if(!PromotionCampaigns2.isEmpty()){
            for(CampaignMember member: members2){
                // se la campagna associata al campaignMember è contenuta nelle liste di campagne di tipo Promotion ,allora inserisco nella mappa
                if(PromotionCampaigns2.contains(member.campaignId)){
                    if(!contactToPromoCampaignMap.containsKey(member.ContactId)){
                        if(member.contactId!=null)
                            contactToPromoCampaignMap.put(member.ContactId,new Set<Id>{member.CampaignId});
                    }   
                    else{
                        if(member.contactId!=null)
                            contactToPromoCampaignMap.get(member.ContactId).add(member.CampaignId);
                    }     
                }
            }
        }
        System.debug('contactToPromoCampaignMap :'+contactToPromoCampaignMap.KeySet());
        
        
        ////qui devo creare una terza mappa con contatto e numero di campagne 'Promotion' da eliminare
        Integer i = 0;
        Map<id,Integer> contactToCountPromoCampaignMap = new Map<id,Integer>();
        for(id contact :contactToCampaignMap.KeySet()){
            i=0;
            for(id camp :contactToCampaignMap.get(contact)){
                if(!PromotionCampaigns2.isEmpty() && PromotionCampaigns2.contains(camp)){
                    i++;                    
                }
            }            
            contactToCountPromoCampaignMap.put(contact,i);
        }
        
        
        //per ogni contatto sottraggo il numero di campagne 'Promotion' totali a cui è associato in generale e il numero di campagne 'promotion' associate
        // al contatto stesso che sto eliminando 
        Set<id> contactIdsToUpdate2 = new Set<id>();
        if(contactToPromoCampaignMap.keySet()!=null ){
            for(id con : contactToPromoCampaignMap.keySet()){
                System.debug('contactToPromoCampaignMap.get(con).size() : '+ contactToPromoCampaignMap.get(con).size());
                if(contactToPromoCampaignMap.get(con).size()-contactToCountPromoCampaignMap.get(con)==0){
                    
                    contactIdsToUpdate2.add(con);
                }
                
            }
        }
        
        List<Contact> contactsToUpdate = new List<Contact>();
        if(contactIdsToUpdate2!=null){
            contactsToUpdate = [SELECT id,In_promotion_campaign__c FROM Contact where id IN : contactIdsToUpdate2 ];
            if(contactsToUpdate!=null){
                for(Contact con :contactsToUpdate){
                    con.In_promotion_campaign__c=False;
                    SYstem.debug('Contact id : '+con.id);
                }
            }
        }
        
        if(contactsToUpdate.size()>0){
            system.debug('Contacts updated');
            update contactsToUpdate;
        }else{
            system.debug('No contact to update');
        }
        
    }
    
    
    
    
}