@isTest
public class Test_CampaignTriggerHandler {
    
    @testSetup static void setup() {
        AllMySmsCustomSetting__c testCustomMySms = new AllMySmsCustomSetting__c(Name='AllMySms',Endpoint__c='urlFalse');
        insert testCustomMySms;
        
        Date myDate = date.newinstance(2019, 12, 17);
        Date myDate2 = date.newinstance(2019, 18, 20);
        
        Campaign testCampaign = new Campaign(Name='testCampaignAfterUpdate',
                                             Channel__c = 'SMS;Partner_Portal;Phone;Customer_Portal',
                                             SMS_Body__c = 'Test', 
                                             Web_Body__c = 'Test', 
                                             startDate=myDate,
                                             endDate=myDate2,
                                             Status='Planned');
        insert testCampaign;
        
        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Reseller').getRecordTypeId();
        Account acc = new Account(Name='ACME33',RecordTypeId=devRecordTypeId);
        insert acc;
        
        Contact testContact = new Contact(FirstName='test',
                                          LastName='contact',
                                          AccountId=acc.id);
        insert testContact;
        
        Lead testLead = new Lead(LastName='testLead',
                                 Company='testACME',
                                 Status='New',
                                 CurrencyIsoCode = 'EUR');
        insert testLead;
        
        CampaignMember cmLead = new CampaignMember(CampaignId=testCampaign.id, LeadId= testLead.id);
        CampaignMember cmContact = new CampaignMember(CampaignId=testCampaign.id,ContactId=testContact.id);
        
        List<CampaignMember> campaignMembers = new List<CampaignMember>();
        campaignMembers.add(cmContact);
        
        insert campaignMembers;
        
    }
    
    
    @isTest
    static void onBeforeInsertTest(){
        
        Campaign testCampaign = new Campaign(Name='testCampaign');
        
        Test.startTest();
        insert testCampaign;
        Test.stopTest();
        
    }
    
    @isTest
    static void onAfterUpdateTest(){
        
        Campaign testCampaign = [SELECT Id, Name, Status FROM Campaign WHERE Name = 'testCampaignAfterUpdate' LIMIT 1];

        testCampaign.Status = 'In Progress';

        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('Test_Campaign');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
 
        Test.startTest();
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock); 

        update testCampaign;
        Test.stopTest();
        
    }
    
    

    
    
    

}