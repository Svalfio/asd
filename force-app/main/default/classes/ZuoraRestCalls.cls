public class ZuoraRestCalls {
    /**
    * getInvoiceId
    * descrizione
    * 
    * @param String QuoteId
    * @param String token
    * @returns String
    */
    public String getInvoiceId(String QuoteId, String token){
        
        Map<String, Object> jsonQuery = new Map<String, Object>();
        ZuoraInvoiceCustomSetting__c zcs = ZuoraInvoiceCustomSetting__c.getOrgDefaults();
        
        zqu__Quote__c quote = [SELECT id, name, zqu__ZuoraSubscriptionID__c FROM zqu__Quote__c WHERE id =: QuoteId];
        system.debug('###'+quote.zqu__ZuoraSubscriptionID__c);
        Http h = new Http();
        HttpRequest httpQuery = new HttpRequest();
        httpQuery.setEndpoint(zcs.Query_Endpoint__c);
        httpQuery.setHeader('authorization', 'bearer ' + token);
        httpQuery.setHeader('content-type', 'application/json');
        httpQuery.setBody('{"queryString":"select id, InvoiceId from InvoiceItem where SubscriptionId =\''+quote.zqu__ZuoraSubscriptionID__c+'\' "}');
        httpQuery.setMethod('POST');
        httpQuery.setTimeout(6000);
        HttpResponse respQuery = h.send(httpQuery);
        system.debug(respQuery.getStatus());
        System.debug('@@@@respToken:  '+respQuery.getBody());
        
        jsonQuery = (Map<String, Object>)JSON.deserializeUntyped(respQuery.getBody());
        
        
        List<Object> risultati = (List<Object>)jsonQuery.get('records');
        String s;
        for(Object o: risultati){
            s = String.valueof(o);
            system.debug(s);
            s= s.split('InvoiceId=')[1];
            s= s.split('}')[0];
            system.debug(s);
        }
        
        system.debug(s);
        return s;
    }
    
    public List<String> getInvoiceIds(List<String> subscriptionIds, String token){
        
        Map<String, Object> jsonQuery = new Map<String, Object>();
        ZuoraInvoiceCustomSetting__c zcs = ZuoraInvoiceCustomSetting__c.getOrgDefaults();
        List<String> invoiceIds = new List<String>();
        
        for(String sub: subscriptionIds){
            System.debug('@@@@@'+sub);
            Http h = new Http();
            HttpRequest httpQuery = new HttpRequest();
            httpQuery.setEndpoint(zcs.Query_Endpoint__c);
            httpQuery.setHeader('authorization', 'bearer ' + token);
            httpQuery.setHeader('content-type', 'application/json');
            httpQuery.setBody('{"queryString":"select id, InvoiceId from InvoiceItem where SubscriptionId =\''+sub+'\' "}');
            httpQuery.setMethod('POST');
            httpQuery.setTimeout(6000);
            HttpResponse respQuery = h.send(httpQuery);
            system.debug(respQuery.getStatus());
            System.debug('@@@@respToken:  '+respQuery.getBody());
            
            jsonQuery = (Map<String, Object>)JSON.deserializeUntyped(respQuery.getBody());
            
            
            List<Object> risultati = (List<Object>)jsonQuery.get('records');
            String s;
            for(Object o: risultati){
                s = String.valueof(o);
                system.debug(s);
                s= s.split('InvoiceId=')[1];
                s= s.split('}')[0];
                system.debug(s);
            }
            if(s != null){
                invoiceIds.add(s);
                system.debug('NON NULLO');
            }
            
        }
        return invoiceIds;
    }
    
    /**
    * getInvoiceNumber
    * descrizione
    * 
    * @param String token
    * @param String InvoiceId 
    * @returns String
    */
    public String getInvoiceNumber(String token, String InvoiceId){
        
        Map<String, Object> jsonQuery = new Map<String, Object>();
        ZuoraInvoiceCustomSetting__c zcs = ZuoraInvoiceCustomSetting__c.getOrgDefaults();
        
        Http h = new Http();
        HttpRequest httpQuery = new HttpRequest();
        httpQuery.setEndpoint(zcs.Query_Endpoint__c);
        httpQuery.setHeader('authorization', 'bearer ' + token);
        httpQuery.setHeader('content-type', 'application/json');
        httpQuery.setBody('{"queryString":"select id, InvoiceNumber from Invoice where id =\''+InvoiceId+'\'"}');
        httpQuery.setMethod('POST');
        httpQuery.setTimeout(6000);
        HttpResponse respQuery = h.send(httpQuery);
        system.debug(respQuery.getStatus());
        System.debug('@@@@respToken:  '+respQuery.getBody());
        
        jsonQuery = (Map<String, Object>)JSON.deserializeUntyped(respQuery.getBody());
        
        
        List<Object> risultati = (List<Object>)jsonQuery.get('records');
        String s;
        for(Object o: risultati){
            s = String.valueof(o);
            system.debug(s);
            s= s.split('InvoiceNumber=')[1];
            s= s.split('}')[0];
            system.debug(s);
        }
        
        system.debug(s);
        return s;
    }
    
    /**
    * getAccessToken
    * descrizione
    * 
    * @returns String
    */
    public String getAccessToken(){
        
        ZuoraInvoiceCustomSetting__c zcs = ZuoraInvoiceCustomSetting__c.getOrgDefaults();
        
        Map<String, Object> jsonToken = new Map<String, Object>();
        
        String token = '';
        String pdfUrl = '';
        String payloadToken = 'client_id=' + EncodingUtil.urlEncode(zcs.Client_Id__c, 'UTF-8') + '&client_secret=' + EncodingUtil.urlEncode(zcs.Client_Secret__c, 'UTF-8') +
            '&grant_type=' + EncodingUtil.urlEncode('client_credentials', 'UTF-8');
        
        Http h = new Http();
        HttpRequest reqToken = new HttpRequest();
        reqToken.setEndpoint(zcs.Token_Endpoint__c); 
        
        
        reqToken.setHeader('apiAccessKeyId', zcs.User_Id__c);
        reqToken.setHeader('apiSecretAccessKey', zcs.User_Password__c);
        reqToken.setHeader('grant_type', 'client_credentials');
        reqToken.setHeader('Content-Type', 'application/x-www-form-urlencoded');     
        reqToken.setTimeout(6000);
        reqToken.setMethod('POST');  
        reqToken.setBody(payloadToken);
        
        HttpResponse respToken = h.send(reqToken);
        
        jsonToken = (Map<String, Object>)JSON.deserializeUntyped(respToken.getBody());
        
        token = (String)jsonToken.get('access_token');
        system.debug(token);
        return token;
    }
    
    /**
    * getPDFUrl
    * descrizione
    * 
    * @param String token
    * @param String InvoiceId
    * 
    * @returns String
    */
    public String getPDFUrl(String token, String InvoiceId){
        ZuoraInvoiceCustomSetting__c zcs = ZuoraInvoiceCustomSetting__c.getOrgDefaults();
        Map<String, Object> jsonLink = new Map<String, Object>();
        String pdfUrl;
        
        HttpRequest reqLink = new HttpRequest();
        
        reqLink.setEndpoint(zcs.Invoice_Endpoint__c + InvoiceId + '/files');
        
        reqLink.setHeader('authorization', 'bearer ' + token);
        reqLink.setHeader('content-type', 'application/json');
        system.debug(zcs.Invoice_Endpoint__c + InvoiceId + '/files');
        reqLink.setTimeout(6000);
        
        reqLink.setMethod('GET');
        
        HttpResponse respLink = new Http().send(reqLink);
        System.debug('@@@@RespLink:  '+respLink.getBody());
        
        
        jsonLink = (Map<String, Object>)JSON.deserializeUntyped(respLink.getBody());
        
        List<Object> invoiceFile = (List<Object>)jsonLink.get('invoiceFiles');
        if(invoiceFile != null){
            for(Object fld : invoiceFile) {
                Map<String,Object> data = (Map<String,Object>)fld;
                pdfUrl = (String)data.get('pdfFileUrl');
                system.debug('@@@pdfUrl: ' + pdfUrl);
            }
        }
        
        return pdfUrl;
    }
    
    /**
    * AttachPDF
    * descrizione
    * 
    * @param String token
    * @param String PDFUrl
    * @param String InvoiceId
    * @param String QuoteId
    * 
    * @returns String
    */
    public String AttachPDF(String token, String PDFUrl, String InvoiceId, String QuoteId){
        ZuoraInvoiceCustomSetting__c zcs = ZuoraInvoiceCustomSetting__c.getOrgDefaults();
        Attachment newAtt = new Attachment();
        Blob body;
        HttpRequest reqPdf = new HttpRequest();
        
        reqPdf.setEndpoint(zcs.Zuora_Endpoint__c + pdfUrl);
        
        reqPdf.setHeader('authorization', 'bearer ' + token);
        reqPdf.setHeader('content-type', 'application/zip');
        
        reqPdf.setTimeout(6000);
        
        reqPdf.setMethod('GET');
        
        HttpResponse respPdf = new Http().send(reqPdf);
        System.debug('@@@@respPdf:  '+respPdf);
        
        
        body = respPdf.getBodyAsBlob();
        
        String nameFile = InvoiceId+'.pdf';
        try{
        	newAtt = [SELECT Name, Body, ParentId, ContentType FROM Attachment Where Name =: nameFile AND ParentId =: QuoteId];
            newAtt.Body = body;
            update newAtt;
        }catch(Exception e){
            newAtt.Name = nameFile;
            newAtt.Body = body;
            newAtt.ParentId = QuoteId;
            newAtt.ContentType = 'application/pdf';
            
            insert newAtt;
        }
        return newAtt.id;
    }
    
    
    public String DownloadPdf(String token, String PDFUrl, String InvoiceName, String InvoiceId){
        
        
        ZuoraInvoiceCustomSetting__c zcs = ZuoraInvoiceCustomSetting__c.getOrgDefaults();
        Attachment newAtt = new Attachment();
        Blob body;
        HttpRequest reqPdf = new HttpRequest();
        
        reqPdf.setEndpoint(zcs.Zuora_Endpoint__c + pdfUrl);
        
        reqPdf.setHeader('authorization', 'bearer ' + token);
        reqPdf.setHeader('content-type', 'application/zip');
        
        reqPdf.setTimeout(6000);
        
        reqPdf.setMethod('GET');
        
        HttpResponse respPdf = new Http().send(reqPdf);
        System.debug('@@@@respPdf:  '+respPdf);
        
        
        body = respPdf.getBodyAsBlob();      
        Attachment at= new Attachment(ContentType = 'application/octet-stream');             
        at.parentId = InvoiceId;            
        at.body = body;             
        at.name = InvoiceName+'.pdf';            
        system.debug('at.name** '+ at.name);            
        try
        {             
            insert at;             
        }             
        catch (System.QueryException e)
        {             
            system.debug('System.QueryException of method attSave for insert operation'+ e);             
        }             
        String attachmentId = at.id;                                     
        String pageRef = '/servlet/servlet.FileDownload?file='+attachmentid;
        system.debug('attachmentId** '+attachmentId );             
        system.debug('at.name** '+ at.name);                          
                    
        return pageRef;         
    
}

    /**
    * getPDF
    * descrizione
    * 
    * @param String token
    * @param String PDFUrl
    * 
    * @returns String
    */
    public String getPDF(String token, String PDFUrl){
        ZuoraInvoiceCustomSetting__c zcs = ZuoraInvoiceCustomSetting__c.getOrgDefaults();
        Attachment newAtt = new Attachment();
        Blob body;
        HttpRequest reqPdf = new HttpRequest();
        
        reqPdf.setEndpoint(zcs.Zuora_Endpoint__c + pdfUrl);
        
        reqPdf.setHeader('authorization', 'bearer ' + token);
        reqPdf.setHeader('content-type', 'application/zip');
        
        reqPdf.setTimeout(6000);
        
        reqPdf.setMethod('GET');
        
        HttpResponse respPdf = new Http().send(reqPdf);
        System.debug('@@@@respPdf:  '+respPdf);
        
        body = respPdf.getBodyAsBlob();
        return respPdf.getBody();
        //return body;
    }
}