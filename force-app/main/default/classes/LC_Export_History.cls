public class LC_Export_History{

	public List<Wallet_Transaction__c> walletTransactions{get;set;}
    
    public pageReference DownloadHistory(){
        String recordId = apexpages.currentpage().getparameters().get('recordId');
        String startDate = apexpages.currentpage().getparameters().get('startDate');
       	String endDate = apexpages.currentpage().getparameters().get('endDate');
        Date dt1 = Date.valueOf(startDate);
        Date dt2 = Date.valueOf(endDate);
        System.debug(recordId);
        system.debug(startDate);
        system.debug(dt1);
        walletTransactions = [SELECT Id, Name, Date__c, Wallet__c, Amount__c, Payment_Method__c, Transaction_Category__c, Description__c FROM Wallet_Transaction__c WHERE Wallet__c =:recordId
                             AND Date__c >= :dt1 AND Date__c <= :dt2 AND Transaction_Category__c = 'Top-up' ORDER BY Date__c DESC];
        return null;
    }
}