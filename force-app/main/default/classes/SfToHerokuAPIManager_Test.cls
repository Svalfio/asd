@isTest
public class SfToHerokuAPIManager_Test {

    @isTest
    static void test1(){
       
        SfToHerokuAPIManager.HerokuResponse response = new SfToHerokuAPIManager.HerokuResponse();
        
        SfToHK_Urls__c urls = new SfToHK_Urls__c();
        urls.Base_Url__c='///';
        insert urls;
        
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new CallOutMockSetter('201', null));
		response = SfToHerokuAPIManager.createCustomerAccount(null);
		response = SfToHerokuAPIManager.createTechnicalAccount(null);
        response = SfToHerokuAPIManager.createCustomerUser(null);
        response = SfToHerokuAPIManager.technicalServiceCreation(null);
        Test.setMock(HttpCalloutMock.class, new CallOutMockSetter('200', null));
        response = SfToHerokuAPIManager.serviceCoverageCheck(null, null, null, null, null);
        response = SfToHerokuAPIManager.retrieveTechAccount(null);
        response = SfToHerokuAPIManager.sendSingleNotification(null);
        response = SfToHerokuAPIManager.preActivationCheck(null, null);
        response = SfToHerokuAPIManager.cancellationTechAccount(null);
        response = SfToHerokuAPIManager.technicalServiceEdit(null, null);
        response = SfToHerokuAPIManager.retrieveTechService(null);
        response = SfToHerokuAPIManager.technicalAccountChangeItem(null);
        response = SfToHerokuAPIManager.technicalAccountAssignAddon(null);
        test.stopTest();
    }    
}