public class AccountTriggerHandler{
    
    public void onBeforeInsert(List<Account> triggerNew){
        for(Account a: triggerNew) {
            if (a.BillingCountry != null && !String.isBlank(a.BillingCountry)) {
                a.Billing_Country_Name__c = this.getCountryName(a.BillingCountry);
            }
        }
    }

    public void OnAfterUpdate(Map<id,Account> triggerOld , Map<id,Account> triggerNew){
        system.debug('AccountTriggerHandler------OnAfterUpdate');
        Id resellerRTId =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_RESELLER).getRecordTypeId();
        Set<String> accnIdSet = new  Set<String>();
        Map<String,String> accModelCurrMap = new Map<String,String>();
        
        for(Id key: triggerNew.keySet()){
            if(triggerOld.get(key).CurrencyIsoCode != triggerNew.get(key).CurrencyIsoCode && triggerOld.get(key).RecordTypeId == resellerRTId){
                accnIdSet.add(key);
                accModelCurrMap.put(key,triggerNew.get(key).CurrencyIsoCode);
            }    
        }
        
        List<Commission_Model__c> lstModels = [SELECT Id,CurrencyIsoCode,Reseller_Id__c FROM Commission_Model__c WHERE Reseller_Id__c = :accnIdSet];
        List<Commission_Item__c> lstItems = [SELECT Id,CurrencyIsoCode,Commission_Model__r.Reseller_Id__c FROM Commission_Item__c WHERE Commission_Model__r.Reseller_Id__c = :accnIdSet];
        
        List<Commission_Model__c> modelUpdLst = new List<Commission_Model__c>();
        List<Commission_Item__c> itemUpdLst = new List<Commission_Item__c>();
        
        for(Commission_Model__c tmpMod : lstModels){
            
            if(accModelCurrMap.containsKey(tmpMod.Reseller_Id__c)){
                tmpMod.CurrencyIsoCode = accModelCurrMap.get(tmpMod.Reseller_Id__c);
                modelUpdLst.add(tmpMod);
            }
        }
        
        for(Commission_Item__c tmpMod2 : lstItems){
            
            if(accModelCurrMap.containsKey(tmpMod2.Commission_Model__r.Reseller_Id__c)){
                tmpMod2.CurrencyIsoCode = accModelCurrMap.get(tmpMod2.Commission_Model__r.Reseller_Id__c);
                itemUpdLst.add(tmpMod2);
            }
        }

        if(modelUpdLst.size() > 0)
            update modelUpdLst;
        
        if(itemUpdLst.size() > 0)
            update itemUpdLst;
    }    
    
    
    public void OnBeforeUpdate(Map<id,Account> triggerOld , Map<id,Account> triggerNew){
        system.debug('AccountTriggerHandler------OnBeforeUpdate');
  
        for(Id key: triggerNew.keySet()){
            
            if(triggerOld.get(key).Commission_Payment_Method__c!=null && triggerNew.get(key).Commission_Payment_Method__c!=null){
                if(!triggerOld.get(key).Commission_Payment_Method__c.equals(triggerNew.get(key).Commission_Payment_Method__c) ){
                    if(Date.today().daysBetween(Date.today().addMonths(1).toStartofMonth()) <= 14){
                        triggerNew.get(key).Commission_Payment_Method__c.addError('You cannot update this field in this day');
                    }
                }
            }

            if((triggerOld.get(key).Commission_Payment_Method__c==null && triggerNew.get(key).Commission_Payment_Method__c!=null) ||(triggerOld.get(key).Commission_Payment_Method__c!=null && triggerNew.get(key).Commission_Payment_Method__c==null)){
                if(Date.today().daysBetween(Date.today().addMonths(1).toStartofMonth()) <= 14){
                    triggerNew.get(key).Commission_Payment_Method__c.addError('You cannot update this field in this day');
                }
            }

            // Billing_Country_Name__c
            if (triggerOld.get(key).BillingCountry!=null && triggerNew.get(key).BillingCountry!=null){
                if (!triggerOld.get(key).BillingCountry.equals(triggerNew.get(key).BillingCountry)) {
                    triggerNew.get(key).Billing_Country_Name__c = this.getCountryName(triggerNew.get(key).BillingCountry);
                }
            } else if (triggerOld.get(key).BillingCountry==null && triggerNew.get(key).BillingCountry!=null) {
                triggerNew.get(key).Billing_Country_Name__c = this.getCountryName(triggerNew.get(key).BillingCountry);
            } else if (triggerOld.get(key).BillingCountry!=null && triggerNew.get(key).BillingCountry==null){
                triggerNew.get(key).Billing_Country_Name__c = '';
            }
        }
        
        Id indirectRecordTypeId = Schema.SObjectType.Commission_Model__c.getRecordTypeInfosByDeveloperName().get('Indirect').getRecordTypeId();
        Id resellerRTId =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_RESELLER).getRecordTypeId();
                
        Set<String> accnIdSet = new  Set<String>();
        //Map<String,String> accCurrMap = new Map<String,String>();
        Map<String,List<Commission_Model__c>> accModelMap = new Map<String,List<Commission_Model__c>>();
        
        for(Id key: triggerNew.keySet()){
            system.debug('Old: '+triggerOld.get(key).CurrencyIsoCode);
            system.debug('New: '+triggerNew.get(key).CurrencyIsoCode);
            system.debug('RecordTypeId: '+ triggerNew.get(key).RecordTypeId);
            system.debug('resellerRTId: '+resellerRTId);
            if(triggerOld.get(key).CurrencyIsoCode != triggerNew.get(key).CurrencyIsoCode && triggerNew.get(key).RecordTypeId == resellerRTId){
                accnIdSet.add(key);
               // accCurrMap.put(key,triggerNew.get(key).CurrencyIsoCode);
            }    
        }
        
        List<Commission_Model__c> lstModelsInd = [SELECT Id,CurrencyIsoCode,Reseller_Id__c,Sub_Reseller_Id__c,Sub_Reseller_Id__r.CurrencyIsoCode FROM Commission_Model__c WHERE RecordTypeId = : indirectRecordTypeId AND Reseller_Id__c = :accnIdSet];
       	Map<String,String> subCurrMap = new Map<String,String>();
        
        for(Commission_Model__c tmpModel : lstModelsInd){
            subCurrMap.put(tmpModel.Sub_Reseller_Id__c,tmpModel.Sub_Reseller_Id__r.CurrencyIsoCode);
            if(accModelMap.containsKey(tmpModel.Reseller_Id__c)){
                List<Commission_Model__c> tmpLst = accModelMap.get(tmpModel.Reseller_Id__c);
                tmpLst.add(tmpModel);
                accModelMap.put(tmpModel.Reseller_Id__c,tmpLst);
            }
            else{
                List<Commission_Model__c> tmpLst = new List<Commission_Model__c>();
                tmpLst.add(tmpModel);
                accModelMap.put(tmpModel.Reseller_Id__c,tmpLst);
            }            
        }
        
        for(Id key: triggerNew.keySet()){
            if(accModelMap.containsKey(Key)){
                List<Commission_Model__c> checkLst = accModelMap.get(Key);
                for(Commission_Model__c tmpcheckModel : checkLst){
                    String resCurrency =  '';
                    
                    if(subCurrMap.containsKey(tmpcheckModel.Sub_Reseller_Id__c))
                		resCurrency = subCurrMap.get(tmpcheckModel.Sub_Reseller_Id__c);
                    
                    if(resCurrency != triggerNew.get(key).CurrencyIsoCode)
                        triggerNew.get(key).CurrencyIsoCode.addError('You cannot update this field, please check the currency of linked Resellers');
                }
            }
        }
        List<Contact> contactList = [SELECT Id,Email,Phone,AccountId FROM Contact WHERE AccountId IN :triggerNew.keySet()];
        Map<Id, Contact> contactMap = new Map<Id, Contact>();
        for(Contact currentCont : contactList){
            contactMap.put(currentCont.AccountId, currentCont);
        }
        for(Id key: triggerNew.keySet()){
            
            if(triggerNew.get(key).Customer_Portal_Access__c=='Yes'){
                
                SfToHerokuAPIManager.CustomerUserCreationRequest request = new SfToHerokuAPIManager.CustomerUserCreationRequest();
                request.accountId=Integer.valueOf(triggerNew.get(key).OSS_Customer_Account_ID__c);
                request.username=triggerNew.get(key).Name;
                request.mainContactMail=contactMap.get(key).Email;
                request.mainContactPhone=contactMap.get(key).Phone;
                request.enabled=true;
                
                SfToHerokuAPIManager.HerokuResponse response = SfToHerokuAPIManager.createCustomerUser(request);
                
                if(response.errorCode==201){
                    
                    SfToHerokuAPIManager.CustomerUserCreationResponse userDto = (SfToHerokuAPIManager.CustomerUserCreationResponse)response.data;
                    
                    triggerNew.get(key).OSS_Customer_User_ID__c = String.valueOf(userDto.id);
                }
            }
        }
    }
    
    public void onAfterInsert(List<Account> accountList){
        system.debug('AccountTriggerHandler------onAfterInsert--------START');
        
        Map<Id,Account> resellerMap = new Map<Id,Account>();
        Map<Id,Account> subResellerMap = new Map<Id,Account>();
        Map<Id,Account> DistributorMap = new Map<Id,Account>();
        
        for(Account a : accountList){
            if(a.Account_Type__c=='Reseller'){
                if(!resellerMap.containsKey(a.id))
                    resellerMap.put(a.Id,a);
                
                //createResellerCommissionModel(resellerMap);
                
            }
            
            else if(a.Account_Type__c=='Sub_Reseller'){
                if(!subResellerMap.containsKey(a.id))
                    subResellerMap.put(a.Id,a);
                
                //createSubResellerCommissionModel(subResellerMap);
                //createResellerCommissionModel(subResellerMap);
                
            }
            
            /*
            else if(a.Account_Type__c=='Distributor'){
                if(!DistributorMap.containsKey(a.id))
                    DistributorMap.put(a.Id,a);
                
            }
			*/
            
            
        }
        
        system.debug('resellerMap: '+Json.serialize(resellerMap));
        system.debug('subResellerMap: '+Json.serialize(subResellerMap));
        //system.debug('DistributorMap: '+Json.serialize(DistributorMap));
        
        createResellerWallet(resellerMap);
        createSubResellerWallet(subResellerMap);
        //createDistributorWallet(DistributorMap);
        
    }
    /*
    public void onBeforeDelete(List<Account> accountList){
        
        Map<Id,Account> resellerMap = new Map<Id,Account>();
        Map<Id,Account> subResellerMap = new Map<Id,Account>();
        
        for(Account a : accountList){
            if(a.Account_Type__c=='Reseller'){
                if(!resellerMap.containsKey(a.id))
                    resellerMap.put(a.Id,a);
                
                //deleteResellerCommissionModel(resellerMap);
                
            }
            else if(a.Account_Type__c=='Sub_Reseller'){
                if(!subResellerMap.containsKey(a.id))
                    subResellerMap.put(a.Id,a);
                
                //deleteSubResellerCommissionModel(subResellerMap);
                
            }
        }
    }
    */
    ////////////////////////
    ////////////////////////
    //  Internal Methods  //
    ////////////////////////
    ////////////////////////
    
    public static void createResellerWallet(Map<Id,Account> resellerMap){
        system.debug('AccountTriggerHandler-------CreateResellerWallet------START');
        Id idRT = Schema.SObjectType.Wallet__c.getRecordTypeInfosByDeveloperName().get('Reseller_Wallet').getRecordTypeId();
        List<Wallet__c> resellerWalletList =new List<Wallet__c>();
        for(Account a : resellerMap.values()){
            Wallet__c resellerWallet = new Wallet__c();
            resellerWallet.RecordTypeId=idRT;
            resellerWallet.Account__c=a.Id;
            resellerWallet.Amount__c=0;
            resellerWallet.Status__c='Active';
            resellerWallet.Activation_Start_Date__c=Date.today();
            resellerWalletList.add(resellerWallet);
        }
        
        if(resellerWalletList.size()>0){
            system.debug('ResellerWalletList: '+Json.serialize(resellerWalletList));
            insert ResellerWalletList;
        }
        
        
    }
    
    public static void createSubResellerWallet(Map<Id,Account> subResellerMap){
        system.debug('AccountTriggerHandler-------CreateSubResellerWallet------START');
        Id idRT =  Schema.SObjectType.Wallet__c.getRecordTypeInfosByDeveloperName().get('Sub_Reseller_Wallet').getRecordTypeId();
        system.debug('subResellerMap: '+JSON.serialize(subResellerMap));
        
        
        Set<Id> resellerSet = new Set<Id>();
        for(Account a : subResellerMap.values()){
            resellerSet.add(a.ParentId);
        }
        
        List<Account> resellerList = [Select id,Wallet__c From Account where id in : resellerSet];
        Set<Id> resellerWalletSet = new Set<Id>();
        for(Account res : resellerList)
            resellerWalletSet.add(res.wallet__c);
        
        List<Wallet__c> fatherWallet = [Select id,Account__c from wallet__c where id in : resellerWalletSet];
        Map<Id,Id> resellerWalletMap = new Map<Id,Id>();
        for(Wallet__c wall : fatherWallet){
            if(!resellerWalletMap.containsKey(wall.account__c))
                resellerWalletMap.put(wall.account__c,wall.id);
        }
        
        List<Wallet__c> subResellerWalletList = new List<Wallet__c>();
        for(Account a : subResellerMap.values()){
            Wallet__c subResellerWallet = new Wallet__c();
            subResellerWallet.RecordTypeId=idRT;
            subResellerWallet.Account__c=a.Id;
            subResellerWallet.Amount__c=0;
            subResellerWallet.Status__c='Active';
            subResellerWallet.Activation_Start_Date__c=Date.today();
            subResellerWallet.Reseller_Wallet__c=resellerWalletMap.get(a.ParentId);
            subResellerWalletList.add(subResellerWallet);
            
        }
        if(subResellerWalletList.size()>0)
            insert subResellerWalletList;
        
    }

    /**
     * getCountryName
     * Returns extended country name
     *
     * @param String countryCode
     * @returns string
     */
    private String getCountryName(String countryCode){
        String returnValue = '';
        switch on countryCode {
            when 'ABW' {
                returnValue = 'Aruba';
            }
            when 'AFG' {
                returnValue = 'Afghanistan';
            }
            when 'AGO' {
                returnValue = 'Angola';
            }
            when 'AIA' {
                returnValue = 'Anguilla';
            }
            when 'ALA' {
                returnValue = 'Åland Islands';
            }
            when 'ALB' {
                returnValue = 'Albania';
            }
            when 'AND' {
                returnValue = 'Andorra';
            }
            when 'ARE' {
                returnValue = 'United Arab Emirates';
            }
            when 'ARG' {
                returnValue = 'Argentina';
            }
            when 'ARM' {
                returnValue = 'Armenia';
            }
            when 'ASM' {
                returnValue = 'American Samoa';
            }
            when 'ATA' {
                returnValue = 'Antarctica';
            }
            when 'ATF' {
                returnValue = 'French Southern Territories';
            }
            when 'ATG' {
                returnValue = 'Antigua and Barbuda';
            }
            when 'AUS' {
                returnValue = 'Australia';
            }
            when 'AUT' {
                returnValue = 'Austria';
            }
            when 'AZE' {
                returnValue = 'Azerbaijan';
            }
            when 'BDI' {
                returnValue = 'Burundi';
            }
            when 'BEL' {
                returnValue = 'Belgium';
            }
            when 'BEN' {
                returnValue = 'Benin';
            }
            when 'BES' {
                returnValue = 'Bonaire, Sint Eustatius and Saba';
            }
            when 'BFA' {
                returnValue = 'Burkina Faso';
            }
            when 'BGD' {
                returnValue = 'Bangladesh';
            }
            when 'BGR' {
                returnValue = 'Bulgaria';
            }
            when 'BHR' {
                returnValue = 'Bahrain';
            }
            when 'BHS' {
                returnValue = 'Bahamas';
            }
            when 'BIH' {
                returnValue = 'Bosnia and Herzegovina';
            }
            when 'BLM' {
                returnValue = 'Saint Barthélemy';
            }
            when 'BLR' {
                returnValue = 'Belarus';
            }
            when 'BLZ' {
                returnValue = 'Belize';
            }
            when 'BMU' {
                returnValue = 'Bermuda';
            }
            when 'BOL' {
                returnValue = 'Bolivia (Plurinational State of)';
            }
            when 'BRA' {
                returnValue = 'Brazil';
            }
            when 'BRB' {
                returnValue = 'Barbados';
            }
            when 'BRN' {
                returnValue = 'Brunei Darussalam';
            }
            when 'BTN' {
                returnValue = 'Bhutan';
            }
            when 'BVT' {
                returnValue = 'Bouvet Island';
            }
            when 'BWA' {
                returnValue = 'Botswana';
            }
            when 'CAF' {
                returnValue = 'Central African Republic';
            }
            when 'CAN' {
                returnValue = 'Canada';
            }
            when 'CCK' {
                returnValue = 'Cocos (Keeling) Islands';
            }
            when 'CHE' {
                returnValue = 'Switzerland';
            }
            when 'CHL' {
                returnValue = 'Chile';
            }
            when 'CHN' {
                returnValue = 'China';
            }
            when 'CIV' {
                returnValue = 'Côte d\'Ivoire';
            }
            when 'CMR' {
                returnValue = 'Cameroon';
            }
            when 'COD' {
                returnValue = 'Congo, Democratic Republic of the';
            }
            when 'COG' {
                returnValue = 'Congo';
            }
            when 'COK' {
                returnValue = 'Cook Islands';
            }
            when 'COL' {
                returnValue = 'Colombia';
            }
            when 'COM' {
                returnValue = 'Comoros';
            }
            when 'CPV' {
                returnValue = 'Cabo Verde';
            }
            when 'CRI' {
                returnValue = 'Costa Rica';
            }
            when 'CUB' {
                returnValue = 'Cuba';
            }
            when 'CUW' {
                returnValue = 'Curaçao';
            }
            when 'CXR' {
                returnValue = 'Christmas Island';
            }
            when 'CYM' {
                returnValue = 'Cayman Islands';
            }
            when 'CYP' {
                returnValue = 'Cyprus';
            }
            when 'CZE' {
                returnValue = 'Czechia';
            }
            when 'DEU' {
                returnValue = 'Germany';
            }
            when 'DJI' {
                returnValue = 'Djibouti';
            }
            when 'DMA' {
                returnValue = 'Dominica';
            }
            when 'DNK' {
                returnValue = 'Denmark';
            }
            when 'DOM' {
                returnValue = 'Dominican Republic';
            }
            when 'DZA' {
                returnValue = 'Algeria';
            }
            when 'ECU' {
                returnValue = 'Ecuador';
            }
            when 'EGY' {
                returnValue = 'Egypt';
            }
            when 'ERI' {
                returnValue = 'Eritrea';
            }
            when 'ESH' {
                returnValue = 'Western Sahara';
            }
            when 'ESP' {
                returnValue = 'Spain';
            }
            when 'EST' {
                returnValue = 'Estonia';
            }
            when 'ETH' {
                returnValue = 'Ethiopia';
            }
            when 'FIN' {
                returnValue = 'Finland';
            }
            when 'FJI' {
                returnValue = 'Fiji';
            }
            when 'FLK' {
                returnValue = 'Falkland Islands (Malvinas)';
            }
            when 'FRA' {
                returnValue = 'France';
            }
            when 'FRO' {
                returnValue = 'Faroe Islands';
            }
            when 'FSM' {
                returnValue = 'Micronesia (Federated States of)';
            }
            when 'GAB' {
                returnValue = 'Gabon';
            }
            when 'GBR' {
                returnValue = 'United Kingdom of Great Britain and Northern Ireland';
            }
            when 'GEO' {
                returnValue = 'Georgia';
            }
            when 'GGY' {
                returnValue = 'Guernsey';
            }
            when 'GHA' {
                returnValue = 'Ghana';
            }
            when 'GIB' {
                returnValue = 'Gibraltar';
            }
            when 'GIN' {
                returnValue = 'Guinea';
            }
            when 'GLP' {
                returnValue = 'Guadeloupe';
            }
            when 'GMB' {
                returnValue = 'Gambia';
            }
            when 'GNB' {
                returnValue = 'Guinea-Bissau';
            }
            when 'GNQ' {
                returnValue = 'Equatorial Guinea';
            }
            when 'GRC' {
                returnValue = 'Greece';
            }
            when 'GRD' {
                returnValue = 'Grenada';
            }
            when 'GRL' {
                returnValue = 'Greenland';
            }
            when 'GTM' {
                returnValue = 'Guatemala';
            }
            when 'GUF' {
                returnValue = 'French Guiana';
            }
            when 'GUM' {
                returnValue = 'Guam';
            }
            when 'GUY' {
                returnValue = 'Guyana';
            }
            when 'HKG' {
                returnValue = 'Hong Kong';
            }
            when 'HMD' {
                returnValue = 'Heard Island and McDonald Islands';
            }
            when 'HND' {
                returnValue = 'Honduras';
            }
            when 'HRV' {
                returnValue = 'Croatia';
            }
            when 'HTI' {
                returnValue = 'Haiti';
            }
            when 'HUN' {
                returnValue = 'Hungary';
            }
            when 'IDN' {
                returnValue = 'Indonesia';
            }
            when 'IMN' {
                returnValue = 'Isle of Man';
            }
            when 'IND' {
                returnValue = 'India';
            }
            when 'IOT' {
                returnValue = 'British Indian Ocean Territory';
            }
            when 'IRL' {
                returnValue = 'Ireland';
            }
            when 'IRN' {
                returnValue = 'Iran (Islamic Republic of)';
            }
            when 'IRQ' {
                returnValue = 'Iraq';
            }
            when 'ISL' {
                returnValue = 'Iceland';
            }
            when 'ISR' {
                returnValue = 'Israel';
            }
            when 'ITA' {
                returnValue = 'Italy';
            }
            when 'JAM' {
                returnValue = 'Jamaica';
            }
            when 'JEY' {
                returnValue = 'Jersey';
            }
            when 'JOR' {
                returnValue = 'Jordan';
            }
            when 'JPN' {
                returnValue = 'Japan';
            }
            when 'KAZ' {
                returnValue = 'Kazakhstan';
            }
            when 'KEN' {
                returnValue = 'Kenya';
            }
            when 'KGZ' {
                returnValue = 'Kyrgyzstan';
            }
            when 'KHM' {
                returnValue = 'Cambodia';
            }
            when 'KIR' {
                returnValue = 'Kiribati';
            }
            when 'KNA' {
                returnValue = 'Saint Kitts and Nevis';
            }
            when 'KOR' {
                returnValue = 'Korea, Republic of';
            }
            when 'KWT' {
                returnValue = 'Kuwait';
            }
            when 'LAO' {
                returnValue = 'Lao People\'s Democratic Republic';
            }
            when 'LBN' {
                returnValue = 'Lebanon';
            }
            when 'LBR' {
                returnValue = 'Liberia';
            }
            when 'LBY' {
                returnValue = 'Libya';
            }
            when 'LCA' {
                returnValue = 'Saint Lucia';
            }
            when 'LIE' {
                returnValue = 'Liechtenstein';
            }
            when 'LKA' {
                returnValue = 'Sri Lanka';
            }
            when 'LSO' {
                returnValue = 'Lesotho';
            }
            when 'LTU' {
                returnValue = 'Lithuania';
            }
            when 'LUX' {
                returnValue = 'Luxembourg';
            }
            when 'LVA' {
                returnValue = 'Latvia';
            }
            when 'MAC' {
                returnValue = 'Macao';
            }
            when 'MAF' {
                returnValue = 'Saint Martin (French part)';
            }
            when 'MAR' {
                returnValue = 'Morocco';
            }
            when 'MCO' {
                returnValue = 'Monaco';
            }
            when 'MDA' {
                returnValue = 'Moldova, Republic of';
            }
            when 'MDG' {
                returnValue = 'Madagascar';
            }
            when 'MDV' {
                returnValue = 'Maldives';
            }
            when 'MEX' {
                returnValue = 'Mexico';
            }
            when 'MHL' {
                returnValue = 'Marshall Islands';
            }
            when 'MKD' {
                returnValue = 'North Macedonia';
            }
            when 'MLI' {
                returnValue = 'Mali';
            }
            when 'MLT' {
                returnValue = 'Malta';
            }
            when 'MMR' {
                returnValue = 'Myanmar';
            }
            when 'MNE' {
                returnValue = 'Montenegro';
            }
            when 'MNG' {
                returnValue = 'Mongolia';
            }
            when 'MNP' {
                returnValue = 'Northern Mariana Islands';
            }
            when 'MOZ' {
                returnValue = 'Mozambique';
            }
            when 'MRT' {
                returnValue = 'Mauritania';
            }
            when 'MSR' {
                returnValue = 'Montserrat';
            }
            when 'MTQ' {
                returnValue = 'Martinique';
            }
            when 'MUS' {
                returnValue = 'Mauritius';
            }
            when 'MWI' {
                returnValue = 'Malawi';
            }
            when 'MYS' {
                returnValue = 'Malaysia';
            }
            when 'MYT' {
                returnValue = 'Mayotte';
            }
            when 'NAM' {
                returnValue = 'Namibia';
            }
            when 'NCL' {
                returnValue = 'New Caledonia';
            }
            when 'NER' {
                returnValue = 'Niger';
            }
            when 'NFK' {
                returnValue = 'Norfolk Island';
            }
            when 'NGA' {
                returnValue = 'Nigeria';
            }
            when 'NIC' {
                returnValue = 'Nicaragua';
            }
            when 'NIU' {
                returnValue = 'Niue';
            }
            when 'NLD' {
                returnValue = 'Netherlands';
            }
            when 'NOR' {
                returnValue = 'Norway';
            }
            when 'NPL' {
                returnValue = 'Nepal';
            }
            when 'NRU' {
                returnValue = 'Nauru';
            }
            when 'NZL' {
                returnValue = 'New Zealand';
            }
            when 'OMN' {
                returnValue = 'Oman';
            }
            when 'PAK' {
                returnValue = 'Pakistan';
            }
            when 'PAN' {
                returnValue = 'Panama';
            }
            when 'PCN' {
                returnValue = 'Pitcairn';
            }
            when 'PER' {
                returnValue = 'Peru';
            }
            when 'PHL' {
                returnValue = 'Philippines';
            }
            when 'PLW' {
                returnValue = 'Palau';
            }
            when 'PNG' {
                returnValue = 'Papua New Guinea';
            }
            when 'POL' {
                returnValue = 'Poland';
            }
            when 'PRI' {
                returnValue = 'Puerto Rico';
            }
            when 'PRK' {
                returnValue = 'Korea (Democratic People\'s Republic of)';
            }
            when 'PRT' {
                returnValue = 'Portugal';
            }
            when 'PRY' {
                returnValue = 'Paraguay';
            }
            when 'PSE' {
                returnValue = 'Palestine, State of';
            }
            when 'PYF' {
                returnValue = 'French Polynesia';
            }
            when 'QAT' {
                returnValue = 'Qatar';
            }
            when 'REU' {
                returnValue = 'Réunion';
            }
            when 'ROU' {
                returnValue = 'Romania';
            }
            when 'RUS' {
                returnValue = 'Russian Federation';
            }
            when 'RWA' {
                returnValue = 'Rwanda';
            }
            when 'SAU' {
                returnValue = 'Saudi Arabia';
            }
            when 'SDN' {
                returnValue = 'Sudan';
            }
            when 'SEN' {
                returnValue = 'Senegal';
            }
            when 'SGP' {
                returnValue = 'Singapore';
            }
            when 'SGS' {
                returnValue = 'South Georgia and the South Sandwich Islands';
            }
            when 'SHN' {
                returnValue = 'Saint Helena, Ascension and Tristan da Cunha';
            }
            when 'SJM' {
                returnValue = 'Svalbard and Jan Mayen';
            }
            when 'SLB' {
                returnValue = 'Solomon Islands';
            }
            when 'SLE' {
                returnValue = 'Sierra Leone';
            }
            when 'SLV' {
                returnValue = 'El Salvador';
            }
            when 'SMR' {
                returnValue = 'San Marino';
            }
            when 'SOM' {
                returnValue = 'Somalia';
            }
            when 'SPM' {
                returnValue = 'Saint Pierre and Miquelon';
            }
            when 'SRB' {
                returnValue = 'Serbia';
            }
            when 'SSD' {
                returnValue = 'South Sudan';
            }
            when 'STP' {
                returnValue = 'Sao Tome and Principe';
            }
            when 'SUR' {
                returnValue = 'Suriname';
            }
            when 'SVK' {
                returnValue = 'Slovakia';
            }
            when 'SVN' {
                returnValue = 'Slovenia';
            }
            when 'SWE' {
                returnValue = 'Sweden';
            }
            when 'SWZ' {
                returnValue = 'Eswatini';
            }
            when 'SXM' {
                returnValue = 'Sint Maarten (Dutch part)';
            }
            when 'SYC' {
                returnValue = 'Seychelles';
            }
            when 'SYR' {
                returnValue = 'Syrian Arab Republic';
            }
            when 'TCA' {
                returnValue = 'Turks and Caicos Islands';
            }
            when 'TCD' {
                returnValue = 'Chad';
            }
            when 'TGO' {
                returnValue = 'Togo';
            }
            when 'THA' {
                returnValue = 'Thailand';
            }
            when 'TJK' {
                returnValue = 'Tajikistan';
            }
            when 'TKL' {
                returnValue = 'Tokelau';
            }
            when 'TKM' {
                returnValue = 'Turkmenistan';
            }
            when 'TLS' {
                returnValue = 'Timor-Leste';
            }
            when 'TON' {
                returnValue = 'Tonga';
            }
            when 'TTO' {
                returnValue = 'Trinidad and Tobago';
            }
            when 'TUN' {
                returnValue = 'Tunisia';
            }
            when 'TUR' {
                returnValue = 'Turkey';
            }
            when 'TUV' {
                returnValue = 'Tuvalu';
            }
            when 'TWN' {
                returnValue = 'Taiwan, Province of China';
            }
            when 'TZA' {
                returnValue = 'Tanzania, United Republic of';
            }
            when 'UGA' {
                returnValue = 'Uganda';
            }
            when 'UKR' {
                returnValue = 'Ukraine';
            }
            when 'UMI' {
                returnValue = 'United States Minor Outlying Islands';
            }
            when 'URY' {
                returnValue = 'Uruguay';
            }
            when 'USA' {
                returnValue = 'United States of America';
            }
            when 'UZB' {
                returnValue = 'Uzbekistan';
            }
            when 'VAT' {
                returnValue = 'Holy See';
            }
            when 'VCT' {
                returnValue = 'Saint Vincent and the Grenadines';
            }
            when 'VEN' {
                returnValue = 'Venezuela (Bolivarian Republic of)';
            }
            when 'VGB' {
                returnValue = 'Virgin Islands (British)';
            }
            when 'VIR' {
                returnValue = 'Virgin Islands (U.S.)';
            }
            when 'VNM' {
                returnValue = 'Viet Nam';
            }
            when 'VUT' {
                returnValue = 'Vanuatu';
            }
            when 'WLF' {
                returnValue = 'Wallis and Futuna';
            }
            when 'WSM' {
                returnValue = 'Samoa';
            }
            when 'YEM' {
                returnValue = 'Yemen';
            }
            when 'ZAF' {
                returnValue = 'South Africa';
            }
            when 'ZMB' {
                returnValue = 'Zambia';
            }
            when 'ZWE' {
                returnValue = 'Zimbabwe';
            }
            when else {
                returnValue = countryCode;   
            }
        }
        return returnValue;
    }
    
    /*
    public static void createDistributorWallet(Map<Id,Account> distributorMap){
        system.debug('AccountTriggerHandler-------CreateDistributorWallet------START');
        Id idRT = Schema.SObjectType.Wallet__c.getRecordTypeInfosByDeveloperName().get('Distributor_Wallet').getRecordTypeId();
        List<Wallet__c> distributorWalletList =new List<Wallet__c>();
        for(Account a : distributorMap.values()){
            Wallet__c distributorWallet = new Wallet__c();
            distributorWallet.RecordTypeId=idRT;
            distributorWallet.Account__c=a.Id;
            distributorWallet.Amount__c=0;
            distributorWallet.Status__c='Active';
            distributorWallet.Activation_Start_Date__c=Date.today();
            distributorWalletList.add(distributorWallet);
        }
        
        if(distributorWalletList.size()>0){
            system.debug('DistributorWalletList: '+Json.serialize(distributorWalletList));
            insert distributorWalletList;
        }
        
        
    }
	*/
    
    /*
public static void createResellerCommissionModel(Map<Id,Account> resellerMap){

List<Commission_Model__c> resellerCommissionModelList = new List<Commission_Model__c>();
for(Account a : resellerMap.values()){
Commission_Model__c commissionModel = new Commission_Model__c();
Id idRT =  Schema.SObjectType.Commission_Model__c.getRecordTypeInfosByDeveloperName().get('Direct').getRecordTypeId();
commissionModel.RecordTypeId = idRT;
commissionModel.Reseller_Id__c = a.Id;
commissionModel.Country__c = a.Country__c;
resellerCommissionModelList.add(commissionModel);
}
if(resellerCommissionModelList.size()>0)
insert resellerCommissionModelList;

}
*/
    /*
//DEPRECATED
public static void createSubResellerCommissionModel(Map<Id,Account> subResellerMap){

List<Commission_Model__c> subResellerCommissionModelList = new List<Commission_Model__c>();
for(Account a : subResellerMap.values()){
Commission_Model__c commissionModel = new Commission_Model__c();
Id idRT =  Schema.SObjectType.Commission_Model__c.getRecordTypeInfosByDeveloperName().get('Indirect').getRecordTypeId();
commissionModel.RecordTypeId = idRT;
commissionModel.Sub_Reseller_Id__c = a.Id;
commissionModel.Reseller_Id__c = a.ParentId;
commissionModel.Country__c = a.Country__c;
subResellerCommissionModelList.add(commissionModel);
}
if(subResellerCommissionModelList.size()>0)
insert subResellerCommissionModelList;

}
*/
    /*
//DEPRECATED
public static void deleteResellerCommissionModel(Map<Id,Account> ResellerMap){

List<Commission_Model__c> modelsToDelete = [SELECT Id FROM Commission_Model__c WHERE Reseller_Id__c IN: ResellerMap.Keyset()];
if(modelsToDelete.size()>0)
delete modelsToDelete;

}
*/
    /*
//DEPRECATED
public static void deleteSubResellerCommissionModel(Map<Id,Account> subResellerMap){

List<Commission_Model__c> modelsToDelete = [SELECT Id FROM Commission_Model__c WHERE Sub_Reseller_Id__c IN: subResellerMap.Keyset()];
if(modelsToDelete.size()>0)
delete modelsToDelete;

}
*/
    
}