@IsTest
public class Test_SearchByCaseIdController {
    
    @TestSetup
    public static void setup(){
        Id RecordTypeResidentialId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Residential').getRecordTypeId();
        Id RecordTypeResellerId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Reseller').getRecordTypeId();
        Account partner = new Account(Name = 'Partner Test', recordTypeId = RecordTypeResellerId);
        Account customer  = new Account(Name = 'Customer Test', recordTypeId = RecordTypeResidentialId);
        
        List<Account> accounts =  new List<Account>();
        accounts.add(partner);
        accounts.add(customer);
        insert accounts;
        
        Case casePartner = new Case(AccountId = partner.id);
        Case caseCustomer = new Case(AccountId = customer.id);
        
        List<Case> cases = new List<Case>();
        cases.add(casePartner);
        cases.add(caseCustomer);
        insert cases;
    }
    
    @IsTest
    public static void test(){
        Account Partner = [SELECT Id FROM Account WHERE Name = 'Partner Test'];
        Case caseP = [SELECT Id FROM Case WHERE AccountId =: Partner.id];
        Account Customer = [SELECT Id FROM Account WHERE Name = 'Customer Test'];
        Case caseC = [SELECT Id FROM Case WHERE AccountId =: Customer.id];
        
        Test.startTest();
        LC_SearchByCaseId_Controller.getCaseById(caseP.id);
        LC_SearchByCaseId_Controller.getCaseById('error');
        
        LC_SearchByCaseId_Controller.getPartnerByCaseId(caseP.id);
        LC_SearchByCaseId_Controller.getPartnerByCaseId('error');
        
        LC_SearchByCaseId_Controller.getCustomerByCaseId(caseC.id);
        LC_SearchByCaseId_Controller.getCustomerByCaseId('error');
        Test.stopTest();
    }

}