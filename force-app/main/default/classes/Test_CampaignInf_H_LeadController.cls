@isTest
public class Test_CampaignInf_H_LeadController {
    
    @isTest
    static void DownloadInformationsTest(){
        PageReference testPage = Page.VP_CampaignInfoLead;
        testPage.getParameters().put('recordId', '00Q6E000005DI8kUAG');  
        Test.setCurrentPage(testPage);
        Test.StartTest();
        LC_CampaignInf_H_LeadController controller = new LC_CampaignInf_H_LeadController();
        controller.DownloadInformations();
        Test.StopTest();
    }

}