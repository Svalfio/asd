global class OptionChangeWholesale implements zqu.QuickListController.IHierarchyAllowedRecordPlugin {
    global zqu.QuickListController.PluginResult getAllowedRecords (Map<Object, Object> contextIds, Map<String, String> pluginParams){
            
        zqu.QuickListController.PluginResult result = new zqu.QuickListController.PluginResult();    
        result.records = new List<zqu.QuickListController.PluginRecord>();
		
        Id quoteId = null;
        List<zqu__Quote__c> QuoteList = new List<zqu__Quote__c>();
        List<Account> ParentAccount = new List<Account>();
        List<String> ZuoraLiveRatePlanIds = new List<String>();
		List<String> RatePlanSubscription = new List<String>();
		List<zqu__ProductRatePlan__c> listProductToRemove = new List<zqu__ProductRatePlan__c>();
		List<zqu__ProductRatePlan__c> listProductBundleToCheck = new List<zqu__ProductRatePlan__c>(); 
		List<zqu__ProductOption__c> listProductBundleToRemove = new List<zqu__ProductOption__c>(); 
        List<zqu__ProductOption__c> listBundleOptionsToRemove = new List<zqu__ProductOption__c>();
        List<Account> CustomerAccount = new List<Account>();
        
		Set<Id> listProdBundleId = new Set<Id>();
      	for (Object k : contextIds.keySet()) {quoteId = (Id) contextIds.get(k);}              
        
        if(quoteId != null){
            try{
                    QuoteList = [SELECT zqu__Account__r.Id,zqu__Account__r.Billing__c, zqu__ExistSubscriptionID__c FROM zqu__Quote__c WHERE Id =: quoteId]; 
					CustomerAccount = [SELECT Customer_Subscription_ID__c, Billing__c FROM Account where Id=: QuoteList[0].zqu__Account__r.Id];
                    if(QuoteList.size()>0){
                        for(Zuora__SubscriptionRatePlan__c subRatePlan : [SELECT Zuora__OriginalProductRatePlanId__c 
                                                                  FROM Zuora__SubscriptionRatePlan__c 
                                                                  WHERE Zuora__Subscription__r.Zuora__Zuora_Id__c =: QuoteList[0].zqu__ExistSubscriptionID__c]){
                            ZuoraLiveRatePlanIds.add(subRatePlan.Zuora__OriginalProductRatePlanId__c);
                        }
                            
                        if(ZuoraLiveRatePlanIds.size()>0){
                            for(zqu__ProductRatePlan__c prodRatePlan : [SELECT Id, zqu__Product__r.Volume_GB__c, zqu__Product__r.Family, zqu__Product__r.zqu__Type__c, zqu__ZuoraId__c, zqu__Product__c 
                                                                        FROM zqu__ProductRatePlan__c 
                                                                        WHERE zqu__ZuoraId__c =:ZuoraLiveRatePlanIds]){
                                                                            
                                    if(prodRatePlan.zqu__Product__r.Family == 'Option')
                                		listProductToRemove.add(prodRatePlan);
                                       
                                    if(prodRatePlan.zqu__Product__r.zqu__Type__c == 'Bundle')
										listProductBundleToCheck.add(prodRatePlan);
                            }
							
							for(zqu__ProductRatePlan__c listPBCheck : listProductBundleToCheck){
								listProdBundleId.add(listPBCheck.zqu__Product__c);
							}
							
							for(zqu__ProductOption__c prodBundleOption : [SELECT Id, Name, zqu__ParentProduct__c, zqu__ChildProduct__r.Family
														FROM zqu__ProductOption__c 
														WHERE zqu__ParentProduct__c IN :listProdBundleId]){
									
									if(prodBundleOption.zqu__ChildProduct__r.Family == 'Option')
										listProductBundleToRemove.add(prodBundleOption);
							}
                        }
						
						List<zqu__ProductRatePlan__c> allowedProdRatePlans = new List<zqu__ProductRatePlan__c>();
						List<zqu__ProductRatePlan__c> mergedProdRatePlans = new List<zqu__ProductRatePlan__c>();
						List<zqu__ProductRatePlan__c> finalProdRatePlans = new List<zqu__ProductRatePlan__c>();
						
						for(Zuora__SubscriptionRatePlan__c sub : [SELECT id, Zuora__ProductRatePlanId__c FROM Zuora__SubscriptionRatePlan__c WHERE
										  Zuora__Subscription__c=: CustomerAccount[0].Customer_Subscription_ID__c AND
										  Zuora__ProductRatePlanId__c!='']){
											  system.debug('RatePlanSubscription:'+sub.Zuora__ProductRatePlanId__c);
											  RatePlanSubscription.add(sub.Zuora__ProductRatePlanId__c);
										  }
										
                        if(RatePlanSubscription.size()>0){
                            system.debug('qui0');
                            for(zqu__ProductRatePlan__c ProdRatePlan : [SELECT id, zqu__Product__c FROM zqu__ProductRatePlan__c 
                                                                        WHERE zqu__ZuoraId__c =:RatePlanSubscription AND zqu__Product__r.Status__c != 'Inactive' 
                                                                        AND zqu__Product__r.Family='Option' AND zqu__Product__r.Product_Type__c=: QuoteList[0].zqu__Account__r.Billing__c 
                                                                        AND zqu__Product__c!=null]){
                                                                            allowedProdRatePlans.add(ProdRatePlan);
                                                                        }
                            
                            system.debug('Product_RatePlans: '+allowedProdRatePlans);
                        }
                    
                    	system.debug('allowedProdRatePlans '+allowedProdRatePlans);
                        system.debug('listProductToRemove '+listProductToRemove);
                        system.debug('listProductBundleToCheck '+listProductBundleToCheck);
                        system.debug('listProductBundleToRemove '+listProductBundleToRemove);
                        
                        Id IdProductToRemove;
                        if(listProductToRemove.size() > 0){
                            for (zqu__ProductRatePlan__c allowRP : allowedProdRatePlans){
                                for (zqu__ProductRatePlan__c prodToRemove : listProductToRemove){
                                    IdProductToRemove = prodToRemove.zqu__Product__c;
                                    if(allowRP.zqu__Product__c != IdProductToRemove)
                                        finalProdRatePlans.add(allowRP);
                                }
                            }
                        }
                        
                        if(finalProdRatePlans.size() > 0 && listProductBundleToRemove.size() > 0){
                            Integer i = 0;
                            for(i=0;i<finalProdRatePlans.size();i++){
                                Integer j = 0;
                                for(j=0;j<listProductBundleToRemove.size();j++){
                                    if(listProductBundleToRemove[j].Name == finalProdRatePlans[i].Name)
                                        finalProdRatePlans.remove(i);
                                }
                            }
                        }
                        
                        system.debug('finalProdRatePlans '+finalProdRatePlans);
                        if(finalProdRatePlans.size() > 0)
							mergedProdRatePlans = finalProdRatePlans;
						else
							mergedProdRatePlans = allowedProdRatePlans;
				
                        for(zqu__ProductRatePlan__c tempProducts : mergedProdRatePlans){     
                            String tProductId = tempProducts.zqu__Product__c;
                            List<Id> ratePlanIdOk = new List<Id>();
                                
                            for(zqu__ProductRatePlan__c tempRatePlan : mergedProdRatePlans){
                                if (tempRatePlan.zqu__Product__c == tProductId){
                                    ratePlanIdOk.add(tempRatePlan.Id);
                                }   
                            }
                            
                            zqu.QuickListController.PluginRecord record = new zqu.QuickListController.PluginRecord();
                            record.recordId = tProductId;
                            record.relatedObjectIds = new Map<String, List<ID>
                            >{'zqu__productrateplan__c' => ratePlanIdOk};
                            result.records.add(record);  
                		}
                    }
            	}
            	catch(exception e){
            	System.debug('Error : '+e.getMessage());
            }     
		}
        return result;
    }
}