@IsTest
public class Test_CampaignByAccountController {

    @TestSetup
    public static void setup(){
        Account a = new Account(Name = 'Test Account Test');
        insert a;
        Contact c = new Contact(LastName = 'Test', AccountId=a.id);
        insert c;
        Campaign cam = new Campaign(Name= 'Test Campaing');
        insert cam;
        CampaignMember cm = new CampaignMember(CampaignId=cam.id, ContactId=c.Id);
        insert cm;
    }
    
    @IsTest
    public static void getCampaignsTest(){
        Account a = [SELECT Id FROM Account WHERE Name = 'Test Account Test'];
        Test.startTest();
        LC_CampaignByAccountController.getCampaigns(a.id);
        Test.stopTest();
    }
}