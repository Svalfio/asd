@isTest
public class Test_LC_ShowPlanList {
	@isTest
	static void ShowPlanList(){

		Account testAccount = new Account(name ='Grazitti');
       
		insert testAccount;

		Contact con = new Contact(LastName ='testCon',AccountId = testAccount.Id);
		insert con;

		User testUser = [SELECT id FROM User LIMIT 1];

		system.runAs(testUser){

			Wallet__c walletTest = new Wallet__c(OwnerId=testUser.id, Amount__c = 6000, CurrencyIsoCode = 'EUR', Threshold_Amount__c = 20.20);
			insert walletTest;

			zqu__ProductRatePlanCharge__c testPRodCharge = new zqu__ProductRatePlanCharge__c(Name='test prod');
			insert testPRodCharge;

		List<zqu__ProductRatePlanChargeTier__c> prodList = new List<zqu__ProductRatePlanChargeTier__c>();

		zqu__ProductRatePlanChargeTier__c testProduct = new zqu__ProductRatePlanChargeTier__c(Name='testProd', zqu__Price__c = 20, CurrencyIsoCode = 'EUR', zqu__ProductRatePlanCharge__c = testPRodCharge.id);
		prodList.add(testProduct);

		zqu__ProductRatePlanChargeTier__c testProduct1 = new zqu__ProductRatePlanChargeTier__c(Name='testProd1', zqu__Price__c = 60002, CurrencyIsoCode = 'EUR', zqu__ProductRatePlanCharge__c = testPRodCharge.id);
		prodList.add(testProduct1);

		zqu__ProductRatePlanChargeTier__c testProduct2 = new zqu__ProductRatePlanChargeTier__c(Name='testProd2', zqu__Price__c = 96, CurrencyIsoCode = 'EUR', zqu__ProductRatePlanCharge__c = testPRodCharge.id);
		prodList.add(testProduct2);

		zqu__ProductRatePlanChargeTier__c testProduct3 = new zqu__ProductRatePlanChargeTier__c(Name='testProd3', zqu__Price__c = 9846, CurrencyIsoCode = 'EUR', zqu__ProductRatePlanCharge__c = testPRodCharge.id);
		prodList.add(testProduct3);

		insert prodList;

		Test.startTest();

		List<zqu__ProductRatePlanChargeTier__c> result = LC_ShowPlanList.getProductList(testUser.id, testAccount.BillingCountry);
		Decimal resultD = LC_ShowPlanList.checkWallet(testUser.id);
		Test.stopTest();
	}
}
}