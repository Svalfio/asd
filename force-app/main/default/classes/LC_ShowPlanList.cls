public class LC_ShowPlanList {
    
    public static List<zqu__ProductRatePlanChargeTier__c> productList = new List<zqu__ProductRatePlanChargeTier__c>();
    public static Wallet__c w = new Wallet__c();
    
    @AuraEnabled
    public static List<zqu__ProductRatePlanChargeTier__c> getProductList(String userId, String countryString) {
        
        User user = [SELECT Id, AccountId FROM User WHERE Id =: userId];
        
        w = [SELECT Id, Name, CurrencyIsoCode, Amount__c, Threshold_Amount__c, Account__r.Name FROM Wallet__c WHERE Account__c=:user.AccountId];
        
        productList = [SELECT Id, Name, zqu__Price__c, CurrencyIsoCode, zqu__ProductRatePlanCharge__r.zqu__ProductRatePlan__r.Country__c
                       FROM zqu__ProductRatePlanChargeTier__c
                       WHERE CurrencyIsoCode =: w.CurrencyIsoCode
                       AND zqu__ProductRatePlanCharge__r.zqu__ProductRatePlan__r.Country__c =: countryString AND
                      zqu__ProductRatePlanCharge__r.zqu__ProductRatePlan__r.zqu__EffectiveStartDate__c <= TODAY AND
                      zqu__ProductRatePlanCharge__r.zqu__ProductRatePlan__r.zqu__EffectiveEndDate__c >= TODAY];
        
        return productList;
    }
    
    @AuraEnabled
    public static Decimal checkWallet(String userId){
        User user = [SELECT Id, AccountId FROM User WHERE Id =: userId];
        w = [SELECT Id, Name, CurrencyIsoCode, Amount__c, Threshold_Amount__c, Account__r.Name FROM Wallet__c WHERE Account__c=:user.AccountId];
        return w.Amount__c - w.Threshold_Amount__c;
    }
    
}