public class Wallet_message {
    //classe per componente che gestisce la notifica del wallet
    @AuraEnabled public static Boolean VerifyAmount(String UserId){
        Wallet__c wallet = [SELECT Id, Amount__C, Threshold_Amount__c FROM Wallet__c WHERE OwnerId =:UserId];
        if(wallet != null || wallet.Amount__c != null || wallet.Threshold_Amount__c != null){
            if(wallet.Amount__c >= wallet.Threshold_Amount__c){
                return false;
            }else{
                return true;
            }  
        }else{
            return false;
        }

    }
    
 }