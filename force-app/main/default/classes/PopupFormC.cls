public class PopupFormC {

    @AuraEnabled
    public static String returnMessage(String record_Id){
        String popUpMessage='';
        for(zqu__ProductRatePlan__c prp : [Select id, Total_Eligible_Partners__c, Partner__c from zqu__ProductRatePlan__c where id=:record_Id]){
            system.debug('Test');
            if(prp.Partner__c!='All' && prp.Total_Eligible_Partners__c==0) popUpMessage='You need to add at least one eligible partner.';
        }
        system.debug('popUpMessage: '+popUpMessage);
        return popUpMessage;
    }

}