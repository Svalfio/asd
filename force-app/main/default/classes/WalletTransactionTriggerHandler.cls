public class WalletTransactionTriggerHandler {
    
    public void onBeforeInsert(List<Wallet_Transaction__c> triggerNew){
        
        system.debug('*** START WalletTransactionTriggerHandler BeforeInsert ***');
        
        Set<String> walletIdSet = new Set<String>();
        Map<String, Decimal> wallAmountMap = new Map<String, Decimal>();
        
        //StaticResource sr = [select Id,Name,LastModifiedDate from StaticResource where Name = 'Wallet_Recharge_Invoice' LIMIT 1];
        
        for(Wallet_Transaction__c wallTrans : triggerNew){           
            walletIdSet.add(wallTrans.Wallet__c);
        }
        
        List<Wallet__c> wllList = [SELECT Id,Amount__c,RecordType.Name,Account__c FROM Wallet__c WHERE Id IN :walletIdSet];
        
        Map<Id,String> wllMap = new Map<Id,String>();
        Map<Id,Id> wllAccMap = new Map<Id,Id>();
        
        for(Wallet__c tmpWall : wllList){
            wallAmountMap.put(tmpWall.Id,tmpWall.Amount__c);
            wllMap.put(tmpWall.Id,tmpWall.RecordType.Name);
            wllAccMap.put(tmpWall.Id, tmpWall.Account__c);
        }
        
        
        system.debug('wallAmountMap: '+wallAmountMap);
        
        for(Wallet_Transaction__c wallTransTmp : triggerNew){
            
            
            
            if(wallTransTmp.Sub_Category__c=='Standard Top-up' && 
               (wallTransTmp.Payment_Method__c == 'Cash' || wallTransTmp.Payment_Method__c == 'Bank Transfer')
              && wallTransTmp.Transaction_Category__c == 'Top-up' ){
                
                  if(wallAmountMap.containsKey(wallTransTmp.Wallet__c)){
                      wallTransTmp.Wallet_Balance__c = wallAmountMap.get(wallTransTmp.Wallet__c);
                      wallTransTmp.Partner_Account__c = wllAccMap.get(wallTransTmp.Wallet__c);
                  }  
                
            }/*
			else if(wallTransTmp.Sub_Category__c=='Standard Recharge'){
                wallTransTmp.Recharge_Document_ID__c = '/resource/'+sr.LastModifiedDate.getTime()+'/'+sr.Name ;
            }*/
            /*
            if(wllMap.containsKey(wallTransTmp.Wallet__c)){
                String rTypeName = wllMap.get(wallTransTmp.Wallet__c);
                wallTransTmp.Tech_Wallet_Record_Type__c = rTypeName;
            }
			*/
			
        }
        
    }
}