@isTest
public class Test_CampaignMemberHistoryTriggerHandler {
	
    @isTest
    static void onAfterInsertTest(){
    	CampaignMember cmLead = new CampaignMember(CampaignId='7016E000000d1p4QAA', LeadId='00Q6E000005CQ7KUAW');
        CampaignMember cmContact = new CampaignMember(CampaignId='7016E000000d1p4QAA',ContactId='0036E00000oRadVQAS');
        List<CampaignMember> campaignMembers = new List<CampaignMember>();
        campaignMembers.add(cmLead);
        campaignMembers.add(cmContact);
        
        Test.startTest();
        insert campaignMembers;
        Test.stopTest();
        
    }
    
    @isTest
    static void onBeforeDeleteTest(){
        CampaignMember cmContact = new CampaignMember(CampaignId='7016E000000d1p4QAA',ContactId='0036E00000pe2CWQAY');
        Test.startTest();
        insert cmContact;
        delete cmContact;
        Test.stopTest();
    }
}