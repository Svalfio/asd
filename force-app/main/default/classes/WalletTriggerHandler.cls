public class WalletTriggerHandler {
    
    public void OnAfterUpdate(Map<Id,Wallet__c> triggerNew, Map<Id, Wallet__c> triggerOld){
        
        Set<Id> wallet_subresellers = new Set<Id>();
        Set<Id> wallet_subresellers_recharged = new Set<Id>();
        Set<Id> wallet_resellers_recharged = new Set<Id>();
        Set<Id> wallet_distributors_recharged = new Set<Id>();
        
        Id RecordTypeSubWalletId = Schema.SObjectType.Wallet__c.getRecordTypeInfosByDeveloperName().get('Sub_Reseller_Wallet').getRecordTypeId();
        Id RecordTypeWalletId = Schema.SObjectType.Wallet__c.getRecordTypeInfosByDeveloperName().get('Reseller_Wallet').getRecordTypeId();
        Id RecordTypeDistributorWalletId = Schema.SObjectType.Wallet__c.getRecordTypeInfosByDeveloperName().get('Distributor_Wallet').getRecordTypeId();
        
        system.debug('++++++++++++++' + RecordTypeSubWalletId);
        for(Id key: triggerNew.keySet()){
            //mi prendo solo i wallet dei sub reseller che hanno un amount inferiore del threshold
            system.debug('--------------'+ triggerNew.get(key).RecordTypeId);
            system.debug('--------------'+ triggerNew.get(key).RecordType.DeveloperName);
            system.debug('--------------'+ triggerNew.get(key).RecordType.Name);
            if(triggerNew.get(key).RecordTypeId == RecordTypeSubWalletId && (triggerNew.get(key).Amount__c < triggerNew.get(key).Threshold_Amount__c)){
                system.debug('WALLET '+ key);
                wallet_subresellers.add(key);
            }
            if(triggerNew.get(key).RecordTypeId == RecordTypeSubWalletId && (triggerOld.get(key).Amount__c < triggerNew.get(key).Amount__c)){
                wallet_subresellers_recharged.add(key);
            }
            if(triggerNew.get(key).RecordTypeId == RecordTypeWalletId && (triggerOld.get(key).Amount__c < triggerNew.get(key).Amount__c)){
                wallet_resellers_recharged.add(key);
            }
            if(triggerNew.get(key).RecordTypeId == RecordTypeDistributorWalletId && (triggerOld.get(key).Amount__c < triggerNew.get(key).Amount__c)){
                wallet_distributors_recharged.add(key);
            }
        }
        
        if(wallet_subresellers.size() > 0){
        	System.enqueueJob(new AsyncWalletUpdate(wallet_subresellers));
        }
        if(wallet_subresellers_recharged.size() > 0){
       		System.enqueueJob(new AsyncWalletUpdateRecharged(wallet_subresellers_recharged));
        }
        if(wallet_resellers_recharged.size() > 0){
        	System.enqueueJob(new AsyncWalletResellerRecharged(wallet_resellers_recharged));
        }
        if(wallet_distributors_recharged.size() > 0){
        	System.enqueueJob(new AsyncWalletDistributorRecharged(wallet_distributors_recharged));
        }
    }

    public void OnBeforeUpdate(Map<Id,Wallet__c> triggerNew, Map<Id, Wallet__c> triggerOld){
        for(Id key: triggerNew.keySet()){
            System.debug('BEFORE');
            if(triggerNew.get(key).Amount__c != triggerOld.get(key).Amount__c){
                triggerNew.get(key).Old_Amount__c = triggerOld.get(key).Amount__c;
                System.debug('AGGIORNATO');
            }
        }
    }

}