@isTest
public class Test_CreateDataSetup {
    
    
    @isTest
    public static void createAccountHierarchy(){
       
        Id resellerRTId=  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_RESELLER).getRecordTypeId();
        Account reseller =  new Account(Name = 'Reseller',
                                        RecordTypeId=resellerRTId);
        
        insert reseller;
        
        Id subResellerRTId =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_SUBRESELLER).getRecordTypeId();
        account subReseller = new Account(Name='SubReseller1',
                                          RecordTypeId=subResellerRTId,
                                          ParentId=reseller.id);
        insert subReseller;
    }
    
    
    @isTest
    public static void createAllMySmsCustomSetting(){
        
         AllMySmsCustomSetting__c testCustomMySms = new AllMySmsCustomSetting__c(Name='AllMySms',Endpoint__c='urlFalse');
        insert testCustomMySms;
    }

    
    @isTest
    public static void createCommissionItemHierarchy(){
         Commission_Model__c commissionModel = new Commission_Model__c();
        insert commissionModel;
        
        Commission_Item__c testCommissionItem = new Commission_Item__c(Name='testItem',Commission_Model__c=commissionModel.Id,Transaction_Type__c='ADD_ON_FEE');
        insert testCommissionItem;
        
    }
}