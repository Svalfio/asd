@isTest
public class Test_VF_PaymentWalletReceipt {
	
    @testSetup
    static void createData(){
        Id resellerRTId=  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_RESELLER).getRecordTypeId();
        
        Account reseller =  new Account(Name = 'Reseller',
                                        RecordTypeId=resellerRTId,
										Headquarter_Country__c = 'Congo',
                                        VAT_Number__c = '33333'
                                        );  
        
        insert reseller;
        
        Wallet__c testWallet = new Wallet__c(Amount__c=199,Account__c=reseller.Id);
        insert testWallet;
        
        Wallet_Transaction__c testWT = new Wallet_Transaction__c();
        testWT.Wallet__c = testWallet.Id;
        testWT.Transaction_Category__c = 'Top-up';
        testWT.Amount__c = 200;
        testWT.Payment_Method__c = 'Cash';
        testWT.Sub_Category__c = 'Standard Top-up';
        testWT.Description__c = 'test';
        
        insert testWT;
        
    }
    
    @isTest
    static void testpage(){
        
        test.startTest();
        Wallet_Transaction__c wallTrans = [Select id from Wallet_Transaction__c limit 1];
        ApexPages.currentPage().getParameters().put('walletTransId',wallTrans.Id);
    	VF_PaymentWalletReceipt t = new VF_PaymentWalletReceipt();
        test.stopTest();
    }  
    
}