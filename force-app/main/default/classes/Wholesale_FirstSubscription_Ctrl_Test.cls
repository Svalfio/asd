@isTest
public class Wholesale_FirstSubscription_Ctrl_Test {

    @isTest
    public static void checkSub(){
        List<Zuora__Subscription__c> testSubscriptionList = new List<Zuora__Subscription__c>();
         Zuora__Subscription__c testSubscription;
         integer i=0;
         List<String> jsonStringList= new List<String>();
         String jsonString='';
         do{
             testSubscription= new Zuora__Subscription__c(Name = 'TestSubscription'+i ,Country__c='Cote d\'Ivoire');
             testSubscriptionList.add(testSubscription);
             i++;
         }while(i<100);
			jsonString=JSON.serialize(testSubscriptionList);
        	jsonStringList.add(jsonString);
        	
        Wholesale_FirstSubscription_Controller.ParseSubscription(jsonStringList); 
    }
}