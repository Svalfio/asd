public class LC_CommissionController {
    
    @AuraEnabled
    public static List<Commission_Item__c> retrieveDirectCommissionItems(String resId, String country, String commName, String status){
       	
		Date sysdate = Date.today();
        
        system.debug('*** sysdate ***:' + sysdate);
        
    	Id directRecordTypeId = Schema.SObjectType.Commission_Model__c.getRecordTypeInfosByDeveloperName().get('Direct').getRecordTypeId();
        
        system.debug('Country: '+ country);
        system.debug('Commission Model Name: '+ commName);
        
        
        String query = 'SELECT Id,Commission_Model_Name__c,Name,Commission_Amount__c,Commission_Percentage__c,Transaction_Type__c,Commission_Model_Country__c,Commission_Model_Start_Date__c,Commission_Model_End_Date__c,Commission_Amount_Calc__c' + 
                       ' FROM Commission_Item__c WHERE Commission_Model__r.RecordTypeId = :directRecordTypeId' + 
                       ' AND Commission_Model__r.Reseller_Id__c = :resId';
        
        system.debug('*** query ***:' + query);
        
        if(country != null && country != '' && country != 'All' )
        	query += ' AND Commission_Model__r.Country__c = :country';
        
        system.debug('*** query ***:' + query);
        
        if(commName != null && commName != '' && commName != 'All' )
        	query += ' AND Commission_Model_Name__c = :commName';
        
         system.debug('*** query ***:' + query);
        
        if(status == 'Active' )
        	query += ' AND Commission_Model__r.Start_Date__c <= :sysdate AND Commission_Model__r.End_Date__c >= :sysdate';
        else
            query += ' AND (Commission_Model__r.Start_Date__c > :sysdate OR Commission_Model__r.End_Date__c < :sysdate)';
        system.debug('*** query ***:' + query);
        
        List<sObject> lstCommItems = Database.query(query);
        
        system.debug('*** lstCommItems ***:' + lstCommItems);

        return lstCommItems;
    }
    
    @AuraEnabled
    public static String updateCommissionItems(List<Commission_Item__c> lst){
        	String error = 'OK';
  
            system.debug('*** lst ***:'+lst);
        	try{
        		update lst;
            } 
        	catch(Exception e){
            	System.debug( 'Exception: ' + e.getMessage() + ' Line Number: ' + e.getLineNumber() );
            	error = e.getMessage();
        	}
        	return error;
    }
    
    @AuraEnabled
    public static List<Commission_Item__c> retrieveIndirectCommissionItems(String resId,String country,String commName, String status, String resellerName){
        
        Date sysdate = Date.today();
        
        system.debug('*** sysdate ***:' + sysdate);
        
    	Id indirectRecordTypeId = Schema.SObjectType.Commission_Model__c.getRecordTypeInfosByDeveloperName().get('Indirect').getRecordTypeId();
        
        system.debug('Country: '+ country);
        system.debug('Commission Model Name: '+ commName);
        
        
        String query = 'SELECT Id,Name,Commission_Model_Name__c,Sub_Reseller_Name__c,Commission_Amount__c,Commission_Percentage__c,Transaction_Type__c,Commission_Model_Country__c,Commission_Model_Start_Date__c,Commission_Model_End_Date__c,Commission_Amount_Calc__c' + 
                       ' FROM Commission_Item__c WHERE Commission_Model__r.RecordTypeId = :indirectRecordTypeId' + 
                       ' AND Commission_Model__r.Reseller_Id__c = :resId';
        
        system.debug('*** query ***:' + query);
        
        if(country != null && country != '' && country != 'All' )
        	query += ' AND Commission_Model__r.Country__c = :country';
        
        system.debug('*** query ***:' + query);
        
        if(commName != null && commName != '' && commName != 'All' )
        	query += ' AND Commission_Model_Name__c = :commName';
        
        system.debug('*** query ***:' + query);   
        
        if(status == 'Active' )
        	query += ' AND Commission_Model__r.Start_Date__c <= :sysdate AND Commission_Model__r.End_Date__c >= :sysdate';
        else
            query += ' AND (Commission_Model__r.Start_Date__c > :sysdate OR Commission_Model__r.End_Date__c < :sysdate)';
        
        system.debug('*** query ***:' + query);

        if(resellerName != null && resellerName != '' && resellerName != 'All' )
        	query += ' AND Commission_Model__r.Sub_Reseller_Id__r.Name = :resellerName';
        
        system.debug('*** query ***:' + query);
        
        List<sObject> lstCommItems = Database.query(query);
        
        system.debug('*** lstCommItems ***:' + lstCommItems);

        return lstCommItems;
    }
    
    @AuraEnabled
	public static List<String> getCountryList(){
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Commission_Model__c.Country__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }    
    
        return pickListValuesList;
    }
    
    @AuraEnabled
	public static List<String> getModelList(String accountId,String commType){
        
        system.debug('accountId: ' + accountId);
        system.debug('commType: ' + commType);
        
        Id directRecordTypeId = Schema.SObjectType.Commission_Model__c.getRecordTypeInfosByDeveloperName().get('Direct').getRecordTypeId();
        Id indirectRecordTypeId = Schema.SObjectType.Commission_Model__c.getRecordTypeInfosByDeveloperName().get('Indirect').getRecordTypeId();
        
        List<String> modelNameList= new List<String>();
        List<Commission_Model__c> commModel = new List<Commission_Model__c>();
        
        if(accountId != null && accountId != '' && commType == 'Direct'){
            commModel = [SELECT Id,Name FROM Commission_Model__c 
                                                   WHERE RecordTypeId = :directRecordTypeId
                                                   AND Reseller_Id__c = :accountId];
        }
        else if(accountId != null && accountId != '' && commType == 'Indirect'){
            commModel = [SELECT Id,Name FROM Commission_Model__c 
                                                   WHERE RecordTypeId = :indirectRecordTypeId
                                                   AND Reseller_Id__c = :accountId];
        }
        
        system.debug('Commission model picklist: '+commModel);
        
        for(Commission_Model__c tmpModel : commModel){
            modelNameList.add(tmpModel.Name);
        }
        
        return modelNameList;
    }
    
    @AuraEnabled
	public static List<String> getResellerList(String accountId){
        Id indirectRecordTypeId = Schema.SObjectType.Commission_Model__c.getRecordTypeInfosByDeveloperName().get('Indirect').getRecordTypeId();
        List<String> ResellerNameList= new List<String>();
        Set<String> resNameSet = new Set<String>();
        
        List<Commission_Model__c> commModel = [SELECT Id,Sub_Reseller_Id__r.Name FROM Commission_Model__c 
                                                   WHERE RecordTypeId = :indirectRecordTypeId
                                                   AND Reseller_Id__c = :accountId];
        
        system.debug('Reseller picklist: '+commModel);
        
        for(Commission_Model__c tmpModel : commModel){
            resNameSet.add(tmpModel.Sub_Reseller_Id__r.Name);
        }
        
        for(String key : resNameSet){
            ResellerNameList.add(key);
        }
        
        return ResellerNameList;
        
    }
}