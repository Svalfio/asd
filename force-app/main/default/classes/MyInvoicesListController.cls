public class MyInvoicesListController {
    
    @AuraEnabled
    public static List<Zuora__ZInvoice__c> getMyInvoices(String UserId, String NameFilter, DateTime DateFilter, String DatePicklistFilter, Integer AmountFilter, String AmountPicklistFilter, Integer BalanceFilter, String BalancePicklistFilter, String StatusFilter){
        List<zqu__Quote__c> quotes = new List<zqu__Quote__c>();
        List<Zuora__ZInvoice__c> invoices = new List<Zuora__ZInvoice__c>();
        List<String> subscriptionIds = new List<String>();
        List<String> invoiceIds = new List<String>();
        Id RecordTypeQuoteId = Schema.SObjectType.zqu__Quote__c.getRecordTypeInfosByDeveloperName().get('Order_Validated').getRecordTypeId();
        quotes = [SELECT id, name, zqu__ZuoraSubscriptionID__c FROM zqu__Quote__c WHERE OwnerId =: UserId AND RecordTypeId =: RecordTypeQuoteId  ORDER BY CreatedDate DESC LIMIT 50];
        ZuoraRestCalls zuoraCalls = new ZuoraRestCalls();
        Id AccountId = [SELECT id, AccountId from User WHERE id =: UserId].AccountId;
        
        String token = zuoraCalls.getAccessToken();
        for(zqu__Quote__c q: quotes){
            subscriptionIds.add(q.zqu__ZuoraSubscriptionID__c);
        }
        invoiceIds = zuoraCalls.getInvoiceIds(subscriptionIds, token);
        for(String s: invoiceIds){
            system.debug('###'+s);
        }
        //String q = 'SELECT Name, Zuora__Generated_Date__c, Zuora__TotalAmount__c, Zuora__Balance2__c, Zuora__Status__c FROM Zuora__ZInvoice__c WHERE Zuora__Zuora_Id__c IN :invoiceIds';
        String q = 'SELECT Name, Zuora__Generated_Date__c, Zuora__TotalAmount__c, Zuora__Balance2__c, Zuora__Status__c FROM Zuora__ZInvoice__c WHERE 	Zuora__Account__c = \''+AccountId+'\'';
        String order = ' ORDER BY Zuora__Generated_Date__c DESC';
        List<String> filters = new List<String>();
        system.debug('nome' + NameFilter + ' data '+ DateFilter + ' amount ' + AmountFilter + ' balance '+ BalanceFilter + ' status ' +StatusFilter);
        if(NameFilter != null){
            filters.add('Name = \''+NameFilter+'\'');
        }
        if(DateFilter != null){
            if(DatePicklistFilter == 'Equals'){
                filters.add('Zuora__Generated_Date__c = '+DateFilter.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\''));
            }else if(DatePicklistFilter == 'GreaterThan'){
                filters.add('Zuora__Generated_Date__c > '+DateFilter.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\''));
            }else{
                filters.add('Zuora__Generated_Date__c < '+DateFilter.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\''));
            }
        }
        if(AmountFilter != null){
            if(AmountPicklistFilter == 'Equals'){
                filters.add('Zuora__TotalAmount__c = '+AmountFilter );
            }else if(AmountPicklistFilter == 'GreaterThan'){
                filters.add('Zuora__TotalAmount__c > '+AmountFilter );
            }else{
                filters.add('Zuora__TotalAmount__c < '+AmountFilter );
            }
        }
        if(BalanceFilter != null){
            if(BalancePicklistFilter == 'Equals'){
                filters.add('Zuora__Balance2__c = '+BalanceFilter );
            }else if(BalancePicklistFilter == 'GreaterThan'){
                filters.add('Zuora__Balance2__c > '+BalanceFilter );
            }else{
                filters.add('Zuora__Balance2__c < '+BalanceFilter );
            }
        }
        if(StatusFilter != null){
            filters.add('Zuora__Status__c = \''+ StatusFilter+'\'');
        }
        String filter = String.join(filters, ' AND ');
        if(filter != ''){
            q = q +' AND ' + filter + order;
            
        }else{
            q = q + order;
        }
        system.debug('	QUERY FINALE'+ q);
        system.debug(invoiceIds);
        invoices = Database.query(q);
        return invoices;
    }
    
    @AuraEnabled
    public static String DownloadInvoice(String InvoiceName){
        Zuora__ZInvoice__c invoice = [SELECT Id, Name, Zuora__Zuora_Id__c FROM Zuora__ZInvoice__c WHERE Name =:InvoiceName];
        system.debug('####'+invoice.Name);
        ZuoraRestCalls zuoraCalls = new ZuoraRestCalls();
        String page;
        String token = zuoraCalls.getAccessToken();
        system.debug('#### TOKEN'+ token);
        String url = zuoraCalls.getPDFUrl(token, invoice.Zuora__Zuora_Id__c);
        if(url != null){
            system.debug('#### URL'+ url);
            page = zuoraCalls.DownloadPdf(token, url, InvoiceName, invoice.Id);
        }
        return page;
    }
}