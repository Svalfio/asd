public with sharing class InstallationWorkOrderClass {
	Id posId {get; set;}
	String header {get; set;}
	private ApexPages.StandardController controller;

	public InstallationWorkOrderClass(ApexPages.StandardController controller) {
		posId = ApexPages.CurrentPage().getparameters().get('id');
		header = ApexPages.currentPage().getHeaders().get('Host');
	}

	public PageReference redirect(){
		PageReference pageRef = new PageReference('https://' + header + '/partner/s/work-order-detail?id=' + posId);
		return pageRef;
}
}