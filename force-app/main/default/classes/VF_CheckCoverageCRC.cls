global class VF_CheckCoverageCRC {
	public Id recordId {get; set;}
    public Account acc {get; set;}
    private Account[] res {get; set;}
    public String resString {get; set;}

    /**
     * VF_CheckCoverageCRC
     */
    public VF_CheckCoverageCRC(ApexPages.StandardController controller) {
        recordId = ApexPages.currentPage().getParameters().get('Id');
        if (recordId != null) {
            this.acc = retrieveAccount();
        }
        this.res = retrieveResellers();
        this.resString = JSON.serialize(this.res, true);
        //this.accCity = this.acc.BillingAddress.city
    }
    
    /**
     * retrieveAccount
     *
     * @returns Account
     */
    public Account retrieveAccount() {
        return [SELECT Id, Name, BillingCountry, BillingPostalCode, BillingState, BillingCity, BillingStreet, 
              BillingLatitude, BillingLongitude FROM Account WHERE Id = :recordId LIMIT 1];
    }
    
    /**
     * retrieveResellers
     *
     * @returns Account[]
     */
    public Account[] retrieveResellers() {
        return [SELECT Id, RecordType.Name, BillingAddress, Name, Phone FROM Account WHERE RecordType.Name = 'Reseller' OR RecordType.Name = 'Sub-Reseller'];
    }

    @RemoteAction
    global static SfToHerokuAPIManager.HerokuResponse serviceCoverageCheck(Double latitude, Double longitude, String country, String globalService, Integer preferenceBeamId){
        return SfToHerokuAPIManager.serviceCoverageCheck(latitude, longitude, country, globalService, preferenceBeamId);
    }
    
}