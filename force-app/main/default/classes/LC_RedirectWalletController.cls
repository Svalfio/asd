public class LC_RedirectWalletController {
    
    @AuraEnabled
    public static String getWalletId(String UserId){
        String WalletId;
        User currentUser = [SELECT Id, AccountId FROM User WHERE Id =:UserId];
        system.debug('ID Utente corrente '+ currentUser.Id+ ' '+ currentUser.AccountId);
        //WalletId = [SELECT id FROM Wallet__c WHERE Account__c =: currentUser.AccountId LIMIT 1].id;
        WalletId = [SELECT id FROM Wallet__c WHERE Account__c = :currentUser.AccountId LIMIT 1].id;
        return WalletId;
	}

}