public class SubscriptionJSON {
    
    @InvocableMethod(label='getJSONSubscription')
    public static List<String> getJSONSubscription(List<String> AccountId) {
        List<String>data = new List<String>();
		List<sObject> DataList = (List<sObject>)Type.forName('List<Zuora__Subscription__c>').newInstance();
        
        for(String s: AccountId){
            String queryString= 'SELECT id, Name, Zuora__ServiceActivationDate__c, Zuora__ContractEffectiveDate__c, '+
                'Zuora__SubscriptionEndDate__c, Zuora__Status__c, Zuora__SubscriptionStartDate__c, Subscription_Type__c, '+
                'Zuora__Version__c, Country__c FROM Zuora__Subscription__c WHERE Zuora__Account__c = \'' + s+'\'';
            DataList = Database.query(queryString);
            data.add(JSON.serialize(DataList));
        }
        
        return data;
    }

}