global class CheckOSSCreateContact{
    global with sharing class CheckCoverageInputs {
        @InvocableVariable(Label = 'AccountName' Required = false) global String AccountName;
        @InvocableVariable(Label = 'AccountId' Required = false) global String AccountId;
        @InvocableVariable(Label = 'BillingCity' Required = false) global String BillingCity;
        @InvocableVariable(Label = 'BillingCountry' Required = false) global String BillingCountry;
        @InvocableVariable(Label = 'BillingGeocodeAccuracy' Required = false) global String BillingGeocodeAccuracy;
        @InvocableVariable(Label = 'BillingLatitude' Required = false) global Decimal BillingLatitude;
        @InvocableVariable(Label = 'BillingLongitude' Required = false) global Decimal BillingLongitude;
        @InvocableVariable(Label = 'BillingPostalCode' Required = false) global String BillingPostalCode;
        @InvocableVariable(Label = 'BillingState' Required = false) global String BillingState;
        @InvocableVariable(Label = 'BillingStreet' Required = false) global String BillingStreet;
        @InvocableVariable(Label = 'Email_address__c' Required = false) global String AccountEmail;
        @InvocableVariable(Label = 'Fixed_Phone_Number__c' Required = false) global String AccountFixedPhone;
        @InvocableVariable(Label = 'Mobile_Phone_Number__c' Required = false) global String AccountMobilePhone;
	}
    
    @InvocableMethod(Label = 'Check Account Coverage')
	global static void makePostCallout(CheckCoverageInputs[] requestActionInList) 
    {	
			String ContactPhone = '';
            if(requestActionInList[0].AccountMobilePhone != '' && requestActionInList[0].AccountMobilePhone != null){
                ContactPhone = requestActionInList[0].AccountMobilePhone;
            }
            else{
                ContactPhone = requestActionInList[0].AccountFixedPhone;
            }
            Contact conTest = new Contact(
                LastName = requestActionInList[0].AccountName,
                AccountID = requestActionInList[0].AccountId,
                FirstName = requestActionInList[0].AccountName,
                MailingCity = requestActionInList[0].BillingCountry,
                MailingCountry = requestActionInList[0].BillingCountry, 
                MailingGeocodeAccuracy = requestActionInList[0].BillingGeocodeAccuracy,
                MailingLatitude = requestActionInList[0].BillingLatitude,
                MailingLongitude = requestActionInList[0].BillingLongitude,
                MailingPostalCode = requestActionInList[0].BillingPostalCode,
                MailingState = requestActionInList[0].BillingState,
                MailingStreet = requestActionInList[0].BillingStreet,
                Email = requestActionInList[0].AccountEmail,
                Phone = ContactPhone);
            insert conTest;
            
            Account thisAccount = [SELECT Id From Account where Id =: requestActionInList[0].AccountId];
            thisAccount.Covered__c = true;
            update thisAccount;
	}
}