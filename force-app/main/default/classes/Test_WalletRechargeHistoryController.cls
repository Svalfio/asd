@isTest
public class Test_WalletRechargeHistoryController {
	@isTest
	static void WalletRechargeHistoryController(){


		List<Wallet_Transaction__c> walletRecharges = new List<Wallet_Transaction__c>();

		Wallet__c walletTest = new Wallet__c();
		insert walletTest;

		Wallet_Transaction__c walletTransactionTest1 = new Wallet_Transaction__c(Wallet__c=walletTest.Id,
		                                                                         Transaction_Category__c = 'Top-up',
		                                                                         Payment_Method__c = 'Wallet Credit',
		                                                                         Description__c = 'testReference',
		                                                                         Amount__c = 12.5);
		walletRecharges.add(walletTransactionTest1);

		Wallet_Transaction__c walletTransactionTest2 = new Wallet_Transaction__c(Wallet__c=walletTest.Id,
		                                                                         Transaction_Category__c = 'Top-up',
		                                                                         Payment_Method__c = 'Wallet Credit',
		                                                                         Description__c = 'testReference',
		                                                                         Amount__c = 12.5);
		walletRecharges.add(walletTransactionTest2);

		Wallet_Transaction__c walletTransactionTest3 = new Wallet_Transaction__c(Wallet__c=walletTest.Id,
		                                                                         Transaction_Category__c = 'Top-up',
		                                                                         Payment_Method__c = 'Wallet Credit',
		                                                                         Description__c = 'testReference',
		                                                                         Amount__c = 12.5);
		walletRecharges.add(walletTransactionTest3);

		Wallet_Transaction__c walletTransactionTest4 = new Wallet_Transaction__c(Wallet__c=walletTest.Id,
		                                                                         Transaction_Category__c = 'Top-up',
		                                                                         Payment_Method__c = 'Wallet Credit',
		                                                                         Description__c = 'testReference',
		                                                                         Amount__c = 12.5);
		walletRecharges.add(walletTransactionTest4);
		insert walletRecharges;

	Test.startTest();
	List<Wallet_Transaction__c> result = LC_WalletRechargeHistoryController.getRecharges(String.valueOf(walletTest.Id));
	Test.stopTest();

	System.assertEquals(walletRecharges.size(), result.size());
	System.assertEquals(walletRecharges.get(0).id, result.get(0).id);
}
}