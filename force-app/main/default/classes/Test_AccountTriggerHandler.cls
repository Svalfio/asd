@isTest
public class Test_AccountTriggerHandler {

    @testSetup
    static void createDataSetup(){
        
        Test_CreateDataSetup.createAccountHierarchy();
        
    }
    
    @isTest
    static void onAfterInsert(){
        
      
        Id resellerRTId=  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_RESELLER).getRecordTypeId();
        Account reseller =  new Account(Name = 'Reseller',
                                        RecordTypeId=resellerRTId,
                                        CurrencyIsoCode = 'USD');
        
        insert reseller;
        
        Id subResellerRTId =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_SUBRESELLER).getRecordTypeId();
        account subReseller = new Account(Name='SubReseller1',
                                          RecordTypeId=subResellerRTId,
                                          ParentId=reseller.id);
        insert subReseller;
    }
   
    
    @isTest
    static void onBeforeInsertTest(){
        
        List<Account> testAccountList = new List<Account>();
        AccountTriggerHandler handler = new AccountTriggerHandler();
        handler.onBeforeInsert(testAccountList);
    }
    
    @isTest
    static void onBeforeDelete(){
        
        Id resellerRTId=  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_RESELLER).getRecordTypeId();
        Id subResellerRTId =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_SUBRESELLER).getRecordTypeId();
        
        Account reseller = [SELECT Id from Account WHERE RecordTypeId =: resellerRTId LIMIT 1];
        Account subReseller = [SELECT Id from Account WHERE RecordTypeId =: subResellerRTId LIMIT 1];
        
        Test.startTest();
        delete subReseller;
        delete reseller;
        Test.stopTest();
        
    }
    
    @isTest
    static void onBeforeUpdateTest(){
        Id indirectRecordTypeId = Schema.SObjectType.Commission_Model__c.getRecordTypeInfosByDeveloperName().get('Indirect').getRecordTypeId();
        Id resellerRTId=  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_RESELLER).getRecordTypeId();
        Id subResellerRTId =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_SUBRESELLER).getRecordTypeId();

        Account reseller = [SELECT Id from Account WHERE RecordTypeId =: resellerRTId LIMIT 1];
        Account subReseller = [SELECT Id from Account WHERE RecordTypeId =: subResellerRTId LIMIT 1];
        Commission_Model__c commissionModelReseller = new Commission_Model__c(Reseller_Id__c=reseller.id,Sub_Reseller_Id__c=subReseller.id,Country__c='COG',start_date__c=date.newInstance(2019, 02, 01),end_date__c =date.newInstance(2019, 02, 10),RecordTypeId = indirectRecordTypeId);
		insert commissionModelReseller;
        Test.startTest();
        subReseller.CurrencyIsoCode = 'USD';
        update subReseller;
        reseller.CurrencyIsoCode = 'USD';
        update reseller;
        Test.stopTest();
        

    }
   
    
}