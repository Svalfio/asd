public class ContactTriggerHandler {
    
    public void onAfterUpdate(List<Contact> triggerNew){
        system.debug('--------------- AFTER --------------------');
        List<String> contactIds = new List<String>();
        List<CampaignMember> campaignMembers = new List<CampaignMember>();
        List<CampaignMember> campaignMembersInsert = new List<CampaignMember>();
        Map<Id,List<CampaignMember>> campaignMemberToContactMap = new Map<Id,List<CampaignMember>>();  
        
        for(Contact c: triggerNew){
            contactIds.add(c.Id);
            system.debug('+++++++' + c.Id);
        }
        
        campaignMembers = [SELECT Id, Name, ContactId, CampaignId FROM CampaignMember WHERE ContactId IN :contactIds];
        
        
        for(CampaignMember member : campaignMembers){
            if(!campaignMemberToContactMap.containsKey(member.ContactId))
                campaignMemberToContactMap.put(member.ContactId,new List<CampaignMember>{member});
            else
                campaignMemberToContactMap.get(member.ContactId).add(member);
        }
        
        for(Contact c: triggerNew){
            system.debug('Entro 1');
            for(CampaignMember cm: campaignMemberToContactMap.get(c.Id)){
                system.debug('Entro 2');
                cm.Opt_out_Email__c = c.Email_Opt_out__c;
                cm.Opt_out_SMS__c = c.SMS_Opt_out__c;
                cm.Opt_out_Phone__c = c.Phone_Opt_out__c;
                cm.Opt_out_Web__c = c.Web_Opt_out__c;
                
                system.debug('CM' + cm.Opt_out_Email__c + cm.Opt_out_SMS__c+ cm.Opt_out_Phone__c+ cm.Opt_out_Web__c);
                system.debug('CONTACT' + c.Email_Opt_out__c + c.SMS_Opt_out__c + c.Phone_Opt_out__c + c.Web_Opt_out__c);
                campaignMembersInsert.add(cm);
            }
        }
        
        update campaignMembersInsert;
        
    }
    
    public void onAfterUpdateAccount(List<Contact> triggerNew){
        List<Account> accounts = new List<Account>();
        List<String> accountIds = new List<String>();
        List<Account> accountUpdate = new List<Account>();
        for(Contact c: triggerNew){
            accountIds.add(c.AccountId);
        }
        
        accounts = [SELECT Name, Last_Interaction__c, Prospect_Score__c FROM Account WHERE Id IN :accountIds];
        for(Contact c: triggerNew){
            for(Account a: accounts){
                if(c.AccountId == a.Id){
                    a.Last_Interaction__c = 	c.pi__last_activity__c;
                    a.Prospect_Score__c = c.pi__score__c;
					accountUpdate.add(a);                    
                }
            }
        }
        update accountUpdate;
    }

}