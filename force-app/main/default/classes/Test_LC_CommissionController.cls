@isTest
public class Test_LC_CommissionController {
	@testSetup
    static void createDataSet(){
		
        Id resellerRTId=  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_RESELLER).getRecordTypeId();
        Account reseller =  new Account(Name = 'Reseller',
                                        RecordTypeId=resellerRTId);
        
        insert reseller;

        Commission_Model__c commissionModel = new Commission_Model__c();
        Id idRT =  Schema.SObjectType.Commission_Model__c.getRecordTypeInfosByDeveloperName().get('Direct').getRecordTypeId();
        commissionModel.RecordTypeId = idRT;
        commissionModel.Reseller_Id__c = reseller.Id;
        commissionModel.Country__c = reseller.Country__c;
        commissionModel.Start_Date__c = Date.today().addDays(-10);
        commissionModel.End_Date__c = Date.today().addDays(10);
        insert commissionModel;
    }
    
    @isTest
    static void test1Active(){
        
		Account reseller = [Select id from Account limit 1];
        
        Test.startTest();
        LC_CommissionController.retrieveDirectCommissionItems(reseller.id,'Test','Congo','Active');
        Test.stopTest();
        
    }
    
    @isTest
    static void test2Active(){
        
		Account reseller = [Select id from Account limit 1];
        
        Test.startTest();
        LC_CommissionController.retrieveIndirectCommissionItems(reseller.id,'Test','Congo','Active','Test');
        Test.stopTest();
        
    }
    
    @isTest
    static void test1Inactive(){
        
		Account reseller = [Select id from Account limit 1];
        
        Test.startTest();
        LC_CommissionController.retrieveDirectCommissionItems(reseller.id,'Test','Congo','Inactive');
        Test.stopTest();
        
    }
    
    @isTest
    static void test2Inactive(){
        
		Account reseller = [Select id from Account limit 1];
        
        Test.startTest();
        LC_CommissionController.retrieveIndirectCommissionItems(reseller.id,'Test','Congo','Inactive','Test');
        Test.stopTest();
        
    }
    
    @isTest
    static void test3(){
        
        Test.startTest();
        LC_CommissionController.getCountryList();
        Test.stopTest();
        
    }
    
    @isTest
    static void test4Direct(){
        Account reseller = [Select id from Account limit 1];
        Test.startTest();
        LC_CommissionController.getModelList(reseller.Id,'Direct');
        Test.stopTest();
        
    }
    
    @isTest
    static void test4Indirect(){
        Account reseller = [Select id from Account limit 1];
        Test.startTest();
        LC_CommissionController.getModelList(reseller.Id,'Indirect');
        Test.stopTest();
        
    }
    
    @isTest
    static void test5(){
        List<Commission_Item__c> comm = [Select id from Commission_Item__c];
        Test.startTest();
        LC_CommissionController.updateCommissionItems(comm);
        Test.stopTest();
        
    }
    
    @isTest
    static void test6(){
        Account reseller = [Select id from Account limit 1];
        Test.startTest();
        LC_CommissionController.getResellerList(reseller.Id);
        Test.stopTest();
        
    }
}