@IsTest
public class Test_CheckCoverage {
    
    @testSetup 
    static void setup() {
        Account account = new Account(Name = 'Test Class Account', Phone = '+32762727822', Email_address__c = 'test@class.com');
        insert account;
    }
    
    @isTest
    static void getAccountTest(){
        Account account = [SELECT Id, Name From Account Where Name = 'Test Class Account'];
        CheckCoverage.getAccount(account.Id);
    }
    

}