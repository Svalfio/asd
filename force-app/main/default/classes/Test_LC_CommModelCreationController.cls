@isTest
public class Test_LC_CommModelCreationController {
	@testSetup
    static void createDataSet(){
		
        Id resellerRTId=  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_RESELLER).getRecordTypeId();
        Account reseller =  new Account(Name = 'Reseller',
                                        RecordTypeId=resellerRTId);
        
        insert reseller;
        
        Id subResellerRTId =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_SUBRESELLER).getRecordTypeId();
        account subReseller = new Account(Name='SubReseller',
                                          RecordTypeId=subResellerRTId,
                                          ParentId=reseller.id);
        insert subReseller;
    }
	
	@isTest
    static void retrieveInfo(){
        
		Account reseller = [Select id from Account limit 1];
        
        Test.startTest();
        LC_CommModelCreationController.Wrapper wr = LC_CommModelCreationController.retrieveInfo(reseller.Id,'Ad-hoc');
        Test.stopTest();
       
    }
    
    @isTest
    static void createCommissionModel(){
        
		Account reseller = [Select id from Account where name = 'Reseller' limit 1];
        Account subReseller = [Select id from Account where name = 'SubReseller' limit 1];
        
        Test.startTest();
        String r = LC_CommModelCreationController.createCommissionModel(reseller.Id,'Ad-hoc',subReseller.Id,'2019-10-10','2019-12-10','COG');
        Test.stopTest();
       
    }
}