@isTest
public class Invocable_CreateTechnicalAccountTest {
    public static testMethod void doTest() {
        
      	SfToHerokuAPIManager.TechnicalAccountCreationResponse responseNewTechAccount =  new SfToHerokuAPIManager.TechnicalAccountCreationResponse();
        Test.setMock(HttpCalloutMock.class, new CallOutMockSetter('400',(Object)responseNewTechAccount));

       	SfToHK_Urls__c urls = new SfToHK_Urls__c();
        urls.Base_Url__c = 'https://eutelsat-hub-dev.herokuapp.com';
        urls.Create_Customer_Account__c = '/api/v1/oss/tech-accounts';
        insert urls;
        
        zqu__Quote__c quoteTest = new zqu__Quote__c();
        insert quoteTest;
        String quoteId = quoteTest.Id;
        
        Test.startTest();
        Invocable_CreateTechnicalAccount.CreateTechnicalAccountInputs testInputs = new Invocable_CreateTechnicalAccount.CreateTechnicalAccountInputs();
        testInputs.quoteId = quoteId;
        testInputs.timezone = 1;
        testInputs.country = 'IT';
        testInputs.latitude = 10;
        testInputs.satelliteId = 'Eutelsat';
        testInputs.longitude = 10;
        testInputs.name = 'blank';
        testInputs.techServiceId = 1;
        testInputs.technologyId = 1;
        testInputs.customerId = 1;
        testInputs.subPartnerId = 1;
        testInputs.productId = 1;
		
        List<Invocable_CreateTechnicalAccount.CreateTechnicalAccountInputs> listTestInputs = new List<Invocable_CreateTechnicalAccount.CreateTechnicalAccountInputs>();
        listTestInputs.add(testInputs);        
        
        List<String> Esito = Invocable_CreateTechnicalAccount.makePostCallout(listTestInputs);
        Test.stopTest();

        system.assertEquals('','');
    }
}