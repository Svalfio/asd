@RestResource(urlMapping='/Orders/*')
global with sharing class OrderManager {
    @HttpGet
    global static Order getOrderById() {
        RestRequest request = RestContext.request;
        // grab the caseId from the end of the URL
        String orderId = request.requestURI.substring(
          request.requestURI.lastIndexOf('/')+1);
        Order result =  [SELECT Name,Status, BillingAddress
                        FROM Order
                        WHERE Id = :orderId];
        return result;
    }/*
    @HttpPost
    global static ID createOrder(String Name, String status,
        String origin, String priority) {
        Case thisCase = new Case(
            Subject=subject,
            Status=status,
            Origin=origin,
            Priority=priority);
        insert thisCase;
        return thisCase.Id;
    }  */
}