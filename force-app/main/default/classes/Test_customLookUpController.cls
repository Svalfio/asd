@isTest
public class Test_customLookUpController {
	@testSetup
    static void createDataSet(){
		
        Id resellerRTId=  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_RESELLER).getRecordTypeId();
        Account reseller =  new Account(Name = 'Reseller',
                                        RecordTypeId=resellerRTId);
        
        insert reseller;
    }

	@isTest
    static void fetchLookUpValues(){
        
        Test.startTest();
        List<Account> acc = customLookUpController.fetchLookUpValues('Res', 'Account', '');
        Test.stopTest();
       
    }
}