@isTest
public class Test_sendnotificationtosuspendedreseller {
    
    @testSetup
    static void setup(){
		Id RecordTypeSubResId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Sub_Reseller').getRecordTypeId();
       
        Account account_subres1 = new Account(Name = 'SubReseller1', recordTypeId = RecordTypeSubResId, Marketing_Opt_in_Email__c = true, Marketing_Opt_in_Portal__c= true);

        insert account_subres1;
        
        Id account_subres1Id = [SELECT id, Name From Account WHERE Name = 'SubReseller1'].id;
        
        Contact contact_subres1 = new Contact(LastName = 'Sub1', AccountId= account_subres1Id);
        
        insert contact_subres1;
        
        Id contact_subres1Id = [SELECT id, LastName From Contact WHERE LastName = 'Sub1'].id;
        
        User user_sub1 = new User(LastName = 'Sub1', alias = 'sub1', Email= 'sureseller1@gmail.com', Username = 'sureseller1@gmail.com', TimeZoneSidKey='Europe/Rome'	,LocaleSidKey = 'en_US', EmailEncodingKey='ISO-8859-1'	,ProfileId='00e6E000000PLeGQAW',LanguageLocaleKey = 'en_US', ContactId=contact_subres1Id, UserRoleId='2F00E6E000001hp2z' );
       
        insert user_sub1;
    }
    
    @isTest
    static void sendNotificationToSuspendedReseller(){
        
        User subreseller = [SELECT id, LastName FROM User WHERE LastName = 'Sub1'];
        
        List<String> idReseller = new List<String>();
        
        idReseller.add(subreseller.id);
        
        Test.startTest();
        sendNotificationToSuspendedReseller.sendNotification(idReseller);
        Test.stopTest();
    }
}