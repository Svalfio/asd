@isTest
public class Invocable_CreateCustomerAccountTest {
    public static testMethod void doTest() {
        
        SfToHerokuAPIManager.CustomerAccountCreationResponse responseNewCustomerAccount =  new SfToHerokuAPIManager.CustomerAccountCreationResponse();
        responseNewCustomerAccount.externalId = '1';

       	SfToHK_Urls__c urls = new SfToHK_Urls__c();
        urls.Base_Url__c = '200';
        insert urls;
        
        Account accountToUpdate = new Account();
        accountToUpdate.OSS_Customer_Account_ID__c = 'blank';
        accountToUpdate.Name = 'blank';
        insert accountToUpdate;
        String accountIdTest = accountToUpdate.Id;
        
        Test.setMock(HttpCalloutMock.class, new CallOutMockSetter('400',(Object)responseNewCustomerAccount));
        Test.startTest();

        Invocable_CreateCustomerAccount.CreateCustomerAccountInputs testInputs = new Invocable_CreateCustomerAccount.CreateCustomerAccountInputs();
        testInputs.externalId = accountIdTest;
        testInputs.serviceProviderId = 1;
        testInputs.partnerId = 1;
        testInputs.description = 'blank';
        testInputs.phone = '+3334456789';
        testInputs.categoryId = 6;
        testInputs.quoteId = 'blank';

        List<Invocable_CreateCustomerAccount.CreateCustomerAccountInputs> listTestInputs = new List<Invocable_CreateCustomerAccount.CreateCustomerAccountInputs>();
        listTestInputs.add(testInputs);        
 
        List<String> Esito = Invocable_CreateCustomerAccount.makePostCallout(listTestInputs);
        Test.stopTest();
        system.assertEquals('','');
    }
}