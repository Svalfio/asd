@RestResource(urlMapping = '/api/create/attachment')
global class RESTClass_CreateAttachment {
    
    global class AttachmentRequest{
        @TestVisible public List<MonthlyFileInfo> monthlyFiles;
    }
    global class MonthlyFileInfo{
        @TestVisible public String monthlySfId;
        @TestVisible public String fileString;
        @TestVisible public String filename;
    }
    @HttpPost
    global static void doPost(){
        
        RestRequest req = RestContext.request;
        Blob body = req.requestBody;
        String requestString = body.toString();
        AttachmentRequest request = (AttachmentRequest)JSON.deserialize(requestString, AttachmentRequest.class);
        
        List<Attachment> attachmentList = new List<Attachment>();
        List<String> monthlyIds = new List<String>();
        
        for(MonthlyFileInfo currentAtt : request.monthlyFiles){
            system.debug('###currentAtt: Filename: '+currentAtt.filename+' MonthlySfid: '+currentAtt.monthlySfid);
            monthlyIds.add(currentAtt.monthlySfid);
        }
        
        List<Attachment> monthlyFileNames = [SELECT id, ParentId, Name FROM Attachment WHERE ParentId IN :monthlyIds];
        Map<String,Attachment> monthlyFileMap = new Map<String,Attachment>();
        
        for(Attachment cAtt : monthlyFileNames){
                monthlyFileMap.put(cAtt.ParentId, cAtt);
        }
        System.debug('####MAP: '+monthlyFileMap);
        
        for(MonthlyFileInfo currentAtt : request.monthlyFiles){
            
            Attachment att = new Attachment();
            
            system.debug('##FILENAME: ' + currentAtt.filename);
            
            if(!monthlyFileMap.containsKey(currentAtt.monthlySfid) || monthlyFileMap.get(currentAtt.monthlySfid).Name!=currentAtt.filename){
                att.ParentId = currentAtt.monthlySfId;
                att.Name = currentAtt.filename;
                att.Body = EncodingUtil.base64Decode(currentAtt.fileString);
                att.ContentType = 'application/vnd.ms-excel';
            }
            else{
                att=monthlyFileMap.get(currentAtt.monthlySfid);
                att.Body = EncodingUtil.base64Decode(currentAtt.fileString);
            }
            attachmentList.add(att);
            
        }
        
        upsert attachmentList;
    }
    
}