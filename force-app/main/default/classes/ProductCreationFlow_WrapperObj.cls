global class ProductCreationFlow_WrapperObj {
	
    @AuraEnabled
    @ InvocableVariable 
    global Id product_ID;
    
    @AuraEnabled
    @ InvocableVariable
    global Id Zproduct_ID;
    
	@AuraEnabled
    @ InvocableVariable 	
    global zqu__ProductRatePlan__c productRatePlan;
    @AuraEnabled
    @ InvocableVariable 	
    global String productRatePlanName;
    @AuraEnabled
    @ InvocableVariable 
    global Id productRatePlanId;
    
    @AuraEnabled
    @ InvocableVariable 
	global zqu__ProductRatePlanCharge__c productRatePlanCharge;
    @AuraEnabled
    @ InvocableVariable 	
    global String productRatePlanChargeName;
    @AuraEnabled
    @ InvocableVariable 
    global Id productRatePlanChargeId;
    
    @AuraEnabled
    @ InvocableVariable 
	global zqu__ProductRatePlanChargeTier__c productRatePlanChargeTier;
    @AuraEnabled
    @ InvocableVariable
    global String productRatePlanChargeTierName;
    
   
}