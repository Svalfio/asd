global class sampleAllowedRecordPlugin implements zqu.QuickListController.IHierarchyAllowedRecordPlugin {
    global zqu.QuickListController.PluginResult getAllowedRecords
        (Map<Object, Object> contextIds, Map<String, String> pluginParams){
   
        zqu.QuickListController.PluginResult result = 
            new zqu.QuickListController.PluginResult();
   
        zqu.QuickListController.PluginRecord record = 
            new zqu.QuickListController.PluginRecord();
			
        record.recordId = '01t6E000005vewWQAQ';
        record.relatedObjectIds = new Map<String, List<ID>
            >{'zqu__productrateplan__c' => new List<ID> {'a126E000002J8GXQA0'}};
   
        result.records = new List<zqu.QuickListController.PluginRecord>();
        result.records.add(record);
        return result;
     }
}