public class SubscriptionRatePlan_Handler { 
    
    public static void UpdateSubscription(List<Zuora__SubscriptionRatePlan__c> SubscriptionRatePlanToManage){
        Map<Id,zqu__ProductRatePlan__c> MapOfParentRatePlan; 
        Set<Id> SetProductRatePlanId = new Set<Id>();
        
        for(Zuora__SubscriptionRatePlan__c singleSubscriptionPlan : SubscriptionRatePlanToManage){
            SetProductRatePlanId.add(singleSubscriptionPlan.Product_Rate_Plan__c);
        }
        MapOfParentRatePlan = new Map<Id,zqu__ProductRatePlan__c>([SELECT id, zqu__ZuoraId__c FROM zqu__ProductRatePlan__c where id =:SetProductRatePlanId]);
        for(Zuora__SubscriptionRatePlan__c singleSubscriptionRatePlan : SubscriptionRatePlanToManage){
            if(MapOfParentRatePlan.containsKey(singleSubscriptionRatePlan.Product_Rate_Plan__c)){
                singleSubscriptionRatePlan.Zuora__ProductRatePlanId__c	= MapOfParentRatePlan.get(singleSubscriptionRatePlan.Product_Rate_Plan__c).zqu__ZuoraId__c;
            }
        }
              
     }
    public static void getSubscriptionRatePlan(Map<Id,Zuora__SubscriptionRatePlan__c> newMapSubRatePlan, Map<Id,Zuora__SubscriptionRatePlan__c> oldMapSubRatePlan){
        List<Zuora__SubscriptionRatePlan__c> SubscriptionRatePlanToManage = new List<Zuora__SubscriptionRatePlan__c>();
        
        for(Id singleSubscriptionId : newMapSubRatePlan.keySet()){
            if(newMapSubRatePlan.get(singleSubscriptionId).Product_Rate_Plan__c  != oldMapSubRatePlan.get(singleSubscriptionId).Product_Rate_Plan__c )
                SubscriptionRatePlanToManage.add(newMapSubRatePlan.get(singleSubscriptionId));
        }
        
        if(SubscriptionRatePlanToManage.size()>0){
            UpdateSubscription(SubscriptionRatePlanToManage);
        }
        
    }

}