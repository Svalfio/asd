public class futureGeneratePayment{

  @future(Callout=true)
  public static void apexcallout(String zuoraInvoiceId, Decimal amount, String invoiceCurrency, Date createdDate, String accountId, String InvoiceId) {
    
      ZuoraRestCalls zuoraRestCall = new ZuoraRestCalls();
      String token = zuoraRestCall.getAccessToken();
      
      String payDate;
      String bodyResp;
      payDate = string.valueOfGmt(createdDate);  
      payDate = payDate.replace(' ','T');
      
      string jsons = 
          '{'+
          '  "accountId": "'+accountId+'",'+
          '  "amount": '+amount+','+
          '  "comment": "Invoice Payment - Retail Pre Paid model",'+
          '  "currency": "'+invoiceCurrency+'",'+
          '  "effectiveDate": "'+payDate+'",'+
          '  "invoices": ['+
          '    {'+
          '      "amount": '+amount+','+
          '      "invoiceId": "'+zuoraInvoiceId+'"'+
          '    }  ],'+
          '  "type": "External"'+
          '}';
      
      if (token != null) {
          ZuoraInvoiceCustomSetting__c zcs = ZuoraInvoiceCustomSetting__c.getOrgDefaults();
          HttpRequest req = new HttpRequest();
          
          req.setEndpoint(zcs.Zuora_Endpoint__c + '/v1/payments/');
          req.setHeader('authorization', 'bearer ' + token);
          req.setHeader('content-type', 'application/json');
          req.setTimeout(6000);
          req.setMethod('POST');
          req.setBody(jsons);
          
          HttpResponse resp = new Http().send(req);
          System.debug('@@@@resp:  '+resp);
          System.debug(jsons);
          
          bodyResp = resp.getBody();
          System.debug(bodyResp);
          
          Map<String, Object> responseMap = (Map<String, Object>) JSON.deserializeUntyped(bodyResp);
          String paymentId = (String) responseMap.get('id');
          System.debug('Payment Id=' + paymentId);      
          
          Zuora__ZInvoice__c invoiceToUpdate;    
          invoiceToUpdate = [SELECT Id FROM Zuora__ZInvoice__c WHERE Id =: InvoiceId];
          invoiceToUpdate.DirectPaymentZuoraId__c = paymentId;
          update invoiceToUpdate;
          
      	}
	}
}