public with sharing class GetInvoicePdfREST {



	/*public static String getInvoice(String zuoraId, String quoteId){

		ZuoraInvoiceCustomSetting__c zcs = ZuoraInvoiceCustomSetting__c.getOrgDefaults();

		Map<String, Object> jsonToken = new Map<String, Object>();
		Map<String, Object> jsonLink = new Map<String, Object>();

		String token = '';
		String pdfUrl = '';
		String payloadToken = 'client_id=' + EncodingUtil.urlEncode(zcs.Client_Id__c, 'UTF-8') + '&client_secret=' + EncodingUtil.urlEncode(zcs.Client_Secret__c, 'UTF-8') +
		                      '&grant_type=' + EncodingUtil.urlEncode('client_credentials', 'UTF-8');

		Blob body;

		Attachment newAtt = new Attachment();


		Http h = new Http();

		// First call
		HttpRequest reqToken = new HttpRequest();

		reqToken.setEndpoint('https://rest.sandbox.eu.zuora.com/oauth/token');


		System.debug('@@@User ID: '+zcs.User_Id__c);
		System.debug('@@@User PSW: '+zcs.User_Password__c);
		System.debug('@@@Client ID: '+zcs.Client_Id__c);
		System.debug('@@@Client SECRET: '+zcs.Client_Secret__c);
		System.debug('@@@payloadToken: '+payloadToken);


		reqToken.setHeader('apiAccessKeyId', zcs.User_Id__c);
		reqToken.setHeader('apiSecretAccessKey', zcs.User_Password__c);
		reqToken.setHeader('grant_type', 'client_credentials');
		reqToken.setHeader('Content-Type', 'application/x-www-form-urlencoded');

		reqToken.setTimeout(6000);

		reqToken.setMethod('POST');

		reqToken.setBody(payloadToken);

		HttpResponse respToken = h.send(reqToken);
		System.debug('@@@@respToken:  '+respToken.getBody());

		System.debug('HTTP callout failed:'
		             + '\nendPoint=' + reqToken.getEndPoint()
		             + '\nstatusCode=' + respToken.getStatusCode()
		             + '\nstatus=' + respToken.getStatus()
		             + '\nbody=' + respToken.getBody());

		if (respToken.getStatusCode() == 200) {

			jsonToken = (Map<String, Object>)JSON.deserializeUntyped(respToken.getBody());

			token = (String)jsonToken.get('access_token');
			System.debug('@@@TOKEN: ' +token);


			//Second call
			HttpRequest reqLink = new HttpRequest();

			reqLink.setEndpoint('https://rest.sandbox.eu.zuora.com/v1/invoices/' + zuoraId + '/files');

			reqLink.setHeader('authorization', 'bearer ' + token);
			reqLink.setHeader('content-type', 'application/json');

			reqLink.setTimeout(6000);

			reqLink.setMethod('GET');

			HttpResponse respLink = new Http().send(reqLink);
			System.debug('@@@@RespLink:  '+respLink);

			if (respLink.getStatusCode() == 200) {

				jsonLink = (Map<String, Object>)JSON.deserializeUntyped(respLink.getBody());

				List<Object> invoiceFile = (List<Object>)jsonLink.get('invoiceFiles');
				for(Object fld : invoiceFile) {
					Map<String,Object> data = (Map<String,Object>)fld;
					pdfUrl = (String)data.get('pdfFileUrl');
					system.debug('@@@pdfUrl: ' + pdfUrl);
				}

				//Third call
				HttpRequest reqPdf = new HttpRequest();

				reqPdf.setEndpoint('https://rest.sandbox.eu.zuora.com' + pdfUrl);

				reqPdf.setHeader('authorization', 'bearer ' + token);
				reqPdf.setHeader('content-type', 'application/zip');

				reqPdf.setTimeout(6000);

				reqPdf.setMethod('GET');

				HttpResponse respPdf = new Http().send(reqPdf);
				System.debug('@@@@respPdf:  '+respPdf);

				if (respPdf.getStatusCode() == 200) {

					body = respPdf.getBodyAsBlob();

					newAtt.Name = 'PDF_Invoice_' + zuoraId ;
					newAtt.Body = body;
					newAtt.ParentId = quoteId;
					newAtt.ContentType = 'application/pdf';

					insert newAtt;

					return newAtt.id;
                }
                else{
                    throw new AuraHandledException('Something went wrong: '+ respPdf.getStatus());
                }
            }
            else{
                throw new AuraHandledException('Something went wrong: '+ respLink.getStatus());
            }
        }
        else{
            throw new AuraHandledException('Something went wrong: '+ respToken.getStatus());
        }
    }*/
    
    @AuraEnabled
    public static String getInvoice(String zuoraId, String quoteId){
		
        ZuoraRestCalls restCall = new ZuoraRestCalls();
        String token;
        String url;
        String idAttach;
		
        token = restCall.getAccessToken();
		if (token != null) {
			
            url = restCall.getPDFUrl(token, zuoraId);
			
			if (url != null) {

				idAttach = restCall.AttachPDF(token, url, zuoraId, quoteId);

				if (idAttach != null) {

					return idAttach;
                }
                else{
                    throw new AuraHandledException('Error to get and attach file');
                }
            }
            else{
                throw new AuraHandledException('Error to get the url');
            }
        }
        else{
            throw new AuraHandledException('Error to get the token ');
        }
    }
}