public with sharing class downloadInvoiceFromQuote {
    @InvocableMethod(label='Get Invoice Url')
    public static List<String> getInvoiceUrl(List<String> QuoteId){
        ZuoraRestCalls restCall = new ZuoraRestCalls();
        List<String> results = new List<String>();
        String token;
        String InvoiceId;
        String url;
        String urlDownload;
        String InvoiceNumber;
        token = restCall.getAccessToken();
        for(String q: QuoteId){
            
            if(token != null){
                InvoiceId = restCall.getInvoiceId(q, token);
                if(InvoiceId != null){
                     url = restCall.getPDFUrl(token, InvoiceId);
					 InvoiceNumber = restCall.getInvoiceNumber(token, InvoiceId);
                    if (url != null) {
                        Zuora__ZInvoice__c invoice = [select id from Zuora__ZInvoice__c where Name =:InvoiceNumber];
                        urlDownload = restCall.DownloadPdf(token, url, InvoiceNumber,invoice.id);

                        if (urlDownload != null) {
        
                            results.add(urlDownload);
                        }else{
                            results.add('attach file');
                		}
                	}else{
                		results.add('url');
            		}
                }else{
                	results.add('invoice');
        		}
            }else{
                results.add('token');
            }
        }        
        for(String r: results){
            system.debug('###############'+r);
        }
        return results;
    }
}