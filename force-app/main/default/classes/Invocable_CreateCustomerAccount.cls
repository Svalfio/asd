global class Invocable_CreateCustomerAccount {
	global with sharing class CreateCustomerAccountInputs {
        @InvocableVariable(Label = 'externalId' Required = false) global String externalId;
        @InvocableVariable(Label = 'serviceProviderId' Required = false) global Integer serviceProviderId;
        @InvocableVariable(Label = 'partnerId' Required = false) global Integer partnerId;
        @InvocableVariable(Label = 'subPartnerId' Required = false) global Integer subPartnerId;
        @InvocableVariable(Label = 'description' Required = false) global String description;
        @InvocableVariable(Label = 'phone' Required = false) global String phone;
        @InvocableVariable(Label = 'mail' Required = false) global String mail;  
        @InvocableVariable(Label = 'categoryId' Required = false) global Integer categoryId;
        @InvocableVariable(Label = 'quoteId' Required = false) global String quoteId;
        @InvocableVariable(Label = 'Esito' Required = false) global List<String> Esito = new List<String>();
	}
    
    @InvocableMethod(Label = 'API_OSS_CreateCustomerAccount')
	global static List<String> makePostCallout(CreateCustomerAccountInputs[] requestActionInList) 
    {
        SfToHerokuAPIManager.PhoneInfo newPhone = new SfToHerokuAPIManager.PhoneInfo();
       	newPhone.category = 'mobile';
        newPhone.phone = requestActionInList[0].phone;
        newPhone.phoneType = 'billing';
        List<SfToHerokuAPIManager.PhoneInfo> phoneList = new List<SfToHerokuAPIManager.PhoneInfo>();
        phoneList.add(newPhone);
        
        SfToHerokuAPIManager.EmailInfo newEmail = new SfToHerokuAPIManager.EmailInfo();
       	newEmail.address = requestActionInList[0].mail;
       	newEmail.category = 'billing';
        List<SfToHerokuAPIManager.EmailInfo> emailList = new List<SfToHerokuAPIManager.EmailInfo>();
        emailList.add(newEmail);

    	SfToHerokuAPIManager.CustomerAccountCreationRequest newCustomerAccount = new SfToHerokuAPIManager.CustomerAccountCreationRequest();
        
        if(requestActionInList[0].serviceProviderId != null)
        	newCustomerAccount.serviceProviderId = requestActionInList[0].serviceProviderId;
        else
            newCustomerAccount.serviceProviderId = 1;
        
        newCustomerAccount.externalId = requestActionInList[0].externalId;
        newCustomerAccount.subPartnerId = requestActionInList[0].subPartnerId;
        newCustomerAccount.partnerId = requestActionInList[0].partnerId;
        newCustomerAccount.categoryId = requestActionInList[0].categoryId;
        newCustomerAccount.description = 'blank';
        newCustomerAccount.phoneList = phoneList;
		newCustomerAccount.emailList = emailList;
        
		SfToHerokuAPIManager.HerokuResponse responseNewCustomerAccount = SfToHerokuAPIManager.createCustomerAccount(newCustomerAccount);
        
        if(responseNewCustomerAccount != null){
            Integer errCode = responseNewCustomerAccount.errorCode;
        	String descEsito = responseNewCustomerAccount.message;
        
            if(errCode == 200 || errCode == 201){
            	SfToHerokuAPIManager.CustomerAccountCreationResponse dataNewCustomerAccount = (SfToHerokuAPIManager.CustomerAccountCreationResponse)responseNewCustomerAccount.data;         
                Integer customerAccount = dataNewCustomerAccount.id;
                String customerAccountIdString = String.valueOf(customerAccount);
                requestActionInList[0].Esito.add(customerAccountIdString);
                
                Account accountToUpdate = [SELECT OSS_Customer_Account_ID__c FROM Account where Id =:requestActionInList[0].externalId];
                accountToUpdate.OSS_Customer_Account_ID__c = customerAccountIdString;
                update accountToUpdate;
                // da sviluppare il salvataggio del customer account sull'account
                //         
            }else{
                String bodyRequest = JSON.serialize(newCustomerAccount);
                String bodyResponse = JSON.serialize(responseNewCustomerAccount);
                ApexLog__c writeErrorLog = new ApexLog__c();
                writeErrorLog.ClassName__c = 'Invocable_CreateCustomerAccount';
                writeErrorLog.MethodName__c = 'API_OSS_CreateCustomerAccount';
                writeErrorLog.ProcessName__c = 'CreateCustomerAccount';
                writeErrorLog.Body__c = bodyRequest;
                writeErrorLog.Response__c = bodyResponse;
                writeErrorLog.ProcessId__c = requestActionInList[0].quoteId;
                insert writeErrorLog;
                requestActionInList[0].Esito.add('KO');
            }
        }
    	return requestActionInList[0].Esito; 
	}
}