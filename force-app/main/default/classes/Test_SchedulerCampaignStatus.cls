@isTest
public class Test_SchedulerCampaignStatus {
    
    @testSetup
    static void createData(){
        
        Date myDate = date.newinstance(2019, 2, 1);
        Date myDate2 = date.newinstance(2019, 12, 2);
        
        Campaign testCampaign = new Campaign(Name='testCampaign',
                                            Channel__c='SMS;Partner_Portal;Phone;Customer_Portal',
                                            Status='In Progress',
                                            startDate=myDate,
                                            endDate=myDate2,
                                            SMS_Body__c='SMSTest',
                                            Web_Body__c='webTest');
        insert testCampaign;
    }
    
        @isTest
    static void updateStatus(){
        
        Test.startTest();
        SchedulerCampaignStatus sh1 = new SchedulerCampaignStatus();
        String sched = 30 + ' ' + 53 + ' ' + 12 + ' ' + 30 + ' ' + 12 + ' ? ' + 2019; 
        system.schedule('Test Scheduling 4', sched, sh1); 
        Test.stopTest();
    }

}