@isTest
public class Test_CommissionModelTriggerHandler {
    
    
    @testSetup
    static void createData(){
        Id directRecordTypeId = Schema.SObjectType.Commission_Model__c.getRecordTypeInfosByDeveloperName().get('Direct').getRecordTypeId();
        Id indirectRecordTypeId = Schema.SObjectType.Commission_Model__c.getRecordTypeInfosByDeveloperName().get('Indirect').getRecordTypeId();
        
        Id resellerRTId=  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_RESELLER).getRecordTypeId();
        Account reseller =  new Account(Name = 'Reseller',
                                        RecordTypeId=resellerRTId);
        
        insert reseller;
        
        Id subResellerRTId =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(ApexConstants.ACCOUNT_RECORDTYPE_DEVELOPERNAME_SUBRESELLER).getRecordTypeId();
        account subReseller = new Account(Name='SubReseller1',
                                          RecordTypeId=subResellerRTId,
                                          ParentId=reseller.id);
        insert subReseller;
        
        Commission_Model__c commissionModelResellerSetup = new Commission_Model__c(Reseller_Id__c=reseller.id,Country__c='COG',start_date__c=date.newInstance(2019, 02, 01),end_date__c =date.newInstance(2019, 02, 10));
     	Commission_Model__c commissionModelResellerSetup2 = new Commission_Model__c(Reseller_Id__c=reseller.id,Country__c='COG',start_date__c=date.newInstance(2019, 02, 11),end_date__c =date.newInstance(2019, 02, 14));
        insert commissionModelResellerSetup;
        insert commissionModelResellerSetup2;
    }
    
    @isTest
    static void onInsertTest(){
        system.debug('Test_CommissionModelTriggerHandler---------onInsertTest--------START');
        
        Account reseller = [SELECT id FROM Account WHERE Name = 'Reseller'];
        Account subReseller = [SELECT id FROM Account WHERE Name = 'SubReseller1'];
            
        Commission_Model__c commissionModelReseller= new Commission_Model__c(Reseller_Id__c=reseller.id,Country__c='COG',start_date__c=date.newInstance(2019, 01, 01),end_date__c =date.newInstance(2019, 01, 10));
        Commission_Model__c commissionModelSubReseller = new Commission_Model__c(Reseller_Id__c=reseller.id,Sub_Reseller_Id__c=subReseller.id,Country__c='CIV',start_date__c=date.newInstance(2019, 01, 01),end_date__c =date.newInstance(2019, 01, 10));
        Commission_Model__c commissionModelReseller2= new Commission_Model__c(Reseller_Id__c=reseller.id,Country__c='CIV',start_date__c=date.newInstance(2019, 01, 11),end_date__c =date.newInstance(2019, 01, 15));
        
        List<Commission_Model__c> commissionModelList = new List<Commission_Model__c>();
        commissionModelList.add(commissionModelReseller);
        commissionModelList.add(commissionModelReseller2);
        commissionModelList.add(commissionModelSubReseller);
        insert commissionModelList;
        
        List<Commission_Item__c> commissionItemList1 = [SELECT id FROM Commission_Item__c WHERE Commission_Model__c= :commissionModelReseller.id];
        List<Commission_Item__c> commissionItemList2 = [SELECT id FROM Commission_Item__c WHERE Commission_Model__c= :commissionModelReseller2.id];
        List<Commission_Item__c> commissionItemList3 = [SELECT id FROM Commission_Item__c WHERE Commission_Model__c= :commissionModelSubReseller.id];
        
        System.debug(' commissionItemList1 size : '+commissionItemList1.size()+
                    ' commissionItemList2 size : '+commissionItemList2.size()+
                     ' commissionItemList3 size : '+commissionItemList3.size());

    }
    
    @isTest
    static void onUpdateTest(){
        system.debug('Test_CommissionModelTriggerHandler---------onUpdateTest--------START');
        
        Id directRecordTypeId = Schema.SObjectType.Commission_Model__c.getRecordTypeInfosByDeveloperName().get('Direct').getRecordTypeId();
        Id indirectRecordTypeId = Schema.SObjectType.Commission_Model__c.getRecordTypeInfosByDeveloperName().get('Indirect').getRecordTypeId();
        
        Account reseller = [SELECT id FROM Account WHERE Name = 'Reseller'];
        Account subReseller = [SELECT id FROM Account WHERE Name = 'SubReseller1'];
        reseller.CurrencyIsoCode='USD';
       	update reseller;
        
        subReseller.CurrencyIsoCode='USD';
       	update subReseller;
        
        Commission_Model__c commissionModelReseller= new Commission_Model__c(Reseller_Id__c=reseller.id,Country__c='COG',recordtypeId=directRecordTypeId,start_date__c=date.newInstance(2019, 01, 01),end_date__c =date.newInstance(2019, 01, 10));
        Commission_Model__c commissionModelReseller2= new Commission_Model__c(Reseller_Id__c=reseller.id,Sub_Reseller_Id__c=subReseller.Id,Country__c='COG',recordtypeId=indirectRecordTypeId,start_date__c=date.newInstance(2019, 01, 11),end_date__c =date.newInstance(2019, 01, 15));
        
        List<Commission_Model__c> commissionModelList = new List<Commission_Model__c>();
        commissionModelList.add(commissionModelReseller);
        commissionModelList.add(commissionModelReseller2);
        insert commissionModelList;
        
        commissionModelReseller2.start_date__c = date.newInstance(2019, 01, 12);
        update commissionModelReseller2;
        
    }
    
    
    
}