public class AsyncWalletResellerRecharged implements Queueable, Database.AllowsCallouts{
    
    private Set<Id> pcIdResponse;
    
    public AsyncWalletResellerrecharged(Set<Id> WalletIds){
        this.pcIdResponse=WalletIds;
    }
    
    public void execute(QueueableContext cntx) {
        
        List<Wallet__c> wallets = new List<Wallet__c>();
        List<Wallet__c> walletsSub = new List<Wallet__c>();
        List<String> reseller_Ids = new List<String>();
        List<String> subreseller_Ids = new List<String>();
        List<String> reseller_account_Ids = new List<String>();
        List<String> subreseller_account_Ids = new List<String>();
        List<User> resellers = new List<User>();
        List<User> subresellers = new List<User>();
        List<Order> subreseller_orders = new List<Order>();
        List<String> notification_sResIds = new List<String>();
        List<Notification_Item__c> notificationsRes = new List<Notification_Item__c>();
        List<String> notification_sSubResIds = new List<String>();
        List<Notification_Item__c> notificationsSubRes = new List<Notification_Item__c>();
        
        Map<Id, User> reseller_map = new Map<Id, User>();
        Map<Id, Wallet__c> subwallet_map = new Map<Id, Wallet__c>();
        Map<Id, User> subreseller_map = new Map<Id, User>();
        Map<Id, List<Order>> subreseller_order_map = new Map<Id, List<Order>>();
        Map<String, Notification_Item__c> notificationRes_map = new Map<String, Notification_Item__c>();
        Map<String, Notification_Item__c> notificationSubRes_map = new Map<String, Notification_Item__c>();
        
        List<FeedItem> feeds = new List<FeedItem>();
        
        Id communityId = [Select id from Network where Name='Eutelsat Partner Community'].id;
        
        wallets = [SELECT id, Name, Amount__c, Account__c FROM Wallet__c WHERE Id IN :pcIdResponse];
        walletsSub = [SELECT id, Name, OwnerId, Amount__c, Reseller_Wallet__c, Account__c FROM Wallet__c WHERE Reseller_Wallet__c IN :pcIdResponse];
        
        for(Wallet__c w: wallets){
            reseller_Ids.add(w.Account__c);
        }
        
        for(Wallet__c w: walletsSub){
            subreseller_Ids.add(w.Account__c);
            system.debug('++++++++++++++++++++' + w.Account__c);
        }
        
        resellers = [SELECT id, Name, Account.Notifications_Web__c, Account.Notifications_Email__c, Account.Notifications_SMS__c,
                     Account.Id, Account.Notification_Setting__c, Email FROM User WHERE AccountId IN:reseller_Ids];
        subresellers = [SELECT id, Name, Account.Notifications_Web__c, Account.Notifications_Email__c, Account.Notifications_SMS__c,
                        Account.Id, Account.Notification_Setting__c, Email FROM User WHERE AccountId IN:subreseller_Ids];
        
        for(User u: resellers){
            if(!reseller_map.containsKey(u.AccountId)){
                reseller_map.put(u.AccountId, u);
            }
            reseller_account_Ids.add(u.AccountId);
            notification_sResIds.add(u.Account.Notification_Setting__c);
        }
        
        for(User u: subresellers){
            if(!subreseller_map.containsKey(u.AccountId)){
                subreseller_map.put(u.AccountId, u);
                system.debug('id account sub'+u.AccountId);
            }
            subreseller_account_Ids.add(u.Account.Id);
            notification_sSubResIds.add(u.Account.Notification_Setting__c);
        }
        
        notificationsRes = [SELECT id, Notification_Setting__c, Event_Type__r.Event_Code__c, Event_Type__r.Name FROM Notification_Item__c WHERE Notification_Setting__c IN :notification_sResIds];
        notificationsSubRes = [SELECT id, Notification_Setting__c, Event_Type__r.Event_Code__c, Event_Type__r.Name FROM Notification_Item__c WHERE Notification_Setting__c IN :notification_sSubResIds];
        
        for(Notification_Item__c n: notificationsRes){
            system.debug(n.Event_Type__r.Name.split('_0')[1]);
            if(!notificationRes_map.containsKey(n.Notification_Setting__c+'_0'+ n.Event_Type__r.Name.split('_0')[1])){
                system.debug('test');
                notificationRes_map.put(n.Notification_Setting__c+'_0'+ n.Event_Type__r.Name.split('_0')[1], n);
            }
        }
        
        for(Notification_Item__c n: notificationsSubRes){
            system.debug(n.Event_Type__r.Name.split('_0')[1]);
            if(!notificationSubRes_map.containsKey(n.Notification_Setting__c+'_0'+ n.Event_Type__r.Name.split('_0')[1])){
                system.debug('test');
                notificationSubRes_map.put(n.Notification_Setting__c+'_0'+ n.Event_Type__r.Name.split('_0')[1], n);
            }
        }      
        
        AllMySMSPartner__c mc = AllMySMSPartner__c.getInstance('SMSPartner');
        String endpoint = mc.endpoint__c;
        if(endpoint==null)
            System.debug('Endpoint vuota');
        
        
        for(Wallet__c w: walletsSub){
            if(!subwallet_map.containsKey(w.Reseller_Wallet__c)){
                subwallet_map.put(w.Reseller_Wallet__c, w);
                system.debug('wallet map id'+ w.Reseller_Wallet__c);
            }
        }
        
        subreseller_orders = [SELECT id, Name, AccountId, Status FROM Order WHERE AccountId IN :subreseller_account_Ids AND Status = 'Pending'];
        
        for(Order o: subreseller_orders){
            if(!subreseller_order_map.containsKey(o.AccountId)){
                subreseller_order_map.put(o.AccountId,new List<Order>{o});
                system.debug('ordine ++++++++++' + o.AccountId);
            }else{
                subreseller_order_map.get(o.AccountId).add(o);
            }
        }
        
        
        for(Wallet__c w: wallets){
            //gestione notifiche
            //
            if(reseller_map.get(w.Account__c).Account.Notifications_SMS__c == true){
                if(reseller_map.get(w.Account__c).Account.Notification_Setting__c != null){
                    if(notificationRes_map.get(reseller_map.get(w.Account__c).Account.Notification_Setting__c+'_003').Event_Type__r.Event_Code__c != null){
                        Map<String, String> parametersMap = new Map<String, String>{'subject' => String.valueOf(w.Name), 'comments' => String.valueOf(w.Amount__c)};
                        SfToHerokuAPIManager.sendSingleNotificationRequest richiesta = new SfToHerokuAPIManager.sendSingleNotificationRequest();
                        richiesta.accountId = String.valueOf(w.Account__c);
                        richiesta.channel = 'SMS';
                        richiesta.eventCode = notificationRes_map.get(reseller_map.get(w.Account__c).Account.Notification_Setting__c+'_003').Event_Type__r.Event_Code__c;
                        richiesta.parametersMap = parametersMap;
                    
                        SFtoHerokuAPIManager.HerokuResponse response = SfToHerokuAPIManager.sendSingleNotification(richiesta);
                        System.debug(response.errorCode);
                    }
                }
            }
            
            for(id key: reseller_map.keySet()){
                system.debug(key);
            }
            if(reseller_map.get(w.Account__c).Account.Notifications_Web__c == true){
                FeedItem feedRes = new FeedItem();
                feedRes.Title = 'Your wallet has been recharged';
                feedRes.Body = 'Your wallet '+ w.Name+ ' has been recharged. New amount: '+ w.Amount__c;
                system.debug('Your wallet '+ w.Name+ ' has been recharged. New amount: '+ w.Amount__c);
                feedRes.ParentId = reseller_map.get(w.Account__c).Id;
                feedRes.NetworkScope=communityId;
                feeds.add(feedRes);
            }
            
            if(reseller_map.get(w.Account__c).Account.Notifications_Email__c == true){
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.toAddresses = new String[] {reseller_map.get(w.Account__c).Email};
                    message.subject = 'Your walles has been recharged';
                message.plainTextBody =  'Your wallet '+ w.Name+ ' has been recharged. New amount: ' + w.Amount__c;
                Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
                    Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                
                if (results[0].success) {
                    System.debug('The email to reseller was sent successfully.');
                } else {
                    System.debug('The email to reseller failed to send: ' + results[0].errors[0].message);
                }
            }
            
            if(subwallet_map.get(w.Id) == null){
                system.debug('Nullo 1');
                system.debug('Wallet id nullo' + w.Id);
            }
            if(subreseller_map.get(subwallet_map.get(w.Id).Account__c) == null){
                system.debug('Nullo 2');
            }
            if(subreseller_order_map.get(subreseller_map.get(subwallet_map.get(w.Id).Account__c).Account.Id) == null){
                system.debug('NULLO');
            }
            
            
            if(subwallet_map.get(w.Id) != null){
                if(subreseller_map.get(subwallet_map.get(w.Id).Account__c) != null){
                    if(subreseller_map.get(subwallet_map.get(w.Id).Account__c).Account.Notifications_SMS__c == true){
                        if(subreseller_map.get(subwallet_map.get(w.Id).Account__c).Account.Notification_Setting__c != null){
                            if(notificationSubRes_map.get(subreseller_map.get(subwallet_map.get(w.Id).Account__c).Account.Notification_Setting__c+'_007').Event_Type__r.Event_Code__c != null){
                                Map<String, String> parametersMap = new Map<String, String>{'subject' => String.valueOf(w.Name), 'comments' => String.valueOf(w.Amount__c)};
                                SfToHerokuAPIManager.sendSingleNotificationRequest richiesta = new SfToHerokuAPIManager.sendSingleNotificationRequest();
                                richiesta.accountId = String.valueOf(w.Account__c);
                                richiesta.channel = 'SMS';
                                richiesta.eventCode = notificationSubRes_map.get(subreseller_map.get(subwallet_map.get(w.Id).Account__c).Account.Notification_Setting__c+'_007').Event_Type__r.Event_Code__c;
                                richiesta.parametersMap = parametersMap;
                            
                                SFtoHerokuAPIManager.HerokuResponse response = SfToHerokuAPIManager.sendSingleNotification(richiesta);
                                System.debug(response.errorCode);
                            }
                        }
                    }
                    if(subreseller_map.get(subwallet_map.get(w.Id).Account__c).Account.Notifications_Web__c == true && subreseller_order_map.get(subreseller_map.get(subwallet_map.get(w.Id).Account__c).Account.Id) != null){
                        FeedItem feedSubRes = new FeedItem();
                        feedSubRes.Title = 'You can proceed with order(s)';
                        feedSubRes.Body = 'Dear Partner, \nPlease note that you can now proceed with the order(s) which were on hold. \nYour reseller '+w.Account__c+' has recharged his wallet.\nKind regards';
                        system.debug('Dear Partner, \nPlease note that you can now proceed with the order(s) which were on hold. \nYour reseller '+w.Account__c+' has recharged his wallet.\nKind regards');
                        feedSubRes.ParentId = subreseller_map.get(subwallet_map.get(w.Id).Account__c).Id;
                        feedSubRes.NetworkScope=communityId;
                        feeds.add(feedSubRes);
                    }
                    
                    if(subreseller_map.get(subwallet_map.get(w.Id).Account__c).Account.Notifications_Email__c == true && subreseller_order_map.get(subreseller_map.get(subwallet_map.get(w.Id).Account__c).Account.Id) != null){
                        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                        message.toAddresses = new String[] {subreseller_map.get(subwallet_map.get(w.Id).Account__c).Email};
                            message.subject = 'You can proceed with order(s)';
                        message.plainTextBody = 'Dear Partner, \nPlease note that you can now proceed with the order(s) which were on hold. \nYour reseller '+w.Account__c+' has recharged his wallet.\nKind regards';
                        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
                            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                        
                        if (results[0].success) {
                            System.debug('The email to subreseller was sent successfully.');
                        } else {
                            System.debug('The email to subreseller failed to send: ' + results[0].errors[0].message);
                        }
                    }
                }
            }
            
            
        }
        if(feeds.size() > 0){
            insert feeds;
        }
    }
}