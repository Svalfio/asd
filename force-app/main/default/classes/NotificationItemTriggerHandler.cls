public class NotificationItemTriggerHandler {
	
    public void checkEventType(List<Notification_Item__c> listNotItems){
        
        List<Id> nsIDList = new List<Id>();
        
        for(Notification_Item__c niTriggerList : listNotItems){
            nsIDList.add(niTriggerList.Notification_Setting__c);
        }
        
        List<Notification_Item__c> niToCheck = [SELECT Event_Type__c,Notification_Setting__c,Preferred_Language__c FROM Notification_Item__c WHERE Notification_Setting__c IN :nsIDList];
        
        for(Notification_Item__c currentNI : niToCheck){
            for(Notification_Item__c niFromTrigger : listNotItems){
                if(currentNI.Notification_Setting__c==niFromTrigger.Notification_Setting__c && currentNI.Event_Type__c==niFromTrigger.Event_Type__c && currentNI.Preferred_Language__c==niFromTrigger.Preferred_Language__c){

                    niFromTrigger.addError('Can\'t create a duplicate Notification Item for the same Notification Setting');
                    
                }
            }
        }
        
    }
    
}