global class PlanAddOnCustomFilter implements zqu.QuickListController.IHierarchyAllowedRecordPlugin {
    global zqu.QuickListController.PluginResult getAllowedRecords
        (Map<Object, Object> contextIds, Map<String, String> pluginParams){
            
            zqu.QuickListController.PluginResult result = new zqu.QuickListController.PluginResult();    
            result.records = new List<zqu.QuickListController.PluginRecord>();
            
            String quoteId = '';
            for (Object k : contextIds.keySet()) {
                quoteId = (contextIds.containskey('qid') && contextIds.get('qid')!=null)? (Id) contextIds.get('qid'):null;
                System.debug(quoteId);
                //quoteId = (Id) contextIds.get('qid');
            }  
            
            Set<String> idSet = new Set<String>();
            List<ProductCustomMetadata__mdt> productList = [Select Id,External_Id__c from ProductCustomMetadata__mdt Where Hide_Add_On__c  = true];
            system.debug('productList size : ' + productList.size());
            
            if(productList.size()>0){
                for(ProductCustomMetadata__mdt prod : productList){
                    idSet.add(prod.External_Id__c);
                }
            }
            
            List<zqu__Quote__c> QuoteList = [SELECT  zqu__Account__c,Customer_Subscription_ID__c, zqu__Account__r.Parent.SIP_Option__c, zqu__Account__r.Billing__c, Owner.Username 
                                             FROM zqu__Quote__c WHERE Id =: quoteId]; 
            
            if(QuoteList.size()>0){
                List<zqu__ProductRatePlan__c> allowedProdRatePlans = new List<zqu__ProductRatePlan__c>();
                List<Eligible_Partner__c> Eligible_Partner_Product = new List<Eligible_Partner__c>();
                
                //predisposizione filtri aggiuntivi
                Boolean ParentSipOption = QuoteList[0].zqu__Account__r.Parent.SIP_Option__c; 
                String billingType = QuoteList[0].zqu__Account__r.Billing__c;                   
                List<zqu__QuoteRatePlanCharge__c> listProduct = new List<zqu__QuoteRatePlanCharge__c>();
                //Sara: Sto usando questa lista anche sotto per il processo di Wholesale - non eliminatela sara.gaviano@enigen.eu
                listProduct = [SELECT zqu__ProductRatePlanCharge__r.zqu__ProductRatePlan__r.zqu__Product__c, 
                               zqu__ProductRatePlanCharge__r.zqu__ProductRatePlan__r.zqu__Product__r.Service_Category__c
                               FROM zqu__QuoteRatePlanCharge__c WHERE zqu__QuoteRatePlan__r.zqu__Quote__c =: quoteId  AND
                               zqu__ProductRatePlanCharge__r.zqu__ProductRatePlan__r.zqu__Product__r.ProductCode !=: idSet];	
                
                Boolean FreeNight = false;
                Boolean DB50 = false;
                Boolean DB200 = false;
                
                List<Account> ParentAccount = new List<Account>();
                ParentAccount = [SELECT id FROM Account where username__c=: QuoteList[0].Owner.Username];
                
                
                
                if(billingType!='Wholesale'){
                    System.debug('RETAIL!!!');
                    if(listProduct.size()>0){
                        List<id> productIdList = new List<id>();
                        List<zqu__ProductOption__c> listProdOption = new List<zqu__ProductOption__c>();
                        Map<zqu__QuoteRatePlanCharge__c, zqu__ProductOption__c> RatePlanCharge_option = new Map<zqu__QuoteRatePlanCharge__c, zqu__ProductOption__c>();
                        Map<zqu__QuoteRatePlanCharge__c,ProductOptions> RatePlanCharge_ProductOptions = new Map<zqu__QuoteRatePlanCharge__c,ProductOptions>();
                        String ChildProduct_Name ='';
                        Map<id,List<Id>> AllowedRatePlan_Map = new Map<id,List<Id>>();
                    
                    for(zqu__QuoteRatePlanCharge__c tempProduct : listProduct){
                        productIdList.add(tempProduct.zqu__ProductRatePlanCharge__r.zqu__ProductRatePlan__r.zqu__Product__c);
                    }   
                    
                    system.debug('productId list: '+productIdList);
                    
                    listProdOption = [SELECT zqu__ParentProduct__c,zqu__ChildProduct__r.Name FROM zqu__ProductOption__c WHERE zqu__ParentProduct__c =: productIdList];
                    for(zqu__QuoteRatePlanCharge__c tempProduct : listProduct){
                        for(zqu__ProductOption__c po : listProdOption){
                            if(po.zqu__ParentProduct__c==tempProduct.zqu__ProductRatePlanCharge__r.zqu__ProductRatePlan__r.zqu__Product__c) RatePlanCharge_option.put(tempProduct, po);
                        }
                    }
                    
                    system.debug('listProdOption: '+listProdOption);
                    
                    for(zqu__QuoteRatePlanCharge__c tempProduct : RatePlanCharge_option.keyset()){
                        ChildProduct_Name=RatePlanCharge_option.get(tempProduct).zqu__ChildProduct__r.Name;
                        
                        FreeNight=(ChildProduct_Name == 'Night Time Option')?true:false;                        
                        DB50=(ChildProduct_Name == 'Databoost 50GB Option')?true:false;
                        DB200=(ChildProduct_Name == 'Databoost 200GB Option')?true:false;
                        
                        RatePlanCharge_ProductOptions.put(tempProduct, new ProductOptions(FreeNight,DB50,DB200));
                    }
                    
                    system.debug('RatePlanCharge_ProductOptions: '+RatePlanCharge_ProductOptions);
                    
                    
                    Eligible_Partner_Product = [Select id, Product_Rate_Plan__c, Product_Rate_Plan__r.zqu__Product__c from Eligible_Partner__c where Partner__c=: ParentAccount AND
                                                Product_Rate_Plan__r.zqu__Product__r.Status__c='Active' AND Product_Rate_Plan__r.zqu__Product__r.Family='Add-On'
                                                AND Product_Rate_Plan__r.zqu__Product__r.Product_Type__c=: billingType ];
                    
                    list<Id> AddingPlanId = new List<Id>();
                    for(Eligible_Partner__c ep : Eligible_Partner_Product){
                        AddingPlanId.add(ep.Product_Rate_Plan__c);
                    }
                    
                    allowedProdRatePlans = [SELECT Id, zqu__Product__c, zqu__Product__r.Name, Name FROM zqu__ProductRatePlan__c  
                                            WHERE (zqu__Product__r.Product_Type__c =: billingType AND zqu__Product__r.Family = 'Add-On'
                                                   AND zqu__Product__r.Status__c = 'Active' AND Partner__c='All') OR id=:AddingPlanId];
                    
                    
                    
                    system.debug('allowedProdRatePlans: '+allowedProdRatePlans);
                    
                    if(!RatePlanCharge_ProductOptions.isEmpty()){
                        for(zqu__QuoteRatePlanCharge__c qrp : RatePlanCharge_ProductOptions.keyset()){
                            for(zqu__ProductRatePlan__c prp : allowedProdRatePlans){
                                if(!(RatePlanCharge_ProductOptions.get(qrp).DB50 && prp.zqu__Product__r.Name=='Databoost 50GB Option')
                                   && !(RatePlanCharge_ProductOptions.get(qrp).DB200 && prp.zqu__Product__r.Name=='Databoost 200GB Option')){ 
                                       List<Id> allowedRP_List = new List<id>();
                                       if(!ParentSipOption){
                                           if((!RatePlanCharge_ProductOptions.get(qrp).FreeNight || 
                                               (RatePlanCharge_ProductOptions.get(qrp).FreeNight && prp.zqu__Product__r.Name!='Night Time Option')) && 
                                              prp.zqu__Product__r.Name!='SIP Prioritization - Retail'){
                                                  system.debug('test');
                                                  if(AllowedRatePlan_Map.containskey(prp.zqu__Product__c)) 
                                                      allowedRP_List.addAll(AllowedRatePlan_Map.get(prp.zqu__Product__c));
                                                  allowedRP_List.add(prp.id);
                                                  AllowedRatePlan_Map.put(prp.zqu__Product__c,allowedRP_List);
                                              }
                                       }else{
                                           if(!RatePlanCharge_ProductOptions.get(qrp).FreeNight ||
                                              (RatePlanCharge_ProductOptions.get(qrp).FreeNight && prp.zqu__Product__r.Name!='Night Time Option')){
                                                  if(AllowedRatePlan_Map.containskey(prp.zqu__Product__c)) 
                                                      allowedRP_List.addAll(AllowedRatePlan_Map.get(prp.zqu__Product__c));
                                                  allowedRP_List.add(prp.id);
                                                  AllowedRatePlan_Map.put(prp.zqu__Product__c,allowedRP_List);
                                              }
                                       }  
                                   }
                            }
                        }
                    }else{
                        for(zqu__ProductRatePlan__c prp : allowedProdRatePlans){
                            List<Id> allowedRP_List = new List<id>();
                            if(!ParentSipOption && prp.zqu__Product__r.Name!='SIP Prioritization - Retail'){
                                if(AllowedRatePlan_Map.containskey(prp.zqu__Product__c)) 
                                    allowedRP_List.addAll(AllowedRatePlan_Map.get(prp.zqu__Product__c));
                                allowedRP_List.add(prp.id);
                                AllowedRatePlan_Map.put(prp.zqu__Product__c,allowedRP_List);
                            }else if(ParentSipOption){
                                if(AllowedRatePlan_Map.containskey(prp.zqu__Product__c)) 
                                    allowedRP_List.addAll(AllowedRatePlan_Map.get(prp.zqu__Product__c));
                                allowedRP_List.add(prp.id);
                                AllowedRatePlan_Map.put(prp.zqu__Product__c,allowedRP_List);
                            }
                        }
                    }
                    
                    
                    system.debug('AllowedRatePlan_Map: '+AllowedRatePlan_Map);
                    
                    if(!AllowedRatePlan_Map.isEmpty()){
                        zqu.QuickListController.PluginRecord record;
                        for(Id ProdId : AllowedRatePlan_Map.keySet()){
                            record = new zqu.QuickListController.PluginRecord();
                            record.recordId = ProdId;
                            record.relatedObjectIds = new Map<String, List<ID> >{'zqu__productrateplan__c' => AllowedRatePlan_Map.get(ProdId)};
                                result.records.add(record);
                            
                        }
                        System.debug('result.size() : ' + result.records.size());
                    }
                    }
                    
                }else{
                    System.debug('WHOLESALE!!!');
                    List<String> RatePlanSubscription = new List<String>();
                    Map<Id, List<Id>> Product_RatePlans = new Map<Id, List<Id>>();
                    zqu.QuickListController.PluginRecord record;
                    
                    boolean limited = false;
                    List<String> ServiceCategoryCondition= new List<String>();
                    
                    system.debug('listProduct: '+listProduct);
                    
                    for(zqu__QuoteRatePlanCharge__c QpOption : listProduct){
                        String ServiceCategory='';
                        ServiceCategory=QpOption.zqu__ProductRatePlanCharge__r.zqu__ProductRatePlan__r.zqu__Product__r.Service_Category__c;
                        if(ServiceCategory!='') ServiceCategoryCondition.add(ServiceCategory);
                    }
                    
                    if(ParentAccount.size()>0){
                        try{
                            //////
                            /*
                            Set<String> idSet = new Set<String>();
                            
                            List<ProductCustomMetadata__mdt> productList = [Select Id,External_Id__c from ProductCustomMetadata__mdt Where Hide_Add_On__c = true];
                            system.debug('productList size : ' + productList.size());
                            
                            if(productList.size()>0){
                                for(ProductCustomMetadata__mdt prod : productList){
                                    idSet.add(prod.External_Id__c);
                                }
                            }
                            */
                            ///////////
                            for(Zuora__SubscriptionRatePlan__c sub : [SELECT id, Zuora__ProductRatePlanId__c FROM Zuora__SubscriptionRatePlan__c WHERE
                                                                      Zuora__Subscription__c=: QuoteList[0].Customer_Subscription_ID__c AND
                                                                      Zuora__ProductRatePlanId__c!='']){
                                                                          RatePlanSubscription.add(sub.Zuora__ProductRatePlanId__c);
                                                                      }
                            
                            if(RatePlanSubscription.size()>0){
                                
                                
                                
                                for(zqu__ProductRatePlan__c ProdRatePlan : [SELECT id, zqu__Product__c, zqu__Product__r.Service_Category__c FROM zqu__ProductRatePlan__c 
                                                                            WHERE zqu__ZuoraId__c =:RatePlanSubscription AND zqu__Product__r.Status__c != 'Inactive' 
                                                                            AND zqu__Product__r.Family='Add-On' AND zqu__Product__r.Product_Type__c=: QuoteList[0].zqu__Account__r.Billing__c 
                                                                            AND zqu__Product__c!=null ]){
                                                                                if(ServiceCategoryCondition.size()>0 && ServiceCategoryCondition.contains(ProdRatePlan.zqu__Product__r.Service_Category__c))
                                                                                {
                                                                                    List<Id> RatePlansList = new List<Id>();
                                                                                    if(Product_RatePlans.containsKey(ProdRatePlan.zqu__Product__c)){
                                                                                        RatePlansList.addAll(Product_RatePlans.get(ProdRatePlan.zqu__Product__c));	
                                                                                    }
                                                                                    
                                                                                    RatePlansList.add(ProdRatePlan.id);
                                                                                    Product_RatePlans.put(ProdRatePlan.zqu__Product__c,RatePlansList);
                                                                                }
                                                                                
                                                                            }
                                
                                system.debug('Product_RatePlans: '+Product_RatePlans);
                                
                                if(!Product_RatePlans.IsEmpty()) {
                                    
                                    for(id ProdId : Product_RatePlans.keySet()){
                                        record = new zqu.QuickListController.PluginRecord();
                                        record.recordId = ProdId;
                                        record.relatedObjectIds = new Map<String, List<ID> >{'zqu__productrateplan__c' => Product_RatePlans.get(ProdId)};
                                            result.records.add(record); 
                                    }
                                }
                            }
                            
                        }catch(exception e){
                            System.debug('Error in Allowed Option extraction: '+ e.getMessage()+' line: '+e.getLineNumber());
                        }
                    }
                } 
            }
            system.debug('result: '+result);
            return result; 
        }
    
    public class ProductOptions {
        public Boolean FreeNight {get; set;}
        public Boolean DB50 {get; set;}
        public Boolean DB200 {get; set;}
        
        //This is the contructor method. When we create a new wrapAccount object we pass a Account that is set to the acc property. We also set the selected value to false
        public ProductOptions(Boolean FreeNight, Boolean DB50, Boolean DB200) {
            this.FreeNight = FreeNight;
            this.DB50 = DB50;
            this.DB200 = DB200;
        }
    }
    
}