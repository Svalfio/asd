public class CaseTriggerHandler {
    
    
    public void onBeforeInsert(List<Case> triggerNew){
        
        System.debug('CaseTrigger -------OnBeforeInsert------Start');
        QueueSobject agentQueue = [select Id, QueueId from QueueSobject WHERE SobjectType = 'Case' AND Queue.DeveloperName ='Case_Queue' limit 1];
		System.debug('Queue '+agentQueue);
        User u = [Select id,Profile.Name from User where Profile.Name='CRC Agent' limit 1];
        
        String partner = UserInfo.getUserId();
        Contact partnerContact =[Select id,AccountId from Contact where OwnerId=:partner];
        
        for(Case c : triggerNew){
            
            //c.OwnerId=u.id; 
            system.debug('Case Owner: '+c.OwnerId);
            if(String.isBlank(c.Account__c) && c.Account__c == null ){
                c.Account__c = partnerContact.AccountId;
			}
        }
    }

    public  void OnAfterInsert(List<Case> triggerNew) {
        
       System.debug('CaseTrigger ------OnAfterInsert------Start---- '); 
        
        List<FeedItem> FeedItemList = New List<FeedItem>();
        QueueSobject agentQueue = [select Id, QueueId from QueueSobject WHERE SobjectType = 'Case' AND Queue.DeveloperName ='Case_Queue' limit 1];
		System.debug('Queue '+agentQueue);
        User u = [Select id,Profile.Name from User where Profile.Name='CRC Agent' limit 1];
        
        for(Case c : triggerNew){
            
            //c.OwnerId=u.id; 
            
            system.debug('Case Owner: '+c.OwnerId);
            
            if(c.status=='New'){
                FeedItem post = new FeedItem();
                
                post.ParentId = c.OwnerId;
                post.Body ='Hi CRC Agent a new case has been assigned to you';
                post.Title = 'New Case has Been Created';
                post.LinkUrl = '/lightning/r/Case/' + c.ID + '/view';
                FeedItemList.add(post);
            }  
            
            if(FeedItemList.size()>0){
                insert FeedItemList;
            }
        }
      
    }
    
    public  void OnBeforeUpdate(Map<Id, Case> triggerNew, Map<Id,Case> triggerOld){
        List<Case> caseUpdate = new List<Case>();
        System.debug('trigger');
        for(Case c : triggerNew.values()){
            System.debug(c.Id);
            if(triggerNew.get(c.Id).Status == 'Assigned' && triggerOld.get(c.Id).Status == 'New'){
                System.debug(c.Id);
                System.debug(c.Status_Changed__c);
                c.Status_Changed__c = true;
                System.debug(c.Status_Changed__c);
                caseUpdate.add(c);
            }
        }
        update caseUpdate;

    }
}