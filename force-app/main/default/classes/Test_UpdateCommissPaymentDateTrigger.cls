@isTest
public class Test_UpdateCommissPaymentDateTrigger {
	
    @testSetup
    static void createData(){
        
        List<Monthly_Statement__c> monthlyStateList = new List<Monthly_Statement__c>();
        Monthly_Statement__c monthlyState = new Monthly_Statement__c(Payment_Status__c='Unpaid');
        Monthly_Statement__c monthlyState2 = new Monthly_Statement__c();
        Monthly_Statement__c monthlyState3 = new Monthly_Statement__c(Payment_Status__c='Paid');
        Monthly_Statement__c monthlyState4 = new Monthly_Statement__c(Payment_Status__c='Unpaid');
        
        monthlyStateList.add(monthlyState);
        monthlyStateList.add(monthlyState2);
        monthlyStateList.add(monthlyState3);
        monthlyStateList.add(monthlyState4);
        
        insert monthlyStateList;
        
    }
    
    
    @isTest
    static void onBeforeUpdateTest(){
        
        List<Monthly_Statement__c> updatedMonthlyStateList = new List<Monthly_Statement__c>();
        List<Monthly_Statement__c> monthlyStateList = [SELECT id,Payment_Status__c,Commission_Payment_Date__c FROM Monthly_Statement__c];
        for(Monthly_Statement__c monthlyState :monthlyStateList){
            	monthlyState.Payment_Status__c='Paid';
                updatedMonthlyStateList.add(monthlyState);
        }
        update updatedMonthlyStateList; 
    }
    
    
}