global class InvocableGeneratePayment {
 @InvocableMethod(Label = 'InvocagleGeneratePayment')
   public static void invokeapexcallout(list<Zuora__ZInvoice__c> invoices) {
     List<Zuora__CustomerAccount__c> listAcc = [SELECT Zuora__Zuora_Id__c FROM Zuora__CustomerAccount__c WHERE Id =: invoices[0].Zuora__BillingAccount__c];
     futureGeneratePayment.apexcallout(
         invoices[0].Zuora__Zuora_Id__c,
         invoices[0].Zuora__Balance2__c,
         invoices[0].CurrencyIsoCode,
         invoices[0].Zuora__InvoiceDate__c,
         listAcc[0].Zuora__Zuora_Id__c,
     	 invoices[0].Id);
   }
}