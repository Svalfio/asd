public class LC_PrefilledInvoiceController {
    

    public class AccountWrapper{
        public String billaddress {get;set;}
        public String strDate {get;set;}
        public String numFat {get;set;}
        public String monthdate {get;set;}
        public String curraccount {get;set;}
        
        Public AccountWrapper(String billaddress,String strDate,String numFat, String monthdate,String curraccount) {
            this.billaddress = billaddress;
            this.strDate = strDate;
            this.numFat = numFat;
            this.monthdate = monthdate;
            this.curraccount = curraccount;
        }
    }
    
    public class RecordWrapper{
        public String product {get;set;}
        public String quantityfirst {get;set;}
        public String percfirst {get;set;}
        public String planfirst {get;set;}
        public String quantityren {get;set;}
        public String percren {get;set;}
        public String planren {get;set;}
        public Double count {get;set;}
        public Double vat {get;set;}
        
        Public RecordWrapper(String product,String quantityfirst,String percfirst, String planfirst,String quantityren,String percren,String planren,Double count,Double vat) {
            this.product = product;
            this.quantityfirst = quantityfirst;
            this.percfirst = percfirst;
            this.planfirst = planfirst;
            this.quantityren = quantityren;
            this.percren = percren;
            this.planren = planren;
            this.count = count;
            this.vat = vat;
        }
    }
    
    @AuraEnabled
    public static AccountWrapper getAccwr(String resellerId,String month, String year) {
        
        system.debug('start AccountWrapper');
        system.debug('resellerId :' + resellerId);
        
        Account acc = [select ID, Name,BillingAddress,CurrencyIsoCode from Account WHERE Id = :resellerId LIMIT 1];
        String curraccount = acc.CurrencyIsoCode;
        String billAddr = '';
        if(acc.BillingAddress != null){
            if(acc.BillingAddress.getCity() != null)
                billAddr += acc.BillingAddress.getCity()+' ';
            
            if(acc.BillingAddress.getCountry() != null)
                billAddr += acc.BillingAddress.getCountry()+' ';
            
            if(acc.BillingAddress.getCountryCode() != null)
                billAddr += acc.BillingAddress.getCountryCode()+' ';
           
            if(acc.BillingAddress.getGeocodeAccuracy() != null)
                billAddr += acc.BillingAddress.getGeocodeAccuracy()+' ';
            
            if(acc.BillingAddress.getPostalCode() != null)
                billAddr += acc.BillingAddress.getPostalCode()+' ';
            
            if(acc.BillingAddress.getState() != null)
                billAddr += acc.BillingAddress.getState()+' ';
            
            if(acc.BillingAddress.getStateCode() != null)
                billAddr += acc.BillingAddress.getStateCode()+' ';
            
            if(acc.BillingAddress.getStreet() != null)
                billAddr += acc.BillingAddress.getStreet();
        }

        Integer daysInMonth = Date.daysInMonth(Integer.valueOf(year), Integer.valueOf(month));
        String strDate = daysInMonth+'/'+month+'/'+year.substring(2,4);
		String numFat = 'KONNECT/'+year.substring(2,4)+'/'+month;
        String strMonth = '';
        
        switch on Integer.valueOf(month) {
            when 1  {strMonth = 'Jan';}
            when 2  {strMonth = 'Feb';}
            when 3  {strMonth = 'Mar';}
            when 4  {strMonth = 'Apr';}
            when 5  {strMonth = 'May';}
            when 6  {strMonth = 'Jun';}
            when 7  {strMonth = 'Jul';}
            when 8  {strMonth = 'Aug';}
            when 9  {strMonth = 'Sep';}
            when 10 {strMonth = 'Oct';}
            when 11 {strMonth = 'Nov';}
            when 12 {strMonth = 'Dec';}
        }
        
        String monthdate = strMonth +' '+year;
        AccountWrapper Accwr = new AccountWrapper(acc.Name+', '+billAddr,strDate,numFat,monthdate,curraccount);
        return Accwr;
    }
    
    @AuraEnabled
    public static List<recordWrapper> getListRecwr(String resellerId, String country,String month,String year) {
		
        system.debug('start getListRecwr');
        system.debug('resellerId :' + resellerId);
        system.debug('country: ' + country);
        
        List<Commission_Item__c> lstCommItems = [SELECT Id,Commission_Amount__c,Commission_Percentage__c,Transaction_Type__c 
                                                              FROM Commission_Item__c 
                                                              WHERE Commission_Model__r.Sub_Reseller_Id__c = null AND 
                                                 			  Commission_Model__r.Reseller_Id__c = :resellerId AND 
                                                 			  Commission_Model__r.Country__c = :country];
        
        system.debug('*** lstCommItems ***'+lstCommItems);
        
        Map<String,Double> mapCommPerc = new Map<String,Double>();//Mappa type,perc/amount
        
        for(Commission_Item__c tmpComm : lstCommItems){
            if(tmpComm.Commission_Percentage__c != null)
            	mapCommPerc.put(tmpComm.Transaction_Type__c,tmpComm.Commission_Percentage__c);
            else
        		mapCommPerc.put(tmpComm.Transaction_Type__c,tmpComm.Commission_Amount__c);
        }
               
        Integer daysInMonth = Date.daysInMonth(Integer.valueOf(year), Integer.valueOf(month));
		date startDate = Date.newInstance(Integer.valueOf(year), Integer.valueOf(month), 1);
        date endDatetmp = Date.newInstance(Integer.valueOf(year), Integer.valueOf(month),daysInMonth);
        date endDate = endDatetmp.addDays(1);
		system.debug('***start date***: '+ startDate);
        system.debug('***end date***: '+ endDate);
        
        Id userId = UserInfo.getUserId();
        
        List<Order> listOrder = [SELECT Id,Zuora_Quote__c 
                                 FROM Order 
                                 WHERE ownerId =: userId AND
                                 //Subscription_Id__c != null AND
                                 createddate >= :startDate AND
                                 createddate < :endDate];
        
        system.debug('*** listOrder ***'+listOrder);
        Set<String> quoteIdSet = new Set<String>();
        
        for(Order tmpOrder : listOrder){
            quoteIdSet.add(tmpOrder.Zuora_Quote__c);
        }
        
        system.debug('*** quoteIdSet ***' + quoteIdSet);
        
        List<zqu__QuoteRatePlanCharge__c> qtRatePlanList = [SELECT Id,zqu__ProductName__c,zqu__total__c,zqu__Quantity__c,zqu__QuoteRatePlan__r.zqu__Quote__r.zqu__subscriptiontype__c,
                                                            zqu__ProductRatePlanCharge__r.zqu__ProductRatePlan__r.zqu__Product__r.family,
                                                            zqu__ProductRatePlanCharge__r.zqu__ProductRatePlan__r.zqu__Product__r.Id,
                                                            zqu__ProductRatePlanCharge__r.VAT_Rate__c
                                                            FROM zqu__QuoteRatePlanCharge__c 
                                                            WHERE zqu__QuoteRatePlan__r.zqu__Quote__r.Id IN :quoteIdSet
                                                            AND zqu__QuoteRatePlan__r.zqu__Quote__r.zqu__Account__r.BillingCountry = :country
                                                            AND zqu__QuoteRatePlan__r.zqu__Quote__r.zqu__ZuoraSubscriptionID__c != null
                                                           ];
        
        system.debug('*** qtRatePlanList ***'+qtRatePlanList);
        
        Map<String,Double> mapFirstActQty = new Map<String,Double>();//Mappa nome, quantità totale 1st activation
        Map<String,Double> mapRenQty = new Map<String,Double>();//Mappa nome, quantità totale renewal
        Map<String,Double> mapEqQty = new Map<String,Double>();//Mappa nome, quantità totale equipment
        Map<String,List<String>> mapProcCommItemType = new Map<String,List<String>>();//Mappa nome, commission Item Type
        Map<String,Double> mapProdPriceNoTax = new Map<String,Double>();//Mappa nome, prodotto senza iva
        Set<String> nameProd = new Set<String>();
        Double vatRate = 0;
        for(zqu__QuoteRatePlanCharge__c qrtPlanTmp : qtRatePlanList){
            system.debug('*** START ciclo for ***');
            
            system.debug('*** VAT rate ***'+qrtPlanTmp.zqu__ProductRatePlanCharge__r.VAT_Rate__c);
            
            if(qrtPlanTmp.zqu__ProductRatePlanCharge__r.VAT_Rate__c != null)
            	vatRate = qrtPlanTmp.zqu__ProductRatePlanCharge__r.VAT_Rate__c;
            
            Double netAmount = qrtPlanTmp.zqu__total__c  - ( qrtPlanTmp.zqu__total__c * vatRate / 100); 
            mapProdPriceNoTax.put(qrtPlanTmp.zqu__ProductName__c,netAmount);
            
            system.debug('*** family ***' + qrtPlanTmp.zqu__ProductRatePlanCharge__r.zqu__ProductRatePlan__r.zqu__Product__r.family);
            system.debug('*** subscription type ***'+qrtPlanTmp.zqu__QuoteRatePlan__r.zqu__Quote__r.zqu__subscriptiontype__c);
            
            if(qrtPlanTmp.zqu__ProductRatePlanCharge__r.zqu__ProductRatePlan__r.zqu__Product__r.family == 'Equipment'){
                nameProd.add('Terminals');
            }
            else
                nameProd.add(qrtPlanTmp.zqu__ProductName__c);
            
            if(qrtPlanTmp.zqu__ProductRatePlanCharge__r.zqu__ProductRatePlan__r.zqu__Product__r.family == 'Equipment'){
                if(mapEqQty.containsKey('Terminals')){
                    double qnt = mapEqQty.get('Terminals');
                    qnt += qrtPlanTmp.zqu__Quantity__c;
                    mapEqQty.put('Terminals',qnt);                                    
                }
                else
                    mapEqQty.put('Terminals',qrtPlanTmp.zqu__Quantity__c);
            } 
            
            
            else if(qrtPlanTmp.zqu__QuoteRatePlan__r.zqu__Quote__r.zqu__subscriptiontype__c == 'New Subscription'){
                system.debug('*** Start If New Subscription ***');
                if(mapFirstActQty.containsKey(qrtPlanTmp.zqu__ProductName__c)){
                    double qnt = mapFirstActQty.get(qrtPlanTmp.zqu__ProductName__c);
                    qnt += qrtPlanTmp.zqu__Quantity__c;
                    mapFirstActQty.put(qrtPlanTmp.zqu__ProductName__c,qnt);
                                                    
                }
                else
                    mapFirstActQty.put(qrtPlanTmp.zqu__ProductName__c,qrtPlanTmp.zqu__Quantity__c);   
                
                system.debug('*** mapFirstActQty ***'+ mapFirstActQty);
            }
            else if(qrtPlanTmp.zqu__QuoteRatePlan__r.zqu__Quote__r.zqu__subscriptiontype__c == 'Renew Subscription'){
                if(mapRenQty.containsKey(qrtPlanTmp.zqu__ProductName__c)){
                    double qnt = mapRenQty.get(qrtPlanTmp.zqu__ProductName__c);
                    qnt += qrtPlanTmp.zqu__Quantity__c;
                    mapRenQty.put(qrtPlanTmp.zqu__ProductName__c,qnt);
                                                    
                }
                else
                    mapRenQty.put(qrtPlanTmp.zqu__ProductName__c,qrtPlanTmp.zqu__Quantity__c); 
            }

            if(qrtPlanTmp.zqu__ProductRatePlanCharge__r.zqu__ProductRatePlan__r.zqu__Product__r.family == 'Equipment'){
                List<String> newList = new List<String>();
                newList.add('FIRST_EQUIPMENT_ACTIVATION_FEE');
            	mapProcCommItemType.put('Terminals',newList);
            }
            else{
                
                String comType1st = '';
                String comTypeRen = '';
                
                if(qrtPlanTmp.zqu__ProductRatePlanCharge__r.zqu__ProductRatePlan__r.zqu__Product__r.family == 'Option'){
                    comType1st = 'FIRST_OPTION_FEE';
                    comTypeRen = 'OPTION_RENEWAL_FEE';
                }
                else if(qrtPlanTmp.zqu__ProductRatePlanCharge__r.zqu__ProductRatePlan__r.zqu__Product__r.family == 'Plan'){
                    comType1st = 'FIRST_PLAN_FEE';
                    comTypeRen = 'PLAN_RENEWAL_FEE';
                }
                    
                if(mapProcCommItemType.containsKey(qrtPlanTmp.zqu__ProductName__c)){
                    List<String> newList = mapProcCommItemType.get(qrtPlanTmp.zqu__ProductName__c);
                    newList.add(comType1st);
                    newList.add(comTypeRen);
                    mapProcCommItemType.put(qrtPlanTmp.zqu__ProductName__c,newList);
                }
                else{
                   	List<String> newList = new List<String>();
                    newList.add(comType1st);
                    newList.add(comTypeRen);
                    mapProcCommItemType.put(qrtPlanTmp.zqu__ProductName__c,newList);
                }
            }
            
        }
        
        system.debug('***mapProcCommItemType***' + mapProcCommItemType);
        
        List<recordWrapper> ListRecwr = new List<recordWrapper>();
        
        for(String prodName : nameProd){
        	String prodTable = prodName;
            Double qntTable = 0;
            Double qntTableRen = 0;
            Double qntTableEq = 0;
            Double planTable = 0;
            Double planRenTable = 0;
            Double percTable = 0;
            Double percRenTable = 0;
            Double percTermTable = 0;
            Double total = 0;
            
            //1st activation
            if(mapFirstActQty.containsKey(prodName))
           		qntTable = mapFirstActQty.get(prodName);
            //renewal
            if(mapRenQty.containsKey(prodName))
           		qntTableRen = mapRenQty.get(prodName);
            //equipment
            if(mapEqQty.containsKey(prodName)){
               	qntTableEq = mapEqQty.get(prodName);
            }
           	
            if(mapProdPriceNoTax.containsKey(prodName)){
               	planTable = mapProdPriceNoTax.get(prodName);
                planRenTable = mapProdPriceNoTax.get(prodName);
            }

            if(mapProcCommItemType.containsKey(prodName)){
                
                List<String> prodTypeTable = mapProcCommItemType.get(prodName);
                for(String tmpListType : prodTypeTable){
                    if(mapCommPerc.containsKey(tmpListType)){
                        if(tmpListType.contains('FIRST'))
                        	percTable = mapCommPerc.get(tmpListType);
                        else if(tmpListType.contains('RENEWAL'))
                            percRenTable = mapCommPerc.get(tmpListType);  
                        else
                            percTermTable = mapCommPerc.get(tmpListType);  
                           
                    }
                        
                }
                
            }
            
            total = (qntTable * planTable) * percTable / 100 + (qntTableRen * planRenTable) * percRenTable / 100;
            
            recordWrapper Recwr;
            system.debug('*** vat rate ***'+vatRate );
            
            if(prodName == 'Terminals')
            	Recwr = new recordWrapper(prodName,String.valueOf(qntTableEq),'',String.valueOf(percTermTable),'N/A','N/A','N/A',percTermTable,vatRate);
            else    
            	Recwr = new recordWrapper(prodName,String.valueOf(qntTable),String.valueOf(percTable)+'%',String.valueOf(planTable),String.valueOf(qntTableRen),String.valueOf(percRenTable)+'%',String.valueOf(planRenTable),total,vatRate);
			
            ListRecwr.add(Recwr);
        }
        
        
        system.debug('ListRecwr'+ListRecwr);
        return ListRecwr;
            
    }
    
}