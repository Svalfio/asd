global class Invocable_ChangeTechnicalAccount {
    global with sharing class ChangeTechnicalAccountInputs {
        @InvocableVariable(Label = 'techAccountId' Required = false) global Integer techAccountId;
        @InvocableVariable(Label = 'serviceId' Required = false) global Integer serviceId;
        @InvocableVariable(Label = 'planId' Required = false) global Integer planId;
        @InvocableVariable(Label = 'serviceActivationDate' Required = false) global Date serviceActivationDate;
        @InvocableVariable(Label = 'resetCycleDates' Required = false) global Boolean resetCycleDates;
        @InvocableVariable(Label = 'numberOfCycles' Required = false) global Integer numberOfCycles;
        @InvocableVariable(Label = 'quoteId' Required = false) global String quoteId;
        @InvocableVariable(Label = 'optionItems' Required = false) global List<Integer> optionItems = new List<Integer>();
        @InvocableVariable(Label = 'Esito' Required = false) global List<String> Esito = new List<String>();
    }
    
    @InvocableMethod(Label = 'API_OSS_ChangeTechnicalAccount')
	global static List<String> makePostCallout(ChangeTechnicalAccountInputs[] requestActionInList) 
    {
        SfToHerokuAPIManager.TechnicalAccountChangeItemRequest technicalAccountRequest = new SfToHerokuAPIManager.TechnicalAccountChangeItemRequest();
       	technicalAccountRequest.techAccountId = requestActionInList[0].techAccountId;
       	technicalAccountRequest.planId = requestActionInList[0].planId;
        technicalAccountRequest.subnetIpOrIp = 'na';
        technicalAccountRequest.resetCycle = requestActionInList[0].resetCycleDates;
        technicalAccountRequest.resetCounters = false;
        technicalAccountRequest.components = requestActionInList[0].optionItems;
        
        if(requestActionInList[0].resetCycleDates == true){
            List<SfToHerokuAPIManager.CycleDates> cycleDates = new List<SfToHerokuAPIManager.CycleDates>();
			Integer j = 1;
            for(Integer i=0;i<requestActionInList[0].numberOfCycles;i++){
                SfToHerokuAPIManager.CycleDates cycleDate = new SfToHerokuAPIManager.CycleDates();
                cycleDate.dateFrom = requestActionInList[0].serviceActivationDate.addMonths(i);
                cycleDate.dateTo = requestActionInList[0].serviceActivationDate.addMonths(j);
                cycleDates.add(cycleDate);
                j++;
            }
            technicalAccountRequest.cycleDates = cycleDates;
        }
        
        SfToHerokuAPIManager.HerokuResponse responseChangeTechAccount = SfToHerokuAPIManager.technicalAccountChangeItem(technicalAccountRequest);
        
        if(responseChangeTechAccount != null){
            Integer errCode = responseChangeTechAccount.errorCode;
        	String descEsito = responseChangeTechAccount.message;
          	String bodyRequest = JSON.serialize(technicalAccountRequest);
            String bodyResponse = JSON.serialize(responseChangeTechAccount);
        
            if(errCode == 200 || errCode == 201){
				requestActionInList[0].Esito.add('OK');
            }else{
                ApexLog__c writeErrorLog = new ApexLog__c();
                writeErrorLog.ClassName__c = 'Invocable_ChangeTechnicalAccount';
                writeErrorLog.MethodName__c = 'API_OSS_ChangeTechnicalAccount';
                writeErrorLog.ProcessName__c = 'TechnicalAccountChangeItem';
                writeErrorLog.Body__c = bodyRequest;
                writeErrorLog.Response__c = bodyResponse;
                writeErrorLog.ProcessId__c = requestActionInList[0].quoteId;
                insert writeErrorLog;
                requestActionInList[0].Esito.add('KO');
            }
        }
        
        if(responseChangeTechAccount != null){
            Integer errCode = responseChangeTechAccount.errorCode;
        	String descEsito = responseChangeTechAccount.message;
        }
    
  		requestActionInList[0].Esito.add('KO');
    	return requestActionInList[0].Esito;  
	}
}