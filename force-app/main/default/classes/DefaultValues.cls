global class DefaultValues extends zqu.CreateQuoteController.PopulateDefaultFieldValuePlugin{ 
	global override void populateDefaultFieldValue(SObject record, zqu.PropertyComponentController.ParentController pcc){
        
        try{
            super.populateDefaultFieldValue(record, pcc);
            if(record != null){
                Id accountId = (Id) record.get('zqu__Account__c'); 
                String WalletMode = '';
                String CustomerSubId = '';
                List<Contact>contacts = [SELECT Id, Name FROM Contact WHERE Account.Id = :accountId];  
                List<Account>accLive = [SELECT  Customer_Subscription_ID__c, Installation_Latitude__c,
                                                Preferred_Payment_Method__c,Installation_Longitude__c,
                                                Billing__c,Installation_County__c,Installation_Street__c,
                                                BillingEntity__r.Name,Installation_City__c,
                                                Parent.Id
                                                FROM Account WHERE Account.Id = :accountId];
                
                CustomerSubId = accLive[0].Customer_Subscription_ID__c;
                
                if(accLive[0].Billing__c == 'Wholesale'){
                    if(CustomerSubId != null){
                        List<Zuora__Subscription__c> subPartner = [
                            SELECT Wallet_Mode__c	
                            FROM Zuora__Subscription__c 
                            WHERE Id =: CustomerSubId];
                    
                        if(subPartner != null)
                            WalletMode = subPartner[0].Wallet_Mode__c;
                        else
                            WalletMode = 'PrePaid';
                    }else{
                        WalletMode = 'PrePaid';
                    }
                }   

                
                // Update Bill to Contact Information
                if  (contacts.size() > 0) {   
                    record.put('zqu__BillToContact__c', contacts[0].Id);
                    record.put('zqu__SoldToContact__c', contacts[0].Id);
                    
                    // Beforeretrieving  the lookup  options, needs to populate the map first        
                    super.setLookupOptions(pcc);        
                    
                    // Now retrieve the lookup component options 
                    zqu.LookupComponentOptions billToOptions = super.getLookupOption('zqu__BillToContact__c');  
                    billToOptions.targetId = contacts[0].Id;
                    billToOptions.targetName = contacts[0].Name;
                    zqu.LookupComponentOptions soldToOptions  = super.getLookupOption('zqu__SoldToContact__c');   
                    soldToOptions.targetId = contacts[0].Id;
                    soldToOptions.targetName = contacts[0].Name;
                }
                
				//Custom Default Values

				//QUOTE NAME (ALL)
                QuoteAutoNumber__c mc=[SELECT id, Name,Quote_Code__C FROM QuoteAutoNumber__c LIMIT 1];
                String code = mc.Quote_Code__C;
                DateTime d = Date.Today() ;
                String dateStr =  d.format('dd/MM/yyyy') ;
                record.put('Name', code);

                //INVOICE TEMPLATE (ALL)
                if(accLive[0].BillingEntity__r.Name == 'KA-SAT'){
                    record.put('Billing_Entity__c','KA-SAT');
                    record.put('Invoice_Template__c','KA-SAT');
                }
                else{
                    if(accLive[0].Billing__c == 'Retail'){
                    	record.put('Billing_Entity__c','KONNECT');
                    	record.put('Invoice_Template__c','KONNECT');
                    }
                    else{
                        //record.put('Billing_Entity__c','KONNECT');
                    	//record.put('Invoice_Template__c','KONNECT');
                        record.put('Billing_Entity__c','EUTELSAT');
                    	record.put('Invoice_Template__c','EUTELSAT');
                    }    
                }
                
                //QUOTE START and END DATE (ALL)
                record.put('zqu__StartDate__c', Date.today());
                record.put('zqu__ValidUntil__c', Date.today().addMonths(1));
                
                //QUOTE VALUES FOR BUSINESS MODEL
                if (accLive[0].Billing__c == 'Retail' || (accLive[0].Billing__c == 'Wholesale' && WalletMode == 'PrePaid')){
                    
                    if(accLive[0].Billing__c == 'Wholesale'){
                    	record.put('Customer_Subscription_ID__c', accLive[0].Customer_Subscription_ID__c);
                    	record.put('Subscription__c',(Id)accLive[0].Customer_Subscription_ID__c);
                    }
                    
                    record.put('zqu__InitialTerm__c',1);
                    record.put('zqu__Subscription_Term_Type__c','Termed');
                    record.put('zqu__AutoRenew__c',false);
                    record.put('zqu__GenerateInvoice__c',true);
                    
                    if(record.get('zqu__SubscriptionType__c') == 'New Subscription'){
                        record.put('zqu__RenewalTerm__c',0); 
                    }
                    
                    if(record.get('zqu__SubscriptionType__c') == 'Amend Subscription'){
                        record.put('zqu__RenewalTerm__c',0);
  						record.put('zqu__Service_Activation_Date__c',Date.today());	
                    }

                    if(record.get('zqu__SubscriptionType__c') == 'Renew Subscription'){
                        record.put('zqu__RenewalTerm__c',1);
                        String existingSubscriptionId = (String) record.get('zqu__ExistSubscriptionID__c');
                        List <Zuora__Subscription__c> existingSubscription = [SELECT End_Date__c FROM Zuora__Subscription__c WHERE Zuora__External_Id__c =: existingSubscriptionId];
                        record.put('zqu__Service_Activation_Date__c',existingSubscription[0].End_Date__c);
                    } 
                }
                else if (accLive[0].Billing__c == 'Wholesale' && WalletMode == 'PostPaid'){
                    record.put('zqu__Subscription_Term_Type__c','Evergreen');
                    record.put('Customer_Subscription_ID__c', accLive[0].Customer_Subscription_ID__c);
                    record.put('Subscription__c',(Id)accLive[0].Customer_Subscription_ID__c);
                    
                    //record.put('zqu__SubscriptionType__c','New Subscription Wholesale');
                    //record.put('Installation_City__c', accLive[0].Installation_City__c);
                    //record.put('Installation_Country__c',accLive[0].Installation_County__c);
                    //record.put('Installation_Latitude__c', accLive[0].Installation_Latitude__c);
                    //record.put('Installation_Longitude__c',accLive[0].Installation_Longitude__c);
                    //record.put('Installation_Street__c',accLive[0].Installation_Street__c);
                    //record.put('zqu__Service_Activation_Date__c',Date.today().addMonths(1)); //MM
                    //record.put('zqu__Service_Activation_Date__c',Date.today());
                }
                
                //OTHER SHARED DEFAULT VALUES
                record.put('zqu__Amendment_Name__c', 'Order Change - '+contacts[0].Name+' '+dateStr);
                record.put('zqu__PaymentTerm__c', 'Due Upon Receipt');
                record.put('zqu__BillingMethod__c', 'Email');
                record.put('Volume_Consumption__c', 'Cycle Start Date is reset');
                record.put('Number_of_Cycles__c', '1');
                record.put('zqu__PaymentMethod__c','Cash');
                record.put('Installation_City__c', accLive[0].Installation_City__c);
                record.put('Installation_Country__c',accLive[0].Installation_County__c);
                record.put('Installation_Latitude__c', accLive[0].Installation_Latitude__c);
                record.put('Installation_Longitude__c',accLive[0].Installation_Longitude__c);
                record.put('Installation_Street__c',accLive[0].Installation_Street__c);
                
                //CURRENCY
                List<Account> parentAccount = [SELECT CurrencyIsoCode FROM Account WHERE Id =:accLive[0].Parent.Id];
                if(parentAccount.size() > 0)
                    record.put('zqu__Currency__c', parentAccount[0].CurrencyIsoCode);
                else
                    record.put('zqu__Currency__c', 'USD');
                
                //OLD values
                //record.PermissionsEdit=false;
                //record.put('zqu__InitialTerm__c', 1);
                //record.put('zqu__RenewalTerm__c', 12);
                //record.put('zqu__PaymentMethod__c', accLive[0].Preferred_Payment_Method__c);
                //record.put('Name', 'Quote - ' + contacts[0].Name + ' - ' + dateStr);
                //record.put('Name', 'Quote - ' + contacts[0].Name + ' - ' + dateStr);
                //record.put('zqu__ValidUntil__c', Date.today().addDays(30));

            }
        }catch(exception e){
        	System.debug('Error in DefaultValues Class: '+e.getMessage());
        }
        
    }
}