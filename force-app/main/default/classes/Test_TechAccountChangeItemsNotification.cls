@isTest
public class Test_TechAccountChangeItemsNotification {
    
    @testSetup 
    static void setup() {

        TechnicalAccountChangeItemsCustomSetting__c testCustomSetting = new TechnicalAccountChangeItemsCustomSetting__C(Name='HerokuUrl',Endpoint__c='urlFalse');
        insert testCustomSetting;
        
    }
    
    @isTest
    static void notificationTest(){
		List<TechnicalAccountChangeItemsNotification.ClassInputs> InputSetList = new List<TechnicalAccountChangeItemsNotification.ClassInputs>();
        TechnicalAccountChangeItemsNotification.ClassInputs InputSet = new TechnicalAccountChangeItemsNotification.ClassInputs();
        
        InputSet.techAccountId=1;
        InputSet.planId=2;
       // InputSet.cycleDatesList to define 
        InputSet.components= new List<Integer>();
        InputSet.components.add(3);
        InputSet.components.add(4);
        InputSet.IP='216.3.128.12';
        InputSet.resetCycleDates=True;
        InputSet.resetCounters=False;

        InputSetList.add(InputSet);
        
        
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('Test_CampaignMember');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
 
        Test.startTest();
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock); 

        TechnicalAccountChangeItemsNotification.makePostCallout(InputSetList);
        Test.stopTest();
        
    }
    

}