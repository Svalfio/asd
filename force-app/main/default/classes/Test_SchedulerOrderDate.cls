@isTest
public class Test_SchedulerOrderDate {
    
    @TestSetup
    public static void setup(){
        OrderPendingMaxDays__c setting = new OrderPendingMaxDays__c();
        setting.Name = 'MaxDays';
        setting.MaxDays__c = 0;
        insert setting;
        
        
        Account account = new Account(Name = 'Test Account');
        insert account;
        Order order = new Order (Status = 'Pending', AccountId = account.id, EffectiveDate = Date.today());
        insert order;
    }
    
    @isTest
    public static void TestExecute(){
        Test.startTest();
        SchedulerOrderDate sh1 = new SchedulerOrderDate();
        String sch = '0 0 2 * * ?'; 
        system.schedule('Test Scheduling', sch, sh1); 
        Test.stopTest();
    }

}