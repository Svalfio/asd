global class BatchUpdateStatusCampaign implements  Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts   {
    
    //global List<Campaign> books2= new List<Campaign>();
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('Select Id,Name,StartDate,EndDate,Status from Campaign WHERE Status != \'Aborted\' AND Status != \'Completed\' ' );
    }
    
    
    global void execute(Database.BatchableContext bc, List<sObject> scope){
        Campaign[] campaigns = (Campaign[]) scope;
        System.debug('execute-----BatchUpdateStatusCampaign--------');
        if(campaigns!=null && !campaigns.isEmpty()){
            for(Campaign campaign1: campaigns){
                system.debug(campaign1.Id);
                
                if(campaign1.Status != 'Aborted'){   
                    if(Date.today()>=campaign1.StartDate && Date.today()<=campaign1.EndDate){
                        campaign1.Status='In Progress'; 
                    } else if(Date.today()>campaign1.EndDate) {
                        campaign1.Status='Completed';
                    }
                    
                }
            }
            
            //books2=campaigns;
            
            update campaigns;
        }
    }

global void finish(Database.BatchableContext bc){
    
    System.debug(('Codice eseguito'));
    
}    



}