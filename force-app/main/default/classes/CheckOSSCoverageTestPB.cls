global class CheckOSSCoverageTestPB{
    global with sharing class CheckCoverageInputs {
        @InvocableVariable(Label = 'AccountId' Required = false) global String AccountId;
        @InvocableVariable(Label = 'BillingCity' Required = false) global String BillingCity;
        @InvocableVariable(Label = 'BillingCountry' Required = false) global String BillingCountry;
        @InvocableVariable(Label = 'BillingGeocodeAccuracy' Required = false) global String BillingGeocodeAccuracy;
        @InvocableVariable(Label = 'BillingLatitude' Required = false) global Decimal BillingLatitude;
        @InvocableVariable(Label = 'BillingLongitude' Required = false) global Decimal BillingLongitude;
        @InvocableVariable(Label = 'BillingPostalCode' Required = false) global String BillingPostalCode;
        @InvocableVariable(Label = 'BillingState' Required = false) global String BillingState;
        @InvocableVariable(Label = 'BillingStreet' Required = false) global String BillingStreet;
        @InvocableVariable(Label = 'Esito' Required = false) global List<String> Esito = new List<String>();
	}
    
    @InvocableMethod(Label = 'Check Account Coverage')
	global static List<String> makePostCallout(CheckCoverageInputs[] requestActionInList) 
    {
        if (requestActionInList[0].BillingCountry == 'Congo' || 
            requestActionInList[0].BillingCountry == 'Côte d\'Ivoire' || 
            requestActionInList[0].BillingCountry == 'Republic of the Congo' ||
            requestActionInList[0].BillingCountry == 'Democratic Republic of the Congo' ||
           	requestActionInList[0].BillingCountry == 'COG' ||
            requestActionInList[0].BillingCountry == 'COD' ||
            requestActionInList[0].BillingCountry == 'CIV'
           ){
            requestActionInList[0].Esito.add('OK');
        	return requestActionInList[0].Esito;
        }
        else{
        	requestActionInList[0].Esito.add('KO');
        	return requestActionInList[0].Esito; 
        }
	}
}