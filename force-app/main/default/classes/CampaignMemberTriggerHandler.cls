public class CampaignMemberTriggerHandler {
    
    public void onAfterInsert(List<CampaignMember> triggerNew){
        
        system.debug('CampaignMemberTriggerHandler---------onAfterInsert--------START');
        
        List<String> campaignIds = new List<String>();
        List<Campaign> campaigns = new List<Campaign>();
        List<Task> tasks = new List<Task>();
        List<FeedItem> feeds = new List<FeedItem>();
        List<Contact> contacts = new List<Contact>();
        List<User> usersByRole = new List<User>();
        
        
        UserRole role = [SELECT Id, Name FROM UserRole WHERE Name = 'CRC User' LIMIT 1];
        Calendar calendar = [SELECT Id, Name FROM Calendar WHERE Name = 'CRC Calendar' LIMIT 1];
        
        
        usersByRole = [SELECT Id, Name FROM User WHERE UserRoleId =: role.Id];
        
        
        
        Map<Id, List<CampaignMember>> campaignMemberToCampaignMap = new Map<Id, List<CampaignMember>>();
        
        for(CampaignMember member: triggerNew){
            campaignIds.add(member.CampaignId);
        }
        
        
        for(CampaignMember member : triggerNew){
            if(!campaignMemberToCampaignMap.containsKey(member.CampaignId))
                campaignMemberToCampaignMap.put(member.CampaignId,new List<CampaignMember>{member});
            else
                campaignMemberToCampaignMap.get(member.CampaignId).add(member);
        }
        
        
        
        campaigns = [SELECT Id, Name, Channel__c,Status,StartDate,EndDate FROM Campaign WHERE Id IN :campaignIds];
        
        
        Map<Id,Campaign> campaignStatus = new Map<Id,Campaign>();
        for(Campaign c : campaigns){
            if(!campaignStatus.containsKey(c.Id))
                campaignStatus.put(c.Id,c);
        }
        
        Map<Id,Set<Id>> campaignMemberMap = new Map<Id,Set<Id>>();
        for(CampaignMember member: triggerNew){
            if(String.valueOf(campaignStatus.get(member.CampaignId).status).equals('In Progress') && String.valueOf(campaignStatus.get(member.CampaignId).Channel__c).contains('Phone') ){
                if(!campaignMemberMap.containsKey(member.campaignId)){
                    if(member.contactId!=null)
                        campaignMemberMap.put(member.campaignId,new Set<Id>{member.ContactId});
                    else 
                        campaignMemberMap.put(member.campaignId,new Set<Id>{member.leadId});    
                }   
                else{
                    if(member.contactId!=null)
                        campaignMemberMap.get(member.CampaignId).add(member.ContactId);
                    else
                        campaignMemberMap.get(member.CampaignId).add(member.leadId);
                }     
            }
            //}
        }
        
        Id crcCalendar = [SELECT Id FROM Calendar WHERE Name = 'CRC Calendar' LIMIT 1].id;   
        List<Event> phoneEventList = new List<Event>();
        
        if(!campaignMemberMap.KeySet().isEmpty()){
            for(Campaign c : campaignStatus.values()){
                Integer d = c.StartDate.day();
                Integer mo = c.StartDate.month();
                Integer yr = c.StartDate.year();
                DateTime DTStart = DateTime.newInstance(yr, mo, d);
                d = c.EndDate.day();
                mo = c.EndDate.month();
                yr = c.EndDate.year();
                DateTime DTEnd = DateTime.newInstance(yr, mo, d);
                
                for(Id contactId  : campaignMemberMap.get(c.id)){
                    Event e = new Event();
                    e.StartDateTime = DTStart;
                    if(DTStart.date().daysBetween(DTEnd.date())<=14){
                        e.EndDateTime = DTEnd;
                    }
                    else{
                        e.EndDateTime = DTStart.date().addDays(13);
                    }
                    
                    e.Subject = 'Reminder related to '+c.Name+' Call : ';
                    e.WhoId=contactId;
                    e.Campaign__c =c.Id;
                    e.OwnerId = crcCalendar;
                    e.WhatId=c.id;
                    phoneEventList.add(e);
                }
                
                if(phoneEventList.size()>0)
                    insert phoneEventList;
                else
                    system.debug('No Events Created'); 
            }
        }
        
        Map<Id,Set<Id>> campaignMemberMapHeroku = new Map<Id,Set<Id>>();
        for(CampaignMember member: triggerNew){
            System.debug('campaignMemberMapHeroku 1 ---------');
            
            if(String.valueOf(campaignStatus.get(member.CampaignId).status).equals('In Progress') &&  ( String.valueOf(campaignStatus.get(member.CampaignId).Channel__c).contains('SMS') || String.valueOf(campaignStatus.get(member.CampaignId).Channel__c).contains('Customer_Portal')  ) ){
                System.debug('campaignMemberMapHeroku 2 ---------');
                if(!campaignMemberMapHeroku.containsKey(member.campaignId)){
                    if(member.contactId!=null)
                        campaignMemberMapHeroku.put(member.campaignId,new Set<Id>{member.ContactId});
                    else 
                        campaignMemberMapHeroku.put(member.campaignId,new Set<Id>{member.leadId});    
                }   
                else{
                    if(member.contactId!=null)
                        campaignMemberMapHeroku.get(member.CampaignId).add(member.ContactId);
                    else
                        campaignMemberMapHeroku.get(member.CampaignId).add(member.leadId);
                }     
            }
        }
        
        System.enqueueJob(new AsyncCampaignUpdate(campaignMemberMapHeroku.KeySet()));
        
        
        
        List<Id> cmContactIds = new List<Id>();
        for(CampaignMember member: triggerNew){
            if(member.ContactId!=null){
                cmContactIds.add(member.ContactId);
            }
        }
        List<Contact> cmContacts = new List<Contact>();
        cmContacts = [SELECT id, Name, AccountId FROM CONTACT WHERE Id IN :cmContactIds];
        Map<Id, Contact> cmContactMap = new Map<Id, Contact>();
        for(Contact c : cmContacts){
            cmContactMap.put(c.Id, c); 
        }
        
        List<Id> cmCampaignIds = new List<Id>();
        for(CampaignMember member: triggerNew){
            if(member.CampaignId!=null){
                cmCampaignIds.add(member.CampaignId);
            }
        }
        List<Campaign> cmCampaigns = new List<Campaign>();
        cmCampaigns = [SELECT id, Name FROM CAMPAIGN WHERE Id IN :cmCampaignIds];
        Map<Id, Campaign> cmCampaignMap = new Map<Id, Campaign>();
        for(Campaign c : cmCampaigns){
            cmCampaignMap.put(c.Id, c); 
        }
        
        Map<Id,Set<Id>> campaignMemberMapContactFeedItem = new Map<Id,Set<Id>>();
        for(CampaignMember member: triggerNew){
            System.debug('campaignMemberMapContactFeedItem ---------');
            
            if(String.valueOf(campaignStatus.get(member.CampaignId).status).equals('In Progress') &&  ( String.valueOf(campaignStatus.get(member.CampaignId).Channel__c).contains('Partner_Portal') )  ) {
                System.debug('campaignMemberMapContactFeedItem 2 ---------');
                if(!campaignMemberMapContactFeedItem.containsKey(member.campaignId)){
                    if(member.contactId!=null)
                        campaignMemberMapContactFeedItem.put(member.campaignId,new Set<Id>{cmContactMap.get(member.ContactId).AccountId}); 
                }   
                else{
                    if(member.contactId!=null)
                        campaignMemberMapContactFeedItem.get(member.CampaignId).add(cmContactMap.get(member.ContactId).AccountId);
                    
                }     
            }
        }
        
        
        for(Id campaignId : campaignMemberMapContactFeedItem.KeySet()){
            String campaignName = cmCampaignMap.get(campaignId).Name;
            for(Id accId: campaignMemberMapContactFeedItem.get(campaignId)){
                FeedItem post = new FeedItem();
                post.Body = 'Campaign ' +campaignName+ ' In Progress';
                post.ParentId = accId;
                post.Title = 'New Task created';
                feeds.add(post);
            }
            
        }
        
        
        if(feeds.size() > 0)
            insert feeds;
        else
            System.debug('No feeds created');
        
    }
}