@isTest
public class Test_LC_AccountHierarchyController {

    @testSetup
    static void createDataSet(){
		
		Test_CreateDataSetup.createAccountHierarchy();       
      
    }
    
    @isTest
    static void getAccountHierarchyTest(){
        
        Account reseller = [Select id from Account limit 1];
        
        Test.startTest();
        LC_AccountHierarchyController.getAccountHierarchy(reseller.id);
        Test.stopTest();
        
    }
    
}