@isTest
private class TestRedirectToInvoicePDF {


	@isTest static void testMethod1() {

		ZuoraInvoiceCustomSetting__c zcs =  new ZuoraInvoiceCustomSetting__c(
			Client_Id__c = 'test cId', 
			Client_Secret__c = 'UTF-8',
			Token_Endpoint__c = 'Test Endpoint',
			User_Id__c = '98465312dAFAE',
			User_Password__c = 'Test pass'
			);
		insert zcs;

		List<Account> accList = new List<Account>();
		Account acc = new Account(Name = 'Parent');
		accList.add(acc);

		Account acc2 = new Account(Name = 'Child', ParentId = acc.id, Billing__c = 'Wholesale');
		accList.add(acc2);
		insert accList;

	Zuora__zinvoice__c zuorazinvoicecObj = new Zuora__zinvoice__c (
		Name = 'test value',
		Zuora__Zuora_Id__c = 'AA8885DEE8EE595855',
		Zuora__Account__c = acc2.id
		);
	insert zuorazinvoicecObj;

Test.startTest();
	Test.setMock(HttpCalloutMock.class, new MockRedirectToInvoice());
	RedirectToInvoicePDF.DownloadInvoice(zuorazinvoicecObj.Id);
	Test.stopTest();
}

}