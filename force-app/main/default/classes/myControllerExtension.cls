public with sharing class myControllerExtension  {
    
    public final zqu__Quote__c qte;
    public myControllerExtension (ApexPages.StandardController controller) {
        this.qte = (zqu__Quote__c)controller.getRecord();
    } 

    public String getQuoteId() {
        return qte.id;
    }
}