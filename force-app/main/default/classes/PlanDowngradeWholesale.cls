global class PlanDowngradeWholesale implements zqu.QuickListController.IHierarchyAllowedRecordPlugin{
    global zqu.QuickListController.PluginResult getAllowedRecords (Map<Object, Object> contextIds, Map<String, String> pluginParams){
    	system.debug('contextIds: '+contextIds);
        zqu.QuickListController.PluginResult result = new zqu.QuickListController.PluginResult();    
        result.records = new List<zqu.QuickListController.PluginRecord>();
        zqu.QuickListController.PluginRecord record;
        
        List<zqu__Quote__c> QuoteList = new List<zqu__Quote__c>();
        List<Account> CustomerAccount = new List<Account>();
        List<String> RatePlanSubscription = new List<String>();
		Decimal LiveVolume = 0;
        Id quoteId = (contextIds.containskey('qid') && contextIds.get('qid')!=null)? (Id) contextIds.get('qid'):null;
        Map<Id, List<Id>> Product_RatePlans = new Map<Id, List<Id>>();
        List<String> ZuoraLiveRatePlanIds = new List<String>();
		
        if(quoteId!=null){
            try{
                QuoteList = [SELECT zqu__Account__r.Id, zqu__Account__r.Billing__c, zqu__ExistSubscriptionID__c FROM zqu__Quote__c WHERE Id =: quoteId]; 
                if(QuoteList.size()>0){
                    
                    CustomerAccount = [SELECT Customer_Subscription_ID__c, Billing__c FROM Account where Id=: QuoteList[0].zqu__Account__r.Id];
                    if(CustomerAccount.size()>0){   											  
						for(Zuora__SubscriptionRatePlan__c subRatePlan : [SELECT Zuora__OriginalProductRatePlanId__c 
																				  FROM Zuora__SubscriptionRatePlan__c 
																				  WHERE Zuora__Subscription__r.Zuora__Zuora_Id__c =: QuoteList[0].zqu__ExistSubscriptionID__c]){
											ZuoraLiveRatePlanIds.add(subRatePlan.Zuora__OriginalProductRatePlanId__c);
										}
										
					    if(ZuoraLiveRatePlanIds.size()>0){
                            for(zqu__ProductRatePlan__c prodRatePlan : [SELECT Id, zqu__Product__r.Volume_GB__c, zqu__Product__r.Family, zqu__ZuoraId__c, zqu__Product__c, zqu__Product__r.zqu__Type__c 
                                                                        FROM zqu__ProductRatePlan__c 
                                                                        WHERE zqu__ZuoraId__c =:ZuoraLiveRatePlanIds]){
                                if(prodRatePlan.zqu__Product__r.Family == 'Plan'){
                                	LiveVolume = prodRatePlan.zqu__Product__r.Volume_GB__c;                
                                } 
                                                                            
                                if(prodRatePlan.zqu__Product__r.zqu__Type__c == 'Bundle'){
                                	for(zqu__ProductOption__c prodBundleOption : [SELECT Id, Name, zqu__ParentProduct__c, zqu__ChildProduct__r.Volume_GB__c, zqu__ChildProduct__r.Family
                                                                                  FROM zqu__ProductOption__c 
                                                                                  WHERE zqu__ParentProduct__c =: prodRatePlan.zqu__Product__c]){
                                        if(prodBundleOption.zqu__ChildProduct__r.Family == 'Plan'){
                                   			LiveVolume = prodBundleOption.zqu__ChildProduct__r.Volume_GB__c;
                                        }
                                    }   
                                }
                            }
                        }
						
						
						for(Zuora__SubscriptionRatePlan__c sub : [SELECT id, Zuora__ProductRatePlanId__c FROM Zuora__SubscriptionRatePlan__c WHERE
										  Zuora__Subscription__c=: CustomerAccount[0].Customer_Subscription_ID__c AND
										  Zuora__ProductRatePlanId__c!='']){
											  system.debug('RatePlanSubscription:'+sub.Zuora__ProductRatePlanId__c);
											  RatePlanSubscription.add(sub.Zuora__ProductRatePlanId__c);
										  }
										
                        if(RatePlanSubscription.size()>0){
                            system.debug('qui0');
                            for(zqu__ProductRatePlan__c ProdRatePlan : [SELECT id, zqu__Product__c FROM zqu__ProductRatePlan__c 
                                                                        WHERE zqu__ZuoraId__c =:RatePlanSubscription AND zqu__Product__r.Status__c != 'Inactive' 
                                                                        AND zqu__Product__r.Family='Plan' AND zqu__Product__r.Product_Type__c=: QuoteList[0].zqu__Account__r.Billing__c 
                                                                        AND zqu__Product__c!=null AND zqu__Product__r.Volume_GB__c <=: LiveVolume]){
                                                                            List<Id> RatePlansList = new List<Id>();
                                                                            if(Product_RatePlans.containsKey(ProdRatePlan.zqu__Product__c)){
                                                                                RatePlansList.addAll(Product_RatePlans.get(ProdRatePlan.zqu__Product__c));
                                                                            }

                                                                            RatePlansList.add(ProdRatePlan.id);
                                                                            Product_RatePlans.put(ProdRatePlan.zqu__Product__c,RatePlansList);
                                                                        }
                            
                            system.debug('Product_RatePlans: '+Product_RatePlans);
                            
                             system.debug('qui');
                        }
                    	system.debug('qui2');
                    
                        if(!Product_RatePlans.IsEmpty()) {
                            
                            for(id ProdId : Product_RatePlans.keySet()){
                                record = new zqu.QuickListController.PluginRecord();
                                record.recordId = ProdId;
                                record.relatedObjectIds = new Map<String, List<ID> >{'zqu__productrateplan__c' => Product_RatePlans.get(ProdId)};
                                    result.records.add(record); 
                            }
                        }
                        system.debug('qui3');
                    }
                }
            }catch(exception e){
                    System.debug('Error in PlanDowngradeWholesale Class: '+e.getMessage());
                }
		}
        system.debug('Result: '+result);
        return result;
	}
}