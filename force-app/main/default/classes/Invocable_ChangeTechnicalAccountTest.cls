@isTest
public class Invocable_ChangeTechnicalAccountTest {
    public static testMethod void doTest() {
        
        SfToHerokuAPIManager.TechnicalServiceResponse responseChangeTechAccount =  new SfToHerokuAPIManager.TechnicalServiceResponse();
        Test.setMock(HttpCalloutMock.class, new CallOutMockSetter('400',(Object)responseChangeTechAccount));
        
       	SfToHK_Urls__c urls = new SfToHK_Urls__c();
        urls.Base_Url__c = 'https://eutelsat-hub-dev.herokuapp.com';
        urls.Create_Customer_Account__c = '/api/v1/oss/customer/accounts';
        insert urls;
        
        Test.startTest();
        Date newDate = Date.newInstance(2020, 1, 1);
        Invocable_ChangeTechnicalAccount.ChangeTechnicalAccountInputs testInputs = new Invocable_ChangeTechnicalAccount.ChangeTechnicalAccountInputs();
        testInputs.techAccountId = 1;
        testInputs.serviceId = 1;
        testInputs.planId = 1;
        testInputs.serviceActivationDate = newDate;
        testInputs.resetCycleDates = true;
        testInputs.numberOfCycles = 1;
        testInputs.quoteId = 'blank';
		
        List<Invocable_ChangeTechnicalAccount.ChangeTechnicalAccountInputs> listTestInputs = new List<Invocable_ChangeTechnicalAccount.ChangeTechnicalAccountInputs>();
        listTestInputs.add(testInputs);        
        
        List<String> Esito = Invocable_ChangeTechnicalAccount.makePostCallout(listTestInputs);
        Test.stopTest();

        system.assertEquals('','');
    }
}