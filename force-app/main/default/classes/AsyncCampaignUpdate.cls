public class AsyncCampaignUpdate implements Queueable, Database.AllowsCallouts{
    
    private Set<Id> pcIdResponse;
    
    public AsyncCampaignUpdate(Set<Id> CampaignIds){
        this.pcIdResponse=CampaignIds;
    }
    
    public void execute(QueueableContext cntx) {
        system.debug('Queueable ---------EXECUTE------START------');
        List<Campaign> campaigns = new List<Campaign>();
        List<CampaignMember> campaignMemberContact = new List<CampaignMember>();
        List<CampaignMember> campaignMemberLead = new List<CampaignMember>();
        List<FeedItem> feeds = new List<FeedItem>();
        List<String> contactsId = new List<String>();
        List<Contact> contacts = new List<Contact>();
        List<Event> phoneEventList = new List<Event>();
        Calendar calendarId = new Calendar();   
        
        
        Map<Id,List<String>> campaignMemberToCampaignMapC = new Map<Id,List<String>>();
        Map<Id,List<String>> campaignMemberToCampaignMapL = new Map<Id,List<String>>();
        
        campaigns = [SELECT id, Name, Channel__c,startDate,EndDate, SMS_Body__c, Web_Body__c,OwnerId,Type FROM Campaign WHERE Id IN :pcIdResponse];
        campaignMemberContact = [SELECT Id, Name, ContactId, CampaignId FROM CampaignMember WHERE CampaignId IN :pcIdResponse AND ContactId != null];
        campaignMemberLead = [SELECT Id, Name, LeadId, CampaignId FROM CampaignMember WHERE CampaignId IN :pcIdResponse AND LeadId != null];
        
        
        system.debug('pcIdResponse '+pcIdResponse);
        system.debug(campaignMemberContact.size() + '  '+  campaignMemberContact.size());
        
        if((campaignMemberContact != null && !campaignMemberContact.isEmpty()) || (campaignMemberLead != null && !campaignMemberLead.isEmpty())){
            for(CampaignMember member : campaignMemberContact){
                system.debug('Queuealbe ---------FOR MAP---------');
                if(!campaignMemberToCampaignMapC.containsKey(member.CampaignId))
                    campaignMemberToCampaignMapC.put(member.CampaignId,new List<String>{member.ContactId});
                else
                    campaignMemberToCampaignMapC.get(member.CampaignId).add(member.ContactId);
            }
            
            for(CampaignMember member : campaignMemberLead){
                system.debug('Queuealbe ---------FOR MAP---------');
                if(!campaignMemberToCampaignMapL.containsKey(member.CampaignId))
                    campaignMemberToCampaignMapL.put(member.CampaignId,new List<String>{member.LeadId});
                else
                    campaignMemberToCampaignMapL.get(member.CampaignId).add(member.LeadId);
            }
            
            
            AllMySmsCustomSetting__c mc = AllMySmsCustomSetting__c.getInstance('AllMySms');
            String endpoint = mc.Endpoint__c;
            if(endpoint==null)
                System.debug('Endpoint vuota');
            JSONGenerator gen = JSON.createGenerator(true); 
            for(Campaign c : campaigns){
                system.debug('Queuealbe ---------FOR JSON---------');
                gen.writeStartObject(); 
                gen.writeFieldName('contactsId');
                gen.writeStartArray();
                if(campaignMemberContact != null && !campaignMemberContact.isEmpty()){
                    for (String MemberId: campaignMemberToCampaignMapC.get(c.Id)){
                        system.debug('-------------------' + MemberId);
                        gen.writeString(MemberId);
                    }
                }
                gen.writeEndArray();
                
                gen.writeFieldName('leadsId');
                gen.writeStartArray();
                if(campaignMemberLead != null&& !campaignMemberLead.isEmpty()){
                    
                    
                    for (String MemberId: campaignMemberToCampaignMapL.get(c.Id)){
                        system.debug('-------------------' + MemberId);
                        gen.writeString(MemberId);
                    }
                }
                gen.writeEndArray();
                
                gen.writeStringField('campaignName', c.Name);
                if(c.StartDate == null) {
                    gen.writeNullField('startDate');
                } else {
                    DateTime DTStart = DateTime.newInstance(c.startDate.year(), c.startDate.month(), c.startDate.day());
                    
                    System.debug(DTStart.getTime());  
                    
                    gen.writeNumberField('startDate', DTStart.getTime());
                }
                if(c.EndDate == null) {
                    gen.writeNullField('endDate');
                } else {
                    DateTime DTEnd = DateTime.newInstance(c.endDate.year(), c.endDate.month(), c.endDate.day());
                    
                    System.debug(DTEnd.getTime()); 
                    
                    gen.writeNumberField('endDate', DTEnd.getTime());
                }
                gen.writeFieldName('templates');
                gen.writeStartArray();
                
                
                if(c.Channel__c.contains('SMS')){
                    gen.writeStartObject();
                    gen.writeStringField('type', 'Sms');
                    if(c.SMS_Body__c == null){
                        gen.writeNullField('text');
                    } else {
                        gen.writeStringField('text', String.valueOf(c.SMS_Body__c)); 
                    }
                    gen.writeEndObject(); 
                }
                if(c.Channel__c.contains('Customer_Portal')){
                    gen.writeStartObject();
                    gen.writeStringField('type', 'Web');
                    if(c.Web_Body__c == null){ 
                        gen.writeNullField('text');
                    } else {
                        gen.writeStringField('text', String.valueOf(c.Web_Body__c));  
                    }
                    gen.writeStringField('title', 'Notifica web'); 
                    gen.writeEndObject(); 
                }
                
                
                
                gen.writeEndArray();
                gen.writeEndObject(); 
                String jsonS = gen.getAsString();
                system.debug('Queuealbe --------- FINE JSON ---------');
                System.debug('jsonAPINotification'+jsonS);
                
                
                HttpRequest req = new HttpRequest();
                req.setEndpoint(endpoint);
                req.setMethod('POST');
                req.setHeader('Accept', '*/*');
                req.setHeader('Content-Type', 'application/json');
                req.setbody(jsonS);
                Http http = new Http();
                if(!test.isRunningTest()){
                    HTTPResponse response = http.send(req); 
                    System.debug(response.getBody());
                    
                    if(response.getStatusCode() == 200)
                        c.Integration_Status__c = 'OK';
                    else
                        c.Integration_Status__c = 'KO';
                }
                
                if(c.Channel__c.contains('Partner_Portal')){
                    if(campaignMemberContact != null && !campaignMemberContact.isEmpty()){
                        for (String MemberId: campaignMemberToCampaignMapC.get(c.Id)){
                            contactsId.add(MemberId);
                        }
                    }
                }
                
                /*  if(c.Channel__c.contains('Phone')){
System.debug('Prima di definire event-----------');
Integer d = c.StartDate.day();
Integer mo = c.StartDate.month();
Integer yr = c.StartDate.year();
DateTime DTStart = DateTime.newInstance(yr, mo, d);
d = c.EndDate.day();
mo = c.EndDate.month();
yr = c.EndDate.year();
DateTime DTEnd = DateTime.newInstance(yr, mo, d);

for(String cont  :contactsId){
Event e = new Event();
e.StartDateTime = DTStart;
e.EndDateTime = DTEnd;
e.Subject = c.Type;
e.WhoId=cont;
e.Campaign__c =c.Id;

e.OwnerId = calendarId.Id;
phoneEventList.add(e);
}
if(phoneEventList.size()>0)
insert phoneEventList;
else
system.debug('No Events Created');
} */
            }
            
            if(campaigns.size() > 0 )
                update campaigns;
            
            
            contacts= [SELECT id, AccountId From Contact WHERE id IN :contactsId];
 
            
            for(Contact con : contacts ){
                FeedItem post = new FeedItem();
                post.Body = 'Campaign In Progress';
                post.ParentId = con.AccountId;
                post.Title = 'New Task Created';
                feeds.add(post);
            }
            
            if(feeds.size() > 0)
                insert feeds;
            else
                System.debug('No feeds created');
            
        }
        
        
    }
}