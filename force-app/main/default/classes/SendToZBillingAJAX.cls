global class SendToZBillingAJAX 
{ 
   @AuraEnabled
   public static String callZQuoteGlobal(String quoteId, String accountId, String ZaccountId, String ZexistingSubId, Boolean generateInvoice)
   {  
        List<zqu.zQuoteUtil.ZBillingQuoteCollection> quotes = new List<zqu.zQuoteUtil.ZBillingQuoteCollection>();
        zqu.zQuoteUtil.ZBillingQuoteCollection quote = new zqu.zQuoteUtil.ZBillingQuoteCollection();
       	zqu.zQuoteUtil.ZBillingQuoteRequest req = new zqu.zQuoteUtil.ZBillingQuoteRequest();

        quote.quoteRequests = new List<zqu.zQuoteUtil.ZBillingQuoteRequest>();
        req.sfdcQuoteId = quoteId;
        req.generateInvoice = generateInvoice;
       	//req.processPayment = true;
       	//req.zSubscriptionId = ZexistingSubId;
        quote.quoteRequests.add(req);
       	
        if (ZaccountId != '' && ZaccountId != null){
            quote.zAccountId = ZaccountId;
        }
       	else{
            quote.zAccountId = 'New';
            quote.sfdcAccountId = accountId;
        }
       
       	quotes.add(quote); 
       
       	List<zqu.zQuoteUtil.zBillingResult> results = zqu.zQuoteUtil.sendToZBilling(quotes);
        
        for ( zqu.zQuoteUtil.zBillingResult result : results ) {
            System.debug('Result: QuoteId = ' + result.sfdcQuoteId + ', Success = ' + result.success + ', message = ' + result.message );
        }
       
       	return 'OK';
       	/*
      	Map<String,String> result = zqu.zQuoteUtil.sendToZBilling( quoteId , billingAccountId , true, null, null );      
       	Boolean contains = result.containsKey('Success');
    	
       	if (!result.containsKey('Success')){
        	outcome = 'Error sending Quote to Zuora'; //+result.get('Failure');
    	}
    	else{
        	outcome = 'The Quote has been succesfully sent to Zuora';
    	}
		*/
       	//System.debug(outcome);
   }
}