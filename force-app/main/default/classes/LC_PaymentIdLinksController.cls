public class LC_PaymentIdLinksController {
    
    @AuraEnabled
    public static List<Wallet_Transaction__c> getReceipts(String recordId){
        List<Wallet_Transaction__c> walletReceipts = new List<Wallet_Transaction__c>();
        walletReceipts = [SELECT Id,Name, Date__c, Amount__c, Standard_Recharge_Receipt__c FROM Wallet_Transaction__c WHERE
                           Wallet__c =:recordId AND Transaction_Category__c = 'Top-up' ORDER BY Date__c DESC /* LIMIT 5*/];
        return walletReceipts;
    }

}