public class ModifyCustomSettingTriggerHandler {

    
    public void OnAfterInsert(List<zqu__Quote__c> triggerNew){
        system.debug('ModifyCustomSettingTriggerHandler---------onAfterInsert--------START');
        
        QuoteAutoNumber__c mc=[SELECT id, Name,Quote_Code__C FROM QuoteAutoNumber__c LIMIT 1];
        String code = mc.Quote_Code__C;
        Integer num=Integer.ValueOf(code);
        num=num + triggerNew.size();
        String sInteger = String.valueOf(num);
        String lpName = sInteger.leftPad(8,'0');
        mc.Quote_Code__C=lpName;
        update mc;

    }
    
}