public with sharing class CustomSendToZuoraController
{
   public final List<zqu.ZChargeGroup> chargeGroupList {get; set;} 
   private final ApexPages.StandardController controller; 
   public CustomSendToZuoraController(ApexPages.StandardController controller)
   { 
      this.controller = controller; 
      if (this.validate()) 
      { 
         try
         { 
            chargeGroupList = zqu.zQuoteUtil.getChargeGroups(this.controller.getRecord().Id);
         } catch(Exception e) { appendMessage(ApexPages.Severity.ERROR, e.getMessage()); } 
      } 
   }
   public PageReference send()
   { 
      try
      { 
         final String quoteId = this.controller.getRecord().Id; 
         final Map<String, String> finalRes = zqu.zQuoteUtil.sendToZBilling(quoteId ,'New', true, null, null);
         
         String resultString = ''; 
         for (String result : finalRes.values()) { resultString += result; } 
         appendMessage(ApexPages.Severity.INFO, resultString); 
      } catch(Exception e) { appendMessage(ApexPages.Severity.ERROR, e.getMessage()); } 
      return null; 
   } 
   private Boolean validate() 
   { 
      if (null == this.controller.getRecord() || null == this.controller.getRecord().Id) 
      { 
         appendMessage(ApexPages.Severity.ERROR, 'Need to specify the id of quote for creating subscription.'); 
         return false; 
      } 
      return true;
   }
   private static void appendMessage(ApexPages.Severity messageType, String message) 
   { 
      ApexPages.addMessage(new ApexPages.Message(messageType, message));
   }   
}