@isTest
public class Test_SchedulerUpdateCaseStatus {
    
    @testSetup
    static void createData(){
        Id rechargeWalletId=  Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Recharge_Wallet').getRecordTypeId();
        List<Case> caseList = new List<Case>();
        Case newCase = new Case(Status='New',RecordTypeId=rechargeWalletId);
        Case newCase2 = new Case(RecordTypeId=rechargeWalletId);
        caseList.add(newCase);
        caseList.add(newCase2);
        insert caseList;
    }
    
    @isTest
    static void updateStatus(){
        
        Test.startTest();
        SchedulerUpdateCaseStatus sh1 = new SchedulerUpdateCaseStatus();
        String sched = 30 + ' ' + 49 + ' ' + 12 + ' ' + 12 + ' ' + 12 + ' ? ' + 2019; 
        system.schedule('Test Scheduling 2', sched, sh1); 
        Test.stopTest();
        /*
        List<Case> caseList = [SELECT id,Status,CreatedDate FROM Case ];
        System.debug('caseList.size() '+caseList.size());
        if(caseList!=null && !caseList.isEmpty()){
            for(Case singleCase : caseList){
                System.debug('Test Class');
                System.debug('singleCase.Status '+singleCase.Status);
                System.debug('singleCase.CreatedDate '+singleCase.CreatedDate);
            }
        }
*/
    }
    
}