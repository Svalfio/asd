public class SendWorkOrderPDF {
    @AuraEnabled
    public static void sendMail(String quoteId, String emailAddress){
        system.debug('Send email');
        // Create email
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.setToAddresses(new String[]{ emailAddress });
        message.setSubject('Work Order PDF');
        message.setHtmlBody('Here\'s the work order PDF');
        
        // Create PDF
        PageReference WorkOrderPage = new PageReference('/partner/s/sfsites/c/apex/DownloadWorkOrderVfp?id='+quoteId);
        system.debug('a');
        //WorkOrderPage.getParameters().put('id', quoteId);
		system.debug('a');
        Blob workOrderPdf;
        if(WorkOrderPage.getContent() == null){
            system.debug('NULLO');
        }else{
            system.debug('NON NULLO');
        }
        try {
            workOrderPdf = WorkOrderPage.getContentAsPDF();
        }
        catch (Exception e) {
            workOrderPdf = Blob.valueOf('aaaaaa'+e.getMessage());
        }
        
        // Attach PDF to email and send
        Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
        attachment.setContentType('application/pdf');
        attachment.setFileName('WorkOrder.pdf');
        attachment.setInline(false);
        attachment.setBody(workOrderPdf);
        message.setFileAttachments(new Messaging.EmailFileAttachment[]{ attachment });
        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        if (results[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: ' + results[0].errors[0].message);
        }
            
        

        
    }
    
}