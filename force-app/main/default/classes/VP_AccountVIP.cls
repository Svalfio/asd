public with sharing class VP_AccountVIP {

	public static Id recordId {get; set;}

	public VP_AccountVIP(ApexPages.StandardController controller) {
		recordId = ApexPages.currentPage().getParameters().get('Id');
	}


	@AuraEnabled
	public static boolean getAccountRating(String recordId){
		Account acc = [SELECT Name, Id, Account_Rating__c FROM Account WHERE Id=:recordId];
		if (acc.Account_Rating__c == 'VIP') {
			return true;
		}else {
			return false;
    }
}
}