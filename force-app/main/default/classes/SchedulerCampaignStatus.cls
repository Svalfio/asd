global class SchedulerCampaignStatus implements Schedulable{
    
    global void execute(SchedulableContext sc) {
        BatchUpdateStatusCampaign b1 = new BatchUpdateStatusCampaign();
        ID batchprocessid = Database.executeBatch(b1,50);      
    }

}