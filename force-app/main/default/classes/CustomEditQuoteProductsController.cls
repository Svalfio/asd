public with sharing class CustomEditQuoteProductsController {
    public zqu.EditQuoteProductOptions editProductOptions {
        get;
        set;
    }
  
    public CustomEditQuoteProductsController
            (ApexPages.StandardController controller) {
        editProductOptions = new zqu.EditQuoteProductOptions();
        
        editProductOptions.guidedSellingOptions 
            = new zqu.ProductBundleGuidedSellingOptions();
        editProductOptions.guidedSellingOptions.quoteType = 'Subscription';
        editProductOptions.guidedSellingOptions.quoteId = controller.getId();
            //'a1J6E000002FLgnUAG';
        editProductOptions.productSelectorOptions 
            = new zqu.SelectProductComponentOptions();
        editProductOptions.productSelectorOptions.quoteType = 'Subscription';
        editProductOptions.productSelectorOptions.quoteId = controller.getId();
        //editProductOptions.productSelectorOptions.hideSearch = true;
            //roller.getId();'a1J6E000002FLgnUAG';
        editProductOptions.initialComponentMode= zqu.EditQuoteProductOptions.GUIDED_SELLING_MODE;
		
    }
}