public class LC_NotificationFeedItemController {
    
    @AuraEnabled
    public static List<FeedItem> ShowNotifications(String UserId){
        List<FeedItem> feeds = new List<FeedItem>();
        
        /*
        FeedItem f = new FeedItem();
        f.Title  = 'Titolo';
        f.Body = 'Test';
        
        User u = [SELECT id FROM User WHERE Name = 'Reseller Type Test' LIMIT 1];
        system.debug(u.id);
        f.ParentId = u.id;
        f.NetworkScope = Network.getNetworkId();
        
        insert f;*/
        
        system.debug('--------------- USER ID -------------' + UserId);
        //feeds = [SELECT Id, Title, Body, ParentId FROM FeedItem WHERE ParentId = '0056E0000062sLVQAY'];
        feeds = [SELECT Id, Title, Body FROM FeedItem LIMIT 10];
        system.debug('------------------- SIZE --------------- ' +feeds.size());
        return feeds;
    }
    
}