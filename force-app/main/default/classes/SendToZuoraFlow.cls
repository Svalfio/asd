global class SendToZuoraFlow {
	global with sharing class SendToZuoraiInputs {
        @InvocableVariable(Label = 'accountId' Required = false) global String accountId;
        @InvocableVariable(Label = 'zuoraAccountId' Required = false) global String ZaccountId;
        @InvocableVariable(Label = 'quoteId' Required = false) global String quoteId;
        @InvocableVariable(Label = 'processPayment' Required = false) global Boolean processPayment_input; 
        @InvocableVariable(Label = 'generateIvoice' Required = false) global Boolean generateInvoice_input;
        @InvocableVariable(Label = 'Esito' Required = false) global List<String> Esito = new List<String>();
	}
    @InvocableMethod(Label = 'Send to Zuora')
	global static List<String> callZQuoteGlobal(SendToZuoraiInputs[] requestActionInList) 
    {
        List<zqu.zQuoteUtil.ZBillingQuoteCollection> quotes = new List<zqu.zQuoteUtil.ZBillingQuoteCollection>();
        zqu.zQuoteUtil.ZBillingQuoteCollection quote = new zqu.zQuoteUtil.ZBillingQuoteCollection();
       	zqu.zQuoteUtil.ZBillingQuoteRequest req = new zqu.zQuoteUtil.ZBillingQuoteRequest();

        quote.quoteRequests = new List<zqu.zQuoteUtil.ZBillingQuoteRequest>();
        req.sfdcQuoteId = requestActionInList[0].quoteId;
        req.generateInvoice = requestActionInList[0].generateInvoice_input;
        //req.processPayment = requestActionInList[0].processPayment_input;
        quote.quoteRequests.add(req);
       	
        if (requestActionInList[0].ZaccountId != '' && requestActionInList[0].ZaccountId != null){
            quote.zAccountId = requestActionInList[0].ZaccountId;
        }
       	else{
            quote.zAccountId = 'New';
            quote.sfdcAccountId = requestActionInList[0].accountId;
        }
		
       	quotes.add(quote); 
       
       	List<zqu.zQuoteUtil.zBillingResult> results = zqu.zQuoteUtil.sendToZBilling(quotes);
        
		for (zqu.zQuoteUtil.zBillingResult result: results) {
        	if (result.success) {requestActionInList[0].Esito.add('OK');}
            else {requestActionInList[0].Esito.add('KO');}
        }
        return requestActionInList[0].Esito;
    }
}