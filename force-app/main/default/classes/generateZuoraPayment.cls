global class generateZuoraPayment {
    global with sharing class ClassInputs {
        @InvocableVariable(Label = 'invoiceCurrency' Required = false) global String invoiceCurrency;
        @InvocableVariable(Label = 'paymentDate' Required = false) global Date paymentDate;
        @InvocableVariable(Label = 'zuoraAccountId' Required = false) global String zuoraAccountId;
		@InvocableVariable(Label = 'esito' Required = false) global String esito;
        @InvocableVariable(Label = 'zuoraInvoiceId' Required = false) global String zuoraInvoiceId;
        @InvocableVariable(Label = 'amount' Required = false) global Decimal amount;
    }
    
    @InvocableMethod(Label = 'Generate Payment')
    global static void makePostCallout(ClassInputs[] requestActionInList) 
    {
        ZuoraRestCalls zuoraRestCall = new ZuoraRestCalls();
        String token = zuoraRestCall.getAccessToken();
		
        String payDate;
        payDate = string.valueOfGmt(requestActionInList[0].paymentDate);  
        payDate = payDate.replace(' ','T');
        
		string jsons = 
		'{'+
		'  "accountId": "'+requestActionInList[0].zuoraAccountId+'",'+
		'  "amount": '+requestActionInList[0].amount+','+
		'  "comment": "Invoice Payment - Retail Pre Paid model",'+
		'  "currency": "'+requestActionInList[0].invoiceCurrency+'",'+
		'  "effectiveDate": "'+payDate+'",'+
		'  "invoices": ['+
		'    {'+
		'      "amount": '+requestActionInList[0].amount+','+
		'      "invoiceId": "'+requestActionInList[0].zuoraInvoiceId+'"'+
		'    }  ],'+
		'  "type": "External"'+
		'}';
		
        if (token != null) {
            generateCashPayment(jsonS,token);
        }
    }
    
    @future(callout = true)
    public static void generateCashPayment(String body, String token){//(String token, String zuoraAccountId, String invoiceCurrency, Decimal amount, String invoiceId, String paymentDate){
        ZuoraInvoiceCustomSetting__c zcs = ZuoraInvoiceCustomSetting__c.getOrgDefaults();
        HttpRequest req = new HttpRequest();
        
        req.setEndpoint(zcs.Zuora_Endpoint__c + '/v1/payments/');
        req.setHeader('authorization', 'bearer ' + token);
        req.setHeader('content-type', 'application/json');
        req.setTimeout(6000);
        req.setMethod('POST');
		req.setBody(body);
		
        HttpResponse resp = new Http().send(req);
        System.debug('@@@@resp:  '+resp);
		system.debug(body);
        //return resp.getBody();
	}
}