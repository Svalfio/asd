global class TechnicalAccountChangeItemsNotification {
    
    global with sharing class ClassInputs {
        @InvocableVariable(Label = 'techAccountId' Required = false) global Integer techAccountId;
        @InvocableVariable(Label = 'planId' Required = false) global Integer planId;
        //@InvocableVariable(Label = 'cycleDates' Required = false) global List<CycleDates> cycleDatesList;
        @InvocableVariable(Label = 'components' Required = false) global List<Integer> components;
        @InvocableVariable(Label = 'IP' Required = false) global String IP;
        @InvocableVariable(Label = 'resetCycleDates' Required = false) global boolean resetCycleDates;
        @InvocableVariable(Label = 'resetCounters' Required = false) global boolean resetCounters;
        
        @InvocableVariable(Label = 'Esito' Required = false) global List<String> Esito = new List<String>();
    }
    
    @InvocableMethod(Label = 'Heroku Callout')
    global static List<String> makePostCallout(ClassInputs[] requestActionInList) 
    {
        
        //Remember to insert the heroku url (to define) in the custom setting and create a remote site settings
        TechnicalAccountChangeItemsCustomSetting__c mc = TechnicalAccountChangeItemsCustomSetting__C.getInstance('HerokuUrl');
        String endpoint = mc.Endpoint__c;
        if(endpoint==null)
            System.debug('Endpoint vuota');
        JSONGenerator gen = JSON.createGenerator(true);
        system.debug('---------JSON Creation---------');
        if(requestActionInList!=null){
            gen.writeStartObject(); 
            if(requestActionInList[0].techAccountId!=null){
                gen.writeNumberField('techAccountId', requestActionInList[0].techAccountId);
            }else{
                gen.writeNullField('techAccountId');
            }
            if(requestActionInList[0].planId!=null){
                gen.writeNumberField('planId', requestActionInList[0].planId);
            }
            else{
                gen.writeNullField('planId');
            }
            /* Deccommentare se l'API di Heroku rchiede in input questo oggetto
gen.writeFieldName('cycleDates');
gen.writeStartObject();
gen.writeStartArray();
if(requestActionInList[0].cycleDatesList.size()>0 ){
for(cycleDates cycle: requestActionInList[0].cycleDatesList){
if(cycle.dateFrom!=null){
gen.writeStringField('dateFrom',cycle.dateFrom);
}else{
gen.writeNullField('dateFrom');
}	
if(cycle.dateTo!=null){
gen.writeStringField('dateFrom',cycle.dateTo);
}else{
gen.writeNullField('dateTo');
}	

}
}
gen.writeEndArray();
gen.writeEndObject(); 

*/
            gen.writeFieldName('components');
            gen.writeStartArray();
            if(requestActionInList[0].components!=null && requestActionInList[0].components.size()>0 ){
                for(integer numb : requestActionInList[0].components){
                    if(numb!=null){
                        gen.writeNumber(numb);
                    }
                }
            }
            gen.writeEndArray();
            
            if(requestActionInList[0].IP!=null){
                gen.writeStringField('IP',requestActionInList[0].IP);
            }else{
                gen.writeNullField('IP');
            }
            
            if(requestActionInList[0].resetCycleDates!=null){
                gen.writeBooleanField('resetCycleDates',requestActionInList[0].resetCycleDates);
            }else{
                gen.writeNullField('resetCycleDates');
            }
            
            if(requestActionInList[0].resetCounters!=null){
                gen.writeBooleanField('resetCounters',requestActionInList[0].resetCounters);
            }else{
                gen.writeNullField('resetCounters');
            }
            
            gen.writeEndObject(); 
            String jsonS = gen.getAsString();
            system.debug('TechnicalAccountChangeItemsNotification --------- FINE JSON ---------');
            System.debug('jsonAPINotification '+jsonS);
            
            ///////Decommentare dopo che l'api Heroku è completa
            
            //HttpRequest req = new HttpRequest();
            //req.setEndpoint(endpoint);
            //req.setMethod('POST');
            //req.setHeader('Accept', '*/*');
                
            //req.setHeader('Content-Type', 'application/json');
            //req.setbody(jsonS);
            //Http http = new Http();
            ///////if(!test.isRunningTest()){
            //HTTPResponse response = http.send(req); 
            //System.debug(response.getBody());
            /*
            if(response.getStatusCode() == 200)
                requestActionInList[0].Esito.add('OK');
            else
                requestActionInList[0].Esito.add('KO');
            ///////}
            
            */
            
            ////////////////
            ////////////////
            //eliminare dopo che l'api Heroku è completa e decommentare il codice prececente
            HTTPResponse response = new HTTPResponse();
			response.setStatus('200');
			System.debug(response.getStatus());
            
            if(response.getStatus().equals('200')){
                requestActionInList[0].Esito.add('OK');
                System.debug('Status 200');
            }
            else
                requestActionInList[0].Esito.add('KO');
            
            ////////////////
            ////////////////
            
            if(requestActionInList[0].Esito.size()>0){
                return requestActionInList[0].Esito;
            }else{
                return null;
            }
            
        }else{
            return null;
        }    
        
    }
    
}