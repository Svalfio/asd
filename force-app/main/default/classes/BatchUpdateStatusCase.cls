global class BatchUpdateStatusCase implements  Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts   {
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        //Remember that the API name of the Staus value id different from the label (API name = 'New' and label name= 'Open')
        Date todayDate = Date.today().addDays(1);
        System.debug('todayDate '+todayDate);
    	return Database.getQueryLocator('Select Id,Status,RecordType.Name,CreatedDate,Closure_reason__c from Case WHERE Status = \'New\' AND RecordType.Name=\'Recharge Wallet\' AND CreatedDate<:todayDate' );
    }
    
    
    global void execute(Database.BatchableContext bc, List<sObject> scope){
        List<Case> caseList = (List<Case>) scope;
		List<Case> updatedCases = new List<Case>();
        
        System.debug('execute-----BatchUpdateStatusCase--------');
        if(caseList !=null && !caseList.isEmpty()){
            for(Case singleCase : caseList){
                    singleCase.Status='Closed';
                    singleCase.Closure_reason__c='Payment Failed';
                	updatedCases.add(singleCase);
            }
        }
        update updatedCases;
        
    }
    
    global void finish(Database.BatchableContext bc){
        
        System.debug(('Executed code'));
        
    }  
    
}