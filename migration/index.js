var fs = require("fs"),
	fse = require("fs-extra"),
	xml2js = require("xml2js"),
	copyfiles = require("copyfiles"),
	mapping = {
		ApexClass: {
			folder: "classes",
			extensions: [".cls", ".cls-meta.xml"]
		},
		ApexPage: {
			folder: "pages",
			extensions: [".page", ".page-meta.xml"]
		},
		ApexTrigger: {
			folder: "triggers",
			extensions: [".trigger", ".trigger-meta.xml"]
		},
		AuraDefinitionBundle: {
			folder: "aura"
		},
		CustomTab: {
			folder: "tabs",
			extensions: [".tab"]
		},
		ContentAsset: {
			folder: "contentassets",
			extensions: [".asset", ".asset-meta.xml"]
		},
		CustomApplication: {
			folder: "applications",
			extensions: [".app"]
		},
		CustomLabel: {
			folder: "labels",
			onlyfile: ["CustomLabels.labels"]
		},
		CustomMetadata: {
			folder: "customMetadata",
			extensions: [".md"]
		},
		Queue: {
			folder: "queues",
			extensions: [".queue"]
		},
		CustomObject: {
			folder: "objects",
			extensions: [".object"]
		},
		CustomObjectTranslation: {
			folder: "objectTranslations",
			extensions: [".objectTranslation"]
		},
		FlexiPage: {
			folder: "flexipages",
			extensions: [".flexipage"]
		},
		Group: {
			folder: "groups",
			extensions: [".group"]
		},
		Layout: {
			folder: "layouts",
			extensions: [".layout"]
		},
		PathAssistant: {
			folder: "pathAssistants",
			extensions: [".pathAssistant"]
		},
		Profile: {
			folder: "profiles",
			extensions: [".profile"]
		},
		QuickAction: {
			folder: "quickActions",
			extensions: [".quickAction"]
		},
		SharingRules: {
			folder: "sharingRules",
			extensions: [".sharingRules"]
		},
		StaticResource: {
			folder: "staticresources",
			extensions: [".resource", ".resource-meta.xml"]
		},
		Translations: {
			folder: "translations",
			onlyfile: ["de.translation","fr.translation","it.translation","en_US.translation"]
		},
		Flow: {
			folder: "flows",
			extensions: [".flow"]
		},
		FlowDefinition: {
			folder: "flowDefinitions",
			extensions: [".flowDefinition"]
		},
		Workflow: {
			folder: "workflows",
			extensions: [".workflow"]
		},
		DuplicateRule: {
			folder: "duplicateRules",
			extensions: [".duplicateRule"]
		},
		PlatformCachePartition: {
			folder: "cachePartitions",
			extensions: [".cachePartition"]
		},
		RemoteSiteSetting: {
			folder: "remoteSiteSettings",
			extensions: [".remoteSite"]
		},
		Role: {
			folder: "roles",
			extensions: [".role"]
		},
		Dashboard: {
			folder: "dashboards",
			extensions: ["-meta.xml"],
			ext: [".dashboard"]
		},
		EmailTemplate: {
			folder: "email",
			extensions: ["-meta.xml"],
			ext: [".email"]
		},
		Report: {
			folder: "reports",
			extensions: ["-meta.xml"],
			ext: [".report"]
		},
		ReportType: {
			folder: "reportTypes",
			extensions: [".reportType"]
		},
		PermissionSet: {
			folder: "permissionsets",
			extensions: [".permissionset"]
		},
		StandardValueSetTranslation: {
			folder: "standardValueSetTranslations",
			extensions: [".standardValueSetTranslation"]
		},
		StandardValueSet: {
			folder: "standardValueSets",
			extensions: [".standardValueSet"]
		},
		Certificate: {
			folder: "certs",
			extensions: [".crt", ".crt-meta.xml"]
		},
		AssignmentRule: {
			folder: "assignmentRules",
			onlyfile: [""],
			extensions: [".assignmentRules"]
        },
        AutoResponseRules: {
			folder: "autoResponseRules",
			
			extensions: [".autoResponseRules"]
        },
        MatchingRule: {
			folder: "matchingRules",
			onlyfile: [""],
			extensions: [".matchingRule"]
		},
		GlobalValueSet: {
			folder: "globalValueSets",
			extensions: [".globalValueSet"]
		},
		GlobalValueSetTranslation: {
			folder: "globalValueSetTranslations",
			onlyfile: [""],
			extensions: [".globalValueSetTranslation"]
		},
		PathAssistant: {
			folder: "pathAssistants",
			extensions: [".pathAssistant"]
		},
		Settings: {
			folder: "settings",
			extensions: [".settings"]
		},
		ConnectedApp: {
			folder: "connectedApps",
			extensions: [".connectedApp"]
		},
		NamedCredential: {
			folder: "namedCredentials",
			extensions: [".namedCredential"]
		},
		ApprovalProcess: {
			folder: "approvalProcesses",
			extensions: [".approvalProcess"]
		},
		AuthProvider: {
			folder: "authproviders",
			extensions: [".authprovider"]
		},
		ApexTestSuite: {
			folder: "testSuites",
			extensions: [".testSuite"]
		},
		CustomPermission: {
			folder: "customPermissions",
			extensions: [".customPermission"]
		},
		Territory2Type: {
            folder: "territory2Types",
            extensions: [".territory2Type"]
        },
        Territory2Model: {
            folder: "territory2Models",
		},
		LeadConvertSettings: {
			folder: "LeadConvertSettings",
			onlyfile: ["LeadConvertSettings.LeadConvertSetting"]
		},
		CustomNotificationType: {
			folder: "notificationtypes",
			extensions: [".notiftype"]
		}
	},
	resultFiles = new Array,
	specialMapping = {
		AuraDefinitionBundle: "aura"
	},
	exceptionNaturalCopy = ["ApexClass"],
	parser = new xml2js.Parser,
	targetDir = "delta",
	srcDir = "temp";
fs.readFile("package.xml", function (e, s) {
	parser.parseString(s, function (e, s) {
		var a = 1;
		resultFiles[0] = {
			src: "package.xml",
			dest: targetDir + "/package.xml"
		};
		for (var t = 0; t < s.Package.types.length; t++)
			if (null != mapping[s.Package.types[t].name])
				for (var n = 0; n < s.Package.types[t].members.length; n++) {
					console.log("members: " + s.Package.types[t].members[n]), console.log("name: " + s.Package.types[t].name), console.log("folder: " + mapping[s.Package.types[t].name].folder);
					var r = mapping[s.Package.types[t].name].folder;
					if (mapping[s.Package.types[t].name].hasOwnProperty("onlyfile"))
						for (var o = 0; o < mapping[s.Package.types[t].name].onlyfile.length; o++) {
							var l = s.Package.types[t].members[n];
							(l = "*") && (l = "");
							var i = targetDir + "/" + r + "/" + l + mapping[s.Package.types[t].name].onlyfile[o],
								p = srcDir + "/" + r + "/" + l + mapping[s.Package.types[t].name].onlyfile[o];
							resultFiles[a] = {
								src: p,
								dest: i
							}, a++
						} else if (mapping[s.Package.types[t].name].hasOwnProperty("extensions"))
							for (o = 0; o < mapping[s.Package.types[t].name].extensions.length; o++) {
								i = "", p = "";
								"dashboards" == mapping[s.Package.types[t].name].folder || "email" == mapping[s.Package.types[t].name].folder || "reports" == mapping[s.Package.types[t].name].folder ? -1 == s.Package.types[t].members[n].indexOf("/") ? (i = targetDir + "/" + r + "/" + s.Package.types[t].members[n], p = srcDir + "/" + r + "/" + s.Package.types[t].members[n], resultFiles[a] = {
									src: p,
									dest: i
								}, a++, "unfiled$public" != s.Package.types[t].members[n] && (resultFiles[a] = {
									src: p + mapping[s.Package.types[t].name].extensions[o],
									dest: i + mapping[s.Package.types[t].name].extensions[o]
								}, a++)) : (i = targetDir + "/" + r + "/" + s.Package.types[t].members[n] + mapping[s.Package.types[t].name].ext[o], p = srcDir + "/" + r + "/" + s.Package.types[t].members[n] + mapping[s.Package.types[t].name].ext[o], resultFiles[a] = {
									src: p,
									dest: i
								}, a++, "reports" != mapping[s.Package.types[t].name].folder && "dashboards" != mapping[s.Package.types[t].name].folder && /* s.Package.types[t].members[n].indexOf("unfiled$public") > -1  && */ (resultFiles[a] = {
									src: p + mapping[s.Package.types[t].name].extensions[o],
									dest: i + mapping[s.Package.types[t].name].extensions[o]
								}, a++)) : (i = targetDir + "/" + r + "/" + s.Package.types[t].members[n] + mapping[s.Package.types[t].name].extensions[o], p = srcDir + "/" + r + "/" + s.Package.types[t].members[n] + mapping[s.Package.types[t].name].extensions[o], resultFiles[a] = {
									src: p,
									dest: i
								}, a++)
							} else {
								l = s.Package.types[t].members[n];
								s.Package.types[t].members[n].indexOf("SBQQ__") > -1 ? l = s.Package.types[t].members[n].replace("SBQQ__", "") : s.Package.types[t].members[n].indexOf("tfa__") > -1 && (l = s.Package.types[t].members[n].replace("tfa__", ""));
								var m = targetDir.replace("','/") + "/" + r + "/" + l,
									c = srcDir.replace("','/") + "/" + r + "/" + l;
								resultFiles[a] = {
									src: c,
									dest: m
								}, a++
							}
				}
		console.log(resultFiles);
		for (t = 0; t < resultFiles.length; t++) console.log("Coping " + resultFiles[t].src + " into " + resultFiles[t].dest), fse.copySync(resultFiles[t].src, resultFiles[t].dest);
		console.log("Done")
	})
});

if (fs.existsSync('./destructiveChangesPre.xml')) {

		fs.readFile("destructiveChangesPre.xml", function (e, s) {
			parser.parseString(s, function (e, s) {
				var a = 1;
				resultFiles[0] = {
					src: "destructiveChangesPre.xml",
					dest: targetDir + "/destructiveChangesPre.xml"
				};

				console.log(resultFiles);
				for (t = 0; t < resultFiles.length; t++) console.log("Coping " + resultFiles[t].src + " into " + resultFiles[t].dest), fse.copySync(resultFiles[t].src, resultFiles[t].dest);
				console.log("Done")
			})
		});
}
